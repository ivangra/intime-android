package com.technomag.mpageindicator;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.Pair;
import android.view.View;

/**
 * Created by ILYA on 28.11.2016.
 */

public class MPageIndicator extends View implements ViewPager.OnPageChangeListener {

  private static final int DEFAULT_CIRCLES_COUNT = 3;
  private static final int COUNT_NOT_SET = -1;
  private static final int DEFAULT_STROKE_DP = 1;

  private static final int DEFAULT_RADIUS_DP = 6;
  private static final int DEFAULT_PADDING_DP = 8;

  private int radiusPx;
  private int paddingPx;
  private int strokePx;

  private int count;
  private boolean isCountSet;

  //Color
  private int unselectedColor;
  private int selectedColor;

  private DataSetObserver setObserver;
  private ViewPager viewPager;
  private int viewPagerId;

  private Paint fillPaint = new Paint();
  private Paint strokePaint = new Paint();
  private RectF rect = new RectF();

  private int selectedPosition;
  private int selectingPosition;
  private int lastSelectedPosition;

  public MPageIndicator(Context context) {
    super(context);
    init(null);
  }

  public MPageIndicator(Context context, AttributeSet attrs) {
    super(context, attrs);
    init(attrs);
  }

  public MPageIndicator(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init(attrs);
  }

  @TargetApi(Build.VERSION_CODES.LOLLIPOP)
  public MPageIndicator(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
    super(context, attrs, defStyleAttr, defStyleRes);
    init(attrs);
  }

  @Override
  protected void onAttachedToWindow() {
    super.onAttachedToWindow();
    findViewPager();
  }

  @Override
  protected void onDetachedFromWindow() {
    unRegisterSetObserver();
    super.onDetachedFromWindow();
  }

  @SuppressWarnings("UnnecessaryLocalVariable")
  @Override
  protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    int widthMode = MeasureSpec.getMode(widthMeasureSpec);
    int widthSize = MeasureSpec.getSize(widthMeasureSpec);

    int heightMode = MeasureSpec.getMode(heightMeasureSpec);
    int heightSize = MeasureSpec.getSize(heightMeasureSpec);

    int circleDiameterPx = radiusPx * 2;
    int desiredHeight = circleDiameterPx + strokePx;
    int desiredWidth = 0;

    if (count != 0) {
      int diameterSum = circleDiameterPx * count;
      int strokeSum = (strokePx * 2) * count;
      int paddingSum = paddingPx * (count - 1);
      desiredWidth = diameterSum + strokeSum + paddingSum;
    }

    int width;
    int height;

    if (widthMode == MeasureSpec.EXACTLY) {
      width = widthSize;
    } else if (widthMode == MeasureSpec.AT_MOST) {
      width = Math.min(desiredWidth, widthSize);
    } else {
      width = desiredWidth;
    }

    if (heightMode == MeasureSpec.EXACTLY) {
      height = heightSize;
    } else if (heightMode == MeasureSpec.AT_MOST) {
      height = Math.min(desiredHeight, heightSize);
    } else {
      height = desiredHeight;
    }

    if (width < 0) {
      width = 0;
    }

    if (height < 0) {
      height = 0;
    }

    setMeasuredDimension(width, height);
  }

  @Override
  protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
    super.onLayout(changed, left, top, right, bottom);
    //setFrameValues();
  }

  @Override
  protected void onDraw(Canvas canvas) {
    drawIndicatorView(canvas);
  }

  @Override
  public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    //if (interactiveAnimation) {
      onPageScroll(position, positionOffset);
    //}
  }

  @Override
  public void onPageSelected(int position) {
    setSelection(position);
  }

  @Override
  public void onPageScrollStateChanged(int state) {

  }

  private void init(@Nullable AttributeSet attrs) {
    initAttributes(attrs);

    fillPaint.setStyle(Paint.Style.FILL);
    fillPaint.setAntiAlias(true);

    strokePaint.setStyle(Paint.Style.STROKE);
    strokePaint.setAntiAlias(true);
    strokePaint.setStrokeWidth(strokePx);
  }

  private void initAttributes(@Nullable AttributeSet attrs) {
    if (attrs == null) {
      return;
    }

    TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.MPageIndicator);
    initCountAttribute(typedArray);
    initColorAttribute(typedArray);
    initSizeAttribute(typedArray);
    typedArray.recycle();
  }

  private void initCountAttribute(@NonNull TypedArray typedArray) {
    //boolean dynamicCount = typedArray.getBoolean(R.styleable.PageIndicatorView_dynamicCount, false);
    //setDynamicCount(dynamicCount);

    count = typedArray.getInt(R.styleable.MPageIndicator_piv_count, COUNT_NOT_SET);
    if (count != COUNT_NOT_SET) {
      isCountSet = true;
    } else {
      count = DEFAULT_CIRCLES_COUNT;
    }

    int position = typedArray.getInt(R.styleable.MPageIndicator_piv_select, 0);
    if (position < 0) {
      position = 0;
    } else if (count > 0 && position > count - 1) {
      position = count - 1;
    }

    selectedPosition = position;
    selectingPosition = position;
    viewPagerId = typedArray.getResourceId(R.styleable.MPageIndicator_piv_viewPager, 0);
  }

  private void initColorAttribute(@NonNull TypedArray typedArray) {
    unselectedColor = typedArray.getColor(R.styleable.MPageIndicator_piv_unselectedColor, Color.parseColor("#33ffffff"));
    selectedColor = typedArray.getColor(R.styleable.MPageIndicator_piv_selectedColor, Color.parseColor("#ffffff"));
  }

  private void initSizeAttribute(@NonNull TypedArray typedArray) {
    radiusPx = (int) typedArray.getDimension(R.styleable.MPageIndicator_piv_radius, DensityUtils.dpToPx(DEFAULT_RADIUS_DP));
    paddingPx = (int) typedArray.getDimension(R.styleable.MPageIndicator_piv_padding, DensityUtils.dpToPx(DEFAULT_PADDING_DP));

    strokePx = (int) typedArray.getDimension(R.styleable.MPageIndicator_piv_strokeWidth, DensityUtils.dpToPx(DEFAULT_STROKE_DP));
    if (strokePx > radiusPx) {
      strokePx = radiusPx;
    }

  }

  private void findViewPager() {
    if (viewPagerId == 0) {
      return;
    }

    Context context = getContext();
    if (context instanceof Activity) {
      Activity activity = (Activity) getContext();
      View view = activity.findViewById(viewPagerId);

      if (view != null && view instanceof ViewPager) {
        setViewPager((ViewPager) view);
      }
    }
  }

  public void releaseViewPager() {
    if (viewPager != null) {
      viewPager.removeOnPageChangeListener(this);
      viewPager = null;
    }
  }

  public void setViewPager(@Nullable ViewPager pager) {
    releaseViewPager();

    if (pager != null) {
      viewPager = pager;
      viewPager.addOnPageChangeListener(this);

      if (!isCountSet) {
        setCount(getViewPagerCount());
      }
    }
  }

  public void setCount(int count) {
    if (this.count != count) {
      this.count = count;
      this.isCountSet = true;

      requestLayout();
    }
  }

  private int getViewPagerCount() {
    if (viewPager != null && viewPager.getAdapter() != null) {
      return viewPager.getAdapter().getCount();
    } else {
      return count;
    }
  }

  private void unRegisterSetObserver() {
    if (setObserver != null && viewPager != null && viewPager.getAdapter() != null) {
      viewPager.getAdapter().unregisterDataSetObserver(setObserver);
      setObserver = null;
    }
  }

  /**
   * Return number of circle indicators
   */
  public int getCount() {
    return count;
  }

  public void setRadius(int radiusDp) {
    if (radiusDp < 0) {
      radiusDp = 0;
    }

    radiusPx = DensityUtils.dpToPx(radiusDp);
    invalidate();
  }

  public void setRadius(float radiusPx) {
    if (radiusPx < 0) {
      radiusPx = 0;
    }

    this.radiusPx = (int) radiusPx;
    invalidate();
  }

  public int getRadius() {
    return radiusPx;
  }

  public void setPadding(int paddingDp) {
    if (paddingDp < 0) {
      paddingDp = 0;
    }

    paddingPx = DensityUtils.dpToPx(paddingDp);
    invalidate();
  }

  public void setPadding(float paddingPx) {
    if (paddingPx < 0) {
      paddingPx = 0;
    }

    this.paddingPx = (int) paddingPx;
    invalidate();
  }

  /**
   * Return padding in px between each circle indicator. If custom padding is not set,
   * return default value {@link MPageIndicator#DEFAULT_PADDING_DP}.
   */
  public int getPadding() {
    return paddingPx;
  }

  public void setStrokeWidth(float strokePx) {
    if (strokePx < 0) {
      strokePx = 0;

    } else if (strokePx > radiusPx) {
      strokePx = radiusPx;
    }

    this.strokePx = (int) strokePx;
    invalidate();
  }

  public void setStrokeWidth(int strokeDp) {
    int strokePx = DensityUtils.dpToPx(strokeDp);

    if (strokePx < 0) {
      strokePx = 0;

    } else if (strokePx > radiusPx) {
      strokePx = radiusPx;
    }

    this.strokePx = strokePx;
    invalidate();
  }

  public int getStrokeWidth() {
    return strokePx;
  }

  public void setUnselectedColor(int color) {
    unselectedColor = color;
    invalidate();
  }

  public int getUnselectedColor() {
    return unselectedColor;
  }

  public void setSelectedColor(int color) {
    selectedColor = color;
    invalidate();
  }

  public int getSelectedColor() {
    return selectedColor;
  }

  public void setSelection(int position) {
    if (position < 0) {
      position = 0;

    } else if (position > count - 1) {
      position = count - 1;
    }

    lastSelectedPosition = selectedPosition;
    selectedPosition = position;
    invalidate();
  }

  public int getSelection() {
    return selectedPosition;
  }

  /*private void setFrameValues() {
    if (isFrameValuesSet) {
      return;
    }

    //color
    frameColor = selectedColor;
    frameColorReverse = unselectedColor;

    //scale
    frameRadiusPx = radiusPx;
    frameRadiusReversePx = radiusPx;

    //worm
    int xCoordinate = getXCoordinate(selectedPosition);
    if (xCoordinate - radiusPx >= 0) {
      frameLeftX = xCoordinate - radiusPx;
      frameRightX = xCoordinate + radiusPx;

    } else {
      frameLeftX = xCoordinate;
      frameRightX = xCoordinate + (radiusPx * 2);
    }

    //slide
    frameXCoordinate = xCoordinate;

    //fill
    frameStrokePx = radiusPx;
    frameStrokeReversePx = radiusPx / 2;

    if (animationType == AnimationType.FILL) {
      frameRadiusPx = radiusPx / 2;
      frameRadiusReversePx = radiusPx;
    }

    //thin worm
    frameHeight = radiusPx * 2;
    isFrameValuesSet = true;
  }*/

  private void drawIndicatorView(@NonNull Canvas canvas) {
    int y = getHeight() / 2;

    for (int i = 0; i < count; i++) {
      int x = getXCoordinate(i);
      drawCircle(canvas, i, x, y);
    }
  }

  private void drawCircle(@NonNull Canvas canvas, int position, int x, int y) {
    /*boolean selectedItem =  (position == selectedPosition || position == lastSelectedPosition);
    boolean selectingItem = (position == selectingPosition || position == selectedPosition);
    boolean isSelectedItem = selectedItem | selectingItem;*/

    //if (isSelectedItem) {
    //  drawWithAnimationEffect(canvas, position, x, y);
    //} else {
      drawWithNoEffect(canvas, position, x, y);
    //}
  }

  private void drawWithNoEffect(@NonNull Canvas canvas, int position, int x, int y) {
    float radius = radiusPx;
    int color = unselectedColor;

    //if (animationType == AnimationType.SCALE) {
    //  radius *= scaleFactor;
    //}

    if (position == selectedPosition) {
      color = selectedColor;
    }

    Paint paint;
    //if (animationType == AnimationType.FILL) {
    //  paint = strokePaint;
    //  paint.setStrokeWidth(strokePx);
    //} else {
      paint = fillPaint;
    //}

    paint.setColor(color);
    canvas.drawCircle(x, y, radius, paint);
  }

  @SuppressWarnings("UnnecessaryLocalVariable")
  private int getXCoordinate(int position) {
    int actualViewWidth = calculateActualViewWidth();
    int viewCenter = (getWidth() - actualViewWidth) / 2;
    int x = viewCenter;

    if (x < 0) {
      x = 0;
    }

    for (int i = 0; i < count; i++) {
      x += radiusPx + strokePx;
      if (position == i) {
        return x;
      }

      x += radiusPx + paddingPx;
    }

    return x;
  }

  private int calculateActualViewWidth() {
    int width = 0;
    int diameter = (radiusPx * 2) + strokePx;

    for (int i = 0; i < count; i++) {
      width += diameter;

      if (i < count - 1) {
        width += paddingPx;
      }
    }

    return width;
  }

  private void onPageScroll(int position, float positionOffset) {
    Pair<Integer, Float> progressPair = getProgress(position, positionOffset);
    int selectingPosition = progressPair.first;
    float selectingProgress = progressPair.second;

    if (selectingProgress == 1) {
      lastSelectedPosition = selectedPosition;
      selectedPosition = selectingPosition;
    }

    setProgress(selectingPosition, selectingProgress);
  }

  private Pair<Integer, Float> getProgress(int position, float positionOffset) {
    boolean isRightOverScrolled = position > selectedPosition;
    boolean isLeftOverScrolled = position + 1 < selectedPosition;

    if (isRightOverScrolled || isLeftOverScrolled) {
      selectedPosition = position;
    }

    boolean isSlideToRightSide = selectedPosition == position && positionOffset != 0;
    int selectingPosition;
    float selectingProgress;

    if (isSlideToRightSide) {
      selectingPosition = position + 1;
      selectingProgress = positionOffset;

    } else {
      selectingPosition = position;
      selectingProgress = 1 - positionOffset;
    }

    if (selectingProgress > 1) {
      selectingProgress = 1;

    } else if (selectingProgress < 0) {
      selectingProgress = 0;
    }

    return new Pair<>(selectingPosition, selectingProgress);
  }

  public void setProgress(int selectingPosition, float progress) {
    //if (interactiveAnimation) {

      if (selectingPosition < 0) {
        selectingPosition = 0;

      } else if (selectingPosition > count - 1) {
        selectingPosition = count - 1;
      }

      /*if (progress < 0) {
        progress = 0;

      } else if (progress > 1) {
        progress = 1;
      }*/

      this.selectingPosition = selectingPosition;
      /*AbsAnimation animator = getSelectedAnimation();

      if (animator != null) {
        animator.progress(progress);
      }*/
    //}
  }


}
