package com.ua.inr0001.intime;

import android.test.ActivityInstrumentationTestCase2;
import android.widget.EditText;

import com.robotium.solo.Solo;

import intime.llc.ua.R;

/**
 * Created by technomag on 10.05.17.
 */
public class DeliveryActivityTest extends ActivityInstrumentationTestCase2 {

  private static final String LAUNCHER_ACTIVITY_CLASSNAME = "com.ua.inr0001.intime.SplashScreenActivity";
  private static Class<?> launchActivityClass;
  static {
    try {
      launchActivityClass = Class.forName(LAUNCHER_ACTIVITY_CLASSNAME);
    } catch (ClassNotFoundException e) {
      throw new RuntimeException(e);
    }
  }


  private Solo solo;

  //public DeliveryActivityTest() {
  //  super(DeliveryActivity.class);
  //}

  public DeliveryActivityTest(Class<DeliveryActivity> activityClass) {
    super(activityClass);
  }

  public void setUp() throws Exception {
    super.setUp();
    //injectInstrumentation(InstrumentationRegistry.getInstrumentation());

    //Instrumentation mInstrumentation = InstrumentationRegistry.getInstrumentation();
    //solo = new Solo(mInstrumentation);
    solo = new Solo(getInstrumentation(), getActivity());
  }

  @Override
  public void tearDown() throws Exception {
    solo.finishOpenedActivities();
    super.tearDown();
  }

  @SuppressWarnings("unchecked")
  public DeliveryActivityTest() {
    super((Class<DeliveryActivity>) launchActivityClass);
  }

  public void testTrackTTN()
  {
//    solo.clickOnView(solo.getView(R.id.tvTrackTtnButton));
//    if (solo.waitForActivity(TrackActivity.class)) {
//      // select ident type
//      solo.enterText((EditText) solo.getView(R.id.edTTN), "3000002457");
//      solo.clickOnView(solo.getView(R.id.btnGetInfo));
//
//      // add name/label and create ident
//      if (solo.waitForActivity(TtnDetailsActivity.class)) {
//        solo.sleep(2000);
//        solo.goBack();
//      }
//    }
  }

}