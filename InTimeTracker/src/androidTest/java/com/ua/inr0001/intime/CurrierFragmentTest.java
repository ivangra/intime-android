package com.ua.inr0001.intime;

import android.app.Activity;
import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.robotium.solo.Condition;
import com.robotium.solo.Solo;

import org.junit.Before;

import intime.llc.ua.R;

/**
 * Created by technomag on 11.05.17.
 */
public class CurrierFragmentTest extends ActivityInstrumentationTestCase2 {

  private Activity mActivity;

  private static final String LAUNCHER_ACTIVITY_CLASSNAME = "com.ua.inr0001.intime.SplashScreenActivity";
  private static Class<?> launchActivityClass;
  static {
    try {
      launchActivityClass = Class.forName(LAUNCHER_ACTIVITY_CLASSNAME);
    } catch (ClassNotFoundException e) {
      throw new RuntimeException(e);
    }
  }

  private Solo solo;

  //public DeliveryActivityTest() {
  //  super(DeliveryActivity.class);
  //}

  public CurrierFragmentTest(Class<CurrierFragment> activityClass) {
    super(activityClass);
  }

  @Before
  public void setUp() throws Exception {
    super.setUp();
    mActivity = getActivity();
    solo = new Solo(getInstrumentation(), mActivity);
  }

  @Override
  public void tearDown() throws Exception {
    solo.finishOpenedActivities();
    super.tearDown();
  }

  @SuppressWarnings("unchecked")
  public CurrierFragmentTest() {
    super((Class<CurrierFragment>) launchActivityClass);
  }

  public boolean selectStreet()
  {
    solo.clickOnView(solo.getView(R.id.editAddressStreet));
    if (solo.waitForActivity(SelectStreetActivity.class)) {

      final ListView view = (ListView) solo.getView(R.id.lvCities);
      //final Adapter adapter = view.getAdapter();
      solo.enterText((EditText) solo.getView(R.id.editCity), "Заводская");

      solo.waitForCondition(new Condition() {
        @Override
        public boolean isSatisfied() {
          return view.getCount()>0;
        }
      }, 30000);

      //solo.sleep(3000);
      solo.clickInList(0);
      return ((TextView) solo.getView(R.id.editAddressStreet)).getText().length()>0;
    }
    return false;
  }

  public boolean selectCity()
  {
    solo.clickOnView(solo.getView(R.id.editAddressCity));
    if (solo.waitForActivity(SelectCityActivity.class)) {
      final ListView view = (ListView) solo.getView(R.id.lvCities);

      //final Adapter adapter = view.getAdapter();
      solo.enterText((EditText) solo.getView(R.id.editCity), "Запорожье");

      solo.waitForCondition(new Condition() {
        @Override
        public boolean isSatisfied() {
          if (view.getCount()>0)
            Log.d("CurrierTest", "Count cities >0");
          Log.d("CurrierTest", Integer.toString(view.getCount()));
          return view.getCount()>0;
        }
      }, 30000);

        //solo.sleep(3000);
        solo.clickInList(0);
        Log.d("CurrierTest", "City select");
        return ((TextView) solo.getView(R.id.editAddressCity)).getText().length() > 0;

    }
    return false;
  }

  public void testCallCurrier()
  {
    if (solo.waitForActivity(MainActivity.class)) {

      solo.waitForCondition(new Condition() {
        @Override
        public boolean isSatisfied() {
          if (solo.getText(mActivity.getString(R.string.tab_currier)).getVisibility() == View.VISIBLE)
            Log.d("CurrierTest", "Activity Visible");
          Log.d("CurrierTest", "Activity check Visible");
          return solo.getText(mActivity.getString(R.string.tab_currier)).getVisibility() == View.VISIBLE;
        }
      }, 30000);

      solo.clickOnText(mActivity.getString(R.string.tab_currier));
      solo.enterText((EditText) solo.getView(R.id.editClientFullName), "Тестовый Юнит");
      solo.enterText((EditText) solo.getView(R.id.editClientPhone), "99990590300");
      solo.enterText((EditText) solo.getView(R.id.editAddressHouse), "1");
      solo.enterText((EditText) solo.getView(R.id.editAddressRoom), "2");
      if (selectCity())
        if (selectStreet()) {
          solo.takeScreenshot();
          solo.clickOnView(solo.getView(R.id.btnCallCurrier));
          solo.sleep(2000);
          solo.takeScreenshot();
          solo.goBack();
        }
      solo.sleep(2000);
    }
  }

}