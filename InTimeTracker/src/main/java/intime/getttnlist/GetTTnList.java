
package intime.getttnlist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetTTnList {

    @SerializedName("_post_get_ttn_by_api_key_rest")
    @Expose
    public PostGetTtnByApiKeyRest postGetTtnByApiKeyRest;

    public GetTTnList(PostGetTtnByApiKeyRest postGetTtnByApiKeyRest)
    {
        this.postGetTtnByApiKeyRest = postGetTtnByApiKeyRest;
    }

}
