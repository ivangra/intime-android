
package intime.getttnlist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PostGetTtnByApiKeyRest {

    @SerializedName("where")
    @Expose
    public String where;
    @SerializedName("order")
    @Expose
    public String order;
    @SerializedName("api_key")
    @Expose
    public String apiKey;
    @SerializedName("row_start")
    @Expose
    public String rowStart;
    @SerializedName("row_end")
    @Expose
    public String rowEnd;

}
