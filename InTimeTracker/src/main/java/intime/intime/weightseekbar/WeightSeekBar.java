package intime.intime.weightseekbar;

import android.content.Context;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import intime.llc.ua.R;

/**
 * Created by technomag on 22.09.17.
 */

public class WeightSeekBar extends android.support.v7.widget.AppCompatSeekBar implements SeekBar.OnSeekBarChangeListener,
  View.OnTouchListener{

  private final static String TAG = "WeightSeekBar";
  private Context context;
  private EditText tvWeight;
  private ImageView imgHumenWeight;
  private TextView tvWeightDescription;
  private FrameLayout frBoxWeight;
  private ViewGroup.LayoutParams lp;
  private ViewGroup.LayoutParams lpHuman;
  private Double weight = 0.1d;
  private int oldprogress = 0;
  private int index =0;
  private Handler repeatUpdateHandler = new Handler();
  private int longTap = 0;
  private boolean track = false;
  private IWeightSeekBar weightCallBack = null;

  private Integer[] positions = {0, 50, 100, 150, 200, 250, 300, 350, 400, 450, 500};
  private Double[] weihgts = {0.1d, 0.25d, 0.5d, 1.0d, 2.0d, 5.0d, 10.0d, 15.0d, 20.0d, 25.0d, 31.0d};

  private boolean mAutoIncrement= true;
  private boolean mAutoDecrement= true;
  private long startTime = 0;
  private long endTime = 0;

  private TextWatcher twWeight = new TextWatcher() {
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
      Log.d(TAG, tvWeight.getText().toString());
      try {
        setWeight(Double.parseDouble(tvWeight.getText().toString()));
      }
      catch(Exception e)
      {

      }
    }
  };

  public WeightSeekBar(Context context) {
    super(context);
    this.context = context;
    this.setOnSeekBarChangeListener(this);
    this.setOnTouchListener(this);
  }

  public WeightSeekBar(Context context, AttributeSet attrs) {
    super(context, attrs);
    this.context = context;
    this.setOnSeekBarChangeListener(this);
    this.setOnTouchListener(this);
  }

  public WeightSeekBar(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    this.context = context;
    this.setOnSeekBarChangeListener(this);
    this.setOnTouchListener(this);
  }


  private int nearPosition(int position)
  {
    index = 0;
    for (int pos : positions)
    {
      if (pos >= (position-10) && pos <= (position+10))
      {
        return pos;
      }
      index++;
    }
    return -1;
  }

  private void initDescription(final Double weight)
  {
    String descr = context.getResources().getString(R.string.box_weight_hand);
    float width = 16;
    float height = 16;
    float h_width = 58;
    float h_height = 58;
    float alpha = 1.0f;
    if (weight<1.0d) {
      alpha = 1.0f;
      descr = context.getResources().getString(R.string.box_weight_hand);
      width = 10;
      height = 10;
    }
    if (weight>=1.0d) {
      alpha = 1.0f;
      descr = context.getResources().getString(R.string.box_weight_small);
      width = 10;
      height = 10;
    }
    if (weight>=15.0d) {
      alpha = 1.0f;
      descr = context.getResources().getString(R.string.box_weight_middle);
      width = 16;
      height = 16;
    }
    if (weight>=20.0d) {
      alpha = 0.9f;
      width = 24;
      height = 24;
    }
    if (weight>=25.0d) {
      alpha = 0.8f;
      descr = context.getResources().getString(R.string.box_weight_big);
      width = 30;
      height = 30;
    }
    if (weight>=31.0d) {
      alpha = 0.6f;
      width = 38;
      height = 38;
    }
    if (weight>=32.0d) {
      width = 39;
      height = 39;
    }
    if (weight>=33.0d) {
      width = 40;
      height = 40;
    }
    if (weight>=34.0d) {
      width = 41;
      height = 41;
    }
    if (weight>=35.0d) {
      width = 42;
      height = 42;
      h_width = 58;
      h_height = 58;
    }
    if (weight>=36.0d) {
      width = 43;
      height = 43;
      h_width = 57;
      h_height = 57;
    }
    if (weight>=37.0d) {
      width = 44;
      height = 44;
      h_width = 56;
      h_height = 56;
    }
    if (weight>=38.0d) {
      width = 45;
      height = 45;
      h_width = 55;
      h_height = 55;
    }
    if (weight>=39.0d) {
      width = 46;
      height = 46;
      h_width = 54;
      h_height = 54;
    }
    if (weight>=40.0d) {
      width = 47;
      height = 47;
      h_width = 53;
      h_height = 53;
    }
    if (weight>=41.0d) {
      width = 48;
      height = 48;
      h_width = 52;
      h_height = 52;
    }
    if (weight>=42.0d) {
      width = 49;
      height = 49;
    }
    if (weight>=43.0d) {
      width = 50;
      height = 50;
    }
    if (weight>=44.0d) {
      width = 51;
      height = 51;
    }
    if (weight>=45.0d) {
      alpha = 0.5f;
      width = 52;
      height = 52;
    }
    if (weight>=46.0d) {
      width = 53;
      height = 53;
    }
    if (weight>=47.0d) {
      width = 54;
      height = 54;
    }
    if (weight>=48.0d) {
      width = 55;
      height = 55;
    }
    if (weight>=49.0d) {
      width = 56;
      height = 56;
    }
    if (weight>=50.0d) {
      width = 57;
      height = 57;
    }
    if (weight>=51.0d) {
      width = 58;
      height = 58;
    }
    if (weight>=81.0d) {
      alpha = 0.4f;
      descr = context.getResources().getString(R.string.box_weight_very_big);
    }
    if (weight>=200.0d) {
      alpha = 0.3f;
      descr = context.getResources().getString(R.string.box_weight_too_big);
    }
    if (tvWeightDescription!=null)
      tvWeightDescription.setText(descr);
    if (lpHuman!=null && imgHumenWeight!=null)
    {
      lpHuman.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, h_width, getResources().getDisplayMetrics());
      lpHuman.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, h_height, getResources().getDisplayMetrics());
      imgHumenWeight.setLayoutParams(lpHuman);
      imgHumenWeight.requestLayout();
    }
    if (lp!=null && frBoxWeight!=null)
    {
      lp.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, width, getResources().getDisplayMetrics());
      lp.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, height, getResources().getDisplayMetrics());
      frBoxWeight.setLayoutParams(lp);
      frBoxWeight.setAlpha(alpha);
    }
  }

  @Override
  public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
    if (fromUser==true)
    {
      try {
        int nearPos = nearPosition(progress);
        if (nearPos != -1) {
          weight = weihgts[index];
          this.setProgress(nearPos);
        }
        if (progress > 510 && progress <= 800) {
          if (oldprogress < progress)
            weight += 1.0d;
          else
            weight -= 1.0d;
          if (weight < 31.0d)
            weight = 31.0d;
        }
        if (progress > 800 && track == true) {
          longTap = 150;
          if (oldprogress < progress) {
            mAutoIncrement = true;
            mAutoDecrement = false;
          } else {
            mAutoIncrement = false;
            mAutoDecrement = true;
          }
          repeatUpdateHandler.post(new RptUpdater());
        }

      } catch (Exception e) {
        Log.d(TAG, "Exception");
      }

      setTextWeight(String.valueOf(weight));

      if (tvWeightDescription!=null)
        initDescription(weight);
      oldprogress = progress;

    }
  }

  private void setTextWeight(String weight)
  {
    if (tvWeight!=null) {
      tvWeight.removeTextChangedListener(twWeight);
      tvWeight.setText(weight);
      tvWeight.addTextChangedListener(twWeight);
    }
  }

  private void initProgressByWeight(Double weight)
  {
    int progress = 0;
    if (weight<=0.1d)
        progress = positions[0];
    if (weight>0.1d && weight<=0.25d)
        progress = positions[1];
    if (weight>0.25d && weight<=0.5d)
      progress = positions[2];
    if (weight>0.5d && weight<=1.0d)
      progress = positions[3];
    if (weight>1.0d && weight<=2.0d)
      progress = positions[4];
    if (weight>2.0d && weight<=5.0d)
      progress = positions[5];
    if (weight>5.0d && weight<=10.0d)
      progress = positions[6];
    if (weight>15.0d && weight<=20.0d)
      progress = positions[7];
    if (weight>20.0d && weight<=25.0d)
      progress = positions[8];
    if (weight>25.0d && weight<=31.0d)
      progress = positions[9];
    if (weight>31.0d  && weight<=100.0d)
      progress = positions[10];
    if (weight>=200.0d)
      progress = 1000;
    this.setProgress(progress);
  }

  private void setWeight(Double weight)
  {
    this.weight = weight;
    if (tvWeightDescription!=null)
      initDescription(weight);
    initProgressByWeight(weight);
    if (weightCallBack!=null)
      weightCallBack.weightChanged(weight);
  }

  @Override
  public void onStartTrackingTouch(SeekBar seekBar) {
      track=true;
  }

  @Override
  public void onStopTrackingTouch(SeekBar seekBar) {
      track = false;
      if (weightCallBack!=null)
        weightCallBack.weightChanged(weight);
  }

  @Override
  public boolean onTouch(View v, MotionEvent motionEvent) {

    if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
      startTime = motionEvent.getEventTime();
      return true;
    }
    if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
      endTime = motionEvent.getEventTime();
      if (Math.abs(startTime-endTime)<=100)
        return true;
      else
        return false;
    }
    Log.d(TAG, String.valueOf(motionEvent.getEventTime()));
    return false;
  }

  class RptUpdater implements Runnable {
    public void run() {
      if (track) {
        if (mAutoIncrement)
          weight += 1.0d;
        else if (mAutoDecrement)
          weight -= 1.0d;
        if (weight<=31.0d) {
          weight = 31.0d;
          track = false;
        }
        setTextWeight(String.valueOf(weight));
        initDescription(weight);
        repeatUpdateHandler.postDelayed(new RptUpdater(), longTap);
        longTap -= 5;
        if (longTap < 50)
          longTap = 50;
      }
    }
  }

  public void setParams(EditText tvWeight,
                        ImageView imgHumenWeight,
                        TextView tvWeightDescription,
                        FrameLayout frBoxWeight,
                        IWeightSeekBar weightCallBack){
    this.tvWeight = tvWeight;
    this.imgHumenWeight = imgHumenWeight;
    if (this.imgHumenWeight!=null)
      lpHuman = imgHumenWeight.getLayoutParams();
    if (this.tvWeight!=null)
      this.tvWeight.addTextChangedListener(twWeight);
    this.frBoxWeight = frBoxWeight;
    if (this.frBoxWeight!=null)
      lp = frBoxWeight.getLayoutParams();
    this.tvWeightDescription = tvWeightDescription;
    this.weightCallBack = weightCallBack;
    initDescription(this.weight);
  }

  public interface IWeightSeekBar{
    void weightChanged(Double weight);
  }

}
