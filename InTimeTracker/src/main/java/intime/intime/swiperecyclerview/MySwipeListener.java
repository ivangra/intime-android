package intime.intime.swiperecyclerview;

import android.content.Context;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import com.ua.inr0001.intime.TtnHistoryRvCursorAdapter;

/**
 * Created by technomag on 10.01.18.
 */

public class MySwipeListener implements RecyclerView.OnItemTouchListener {

  private static final float SWIPE_MIN_DISTANCE = 120; //120;
  private static final float SWIPE_THRESHOLD_VELOCITY = 200;
  private static final float SWIPE_MAX_OFF_PATH = 20; //250;
  private static final float MOVE_X_DISTANCE_LEFT = 10.0f;
  private static final float MOVE_X_DISTANCE_RIGHT = 20.0f;

  private float DIP_SWIPE_MIN_DISTANCE;
  private float DIP_SWIPE_MAX_OFF_PATH;
  private float DIP_MOVE_X_DISTANCE_LEFT;
  private float DIP_MOVE_X_DISTANCE_RIGHT;
  private OnTouchActionListener mOnTouchActionListener;
  private GestureDetectorCompat mGestureDetector;

  public static interface OnTouchActionListener {
    public void onLeftSwipe(View view, int position);
    public void onRightSwipe(View view, int position);
    public void onClick(View view, int position);
  }

  public MySwipeListener(Context context, final RecyclerView recyclerView,
                         OnTouchActionListener onTouchActionListener){

    DIP_SWIPE_MAX_OFF_PATH = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, SWIPE_MAX_OFF_PATH, context.getResources().getDisplayMetrics());
    DIP_SWIPE_MIN_DISTANCE = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, SWIPE_MIN_DISTANCE, context.getResources().getDisplayMetrics());
    DIP_MOVE_X_DISTANCE_LEFT = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, MOVE_X_DISTANCE_LEFT, context.getResources().getDisplayMetrics());
    DIP_MOVE_X_DISTANCE_RIGHT = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, MOVE_X_DISTANCE_RIGHT, context.getResources().getDisplayMetrics());
    mOnTouchActionListener = onTouchActionListener;
    mGestureDetector = new GestureDetectorCompat(context,new GestureDetector.SimpleOnGestureListener(){

      @Override
      public boolean onSingleTapConfirmed(MotionEvent e) {
        // Find the item view that was swiped based on the coordinates
        View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
        int childPosition = recyclerView.getChildPosition(child);
        mOnTouchActionListener.onClick(child, childPosition);
        return false;
      }

      private void swipeViewLeft(TtnHistoryRvCursorAdapter.TtnHolder holder)
      {
//        if (Math.abs(holder.viewForeground.getX()) <= (holder.viewBackground.getWidth() - DIP_MOVE_X_DISTANCE_LEFT)) {
//          holder.viewForeground.setTranslationX(holder.viewForeground.getX() - DIP_MOVE_X_DISTANCE_LEFT); // swipe left
//          if (holder.prevHolder != null)
//            holder.prevHolder.frBottomCard.setTranslationX(holder.prevHolder.frBottomCard.getX() - DIP_MOVE_X_DISTANCE_LEFT);
//        }
      }

      private void swipeViewRight(TtnHistoryRvCursorAdapter.TtnHolder holder)
      {
//        if (holder.viewForeground.getX() >= 0) {
//          holder.viewForeground.setTranslationX(0);
//          if (holder.prevHolder != null)
//            holder.prevHolder.frBottomCard.setTranslationX(0);
//        } else {
//          holder.viewForeground.setTranslationX(holder.viewForeground.getX() + DIP_MOVE_X_DISTANCE_RIGHT); // swipe right
//          if (holder.prevHolder != null)
//            holder.prevHolder.frBottomCard.setTranslationX(holder.prevHolder.frBottomCard.getX() + DIP_MOVE_X_DISTANCE_RIGHT);
//        }
      }

      @Override
      public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        //Log.d("Swipe", "scroll: " + Float.toString(distanceX));
        if (Math.abs(e1.getY() - e2.getY()) > DIP_SWIPE_MAX_OFF_PATH) {
          //Log.d("Swipe", "max off: " + Math.abs(e1.getY() - e2.getY()));
          return false;
        }

        if (Math.abs(e1.getX() - e2.getX()) > DIP_SWIPE_MIN_DISTANCE) {
          View child = recyclerView.findChildViewUnder(e1.getX(), e1.getY());
          RecyclerView.ViewHolder viewHolder = recyclerView.getChildViewHolder(child);
          if (viewHolder != null && viewHolder instanceof TtnHistoryRvCursorAdapter.TtnHolder) {
            TtnHistoryRvCursorAdapter.TtnHolder holder = (TtnHistoryRvCursorAdapter.TtnHolder) viewHolder;
            if (distanceX >= 0) {
              //Log.d("Swipe", "swipe left ");
              swipeViewLeft(holder);
            } else {
              //Log.d("Swipe", "swipe right ");
              swipeViewRight(holder);
            }
            return super.onScroll(e1, e2, distanceX, distanceY);
          } else
            return false;
        }
        return false;
      }

      @Override
      public boolean onFling(MotionEvent e1, MotionEvent e2,
                             float velocityX, float velocityY) {

        try {
          if (Math.abs(e1.getY() - e2.getY()) > DIP_SWIPE_MAX_OFF_PATH) {
            return false;
          }

          // Find the item view that was swiped based on the coordinates
          View child = recyclerView.findChildViewUnder(e1.getX(), e1.getY());
          int childPosition = recyclerView.getChildPosition(child);

          // right to left swipe
          if (e1.getX() - e2.getX() > DIP_SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {

            if (mOnTouchActionListener != null && child != null) {
              //child.setTranslationX(e1.getX());
              mOnTouchActionListener.onLeftSwipe(child, childPosition);
            }

          } else if (e2.getX() - e1.getX() > DIP_SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
            if (mOnTouchActionListener != null && child != null) {
              mOnTouchActionListener.onRightSwipe(child, childPosition);
            }
          }
        } catch (Exception e) {
          // nothing
        }

        return false;
      }
    });
  }

  @Override
  public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
    mGestureDetector.onTouchEvent(e);
    return false;
  }

  @Override
  public void onTouchEvent(RecyclerView rv, MotionEvent e) {
    //mGestureDetector.onTouchEvent(e);
  }

  @Override
  public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

  }
}
