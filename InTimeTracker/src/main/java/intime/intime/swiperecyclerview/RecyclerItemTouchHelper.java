package intime.intime.swiperecyclerview;

import android.graphics.Canvas;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.TypedValue;
import android.view.View;

import com.ua.inr0001.intime.InTimeTrackerApp;
import com.ua.inr0001.intime.TtnHistoryRvCursorAdapter;

/**
 * Created by ravi on 29/09/17.
 */

public class RecyclerItemTouchHelper extends ItemTouchHelper.SimpleCallback {
    private RecyclerItemTouchHelperListener listener;

    private float oldDx = 0.0f;
    private static final float MOVE_X_DISTANCE_LEFT = 10.0f;
    private static final float MOVE_X_DISTANCE_RIGHT = 20.0f;
    private float DIP_MOVE_X_DISTANCE_LEFT = 10.0f;
    private float DIP_MOVE_X_DISTANCE_RIGHT = 20.0f;

    private int getDirection(float dX)
    {
        //Log.d("Swipe", "OldDx: " + Float.toString(Math.abs(oldDx)) + " dx: " + Float.toString(Math.abs(dX)));
        if (Math.abs(oldDx)>Math.abs(dX))
            return 1;
        else
            return 0;
    }

    public RecyclerItemTouchHelper(int dragDirs, int swipeDirs, RecyclerItemTouchHelperListener listener) {
        super(dragDirs, swipeDirs);
        this.listener = listener;
        DIP_MOVE_X_DISTANCE_LEFT = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, MOVE_X_DISTANCE_LEFT,
          InTimeTrackerApp.getAppContext().getResources().getDisplayMetrics());
        DIP_MOVE_X_DISTANCE_RIGHT = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, MOVE_X_DISTANCE_RIGHT,
          InTimeTrackerApp.getAppContext().getResources().getDisplayMetrics());
    }

    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
        return false;
    }

    @Override
    public void onSelectedChanged(RecyclerView.ViewHolder viewHolder, int actionState) {
        if (viewHolder != null) {
//            if (viewHolder instanceof TtnHistoryRvCursorAdapter.TtnHolder) {
//                final View foregroundView = ((TtnHistoryRvCursorAdapter.TtnHolder) viewHolder).viewForeground;
////                if (actionState == ItemTouchHelper.ACTION_STATE_IDLE)
////                    getDefaultUIUtil().onSelected(foregroundView);
//            }
        }
    }

    private void swipeViewLeft(TtnHistoryRvCursorAdapter.TtnHolder holder)
    {
//        if (Math.abs(holder.viewForeground.getX()) <= (holder.viewBackground.getWidth() - DIP_MOVE_X_DISTANCE_LEFT)) {
//            holder.viewForeground.setTranslationX(holder.viewForeground.getX() - DIP_MOVE_X_DISTANCE_LEFT); // swipe left
//            if (holder.prevHolder != null)
//                holder.prevHolder.frBottomCard.setTranslationX(holder.prevHolder.frBottomCard.getX() - DIP_MOVE_X_DISTANCE_LEFT);
//        }
    }

    private void swipeViewRight(TtnHistoryRvCursorAdapter.TtnHolder holder)
    {
//        if (holder.viewForeground.getX() >= 0) {
//            oldDx = -holder.viewForeground.getWidth();
//            holder.viewForeground.setTranslationX(0);
//            if (holder.prevHolder != null)
//                holder.prevHolder.frBottomCard.setTranslationX(0);
//        } else {
//            holder.viewForeground.setTranslationX(holder.viewForeground.getX() + DIP_MOVE_X_DISTANCE_RIGHT); // swipe right
//            if (holder.prevHolder != null)
//                holder.prevHolder.frBottomCard.setTranslationX(holder.prevHolder.frBottomCard.getX() + DIP_MOVE_X_DISTANCE_RIGHT);
//        }
    }

    @Override
    public void onChildDrawOver(Canvas c, RecyclerView recyclerView,
                                RecyclerView.ViewHolder viewHolder, float dX, float dY,
                                int actionState, boolean isCurrentlyActive) {
        if (viewHolder!=null && viewHolder instanceof TtnHistoryRvCursorAdapter.TtnHolder){
            if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE && isCurrentlyActive==true) {
                if (getDirection(dX)==0) {
                    if (dX<0) {
                        swipeViewLeft((TtnHistoryRvCursorAdapter.TtnHolder) viewHolder);
                    }
                }
                else {
                    swipeViewRight((TtnHistoryRvCursorAdapter.TtnHolder) viewHolder);
                }
                oldDx = dX;
            }
        }

    }

    @Override
    public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
//        if (viewHolder instanceof TtnHistoryRvCursorAdapter.TtnHolder) {
//            final View foregroundView = ((TtnHistoryRvCursorAdapter.TtnHolder) viewHolder).viewForeground;
//            getDefaultUIUtil().clearView(foregroundView);
//        }
    }

    @Override
    public void onChildDraw(Canvas c, RecyclerView recyclerView,
                            RecyclerView.ViewHolder viewHolder, float dX, float dY,
                            int actionState, boolean isCurrentlyActive) {
//        if (viewHolder instanceof TtnHistoryRvCursorAdapter.TtnHolder) {
//            final View foregroundView = ((TtnHistoryRvCursorAdapter.TtnHolder) viewHolder).viewForeground;
//            if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE && isCurrentlyActive==true) {
//            if (Math.abs(dX) > ((TtnHistoryRvCursorAdapter.TtnHolder) viewHolder).viewBackground.getWidth()) {
//                ViewGroup.LayoutParams lp = ((TtnHistoryRvCursorAdapter.TtnHolder) viewHolder).viewBackground.getLayoutParams();
//                lp.width = (int) Math.abs(dX);
//                ((TtnHistoryRvCursorAdapter.TtnHolder) viewHolder).viewBackground.setLayoutParams(lp);
//                //((TtnHistoryRvCursorAdapter.TtnHolder) viewHolder).viewBackground.setWi
//            }
//                foregroundView.setTranslationX(dX);
//            }
//        }

    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
//        if (viewHolder instanceof TtnHistoryRvCursorAdapter.TtnHolder)
//            listener.onSwiped(viewHolder, direction, viewHolder.getAdapterPosition());
    }

    @Override
    public int convertToAbsoluteDirection(int flags, int layoutDirection) {
        return super.convertToAbsoluteDirection(flags, layoutDirection);
    }

    public interface RecyclerItemTouchHelperListener {
        void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position);
    }
}
