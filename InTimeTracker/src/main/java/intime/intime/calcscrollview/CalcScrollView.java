package intime.intime.calcscrollview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ScrollView;

/**
 * Created by technomag on 01.11.17.
 */

public class CalcScrollView extends ScrollView {

  OnBottomReachedListener mBottomListener;
  OnTopReachedListener mTopListener;

  public void setHeightBottomLayout(int heightBottomLayout) {
    if (heightBottomLayout>0)
      this.heightBottomLayout = heightBottomLayout;
  }

  private int heightBottomLayout = 100;

  public CalcScrollView(Context context, AttributeSet attrs,
                               int defStyle) {
    super(context, attrs, defStyle);
  }

  public CalcScrollView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public CalcScrollView(Context context) {
    super(context);
  }

  @Override
  protected void onScrollChanged(int l, int t, int oldl, int oldt) {
    View view = (View) getChildAt(getChildCount()-1);
    int diff = (view.getBottom()-(getHeight()+getScrollY()));
    //Log.d("MyScroll", "Diff: " + Integer.toString(diff));
    if (diff <= heightBottomLayout && mBottomListener != null) {
      mBottomListener.onBottomReached();
    }

    if (diff > heightBottomLayout && mTopListener != null) {
      mTopListener.onTopReached();
    }

    super.onScrollChanged(l, t, oldl, oldt);
  }

  public void setOnBottomReachedListener(
    OnBottomReachedListener onBottomReachedListener) {
    mBottomListener = onBottomReachedListener;
  }

  public void setOnTopReachedListener(
    OnTopReachedListener onTopReachedListener) {
    mTopListener = onTopReachedListener;
  }

  public interface OnBottomReachedListener{
    void onBottomReached();
  }

  public interface OnTopReachedListener{
    void onTopReached();
  }

}
