package intime.intime.ttnlistlayoutmanager;

import android.content.Context;
import android.graphics.Rect;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;

import java.lang.reflect.Field;
import java.util.HashMap;

/**
 * Created by technomag on 25.01.18.
 */

public class MyLayoutManager extends RecyclerView.LayoutManager {

  private static final String TAG = "ScrollManager";
  private static final float VIEW_HEIGHT_PERCENT = 0.75f;

  private int mLastVisiblePosition = 0;

  private SparseArray<View> viewCache = new SparseArray<>();

  private Context context;

  private int mAnchorPos;

  public MyLayoutManager(Context context)
  {

  }

  @Override
  public boolean canScrollVertically() {
    return true;
  }



  @Override
  public int scrollVerticallyBy(int dy, RecyclerView.Recycler recycler, RecyclerView.State state) {
    //Log.d(TAG,  "dy: " + Integer.toString(dy));
    int delta = scrollVerticallyInternal(dy);
    offsetChildrenVertical(-delta);
    //fill(recycler);
    return delta;
    //offsetChildrenVertical(-dy);
    //return dy;
  }

  private int scrollVerticallyInternal(int dy) {
    int childCount = getChildCount();
    int itemCount = getItemCount();
    if (childCount == 0){
      return 0;
    }

    final View topView = getChildAt(0);
    final View bottomView = getChildAt(childCount - 1);

    //Случай, когда все вьюшки поместились на экране
    int viewSpan = getDecoratedBottom(bottomView) - getDecoratedTop(topView);
    if (viewSpan <= getHeight()) {
      return 0;
    }

    int delta = 0;
    //если контент уезжает вниз
    if (dy < 0){
      View firstView = getChildAt(0);
      int firstViewAdapterPos = getPosition(firstView);
      if (firstViewAdapterPos > 0){ //если верхняя вюшка не самая первая в адаптере
        delta = dy;
      } else { //если верхняя вьюшка самая первая в адаптере и выше вьюшек больше быть не может
        int viewTop = getDecoratedTop(firstView);
        delta = Math.max(viewTop, dy);
        //Log.d(TAG, "viewTop:" +  Integer.toString(viewTop));
        //Log.d(TAG, "delta:" +  Integer.toString(delta));
      }
    } else if (dy > 0){ //если контент уезжает вверх
      View lastView = getChildAt(childCount - 1);
      int lastViewAdapterPos = getPosition(lastView);
      if (lastViewAdapterPos < itemCount - 1){ //если нижняя вюшка не самая последняя в адаптере
        delta = dy;
      } else { //если нижняя вьюшка самая последняя в адаптере и ниже вьюшек больше быть не может
        int viewBottom = getDecoratedBottom(lastView);
        int parentBottom = getHeight();
        delta = Math.min(viewBottom - parentBottom, dy);
      }
    }
    return delta;
  }

   private void fill(RecyclerView.Recycler recycler) {
      fillDown(recycler);
   }

  @Override
  public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
    detachAndScrapAttachedViews(recycler);
    //fillDown1(recycler);
    fill(recycler);
  }

  private void fillUp1(RecyclerView.Recycler recycler) {
    boolean fillUp = true;
    int pos = 0; //anchorPos - 1;
    int viewBottom = 0;// = anchorTop;

    while (fillUp && pos >= 0){
      View view = viewCache.get(pos);
      if (view == null){
        view = recycler.getViewForPosition(pos);
        addView(view, 0);
        //measureChildWithDecorationsAndMargin(view, widthSpec, heightSpec);
        measureChildWithMargins(view, 0, 0);
        //int decoratedMeasuredWidth = getDecoratedMeasuredWidth(view);
        layoutDecorated(view, 0, viewBottom - getDecoratedMeasuredHeight(view), getDecoratedMeasuredWidth(view), viewBottom);
        viewCache.put(pos, view);
      } else {
        attachView(view, 0);
        viewCache.remove(pos);
      }
      viewBottom = getDecoratedTop(view);
      fillUp = (viewBottom > 0);
      pos--;
    }
  }

  private void fillDown(RecyclerView.Recycler recycler) {
      int height = 0;
      height = getHeight();
      Log.d(TAG, "Height: " + height);
      int childCount = getChildCount();
      Log.d(TAG, "childCount: " + childCount);
      int itemCount = getItemCount();
      Log.d(TAG, "ItemCount: " + itemCount);
      int pos = getChildCount();
      View view1 = recycler.getViewForPosition(pos);
      int viewTop = getDecoratedTop(view1);
      //pos++;
      while (pos < itemCount) {
        //View view2 = viewCache.get(pos);
        //if (view2 == null) {
          View view = recycler.getViewForPosition(pos);
          addView(view);
          measureChildWithMargins(view, 0, 0);
          layoutDecorated(view, 0, viewTop, getDecoratedMeasuredWidth(view), viewTop + getDecoratedMeasuredHeight(view));
          viewTop = getDecoratedBottom(view) - 70;
          pos++;
        //}
      }
  }

  private void measureChildWithDecorationsAndMargin(View child, int widthSpec, int heightSpec) {
    Rect decorRect = new Rect();
    calculateItemDecorationsForChild(child, decorRect);
    RecyclerView.LayoutParams lp = (RecyclerView.LayoutParams) child.getLayoutParams();
    widthSpec = updateSpecWithExtra(widthSpec, lp.leftMargin + decorRect.left,
      lp.rightMargin + decorRect.right);
    heightSpec = updateSpecWithExtra(heightSpec, lp.topMargin + decorRect.top,
      lp.bottomMargin + decorRect.bottom);
    child.measure(widthSpec, heightSpec);
  }

  private int updateSpecWithExtra(int spec, int startInset, int endInset) {
    if (startInset == 0 && endInset == 0) {
      return spec;
    }
    final int mode = View.MeasureSpec.getMode(spec);
    if (mode == View.MeasureSpec.AT_MOST || mode == View.MeasureSpec.EXACTLY) {
      return View.MeasureSpec.makeMeasureSpec(
        View.MeasureSpec.getSize(spec) - startInset - endInset, mode);
    }
    return spec;
  }

  @Override
  public RecyclerView.LayoutParams generateDefaultLayoutParams() {
    return new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT);
  }

  @Override
  public void onMeasure(RecyclerView.Recycler recycler, RecyclerView.State state, int widthSpec, int heightSpec) {
    Log.d(TAG, "onMeasure()");
    super.onMeasure(recycler, state, widthSpec, heightSpec);
  }

}