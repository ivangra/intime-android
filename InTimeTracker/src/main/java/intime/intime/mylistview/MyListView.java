package intime.intime.mylistview;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.ListView;

/**
 * Created by ILYA on 12.01.2017.
 */

public class MyListView extends ListView {

  private android.view.ViewGroup.LayoutParams params;
  private int oldCount = 0;

  public MyListView(Context context, AttributeSet attrs)
  {
    super(context, attrs);
  }

  @Override
  protected void onDraw(Canvas canvas)
  {
    /*if (getCount() != oldCount)
    {
      int height = getChildAt(0).getHeight() + 2 ;
      oldCount = getCount();
      params = getLayoutParams();
      params.height = getCount() * height;
      setLayoutParams(params);
    }*/

    super.onDraw(canvas);
  }

  @Override
  public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2,
      MeasureSpec.AT_MOST);
    super.onMeasure(widthMeasureSpec, expandSpec);
  }

}
