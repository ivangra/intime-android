package intime.utils;

import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.LocaleList;

import com.ua.inr0001.intime.InTimeTrackerApp;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

/**
 * Created by technomag on 19.12.17.
 */

public class MyStringUtils {

  private static String insertChar(final String inStr, int position)
  {
    String first = inStr.substring(0, position);
    String second = inStr.substring(position, inStr.length());
    return first + " " + second;
  }

  public static String getFormattedTTN(final String ttn)
  {
    try {
      String ttnTemp = ttn;
      ttnTemp = insertChar(ttnTemp, 3);
      return insertChar(ttnTemp, 7);
    }
    catch (Exception e)
    {
      return ttn;
    }
  }

  public static String getFormattedPhone(final String phone)
  {
    try {
      String ph1 = phone;
      ph1 = "+" + ph1;
      ph1 = insertChar(ph1, 3);
      ph1 = insertChar(ph1, 7);
      ph1 = insertChar(ph1, 11);
      ph1 = insertChar(ph1, 14);
      return ph1;
    }
    catch (Exception e)
    {
      return phone;
    }
  }

  public static String roundTotalSum(final Double totalsum)
  {
    DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.ENGLISH);
    DecimalFormat df = new DecimalFormat("#.##", otherSymbols);
    return df.format(totalsum);
  }

//  public static String getLangString(String lang, int stringId )
//  {
//    Resources resources = InTimeTrackerApp.getAppContext().getResources();
//    Configuration configuration = new Configuration(resources.getConfiguration());
//    Locale defaultLocale = new Locale(lang);
//    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//      LocaleList localeList = new LocaleList(defaultLocale);
//      configuration.setLocales(localeList);
//      return InTimeTrackerApp.getAppContext().createConfigurationContext(configuration).getString(stringId);
//    } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1){
//      configuration.setLocale(defaultLocale);
//      return InTimeTrackerApp.getAppContext().createConfigurationContext(configuration).getString(stringId);
//    }
//    return InTimeTrackerApp.getAppContext().getString(stringId);
//  }

  public static String[] getLangStringArray(String lang, int stringId )
  {
    Resources resources = InTimeTrackerApp.getAppContext().getResources();
    Configuration configuration = new Configuration(resources.getConfiguration());
    Locale defaultLocale = new Locale(lang);
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
      LocaleList localeList = new LocaleList(defaultLocale);
      configuration.setLocales(localeList);
      return InTimeTrackerApp.getAppContext().createConfigurationContext(configuration).getResources().getStringArray(stringId);
    } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1){
      configuration.setLocale(defaultLocale);
      return InTimeTrackerApp.getAppContext().createConfigurationContext(configuration).getResources().getStringArray(stringId);
    }
    return InTimeTrackerApp.getAppContext().getResources().getStringArray(stringId);
  }

}
