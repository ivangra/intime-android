package intime.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import intime.llc.ua.R;

public class PermissionUtils {
    public static void showPermissionDialog(
            Context context,
            final PermissionDialogCallback callback
    ) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(context.getString(R.string.permission_dialog_title))
                .setMessage(R.string.permission_dialog_message)
                .setPositiveButton(R.string.permission_dialog_ok,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                callback.ok();
                                dialog.cancel();
                            }
                        })
                .setNegativeButton(R.string.permission_dialog_exit,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                callback.exit();
                                dialog.cancel();
                            }
                        })
                .create()
                .show();
    }

    public interface PermissionDialogCallback {
        void ok();

        void exit();
    }
}
