package intime.utils;

/**
 * <b>Copyright (C) One Eleven studio - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited
 * Proprietary and confidential
 * Written by Bogdan Artemenko <artemenkobogdan@01-11.com> on  on 11/13/18.</b>
 */
public class DbQuickFixUtils {
    public static String checkAndEscape(String param) {
        if (param == null) {
            return "";
        }

        if (param.contains("'")) {
            return param.replace("'", "''");
        }

        return param;
    }
}
