package intime.utils;

import android.database.Cursor;

import com.ua.inr0001.intime.DictionariesDB;
import com.ua.inr0001.intime.GlobalApplicationData;
import com.ua.inr0001.intime.WorkHoursData;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import intime.llc.ua.R;

import static com.ua.inr0001.intime.DictionariesDB.getColumnValue;

/**
 * Created by technomag on 24.04.17.
 */

public class MyDateUtils {

  public static String reformatDate(final String strDate)
  {
    DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
    Date date = null;
    try {
      date = format.parse(strDate);
      SimpleDateFormat sdf1 = new SimpleDateFormat("d MMMM", new MyDateFormatSymbols(GlobalApplicationData.getInstance().getContext()));
      return sdf1.format(date);
    } catch (ParseException e) {
      return strDate;
    }
  }

  public static String reformatDateTime(final String strDate)
  {
    DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
    Date date = null;
    try {
      date = format.parse(strDate);
      SimpleDateFormat sdf1 = new SimpleDateFormat("d MMMM yyyy, HH:mm", new MyDateFormatSymbols(GlobalApplicationData.getInstance().getContext()));
      return sdf1.format(date);
    } catch (ParseException e) {
      return strDate;
    }
  }

  public static String getCurrentDate()
  {
    Calendar c = Calendar.getInstance();
    String months[] = GlobalApplicationData.getInstance().getContext().getResources().getStringArray(R.array.months);
    SimpleDateFormat sdf = new SimpleDateFormat(
      "d MMMM, HH:mm", new MyDateFormatSymbols(GlobalApplicationData.getInstance().getContext()));
    SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
    return sdf.format(c.getTime());
  }

  public static String addDay(final String strDate, int day)
  {
    DateFormat format = new SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH);
    Calendar c = Calendar.getInstance();
    Date date = null;
    try {
      date = format.parse(strDate);
      c.setTime(date);
      c.add(Calendar.DAY_OF_YEAR, day);
      SimpleDateFormat sdf1 = new SimpleDateFormat("d MMMM, EEEE", new MyDateFormatSymbols(GlobalApplicationData.getInstance().getContext()));
      return sdf1.format(c.getTime());
    } catch (ParseException e) {
      return strDate;
    }
  }

  public static boolean nowIsHoliday()
  {
    Calendar calendar = Calendar.getInstance();
    int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
    int month = calendar.get(Calendar.MONTH) + 1;
    return getBoolHolidays(dayOfMonth, month) || calendar.get(Calendar.DAY_OF_WEEK)==Calendar.SUNDAY;
  }

  private static boolean getBoolHolidays(int day, int month)
  {
    if ( (day == 30 && month == 4) ||
      (day == 6 && month == 4) ||
      (day == 1 && month == 5) ||
      (day == 9 && month == 5) ||
      (day == 28 && month == 5)
      )
      return true;
    else
      return false;
  }

  public static String getWorhHoursStart(Cursor cursor)
  {
    Calendar calendar = Calendar.getInstance();
    int day = calendar.get(Calendar.DAY_OF_WEEK);
    int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
    int month = calendar.get(Calendar.MONTH) + 1;
    int []weekd = {0, 7, 1 , 2 , 3 , 4, 5, 6 };
    String workOpen = getColumnValue(cursor, "Open" + Integer.toString(weekd[day]));
    boolean holiday = getBoolHolidays(dayOfMonth, month);
    calendar.add(Calendar.DAY_OF_MONTH, 1);
    dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
    month = calendar.get(Calendar.MONTH) + 1;
    boolean holidayTomorrow = getBoolHolidays(dayOfMonth, month);
    if (holiday == true /*|| holidayTomorrow == true*/)
      return "holiday";
    else if (workOpen==null || workOpen.contains("null"))
      return "weekend";
    else
      return workOpen;
  }

  public static String getWorhHoursEnd(Cursor cursor)
  {
    Calendar calendar = Calendar.getInstance();
    int day = calendar.get(Calendar.DAY_OF_WEEK);
    int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
    int month = calendar.get(Calendar.MONTH) + 1;
    // vs = 1
    // pn = 2
    // vt = 3
    // sr = 4
    // cht = 5
    // pt = 6
    // sb = 7
    int []weekd = {0, 7, 1 , 2 , 3 , 4, 5, 6 };
    String workClose = DictionariesDB.getColumnValue(cursor, "Close" + Integer.toString(weekd[day]));
    boolean holiday = getBoolHolidays(dayOfMonth, month);
    calendar.add(Calendar.DAY_OF_MONTH, 1);
    dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
    month = calendar.get(Calendar.MONTH) + 1;
    boolean holidayTomorrow = getBoolHolidays(dayOfMonth, month);
    if (holiday == true /*|| holidayTomorrow == true*/)
      return "holiday";
    else if (workClose==null || workClose.contains("null"))
      return "weekend";
    else
      return workClose;
  }

  public static WorkHoursData getNextWorhHours(Cursor cursor)
  {
    Calendar calendar = Calendar.getInstance();
    int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
    int nowDayOfMonth = dayOfMonth;
    int month = calendar.get(Calendar.MONTH) + 1;
    int now = calendar.get(Calendar.DAY_OF_WEEK);
    int day = now + 1;
    int weekDay;
    // vs = 1
    // pn = 2
    // vt = 3
    // sr = 4
    // cht = 5
    // pt = 6
    // sb = 7
    int []weekd = {6, 7, 1 , 2 , 3 , 4, 5, 6, 7};
    if (day>weekd.length-1)
      day = 0;

    while(true){
      String workOpen = DictionariesDB.getColumnValue(cursor, "Open" + Integer.toString(weekd[day]));
      weekDay = calendar.get(Calendar.DAY_OF_WEEK);
      boolean holiday = getBoolHolidays(dayOfMonth, month);
      calendar.add(Calendar.DAY_OF_YEAR, 1);
      dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
      month = calendar.get(Calendar.MONTH) + 1;
      boolean holidayTomorrow = getBoolHolidays(dayOfMonth, month);
      if (holiday == true || holidayTomorrow == true) {
        workOpen = "null";
      }
      if (workOpen!=null && !workOpen.contains("null")) {
        int between = Math.abs(nowDayOfMonth - (dayOfMonth - 1));
        if (between>1) {
          Calendar cal1 = Calendar.getInstance(Locale.getDefault());
          cal1.set(Calendar.DAY_OF_WEEK, weekDay);
          //if (holidayTomorrow) {
            SimpleDateFormat sdf = new SimpleDateFormat("d MMMM");
            if (between>3)
              calendar.add(Calendar.DAY_OF_YEAR, -2);
            else
              calendar.add(Calendar.DAY_OF_YEAR, -1);
            return new WorkHoursData(workOpen, sdf.format(calendar.getTime()));
          //}
          //else {
          //  SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
          //  return new WorkHoursData(workOpen, "в " + sdf.format(cal1.getTime()));
          //}
        }
        else
          return new WorkHoursData(workOpen, GlobalApplicationData.getInstance().getContext().getResources().getString(R.string.tomorrow));
      }
      day++;
      if (day>weekd.length-1)
        day=0;
    }
  }
}
