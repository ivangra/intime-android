package intime.utils;

import android.content.Context;
import android.util.DisplayMetrics;

/**
 * Created by ILYA on 27.01.2017.
 */

public class DisplayUnits {

  public static float dpToPx(Context context, int dp) {
    return dp * getPixelScaleFactor(context);
  }

  public static float pxToDp(Context context, int px)
  {
    return px / getPixelScaleFactor(context);
  }

  private static float getPixelScaleFactor(Context context) {
    DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
    return (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT);
  }

}
