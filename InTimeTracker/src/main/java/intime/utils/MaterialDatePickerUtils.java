package intime.utils;

import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class MaterialDatePickerUtils {

  public static void setSundays(DatePickerDialog dpd)
  {
    Calendar sunday;
    List<Calendar> weekends = new ArrayList<>();
    int weeks = 16;

    for (int i = 0; i < (weeks * 7) ; i = i + 7) {
      sunday = Calendar.getInstance();
      sunday.add(Calendar.DAY_OF_YEAR, (Calendar.SUNDAY - sunday.get(Calendar.DAY_OF_WEEK) + 7 + i));
      // saturday = Calendar.getInstance();
      // saturday.add(Calendar.DAY_OF_YEAR, (Calendar.SATURDAY - saturday.get(Calendar.DAY_OF_WEEK) + i));
      // weekends.add(saturday);
      weekends.add(sunday);
    }
    Calendar[] disabledDays = weekends.toArray(new Calendar[weekends.size()]);
    dpd.setDisabledDays(disabledDays);
  }

  private static Calendar addHoliday(int day, int month)
  {
    Calendar calendar = Calendar.getInstance();
    calendar.set(Calendar.DAY_OF_MONTH, day);
    calendar.set(Calendar.MONTH, month);
    return calendar;
  }

  public static void setHolidays(DatePickerDialog dpd)
  {
    List<Calendar> dates = new ArrayList<>();
    dates.add(addHoliday(9, 3));
    dates.add(addHoliday(30, 3));
    dates.add(addHoliday(1, 4));
    dates.add(addHoliday(9, 4));
    Calendar[] disabledDays1 = dates.toArray(new Calendar[dates.size()]);
    dpd.setDisabledDays(disabledDays1);
  }

}
