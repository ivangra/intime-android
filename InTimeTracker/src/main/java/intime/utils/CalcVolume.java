package intime.utils;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

/**
 * Created by ILYA on 17.10.2016.
 */

public class CalcVolume {

  public static String CalcVolume(String strWidth, String strHeight, String strLength)
  {
    try {
      /*Double width = Double.parseDouble(strWidth);
      Double length = Double.parseDouble(strLength);
      Double height = Double.parseDouble(strHeight);
      Double volume = (width * height * length) * 0.000001d;
      //volume = volume < 0.001d ? 0.001d : volume;
      DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.ENGLISH);
      DecimalFormat df = new DecimalFormat("#.###", otherSymbols);
      return df.format(volume);*/
      Double width = Double.parseDouble(strWidth);
      Double length = Double.parseDouble(strLength);
      Double height = Double.parseDouble(strHeight);
      Double volume = (width * height * length) * 0.000001d;
      volume = volume < 0.0001d ? 0.0001d : volume;
      DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.ENGLISH);
      DecimalFormat df = new DecimalFormat("#.####", otherSymbols);
      return df.format(volume);
    } catch (Exception e) {
      return "0.0001";
    }
  }

}
