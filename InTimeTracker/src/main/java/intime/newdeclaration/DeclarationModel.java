package intime.newdeclaration;

import android.util.Log;

import com.ua.inr0001.intime.ICalculate;

/**
 * Created by ILYA on 13.02.2017.
 */

public class DeclarationModel {

  private static DeclarationModel instance;

  private ICalculate calculate;

  private String SenderCityName = "";
  private String SenderCityCode = "";
  private String ReceiverCityName = "";
  private String ReceiverCityCode = "";
  private String TypeFrom = "store";
  private String TypeTo = "store";
  private String CargoType = "cargo";
  private String Weight = "0.1";
  private String Width = "1";
  private String Height = "1";
  private String Length = "1";
  private String Cost = "200";
  private String typePallets = "";
  private String Count = "1";
  private String PodSum= "0";
  private String Package = "0";
  private String Radius = "";
  private String date = "";
  private String returnDocuents = "0";

  public String getReturnDocuents() {
    return returnDocuents;
  }

  public void setReturnDocuents(boolean returnDocuents) {
    if (returnDocuents) {
      this.returnDocuents = "1";
    }
    else {
      this.returnDocuents = "0";
    }
    dataUpdated();
  }

  public String getRadius() {
    return Radius;
  }

  public void setRadius(String radius) {
    Radius = radius.replaceAll("R", "");
    if (radius.length()>0)
      dataUpdated();
  }

  public String getPackage() {
    return Package;
  }

  public void setPackage(String aPackage) {
    Package = aPackage;
    dataUpdated();
  }

  public String getPodSum() {
    return PodSum;
  }

  public void setPodSum(String podSum) {
    PodSum = podSum;
    dataUpdated();
  }

  public String getCount() {
    return Count;
  }

  public void setCount(String count) {
    Count = count;
    dataUpdated();
  }

  public String getTypePallets() {
    return typePallets;
  }

  public void setTypePallets(String typePallets) {
    this.typePallets = typePallets.replaceAll("x", "*");
    Log.d("Calculator", this.typePallets);
    String[] size = this.typePallets.split("\\*");
    dataUpdated();
  }

  public String getCost() {
    return Cost;
  }

  public void setCost(String cost) {
    Cost = cost;
    dataUpdated();
  }

  public void setCostWithoutEvent(String cost)
  {
    Cost = cost;
  }

  public String getHeight() {
    return Height;
  }

  public void setHeight(String height) {
    Height = height;
    if (Width.length()>0 && Length.length()>0)
      dataUpdated();
  }

  public String getLength() {
    return Length;
  }

  public void setLength(String length) {
    Length = length;
    if (Width.length()>0 && Height.length()>0)
      dataUpdated();
  }

  public String getWeight() {
    return Weight;
  }

  public void setWeight(String weight) {
    //Log.d("Test", "Weight: " + weight);
    Weight = weight.replaceAll(",", ".");
    dataUpdated();
  }

  public String getWidth() {
    return Width;
  }

  public void setWidth(String width) {
    Width = width;
    if (Length.length()>0 && Height.length()>0)
      dataUpdated();
  }

  public String getDate() {
    return date;
  }

  public void setDate(String date) {
    this.date = date;
    dataUpdated();
  }

  public void setVolume(String width, String height, String length) {
    Width = width;
    Height = height;
    Length = length;
  }

  public String getCargoType() {
    return CargoType;
  }

  public void setCargoType(String cargoType) {
    CargoType = cargoType;
    if (cargoType.compareTo("cargo")==0)
    {
    }
    if (cargoType.compareTo("doc")==0)
    {
    }
    if (cargoType.compareTo("pallet")==0)
    {
    }
    if (cargoType.compareTo("wheels_car")==0)
    {
    }
    if (cargoType.compareTo("wheels_truck")==0)
    {
    }
    dataUpdated();
  }

  public String getTypeFrom() {
    return TypeFrom;
  }

  public void setTypeFrom(String typeFrom) {
    TypeFrom = typeFrom;
    dataUpdated();
  }

  public String getTypeTo() {
    return TypeTo;
  }

  public void setTypeTo(String typeTo) {
    TypeTo = typeTo;
    dataUpdated();
  }

  public void setTypeTo(int position)
  {
    if (position==0)
      setTypeTo("address");
    else
      setTypeTo("store");
  }

  public void setTypeFrom(int position)
  {
    if (position==0)
      setTypeTo("address");
    else
      setTypeTo("store");
  }

  public void setReceiverCityCode(String receiverCityCode) {
    ReceiverCityCode = receiverCityCode;
    //dataUpdated();
  }

  public void setReceiverCityName(String receiverCityName) {
    ReceiverCityName = receiverCityName;
  }

  public void setSenderCityCode(String senderCityCode) {
    SenderCityCode = senderCityCode;
    //dataUpdated();
  }

  public void setSenderCityName(String senderCityName) {
    SenderCityName = senderCityName;
  }

  public String getReceiverCityCode() {
    return ReceiverCityCode;
  }

  public String getReceiverCityName() {
    return ReceiverCityName;
  }

  public String getSenderCityCode() {
    return SenderCityCode;
  }

  public String getSenderCityName() {
    return SenderCityName;
  }

  private DeclarationModel(ICalculate calc)
  {
    if (this.calculate==null && calc!=null)
      calculate = calc;
  }

  public static DeclarationModel getInstance(ICalculate calc)
  {
    return instance==null ? instance = new DeclarationModel(calc): instance;
  }

  private void dataUpdated()
  {
    if (calculate!=null && ReceiverCityCode.length()>0 && SenderCityCode.length()>0)
    {
      calculate.doCalcRequest();
    }
  }

}
