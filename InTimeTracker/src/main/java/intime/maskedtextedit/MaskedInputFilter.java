package intime.maskedtextedit;

import android.text.InputFilter;
import android.text.Spanned;
import android.widget.EditText;

/**
 * Created by technomag on 01.09.17.
 */

public class MaskedInputFilter implements InputFilter {

  private EditText edit1;
  private char charArrayMask[];
  private char maskedText = '#';

  public MaskedInputFilter(String mask, EditText editText, char maskedText)
  {
    charArrayMask = mask.toCharArray();
    edit1 = editText;
    this.maskedText = maskedText;
  }

  @Override
  public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
    //Log.d(TAG, "Dest: " + dest.toString());
    if (source.length()>1)
      return source;

    // Insert symbol
    if (source.length()>0){

      if ( Character.isDigit(source.charAt(0))) {

        if (dstart <= charArrayMask.length - 1) {
          if (charArrayMask[dstart] == maskedText) {
            if (dstart + 1 <= charArrayMask.length - 1 && charArrayMask[dstart + 1] != maskedText)
              return source + String.valueOf(charArrayMask[dstart + 1]);
            else
              return source;
          } else {

            if (edit1.getText().length() - 1 >= dstart + 1)
              edit1.setSelection(dstart + 1);
          }
        } else
          return "";
      }

    }
    else // remove symbol
    {
      if (dstart<=charArrayMask.length-1) {
        if (charArrayMask[dstart] != maskedText) {
          edit1.setSelection(dstart);
          return String.valueOf(charArrayMask[dstart]);
        }
      }
    }

    return "";
  }
}
