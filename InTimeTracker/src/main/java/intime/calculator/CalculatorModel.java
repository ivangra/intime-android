package intime.calculator;

import android.util.Log;

import com.ua.inr0001.intime.ICalculate;

import intime.calculator.constants.GoodsType;

/**
 * Created by ILYA on 13.02.2017.
 */

public class CalculatorModel {

  private static CalculatorModel instance;

  public Declaration getDeclaration() {
    return declaration;
  }

  private Declaration declaration = new Declaration();
  private Seat seat = new Seat();

  private ICalculate calculate;

  private String SenderCityName = "";
  private String SenderCityCode = "";
  private String ReceiverCityName = "";
  private String ReceiverCityCode = "";
  private String TypeFrom = "store";
  private String TypeTo = "store";
  private String CargoType = "cargo";
  private String Weight = "0.1";
  private String Width = "1";
  private String Height = "1";
  private String Length = "1";
  private String Cost = "200";
  private String typePallets = "";
  private String Count = "1";
  private String PodSum= "0";
  private String Package = "0";
  private String Radius = "";
  private String date = "";
  private String returnDocuents = "0";
  private boolean hasStore = true;

  public boolean isHasStore() {
    return hasStore;
  }

  public void setHasStore(boolean hasStore) {
    this.hasStore = hasStore;
  }

  public String getReturnDocuents() {
    return returnDocuents;
  }

  public void setReturnDocuents(boolean returnDocuents) {
    if (returnDocuents) {
      this.returnDocuents = "1";
      declaration.setCom();
    }
    else {
      declaration.coms.clear();
      this.returnDocuents = "0";
    }
    dataUpdated();
  }

  public String getRadius() {
    return Radius;
  }

  public void setRadius(String radius) {
    Radius = radius.replaceAll("R", "");
    declaration.seats.get(0).radius_tires_drive = Radius;
    if (radius.length()>0)
      dataUpdated();
  }

  public String getPackage() {
    return Package;
  }

  public void setPackage(String aPackage) {
    Package = aPackage;
    declaration.setPackage(aPackage);
    dataUpdated();
  }

  public String getPodSum() {
    return PodSum;
  }

  public void setPodSum(String podSum) {
    if (podSum.length() == 0) {
      PodSum = "0";
      declaration.cod = "0";
    }else {
      PodSum = podSum;
      declaration.cod = podSum;
    }
    dataUpdated();
  }

  public void setPodSumWithoutEvent(String podSum)
  {
    PodSum = podSum;
    declaration.cod = podSum;
  }

  public String getCount() {
    return Count;
  }

  public void setCount(String count) {
    Count = count;
    declaration.seats.get(0).count_m = count;
    dataUpdated();
  }

  public String getTypePallets() {
    return typePallets;
  }

  public void setTypePallets(String typePallets) {
    this.typePallets = typePallets.replaceAll("x", "*");
    Log.d("Calculator", this.typePallets);
    String[] size = this.typePallets.split("\\*");
    declaration.seats.get(0).length_m = Double.toString(Integer.valueOf(size[0])/10);
    declaration.seats.get(0).width_m = Double.toString(Integer.valueOf(size[1])/10);
    dataUpdated();
  }

  public String getCost() {
    return Cost;
  }

  public void setCost(String cost) {
    setCostWithoutEvent(cost);
    dataUpdated();
  }

  public void setCostWithoutEvent(String cost)
  {
    if (cost.length() == 0) {
      declaration.cost_return = "200";
      Cost = "200";
    }
    else {
      declaration.cost_return = cost;
      Cost = cost;
    }
  }

  public String getHeight() {
    return Height;
  }

  public void setHeight(String height) {
    Height = height;
    declaration.seats.get(0).height_m = height;
    if (Width.length()>0 && Length.length()>0)
      dataUpdated();
  }

  public String getLength() {
    return Length;
  }

  public void setLength(String length) {
    Length = length;
    declaration.seats.get(0).length_m = length;
    if (Width.length()>0 && Height.length()>0)
      dataUpdated();
  }

  public String getWeight() {
    return Weight;
  }

  public void setWeight(String weight) {
    //Log.d("Test", "Weight: " + weight);
    Weight = weight.replaceAll(",", ".");
    declaration.seats.get(0).weight_m = weight;
    dataUpdated();
  }

  public String getWidth() {
    return Width;
  }

  public void setWidth(String width) {
    declaration.seats.get(0).width_m = width;
    Width = width;
    if (Length.length()>0 && Height.length()>0)
      dataUpdated();
  }

  public String getDate() {
    return date;
  }

  public void setDate(String date) {
    this.date = date;
    declaration.calculation_date = date + " 09:00:00";
    dataUpdated();
  }

  public void setVolume(String width, String height, String length) {
    Width = width;
    Height = height;
    Length = length;
  }

  public String getCargoType() {
    return CargoType;
  }

  public void setCargoType(String cargoType) {
    CargoType = cargoType;
    if (cargoType.compareTo("cargo")==0)
    {
      seat.goods_type_id = GoodsType.CARGO;
      seat.seats_num = "1";
      seat.weight_m = this.Weight;
      seat.length_m = this.Length;
      seat.width_m = this.Width;
      seat.height_m = this.Height;
      seat.count_m = this.Count;
      seat.radius_tires_drive = null;
      seat.type_tires_drive = null;
    }
    if (cargoType.compareTo("doc")==0)
    {
      seat.goods_type_id = GoodsType.DOC;
      seat.seats_num = "1";
      seat.weight_m = this.Weight;
      seat.length_m = null;
      seat.width_m = null;
      seat.height_m = null;
      seat.count_m = this.Count;
      seat.radius_tires_drive = null;
      seat.type_tires_drive = null;
    }
    if (cargoType.compareTo("pallet")==0)
    {
      seat.goods_type_id = GoodsType.PALLET;
      seat.seats_num = "1";
      seat.weight_m = this.Weight;
      seat.length_m = this.Length;
      seat.width_m = this.Width;
      seat.height_m = this.Height;
      seat.count_m = this.Count;
      seat.radius_tires_drive = null;
      seat.type_tires_drive = null;
    }
    if (cargoType.compareTo("wheels_car")==0)
    {
      seat.goods_type_id = GoodsType.WHEELS_CAR;
      seat.seats_num = "1";
      seat.weight_m = null;
      seat.length_m = null;
      seat.width_m = null;
      seat.height_m = null;
      seat.count_m = this.Count;
      seat.radius_tires_drive = Radius;
      seat.type_tires_drive = "1";
    }
    if (cargoType.compareTo("wheels_truck")==0)
    {
      seat.goods_type_id = GoodsType.WHEELS_TRUCK;
      seat.seats_num = "1";
      seat.weight_m = null;
      seat.length_m = null;
      seat.width_m = null;
      seat.height_m = null;
      seat.count_m = this.Count;
      seat.radius_tires_drive = Radius;
      seat.type_tires_drive = "2";
    }
    declaration.setSeat(seat);
    dataUpdated();
  }

  public String getTypeFrom() {
    return TypeFrom;
  }

  public void setTypeFrom(String typeFrom) {
    TypeFrom = typeFrom;
    declaration.setFormId(TypeFrom, TypeTo);
    dataUpdated();
  }

  public String getTypeTo() {
    return TypeTo;
  }

  public void setTypeTo(String typeTo) {
    TypeTo = typeTo;
    declaration.setFormId(TypeFrom, TypeTo);
    dataUpdated();
  }

  public void setTypeTo(int position)
  {
    if (position==0)
      setTypeTo("address");
    else
      setTypeTo("store");
  }

  public void setTypeFrom(int position)
  {
    if (position==0)
      setTypeTo("address");
    else
      setTypeTo("store");
  }

  public void setReceiverCityCode(String receiverCityCode, String receiverWarehouseId) {
    ReceiverCityCode = receiverCityCode;
    declaration.receiver_locality_id = receiverCityCode;
    declaration.receiver_warehouse_id = receiverWarehouseId;
    //dataUpdated();
  }

  public void setReceiverCityName(String receiverCityName) {
    ReceiverCityName = receiverCityName;
  }

  public void setSenderCityCode(String senderCityCode, String senderWarehouseId) {
    SenderCityCode = senderCityCode;
    declaration.sender_locality_id = senderCityCode;
    declaration.sender_warehouse_id = senderWarehouseId;
    //dataUpdated();
  }

  public void setSenderCityName(String senderCityName) {
    SenderCityName = senderCityName;
  }

  public String getReceiverCityCode() {
    return ReceiverCityCode;
  }

  public String getReceiverCityName() {
    return ReceiverCityName;
  }

  public String getSenderCityCode() {
    return SenderCityCode;
  }

  public String getSenderCityName() {
    return SenderCityName;
  }

  public CalculatorModel(ICalculate calc)
  {
    if (this.calculate==null && calc!=null)
      calculate = calc;
    instance = this;
  }

  public static CalculatorModel getInstance(ICalculate calc)
  {
    return instance==null ? instance = new CalculatorModel(calc): instance;
  }

  private void dataUpdated()
  {
    if (calculate!=null && ReceiverCityCode.length()>0 && SenderCityCode.length()>0)
    {
      calculate.doCalcRequest();
    }
  }

}
