package intime.calculator;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by technomag on 15.08.17.
 */

public class Declaration {
  @SerializedName("sender_locality_id")
  String sender_locality_id = "";
  @SerializedName("receiver_locality_id")
  String receiver_locality_id = "";
  @SerializedName("sender_warehouse_id")
  String sender_warehouse_id = "";
  @SerializedName("receiver_warehouse_id")
  String receiver_warehouse_id = "";
  @SerializedName("calculation_date")
  String calculation_date = "";
  @SerializedName("cost_return")
  String cost_return = "200";
  @SerializedName("seats_num")
  String seats_num = "1";
  @SerializedName("cod")
  String cod = "0";
  @SerializedName("seats")
  public List<Seat> seats = new ArrayList<Seat>();
  @SerializedName("servs")
  List<Serv> servs = new ArrayList<Serv>();
  @SerializedName("coms")
  List<Com> coms = new ArrayList<Com>();
  @SerializedName("boxes")
  List<Box> boxes = new ArrayList<Box>();
  @SerializedName("form_id")
  String form_id = "1"; // store - store


  public void setSeat(Seat seat)
  {
    seats.clear();
    seats.add(seat);
  }

  public void setPackage(String id)
  {
    Box box = new Box();
    box.box_id = id;
    box.box_count = "1";
    boxes.clear();
    boxes.add(box);
  }

  public void setCom()
  {
    Com com = new Com();
    com.com_id = "66"; //"51"; oldValue
    com.com_val = "1";
    coms.clear();
    coms.add(com);
  }

  public void setFormId(String typeFrom, String typeTo)
  {
    if (typeFrom.compareTo("store")==0 && typeTo.compareTo("store")==0)
      form_id = "1";
    else if (typeFrom.compareTo("store")==0 && typeTo.compareTo("address")==0)
      form_id = "2";
    else if (typeFrom.compareTo("store")==0 && typeTo.compareTo("postmat")==0)
      form_id = "3";
    else if (typeFrom.compareTo("address")==0 && typeTo.compareTo("store")==0)
      form_id = "4";
    else if (typeFrom.compareTo("address")==0 && typeTo.compareTo("address")==0)
      form_id = "5";
    else if (typeFrom.compareTo("address")==0 && typeTo.compareTo("postmat")==0)
      form_id = "6";
//    //if ($params['type_from'] == 'store' && $params['type_to'] == 'store') {
//    //            $data["FORM_ID"] = "1";        }
//    // elseif ($params['type_from'] == 'store' && $params['type_to'] == 'address') {
//    //            $data["FORM_ID"] = "2";        }
//    // elseif ($params['type_from'] == 'store' && $params['type_to'] == 'postmat') {
//    //            $data["FORM_ID"] = "3";        }
//    // elseif ($params['type_from'] == 'address' && $params['type_to'] == 'store') {
//    //            $data["FORM_ID"] = "4";        }
//    // elseif ($params['type_from'] == 'address' && $params['type_to'] == 'address') {
//    //            $data["FORM_ID"] = "5";        }
//    // elseif ($params['type_from'] == 'address' && $params['type_to'] == 'postmat') {
//    //            $data["FORM_ID"] = "6";        }
  }

}
