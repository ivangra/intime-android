package intime.calculator.constants;

public class GoodsType {

  public static final String CARGO = "1";
  public static final String DOC = "4";
  public static final String PALLET = "3";
  public static final String WHEELS_CAR = "2";
  public static final String WHEELS_TRUCK = "2";

}
