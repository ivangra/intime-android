package intime.calculator.constants;

public class CargoType {

  public static final int CARGO = 0;
  public static final int DOC = 1;
  public static final int PALLET = 2;
  public static final int WHEEL = 3;

}
