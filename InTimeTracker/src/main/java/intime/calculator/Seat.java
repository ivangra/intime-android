package intime.calculator;

import com.google.gson.annotations.SerializedName;

/**
 * Created by technomag on 15.08.17.
 */

public class Seat {
  @SerializedName("goods_type_id")
  String goods_type_id = "";
  @SerializedName("seats_num")
  String seats_num = "";
  @SerializedName("weight_m")
  public String weight_m = "";
  @SerializedName("length_m")
  public String length_m = "";
  @SerializedName("width_m")
  public String width_m = "";
  @SerializedName("height_m")
  public String height_m = "";
  @SerializedName("count_m")
  String count_m = "1";
  @SerializedName("type_tires_drive")
  String type_tires_drive = "";
  @SerializedName("radius_tires_drive")
  String radius_tires_drive = "";

}
