package intime.calculator;

import com.google.gson.annotations.SerializedName;

/**
 * Created by technomag on 15.08.17.
 */

public class DeclarationWrapper {

  @SerializedName("declaration")
  public Declaration declaration;

  public DeclarationWrapper(Declaration declaration)
  {
    this.declaration = declaration;
  }

}
