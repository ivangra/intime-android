package intime.llc.ua;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.ua.inr0001.intime.DoneTaskCallBack;
import com.ua.inr0001.intime.GlobalApplicationData;
import com.ua.inr0001.intime.IProgressUpdate;
import com.ua.inr0001.intime.IRtfAPI;
import com.ua.inr0001.intime.UpdateDbTask;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class SplashScreenActivity extends Activity implements DoneTaskCallBack, IProgressUpdate {

  private UpdateDbTask dbTask;
  private Context mContext;
  private Call<ResponseBody> callHtml = null;
  private ImageView imgSpalshLogo;

  public void done()
  {
    startActivity(new Intent("com.ua.inr0001.intime.CLEARSCREEN"));
    finish();
  }

  @Override
  public void update(final String... values) {

  }

  @Override
  public void setMaxProgress(int max) {

  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.splash_activity);
    imgSpalshLogo = (ImageView) findViewById(R.id.imgSpalshLogo);
    final Animation animAccelerateDecelerate = AnimationUtils.loadAnimation(this, R.anim.scale_splash_logo);
    imgSpalshLogo.startAnimation(animAccelerateDecelerate);
    mContext = this;
  }

  private void doUpdate(boolean onlyUpdate)
  {
    dbTask = UpdateDbTask.getInstance(this, this, this, onlyUpdate);
    if (!dbTask.getStatus().equals(AsyncTask.Status.RUNNING))
      dbTask.execute();
  }

  @Override
  protected void onResume() {
    super.onResume();
//    if (DictionariesDB.getInstance(mContext).isEmptyDB() && GlobalApplicationData.getInstance().IsOnline())
//      doUpdate(false);
//    else
    if (GlobalApplicationData.getInstance().isUpdateDB() && GlobalApplicationData.getInstance().IsOnline())
      doUpdate(false);
    else
//    if (GlobalApplicationData.getInstance().isLocaleChanged() && GlobalApplicationData.getInstance().IsOnline()) {
//      GlobalApplicationData.getInstance().saveLocale();
//      doUpdate(true);
//    }
//    else
      pauseStart();
  }

  private void pauseStart()
  {
        Thread logoTimer = new Thread()
        {
            public void run()
            {
                try
                {
                    int logoTimer = 0;
                    while(logoTimer < 2000)
                    {
                        sleep(100);
                        logoTimer = logoTimer + 100;
                    };
                    startActivity(new Intent("com.ua.inr0001.intime.CLEARSCREEN"));
                }
                catch (InterruptedException e) { }
                finally
                {
                    finish();
                }
            }
        };
        logoTimer.start();
  }

}
