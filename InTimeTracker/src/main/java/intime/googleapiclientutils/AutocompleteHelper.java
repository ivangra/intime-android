package intime.googleapiclientutils;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.data.DataBufferUtils;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.AutocompletePredictionBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 * Created by technomag on 01.02.18.
 */

public class AutocompleteHelper {

  private GoogleApiClient mGoogleApiClient;
  private LatLngBounds mBounds;
  private AutocompleteFilter mPlaceFilter;

  public AutocompleteHelper(GoogleApiClient mGoogleApiClient)
  {
    this.mGoogleApiClient = mGoogleApiClient;
    mBounds = new LatLngBounds(new LatLng(-34.041458, 150.790100), new LatLng(-33.682247, 151.383362));
    mPlaceFilter = new AutocompleteFilter.Builder()
      .setTypeFilter(AutocompleteFilter.TYPE_FILTER_ADDRESS | AutocompleteFilter.TYPE_FILTER_GEOCODE)
      .build();
  }

  public ArrayList<AutocompletePrediction> getAutocomplete(String constraint) {
    if (mGoogleApiClient!= null && mGoogleApiClient.isConnected()) {
      PendingResult<AutocompletePredictionBuffer> results =
        Places.GeoDataApi
          .getAutocompletePredictions(mGoogleApiClient, constraint,
            mBounds, mPlaceFilter);

      AutocompletePredictionBuffer autocompletePredictions = results
        .await(60, TimeUnit.SECONDS);

      final com.google.android.gms.common.api.Status status = autocompletePredictions.getStatus();
      if (!status.isSuccess()) {
        autocompletePredictions.release();
        return null;
      }
      return DataBufferUtils.freezeAndClose(autocompletePredictions);
    }
    return null;
  }

}
