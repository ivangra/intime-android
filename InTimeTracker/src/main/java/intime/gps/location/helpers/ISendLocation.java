package intime.gps.location.helpers;

import android.location.Location;

/**
 * Created by ILYA on 06.10.2016.
 */

public interface ISendLocation {

  void sendLocation(Location location);

}
