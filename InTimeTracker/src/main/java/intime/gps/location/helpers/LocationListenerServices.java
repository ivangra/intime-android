package intime.gps.location.helpers;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.ua.inr0001.intime.GlobalApplicationData;

/**
 * Created by ILYA on 06.10.2016.
 */

public class LocationListenerServices implements LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener{

  private LocationRequest mLocationRequest;
  private GoogleApiClient googleApiClient;
  private ISendLocation sendLocation;

  private static volatile LocationListenerServices instance;

  public LocationListenerServices(Context context, ISendLocation sendLocation)
  {
    this.sendLocation = sendLocation;
    LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
    boolean wifi = false;
    boolean gps = false;
    if (locationManager!= null) {
      wifi = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
      gps = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }
    if (wifi || gps)
       new OldLocationListenerServices(locationManager, wifi, gps, this.sendLocation);
    else {
      googleApiClient = new GoogleApiClient.Builder(context)
        .addConnectionCallbacks(this)
        .addOnConnectionFailedListener(this)
        .addApi(LocationServices.API)
        .build();
      mLocationRequest = LocationRequest.create();
      mLocationRequest.setInterval(1000);
      mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
      mLocationRequest.setFastestInterval(1000);
      connectClient();
    }
  }

  private void connectClient()
  {
    if (!googleApiClient.isConnected())
      googleApiClient.connect();
  }

  @Override
  public void onLocationChanged(Location location) {
    if (sendLocation != null)
      sendLocation.sendLocation(location);
    googleApiClient.unregisterConnectionCallbacks(this);
    googleApiClient.disconnect();
  }

  @Override
  public void onConnected(@Nullable Bundle bundle) {
    LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, mLocationRequest, this);
  }

  @Override
  public void onConnectionSuspended(int i) {
    Log.d("Test", "Location listener suspended");
  }

  @Override
  public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    Log.d("Test", "Location connection failed");
  }
}
