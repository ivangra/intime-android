package intime.gps.location.helpers;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.ua.inr0001.intime.GlobalApplicationData;
import com.ua.inr0001.intime.InTimeTrackerApp;

import java.util.Calendar;

public class OldLocationListenerServices implements LocationListener {

  private ISendLocation sendLocation;
  private LocationManager locationManager;
  private boolean wifi;
  private boolean gps;

  public OldLocationListenerServices(LocationManager locationManager, boolean wifi, boolean gps, ISendLocation sendLocation) {
    this.sendLocation = sendLocation;
    this.locationManager = locationManager;
    this.wifi = wifi;
    this.gps = gps;
    start();
  }

  private void start() {
    Calendar calendar = Calendar.getInstance();
    long current = calendar.getTimeInMillis() / 1000;
    long between = Math.abs(GlobalApplicationData.getInstance().lastLocatinGetTime - current);

    if (
            ActivityCompat.checkSelfPermission(InTimeTrackerApp.getAppContext(),
                    Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(InTimeTrackerApp.getAppContext(),
                    Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
        return;


      //    ActivityCompat#requestPermissions
      // here to request the missing permissions, and then overriding
      //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
      //                                          int[] grantResults)
      // to handle the case where the user grants the permission. See the documentation
      // for ActivityCompat#requestPermissions for more details.
    }

    if (wifi) {
      if (between < 10 && GlobalApplicationData.getInstance().lastLocatinGetTime != 0) {
        Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        if (sendLocation != null && location != null)
          sendLocation.sendLocation(location);
        else
          locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 1, this);
      }
      else
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 1, this);
    }
    else if (gps) {
      if (between < 10 && GlobalApplicationData.getInstance().lastLocatinGetTime != 0) {
        Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if (sendLocation != null && location != null)
          sendLocation.sendLocation(location);
        else
          locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 1, this);
      }
      else
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 1, this);
    }
  }

  @Override
  public void onLocationChanged(Location location) {
    if (sendLocation != null)
      sendLocation.sendLocation(location);
    locationManager.removeUpdates(this);
    Calendar calendar = Calendar.getInstance();
    GlobalApplicationData.getInstance().lastLocatinGetTime = calendar.getTimeInMillis() / 1000;
  }

  @Override
  public void onStatusChanged(String s, int i, Bundle bundle) {

  }

  @Override
  public void onProviderEnabled(String s) {
    Log.d("MapDeps", "Provider enabled: " + s);
  }

  @Override
  public void onProviderDisabled(String s) {
    Log.d("MapDeps", "Provider disabled: " + s);
  }
}
