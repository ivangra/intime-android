package intime.api.model.calculator;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ILYA on 16.02.2017.
 */

public class CalculatorDeserializer implements JsonDeserializer<Calculator> {
  @Override
  public Calculator deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
    Calculator calculator = new Calculator();

    JsonObject jsonObject = json.getAsJsonObject();

    // Read simple String values.
    boolean error = jsonObject.get("error").getAsBoolean();
    calculator.setError(error);

    if (error)
      calculator.setErrorMessage(jsonObject.get("error_message").getAsString());
    else
    {
      calculator.setParameters(readParametersMap(jsonObject));
    }

    return calculator;
  }

  @Nullable
  private Map<String, String> readParametersMap(@NonNull final JsonObject jsonObject) {
    intime.api.model.calculator.Data data = new intime.api.model.calculator.Data();
    JsonElement paramsElement = jsonObject.get("data");
    if (paramsElement == null) {
      // value not present at all, just return null
      return null;
    }

    JsonObject parametersObject = paramsElement.getAsJsonObject();
    Map<String, String> parameters = new HashMap<>();
    for (Map.Entry<String, JsonElement> entry : parametersObject.entrySet()) {
      String key = entry.getKey();
      String value = "";
      try{
        value = entry.getValue().getAsString();
      }catch(Exception e) {};
      parameters.put(key, value);
    }
    return parameters;
  }

}
