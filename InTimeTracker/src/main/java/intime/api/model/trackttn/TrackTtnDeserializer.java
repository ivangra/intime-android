package intime.api.model.trackttn;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.Map;

/**
 * Created by ILYA on 10.02.2017.
 */

public class TrackTtnDeserializer implements JsonDeserializer<TrackTtn> {

  @Override
  public TrackTtn deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
    TrackTtn trackTtn = new TrackTtn();

    JsonObject jsonObject = json.getAsJsonObject();

    // Read simple String values.
    boolean error = jsonObject.get("error").getAsBoolean();
    trackTtn.setError(error);

    if (error)
      trackTtn.setErrorMessage(jsonObject.get("error_message").getAsString());
    else
    {
      Data data = readParametersMap(jsonObject);
      trackTtn.setData(data);
    }

    return trackTtn;
  }

  @Nullable
  private Data readParametersMap(@NonNull final JsonObject jsonObject) {
    Data data = new Data();
    JsonElement paramsElement = jsonObject.get("data");
    if (paramsElement == null) {
      // value not present at all, just return null
      return data;
    }

    JsonObject parametersObject = paramsElement.getAsJsonObject();
    for (Map.Entry<String, JsonElement> entry : parametersObject.entrySet()) {
      String key = entry.getKey();

      if (key!=null && key.compareTo("number")==0 && !entry.getValue().isJsonNull())
        data.setNumber(entry.getValue().getAsString());

      if (key!=null && key.compareTo("number2")==0 && !entry.getValue().isJsonNull())
        data.setNumber2(entry.getValue().getAsString());

      if (key!=null && key.compareTo("status")==0 && !entry.getValue().isJsonNull())
        data.setStatus(entry.getValue().getAsString());

      if (key!=null && key.compareTo("date")==0 && !entry.getValue().isJsonNull())
        data.setDate(entry.getValue().getAsString());

      if (key!=null && key.compareTo("type")==0 && !entry.getValue().isJsonNull())
        data.setType(entry.getValue().getAsString());

      if (key!=null && key.compareTo("description")==0 && !entry.getValue().isJsonNull())
        data.setDescription(entry.getValue().getAsString());

      if (key!=null && key.compareTo("from")==0 && !entry.getValue().isJsonNull())
        setFrom(data, entry.getValue().getAsJsonObject());

      if (key!=null && key.compareTo("to")==0 && !entry.getValue().isJsonNull())
        setTo(data, entry.getValue().getAsJsonObject());

      if (key!=null && key.compareTo("date_creation")==0 && !entry.getValue().isJsonNull())
        setDateCreation(data, entry.getValue().getAsJsonObject());

      if (key!=null && key.compareTo("date_delivery")==0 && !entry.getValue().isJsonNull())
        setDateDelivery(data, entry.getValue().getAsJsonObject());

      if (key!=null && key.compareTo("status_id")==0 && !entry.getValue().isJsonNull())
        data.setStatusId(entry.getValue().getAsString());

      if (key!=null && key.compareTo("type_id")==0 && !entry.getValue().isJsonNull())
        data.setTypeId(entry.getValue().getAsString());

      if (key!=null && key.compareTo("cargo_type")==0 && !entry.getValue().isJsonNull())
        data.setCargoType(entry.getValue().getAsString());

      if (key!=null && key.compareTo("cost")==0 && !entry.getValue().isJsonNull())
        data.setCost(entry.getValue().getAsString());

      if (key!=null && key.compareTo("to_pay")==0 && !entry.getValue().isJsonNull())
        data.setToPay(entry.getValue().getAsString());

      if (key!=null && key.compareTo("cash_on_delivery")==0 && !entry.getValue().isJsonNull())
        data.setCashOnDelivery(entry.getValue().getAsString());

      if (key!=null && key.compareTo("cost_return")==0 && !entry.getValue().isJsonNull())
        data.setCostReturn(entry.getValue().getAsString());

      if (key!=null && key.compareTo("weight")==0 && !entry.getValue().isJsonNull())
        data.setWeight(entry.getValue().getAsString());

      if (key!=null && key.compareTo("volume")==0 && !entry.getValue().isJsonNull())
        data.setVolume(entry.getValue().getAsString());

      if (key!=null && key.compareTo("seats")==0 && !entry.getValue().isJsonNull())
        data.setSeats(entry.getValue().getAsString());

      if (key!=null && key.compareTo("payer")==0 && !entry.getValue().isJsonNull())
        data.setPayer(entry.getValue().getAsString());
    }
    return data;
  }

  private void setFrom(Data data, JsonObject paramsElement)
  {
    From from = new From();
    for (Map.Entry<String, JsonElement> entry : paramsElement.entrySet()) {
      String key = entry.getKey();

      if (key != null && key.compareTo("city") == 0 && !entry.getValue().isJsonNull())
        from.setCity(entry.getValue().getAsString());
      if (key != null && key.compareTo("store_code") == 0 && !entry.getValue().isJsonNull())
        from.setStoreCode(entry.getValue().getAsString());
    }
    data.setFrom(from);
  }

  private void setTo(Data data, JsonObject paramsElement)
  {
    To to = new To();
    for (Map.Entry<String, JsonElement> entry : paramsElement.entrySet()) {
      String key = entry.getKey();

      if (key != null && key.compareTo("city") == 0 && !entry.getValue().isJsonNull())
        to.setCity(entry.getValue().getAsString());
      if (key != null && key.compareTo("store_code") == 0 && !entry.getValue().isJsonNull())
        to.setStoreCode(entry.getValue().getAsString());
      if (key != null && key.compareTo("store") == 0 && !entry.getValue().isJsonNull() && entry.getValue().isJsonObject())
        setStore(to, entry.getValue().getAsJsonObject());
      else
        setStore(to);
    }
    data.setTo(to);
  }

  private void setDateCreation(Data data, JsonObject paramsElement)
  {
    DateCreation dateCreation = new DateCreation();
    for (Map.Entry<String, JsonElement> entry : paramsElement.entrySet()) {
      String key = entry.getKey();

      if (key != null && key.compareTo("date") == 0 && !entry.getValue().isJsonNull())
        dateCreation.setDate(entry.getValue().getAsString());
    }
    data.setDateCreation(dateCreation);
  }

  private void setDateDelivery(Data data, JsonObject paramsElement)
  {
    DateDelivery dateDelivery = new DateDelivery();
    for (Map.Entry<String, JsonElement> entry : paramsElement.entrySet()) {
      String key = entry.getKey();

      if (key != null && key.compareTo("date") == 0 && !entry.getValue().isJsonNull())
        dateDelivery.setDate(entry.getValue().getAsString());
    }
    data.setDateDelivery(dateDelivery);
  }

  private void setStore(To to)
  {
    Store store = new Store();
    to.setStore(store);
  }

  private void setStore(To to, JsonObject paramsElement)
  {
    Store store = new Store();
    for (Map.Entry<String, JsonElement> entry : paramsElement.entrySet()) {
      String key = entry.getKey();

      if (key != null && key.compareTo("type") == 0 && !entry.getValue().isJsonNull())
        store.setType(entry.getValue().getAsString());

      if (key != null && key.compareTo("address") == 0 && !entry.getValue().isJsonNull())
        store.setAddress(entry.getValue().getAsString());

      if (key != null && key.compareTo("url") == 0 && !entry.getValue().isJsonNull())
        store.setUrl(entry.getValue().getAsString());

      if (key != null && key.compareTo("number") == 0 && !entry.getValue().isJsonNull())
        store.setNumber(entry.getValue().getAsInt());
    }
    to.setStore(store);
  }

}
