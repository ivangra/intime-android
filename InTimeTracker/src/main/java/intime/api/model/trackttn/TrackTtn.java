
package intime.api.model.trackttn;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TrackTtn {

    @SerializedName("error")
    @Expose
    private boolean error;
    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("error_message")
    @Expose
    private String errorMessage;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

}
