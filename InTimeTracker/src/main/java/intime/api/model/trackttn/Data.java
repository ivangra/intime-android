
package intime.api.model.trackttn;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("number")
    @Expose
    private String number;
    @SerializedName("number2")
    @Expose
    private Object number2;
    @SerializedName("status_id")
    @Expose
    private String statusId;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("date_creation")
    @Expose
    private DateCreation dateCreation;
    @SerializedName("date_delivery")
    @Expose
    private DateDelivery dateDelivery;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("type_id")
    @Expose
    private String typeId;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("from")
    @Expose
    private From from;
    @SerializedName("to")
    @Expose
    private To to;
    @SerializedName("cargo_type")
    @Expose
    private String cargoType;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("cost")
    @Expose
    private String cost;
    @SerializedName("cost_return")
    @Expose
    private String costReturn;
    @SerializedName("to_pay")
    @Expose
    private String toPay;
    @SerializedName("cash_on_delivery")
    @Expose
    private String cashOnDelivery;
    @SerializedName("weight")
    @Expose
    private String weight;
    @SerializedName("volume")
    @Expose
    private String volume;
    @SerializedName("seats")
    @Expose
    private String seats;
    @SerializedName("payer")
    @Expose
    private String payer;

    public Data()
    {
        number = "";
        number2 = "";
        status = "";
        type = "";
        description = "";
        seats = "";
        payer = "";
        volume = "";
        toPay = "";
        cost = "";
        weight = "";
        cargoType = "";
        typeId = "";
        statusId = "";
        cashOnDelivery = "";
        costReturn = "";
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Object getNumber2() {
        return number2;
    }

    public void setNumber2(Object number2) {
        this.number2 = number2;
    }

    public String getStatusId() {
        return statusId;
    }

    public void setStatusId(String statusId) {
        this.statusId = statusId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public DateCreation getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(DateCreation dateCreation) {
        this.dateCreation = dateCreation;
    }

    public DateDelivery getDateDelivery() {
        return dateDelivery;
    }

    public void setDateDelivery(DateDelivery dateDelivery) {
        this.dateDelivery = dateDelivery;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public From getFrom() {
        return from;
    }

    public void setFrom(From from) {
        this.from = from;
    }

    public To getTo() {
        return to;
    }

    public void setTo(To to) {
        this.to = to;
    }

    public String getCargoType() {
        return cargoType;
    }

    public void setCargoType(String cargoType) {
        this.cargoType = cargoType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getToPay() {
        return toPay;
    }

    public void setToPay(String toPay) {
        this.toPay = toPay;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public String getSeats() {
        return seats;
    }

    public void setSeats(String seats) {
        this.seats = seats;
    }

    public String getPayer() {
        return payer;
    }

    public void setPayer(String payer) {
        this.payer = payer;
    }

    public String getCostReturn() {
        return costReturn;
    }

    public void setCostReturn(String costReturn) {
        this.costReturn = costReturn;
    }

    public String getCashOnDelivery() {
        return cashOnDelivery;
    }

    public void setCashOnDelivery(String cashOnDelivery) {
        this.cashOnDelivery = cashOnDelivery;
    }

}
