
package intime.api.model.trackttn;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class From {

    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("store_code")
    @Expose
    private String storeCode;

    public From()
    {
        city = "";
        storeCode = "";
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStoreCode() {
        return storeCode;
    }

    public void setStoreCode(String storeCode) {
        this.storeCode = storeCode;
    }

}
