
package intime.api.model.auth;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EntriesAuth {

    @SerializedName("Entry_auth")
    @Expose
    private List<EntryAuth> entryAuth = null;

    public List<EntryAuth> getEntryAuth() {
        return entryAuth;
    }

    public void setEntryAuth(List<EntryAuth> entryAuth) {
        this.entryAuth = entryAuth;
    }

}
