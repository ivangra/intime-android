
package intime.api.model.auth;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Auth {

    @SerializedName("Entries_auth")
    @Expose
    private EntriesAuth entriesAuth;

    public EntriesAuth getEntriesAuth() {
        return entriesAuth;
    }

    public void setEntriesAuth(EntriesAuth entriesAuth) {
        this.entriesAuth = entriesAuth;
    }

}
