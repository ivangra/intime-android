
package intime.api.model.registeruser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RegisterUser {

    @SerializedName("_post_add_user_rest")
    @Expose
    private PostAddUserRest postAddUserRest;

    public PostAddUserRest getPostAddUserRest() {
        return postAddUserRest;
    }

    public void setPostAddUserRest(PostAddUserRest postAddUserRest) {
        this.postAddUserRest = postAddUserRest;
    }

}
