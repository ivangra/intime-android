
package intime.api.model.departments;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DayInDay {

    @SerializedName("1")
    @Expose
    private String day_in1;
    @SerializedName("2")
    @Expose
    private String day_in2;
    @SerializedName("3")
    @Expose
    private String day_in3;
    @SerializedName("4")
    @Expose
    private String day_in4;
    @SerializedName("5")
    @Expose
    private String day_in5;
    @SerializedName("6")
    @Expose
    private String day_in6;
    @SerializedName("7")
    @Expose
    private String day_in7;

    public String get1() {
        return day_in1;
    }

    public void set1(String _1) {
        this.day_in1 = _1;
    }

    public String get2() {
        return day_in2;
    }

    public void set2(String _2) {
        this.day_in2 = _2;
    }

    public String get3() {
        return day_in3;
    }

    public void set3(String _3) {
        this.day_in3 = _3;
    }

    public String get4() {
        return day_in4;
    }

    public void set4(String _4) {
        this.day_in4 = _4;
    }

    public String get5() {
        return day_in5;
    }

    public void set5(String _5) {
        this.day_in5 = _5;
    }

    public String get6() {
        return day_in6;
    }

    public void set6(String _6) {
        this.day_in6 = _6;
    }

    public String get7() {
        return day_in7;
    }

    public void set7(String _7) {
        this.day_in7 = _7;
    }

}
