
package intime.api.model.departments;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Datum {

    @SerializedName("code")
    @Expose
    private int code;
    @SerializedName("lat")
    @Expose
    private String lat;
    @SerializedName("lon")
    @Expose
    private String lon;
    @SerializedName("type")
    @Expose
    private String type;

    public int getType_id() {
        return type_id;
    }

    public void setType_id(int type_id) {
        this.type_id = type_id;
    }

    @SerializedName("type_id")
    @Expose
    private int type_id;
    @SerializedName("number")
    @Expose
    private int number;
    @SerializedName("max_length")
    @Expose
    private String max_length;
    @SerializedName("city_type")
    @Expose
    private int citytype;
    @SerializedName("capacity")
    @Expose
    private String capacity;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("district")
    @Expose
    private String district;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("schedule_d1_o")
    @Expose
    private String scheduleD1O;
    @SerializedName("schedule_d1_c")
    @Expose
    private String scheduleD1C;
    @SerializedName("schedule_d2_o")
    @Expose
    private String scheduleD2O;
    @SerializedName("schedule_d2_c")
    @Expose
    private String scheduleD2C;
    @SerializedName("schedule_d3_o")
    @Expose
    private String scheduleD3O;
    @SerializedName("schedule_d3_c")
    @Expose
    private String scheduleD3C;
    @SerializedName("schedule_d4_o")
    @Expose
    private String scheduleD4O;
    @SerializedName("schedule_d4_c")
    @Expose
    private String scheduleD4C;
    @SerializedName("schedule_d5_o")
    @Expose
    private String scheduleD5O;
    @SerializedName("schedule_d5_c")
    @Expose
    private String scheduleD5C;
    @SerializedName("schedule_d6_o")
    @Expose
    private String scheduleD6O;
    @SerializedName("schedule_d6_c")
    @Expose
    private String scheduleD6C;
    @SerializedName("schedule_d0_o")
    @Expose
    private String scheduleD7O;
    @SerializedName("schedule_d0_c")
    @Expose
    private String scheduleD7C;
    @SerializedName("phones")
    @Expose
    private List<String> phones = null;
    @SerializedName("day_in_day")
    @Expose
    private DayInDay dayInDay;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getCityType() {
        return citytype;
    }

    public void setCityType(int city_type) {
        this.citytype = city_type;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getCapacity() {
        return capacity;
    }

    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getScheduleD1O() {
        return scheduleD1O;
    }

    public void setScheduleD1O(String scheduleD1O) {
        this.scheduleD1O = scheduleD1O;
    }

    public String getScheduleD1C() {
        return scheduleD1C;
    }

    public void setScheduleD1C(String scheduleD1C) {
        this.scheduleD1C = scheduleD1C;
    }

    public String getScheduleD2O() {
        return scheduleD2O;
    }

    public void setScheduleD2O(String scheduleD2O) {
        this.scheduleD2O = scheduleD2O;
    }

    public String getScheduleD2C() {
        return scheduleD2C;
    }

    public void setScheduleD2C(String scheduleD2C) {
        this.scheduleD2C = scheduleD2C;
    }

    public String getScheduleD3O() {
        return scheduleD3O;
    }

    public void setScheduleD3O(String scheduleD3O) {
        this.scheduleD3O = scheduleD3O;
    }

    public String getScheduleD3C() {
        return scheduleD3C;
    }

    public void setScheduleD3C(String scheduleD3C) {
        this.scheduleD3C = scheduleD3C;
    }

    public String getScheduleD4O() {
        return scheduleD4O;
    }

    public void setScheduleD4O(String scheduleD4O) {
        this.scheduleD4O = scheduleD4O;
    }

    public String getScheduleD4C() {
        return scheduleD4C;
    }

    public void setScheduleD4C(String scheduleD4C) {
        this.scheduleD4C = scheduleD4C;
    }

    public String getScheduleD5O() {
        return scheduleD5O;
    }

    public void setScheduleD5O(String scheduleD5O) {
        this.scheduleD5O = scheduleD5O;
    }

    public String getScheduleD5C() {
        return scheduleD5C;
    }

    public void setScheduleD5C(String scheduleD5C) {
        this.scheduleD5C = scheduleD5C;
    }

    public String getScheduleD6O() {
        return scheduleD6O;
    }

    public void setScheduleD6O(String scheduleD6O) {
        this.scheduleD6O = scheduleD6O;
    }

    public String getScheduleD6C() {
        return scheduleD6C;
    }

    public void setScheduleD6C(String scheduleD6C) {
        this.scheduleD6C = scheduleD6C;
    }

    public String getScheduleD7O() {
        return scheduleD7O;
    }

    public void setScheduleD7O(String scheduleD7O) {
        this.scheduleD7O = scheduleD7O;
    }

    public String getScheduleD7C() {
        return scheduleD7C;
    }

    public void setScheduleD7C(String scheduleD7C) {
        this.scheduleD7C = scheduleD7C;
    }

    public DayInDay getDayInDay() {
        return dayInDay;
    }

    public void setDayInDay(DayInDay dayInDay) {
        this.dayInDay = dayInDay;
    }

    public List<String> getPhones() {
        return phones;
    }

    public void setPhones(List<String> phones) {
        this.phones = phones;
    }

    public String getPhonesString()
    {
        StringBuilder result = new StringBuilder();
        for (String phone : this.phones)
        {
           if (phone.length()>0) {
               result.append(phone);
               result.append(";");
           }
        }
        return result.toString();
    }

    public String getMax_length() {
        return max_length;
    }

    public void setMax_length(String max_length) {
        this.max_length = max_length;
    }

}
