
package intime.api.model.userinfo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserInfo {

    @SerializedName("Entries_get_people_by_phone")
    @Expose
    private EntriesGetPeopleByPhone entriesGetPeopleByPhone;

    public EntriesGetPeopleByPhone getEntriesGetPeopleByPhone() {
        return entriesGetPeopleByPhone;
    }

    public void setEntriesGetPeopleByPhone(EntriesGetPeopleByPhone entriesGetPeopleByPhone) {
        this.entriesGetPeopleByPhone = entriesGetPeopleByPhone;
    }

}
