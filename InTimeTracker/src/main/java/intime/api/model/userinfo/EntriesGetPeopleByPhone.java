
package intime.api.model.userinfo;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EntriesGetPeopleByPhone {

    @SerializedName("Entry_get_people_by_phone")
    @Expose
    private List<PersonInfo> entryGetPeopleByPhone = null;

    public List<PersonInfo> getEntryGetPeopleByPhone() {
        return entryGetPeopleByPhone;
    }

    public void setEntryGetPeopleByPhone(List<PersonInfo> entryGetPeopleByPhone) {
        this.entryGetPeopleByPhone = entryGetPeopleByPhone;
    }

}
