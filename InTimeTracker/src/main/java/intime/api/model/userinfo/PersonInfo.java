
package intime.api.model.userinfo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PersonInfo {

    @SerializedName("Id")
    @Expose
    private String id = "";
    @SerializedName("Surname")
    @Expose
    private String surname = "";
    @SerializedName("Name")
    @Expose
    private String name = "";
    @SerializedName("Patronymic")
    @Expose
    private String patronymic = "";
    @SerializedName("Tax_Number")
    @Expose
    private String taxNumber = "";
    @SerializedName("Phone_Numbers")
    @Expose
    private String phoneNumbers = "";
    @SerializedName("Emails")
    @Expose
    private String emails = "";
    @SerializedName("Api_Key")
    @Expose
    private String apiKey = "";
    @SerializedName("Status")
    @Expose
    private String status = "";
    @SerializedName("Last_Change")
    @Expose
    private String lastChange = "";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getTaxNumber() {
        return taxNumber;
    }

    public void setTaxNumber(String taxNumber) {
        this.taxNumber = taxNumber;
    }

    public String getPhoneNumbers() {
        return phoneNumbers;
    }

    public void setPhoneNumbers(String phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }

    public String getEmails() {
        return emails;
    }

    public void setEmails(String emails) {
        this.emails = emails;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLastChange() {
        return lastChange;
    }

    public void setLastChange(String lastChange) {
        this.lastChange = lastChange;
    }

}
