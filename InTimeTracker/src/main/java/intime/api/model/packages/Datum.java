
package intime.api.model.packages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Datum {

    public Datum()
    {
        name = "";
    }

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("order")
    @Expose
    private int order;
    @SerializedName("sizes")
    @Expose
    private List<Size> sizes = null;
    @SerializedName("cargo_types")
    @Expose
    private List<String> cargoTypes = null;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public List<Size> getSizes() {
        return sizes;
    }

    public void setSizes(List<Size> sizes) {
        this.sizes = sizes;
    }

    public List<String> getCargoTypes() {
        return cargoTypes;
    }

    public void setCargoTypes(List<String> cargoTypes) {
        this.cargoTypes = cargoTypes;
    }

}
