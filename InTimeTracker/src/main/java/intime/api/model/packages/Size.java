
package intime.api.model.packages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Size {

    public Size()
    {
        code = "";
        depth = "";
        price = "";
        width = "";
        height = "";
        weight_max = "";
        weight_min = "";
    }

    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("depth")
    @Expose
    private String depth;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("width")
    @Expose
    private String width;
    @SerializedName("height")
    @Expose
    private String height;
    @SerializedName("weight_max")
    @Expose
    private String weight_max;
    @SerializedName("weight_min")
    @Expose
    private String weight_min;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDepth() {
        return depth;
    }

    public void setDepth(String depth) {
        this.depth = depth;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWeight_max() {
        return weight_max;
    }

    public void setWeight_max(String weight_max) {
        this.weight_max = weight_max;
    }

    public String getWeight_min() {
        return weight_min;
    }

    public void setWeight_min(String weight_min) {
        this.weight_min = weight_min;
    }
}
