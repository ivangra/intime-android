
package intime.api.model.recovery;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Recovery {

    @SerializedName("Entries_change_password")
    @Expose
    private EntriesChangePassword entriesChangePassword;

    public EntriesChangePassword getEntriesChangePassword() {
        return entriesChangePassword;
    }

    public void setEntriesChangePassword(EntriesChangePassword entriesChangePassword) {
        this.entriesChangePassword = entriesChangePassword;
    }

}
