
package intime.api.model.recovery;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EntriesChangePassword {

    @SerializedName("Entry_change_password")
    @Expose
    private List<EntryChangePassword> entryChangePassword = null;

    public List<EntryChangePassword> getEntryChangePassword() {
        return entryChangePassword;
    }

    public void setEntryChangePassword(List<EntryChangePassword> entryChangePassword) {
        this.entryChangePassword = entryChangePassword;
    }

}
