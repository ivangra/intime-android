
package intime.api.model.billingcalc;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BillingCalculator {

    @SerializedName("RowData")
    @Expose
    public RowData rowData;

}
