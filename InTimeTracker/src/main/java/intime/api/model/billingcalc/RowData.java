
package intime.api.model.billingcalc;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RowData {

    @SerializedName("CALCULATION_DATA")
    @Expose
    public CalculationData calculationData;

}
