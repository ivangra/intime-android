
package intime.api.model.billingcalc;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Servs {

    public List<Serv> getServ() {
        return serv;
    }

    public void setServ(List<Serv> serv) {
        this.serv = serv;
    }

    @SerializedName("SERV")
    @Expose
    public List<Serv> serv = null;

}
