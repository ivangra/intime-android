package intime.api.model.billingcalc;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by technomag on 15.08.17.
 */

public class RowDataWrapper {

  @SerializedName("ROWDATA")
  @Expose
  public RowData rowData;

}
