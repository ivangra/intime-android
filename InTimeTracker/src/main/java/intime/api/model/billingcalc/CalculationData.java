
package intime.api.model.billingcalc;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CalculationData {

    @SerializedName("CALCULATION_DATE")
    @Expose
    public String CalculationDate;
    @SerializedName("CALCULATION_SPEED_MS")
    @Expose
    public Integer CalculationSpeedMs;
    @SerializedName("CALC_PARAMS")
    @Expose
    public CalcParams Calcparams;
    @SerializedName("SERVS")
    @Expose
    public Servs servs;

}
