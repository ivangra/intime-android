
package intime.api.model.billingcalc;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CalcParams {

    @SerializedName("DAY_TO_DELIVERY")
    @Expose
    public Integer Daytodelivery;

}
