
package intime.api.model.billingcalc;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Serv {

    @SerializedName("COST")
    @Expose
    public Double cost;
    @SerializedName("SERV_NAME")
    @Expose
    public String servName;
    @SerializedName("SUM")
    @Expose
    public Double sum;
    @SerializedName("SERV_ID")
    @Expose
    public Integer servId;
    @SerializedName("DISCOUNT")
    @Expose
    public Double discount;

}
