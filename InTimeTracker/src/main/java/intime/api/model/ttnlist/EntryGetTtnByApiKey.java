
package intime.api.model.ttnlist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EntryGetTtnByApiKey {

    @SerializedName("ID")
    @Expose
    public String ID;
    @SerializedName("DECL_NUM")
    @Expose
    public String DeclNum;
    @SerializedName("CLIENT_DOC_ID")
    @Expose
    public Object ClientDocId;
    @SerializedName("DECL_NUM_1C")
    @Expose
    public String DeclNum1C;
    @SerializedName("DATE_CREATION")
    @Expose
    public String DateCreation;
    @SerializedName("DATE_DELIVERY")
    @Expose
    public String DeteDelivery;
    @SerializedName("DECL_TYPE")
    @Expose
    public String DeclType;
    @SerializedName("PAYER_TYPE")
    @Expose
    public String PayerType;
    @SerializedName("PAYMENT_TYPE")
    @Expose
    public String PaymentType;
    @SerializedName("LOCALITY_S")
    @Expose
    public String LocalitySender;
    @SerializedName("SENDER_WAREHOUSE_ID")
    @Expose
    public String SenderWarehouseId;
    @SerializedName("SENDER_ADRES")
    @Expose
    public Object SenderAddress;
    @SerializedName("LOCALITY_R")
    @Expose
    public String LocalityReceiver;
    @SerializedName("RECEIVER_WAREHOUSE_ID")
    @Expose
    public String ReceiverWarehouseId;
    @SerializedName("RECEIVER_ADRES")
    @Expose
    public String ReceiverAddress;
    @SerializedName("STATUSID")
    @Expose
    public String StatusId;
    @SerializedName("SENDER_CLIENT_NAME")
    @Expose
    public String SenderClientName;
    @SerializedName("RECEIVER_CLIENT_NAME")
    @Expose
    public String ReceiverClientName;
    @SerializedName("FORM_NAME")
    @Expose
    public String FormName;
    @SerializedName("COST_RETURN")
    @Expose
    public String CostReturn;
    @SerializedName("COD")
    @Expose
    public String Cod;
    @SerializedName("COST")
    @Expose
    public String Cost;
    @SerializedName("PAID")
    @Expose
    public String Paid;
    @SerializedName("SITE_NAME_UKR")
    @Expose
    public String SiteNameUkr;
    @SerializedName("WH_S")
    @Expose
    public String WarehouseSender;
    @SerializedName("WH_R")
    @Expose
    public String WarehouseReceiver;
    @SerializedName("UL_NAME_S")
    @Expose
    public Object uLNAMES;
    @SerializedName("UL_NAME_R")
    @Expose
    public Object uLNAMER;
    @SerializedName("FL_NAME_S")
    @Expose
    public String fLNAMES;
    @SerializedName("FL_NAME_R")
    @Expose
    public String fLNAMER;
    @SerializedName("UL_ID_S")
    @Expose
    public String uLIDS;
    @SerializedName("UL_ID_R")
    @Expose
    public String uLIDR;
    @SerializedName("FL_ID_S")
    @Expose
    public String fLIDS;
    @SerializedName("FL_ID_R")
    @Expose
    public String fLIDR;
    @SerializedName("FORM_ID")
    @Expose
    public String FormId;
    @SerializedName("FORM_NAME_RU")
    @Expose
    public String fORMNAMERU;
    @SerializedName("STATUS_NAME_RU")
    @Expose
    public String StatusNameRu;
    @SerializedName("WEIGHT")
    @Expose
    public String Weight;
    @SerializedName("GSIZE")
    @Expose
    public String gSize;
    @SerializedName("SEATS_NUM")
    @Expose
    public String SeatsNum;
    @SerializedName("SENDER_LOCALITY_ID")
    @Expose
    public String SenderLocalityId;
    @SerializedName("RECEIVER_LOCALITY_ID")
    @Expose
    public String ReceiverLocalityId;
    @SerializedName("GOODS_TYPE_ID")
    @Expose
    public String GoodsTypeId;
    @SerializedName("PAID_AMOUNT")
    @Expose
    public String PaidAmount;
    @SerializedName("WH_TEL_S")
    @Expose
    public String wHTELS;
    @SerializedName("WH_TEL_R")
    @Expose
    public String RecordCount;
    @SerializedName("RECORD_COUNT")
    @Expose
    public String rECORDCOUNT;
    @SerializedName("LOCALITY_S_RU")
    @Expose
    public String LocalitySenderRu;
    @SerializedName("LOCALITY_R_RU")
    @Expose
    public String LocalityReceiverRu;

}
