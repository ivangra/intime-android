
package intime.api.model.ttnlist;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EntriesGetTtnByApiKey {

    @SerializedName("Entry_get_ttn_by_api_key")
    @Expose
    public List<EntryGetTtnByApiKey> entryGetTtnByApiKey = null;

}
