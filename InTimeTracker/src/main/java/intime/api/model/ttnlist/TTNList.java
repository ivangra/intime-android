
package intime.api.model.ttnlist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TTNList {

    @SerializedName("Entries_get_ttn_by_api_key")
    @Expose
    public EntriesGetTtnByApiKey entriesGetTtnByApiKey;

}
