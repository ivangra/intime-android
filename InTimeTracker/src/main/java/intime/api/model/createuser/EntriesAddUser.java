
package intime.api.model.createuser;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EntriesAddUser {

    @SerializedName("Entry_add_user")
    @Expose
    private List<EntryAddUser> entryAddUser = null;

    public List<EntryAddUser> getEntryAddUser() {
        return entryAddUser;
    }

    public void setEntryAddUser(List<EntryAddUser> entryAddUser) {
        this.entryAddUser = entryAddUser;
    }

}
