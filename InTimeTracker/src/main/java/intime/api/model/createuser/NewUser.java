
package intime.api.model.createuser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NewUser {

    @SerializedName("Entries_add_user")
    @Expose
    private EntriesAddUser entriesAddUser;

    public EntriesAddUser getEntriesAddUser() {
        return entriesAddUser;
    }

    public void setEntriesAddUser(EntriesAddUser entriesAddUser) {
        this.entriesAddUser = entriesAddUser;
    }

}
