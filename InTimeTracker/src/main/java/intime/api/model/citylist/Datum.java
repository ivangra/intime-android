
package intime.api.model.citylist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("warehouse_id")
    @Expose
    private String warehouse_id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("area")
    @Expose
    private String area;
    @SerializedName("has_store")
    @Expose
    private int has_store;
    @SerializedName("has_postmat")
    @Expose
    private int has_postmat;
    @SerializedName("city_type")
    @Expose
    private int city_type;
    @SerializedName("hits")
    @Expose
    private int hits;

    public Datum()
    {
        code = "";
        name = "";
        state = "";
        area = "";
        has_store = 0;
        has_postmat = 0;
        city_type = 0;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public int getHas_store() {
        return has_store;
    }

    public void setHas_store(int has_store) {
        this.has_store = has_store;
    }

    public int getHas_postmat() {
        return has_postmat;
    }

    public void setHas_postmat(int has_postmat) {
        this.has_postmat = has_postmat;
    }

    public int getCity_type() {
        return city_type;
    }

    public void setCity_type(int city_type) {
        this.city_type = city_type;
    }

    public int getHits() {
        return hits;
    }

    public void setHits(int hits) {
        this.hits = hits;
    }

    public String getWarehouse_id() {
        return warehouse_id;
    }

    public void setWarehouse_id(String warehouse_id) {
        this.warehouse_id = warehouse_id;
    }
}
