package intime.ttnlistcommand;

import android.content.Context;

import com.ua.inr0001.intime.DictionariesDB;
import com.ua.inr0001.intime.InTimeDB;

/**
 * Created by technomag on 26.12.17.
 */

public class DeleteCommand extends TtnListCommand {

  public DeleteCommand(Context contect) {
    super(contect);
  }

  @Override
  public void undo() {
    InTimeDB.getInstance(context).tableTTNHistory.undoDelete(this.idItem);
  }

  @Override
  public boolean execute(String idItem) {
      InTimeDB.getInstance(context).tableTTNHistory.delete(idItem);
      this.idItem = idItem;
      return true;
  }
}
