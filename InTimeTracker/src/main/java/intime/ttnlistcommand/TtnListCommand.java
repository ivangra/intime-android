package intime.ttnlistcommand;

import android.content.Context;

/**
 * Created by technomag on 26.12.17.
 */

public abstract class TtnListCommand {

  protected String idItem;
  protected Context context;

  public TtnListCommand(Context context)
  {
      this.context = context;
  }

  public abstract boolean execute(String idItem);

  public abstract void undo();

}
