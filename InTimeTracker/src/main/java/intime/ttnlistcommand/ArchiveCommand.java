package intime.ttnlistcommand;

import android.content.Context;

import com.ua.inr0001.intime.DictionariesDB;
import com.ua.inr0001.intime.InTimeDB;

/**
 * Created by technomag on 26.12.17.
 */

public class ArchiveCommand extends TtnListCommand {
  public ArchiveCommand(Context contect) {
    super(contect);
  }

  @Override
  public boolean execute(String idItem) {
    InTimeDB.getInstance(context).tableTTNHistory.archive(idItem);
    this.idItem = idItem;
    return true;
  }

  @Override
  public void undo() {
    InTimeDB.getInstance(context).tableTTNHistory.undoarchive(idItem);
  }
}
