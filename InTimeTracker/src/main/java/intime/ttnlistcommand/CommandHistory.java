package intime.ttnlistcommand;

import java.util.Stack;

/**
 * Created by technomag on 26.12.17.
 */

public class CommandHistory {

  private Stack history = new Stack();

  public void push(TtnListCommand c) {
    history.push(c);
  }

  public TtnListCommand pop() {
    return (TtnListCommand) history.pop();
  }

  public Boolean isEmpty() { return history.isEmpty(); }

}
