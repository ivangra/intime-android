package intime.map.builds.route;

import android.graphics.Color;
import android.os.AsyncTask;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.InputStream;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

/**
 * Created by ILYA on 30.01.2017.
 */

public class GetRoute extends AsyncTask<Void, Void, Void> {

  private String url;

  private String distance;
  private String duration;

  private double swBoundLongitude;
  private double swBoundLatitude;

  private double neBoundLongitude;
  private double neBoundLatitude;

  private PolylineOptions polylineOptions;
  private GoogleMap map = null;

  private ArrayList<LatLng> route;
  private Polyline finalPolyline;

  private IBuildRoute buildRoite;

  /** Построение маршрута проезда к складу от текущей позиции
   * @param startLatitude
   * @param startLongitude
   * @param endLatitude
   * @param endLongitude
   */
  public GetRoute(GoogleMap map, IBuildRoute iBuildRoute, String startLatitude, String startLongitude, String endLatitude, String endLongitude)
  {
    this.map = map;
    this.buildRoite = iBuildRoute;
    this.route = new ArrayList<LatLng>();

    this.url = "http://maps.googleapis.com/maps/api/directions/xml?"
      + "origin=" + startLatitude + "," + startLongitude
      + "&destination=" + endLatitude + "," + endLongitude
      + "&sensor=false&units=metric&mode=driving";
  }

  private int getNodeIndex(NodeList nl, String nodename) {
    for (int i = 0; i < nl.getLength(); i++) {
      if (nl.item(i).getNodeName().equals(nodename))
        return i;
    }
    return -1;
  }

  private ArrayList<LatLng> decodePolyLine(final String encoded) {
    final ArrayList<LatLng> poly = new ArrayList<LatLng>();
    int index = 0;
    final int len = encoded.length();
    int lat = 0, lng = 0;
    while (index < len) {
      int bVar, shift = 0, result = 0;
      do {
        bVar = encoded.charAt(index++) - 63;
        result |= (bVar & 0x1f) << shift;
        shift += 5;
      } while (bVar >= 0x20);
      final int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
      lat += dlat;
      shift = 0;
      result = 0;
      do {
        bVar = encoded.charAt(index++) - 63;
        result |= (bVar & 0x1f) << shift;
        shift += 5;
      } while (bVar >= 0x20);
      final int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
      lng += dlng;

      final LatLng position = new LatLng(lat / 1E5, lng / 1E5);
      poly.add(position);
    }
    return poly;
  }


  private void getMapBounds(Document document)
  {
    NodeList nl1, nl2, pl3;
    nl1 = document.getElementsByTagName("bounds");
    if (nl1.getLength() > 0) {
      for (int i = 0; i < nl1.getLength(); i++) {
        Node node1 = nl1.item(i);
        nl2 = node1.getChildNodes();

        Node polylineNode = nl2.item(getNodeIndex(nl2, "southwest"));
        pl3 = polylineNode.getChildNodes();
        Node polyNode = pl3.item(getNodeIndex(pl3, "lat"));

        swBoundLatitude = Double.parseDouble(polyNode.getTextContent());
        //Log.d("XML", "swbounds: " + polyNode.getTextContent());
        polyNode = pl3.item(getNodeIndex(pl3, "lng"));
        swBoundLongitude = Double.parseDouble(polyNode.getTextContent());
        //Log.d("XML", "swbounds: " + polyNode.getTextContent());

        // --------------
        Node polylineNode1 = nl2.item(getNodeIndex(nl2, "northeast"));
        pl3 = polylineNode1.getChildNodes();
        Node polyNode1 = pl3.item(getNodeIndex(pl3, "lat"));

        neBoundLatitude = Double.parseDouble(polyNode1.getTextContent());
        //Log.d("XML", "nebounds: " + polyNode1.getTextContent());
        polyNode1 = pl3.item(getNodeIndex(pl3, "lng"));
        neBoundLongitude = Double.parseDouble(polyNode1.getTextContent());
        //Log.d("XML", "nebounds: " + polyNode1.getTextContent());

      }
    }
  }

  private ArrayList<LatLng> parseRoute(Document document)
  {

    NodeList nl1, nl2, pl3;
    ArrayList<LatLng> listGeopoints = new ArrayList<LatLng>();
    nl1 = document.getElementsByTagName("step");
    if (nl1.getLength() > 0) {
      for (int i = 0; i < nl1.getLength(); i++) {
        Node node1 = nl1.item(i);
        nl2 = node1.getChildNodes();

        Node polylineNode = nl2.item(getNodeIndex(nl2, "polyline"));
        pl3 = polylineNode.getChildNodes();
        Node polyNode = pl3.item(getNodeIndex(pl3, "points"));
        ArrayList<LatLng> listPoly = decodePolyLine(polyNode.getTextContent());

        listGeopoints.addAll(listPoly);

        //Log.d("XML", Double.toString(lat) + ", " + Double.toString(lng));
      }
    }
    return listGeopoints;
  }

  public String getDurationText(Document doc) {
    try {

      NodeList nl1 = doc.getElementsByTagName("duration");
      Node node1 = nl1.item(nl1.getLength()-1);
      NodeList nl2 = node1.getChildNodes();
      Node node2 = nl2.item(getNodeIndex(nl2, "text"));
      return node2.getTextContent();
    } catch (Exception e) {
      return "0";
    }
  }

  public String getDistanceText(Document doc) {
    try {
      NodeList nl1 = doc.getElementsByTagName("distance");
      Node node1 = nl1.item(nl1.getLength()-1);
      NodeList nl2 = node1.getChildNodes();
      Node node2 = nl2.item(getNodeIndex(nl2, "text"));
      return node2.getTextContent();
    } catch (Exception e) {
      return "0";
    }
  }

  @Override
  protected Void doInBackground(Void... arg0) {
    try {
      HttpClient httpClient = new DefaultHttpClient();
      HttpContext localContext = new BasicHttpContext();
      HttpPost httpPost = new HttpPost(url);
      httpPost.setHeader("Accept-Language", "ru");
      HttpResponse response = httpClient.execute(httpPost, localContext);
      InputStream in = response.getEntity().getContent();

      DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
      Document document = builder.parse(in);
      route = parseRoute(document);
      duration = getDurationText(document);
      distance = getDistanceText(document);
      getMapBounds(document);

      //Log.d("XML", "Duration " + duration);
      //Log.d("XML", "Disatnce " + distance);
    } catch (Exception e) {
    }
    return null;
  }

  @Override
  protected void onPostExecute(Void result) {
    try
    {
      /*MarkerOptions mo = new MarkerOptions()
        .position(new LatLng(GlobalApplicationData.getInstance().getYourLocation().getLatitude(),
          GlobalApplicationData.getInstance().getYourLocation().getLongitude()))
        .title(context.getResources().getString(R.string.your_location));
      map.addMarker(mo);*/
      polylineOptions = new PolylineOptions().geodesic(true);
      polylineOptions.color(Color.BLUE);
      polylineOptions.addAll(route);
      finalPolyline = map.addPolyline(polylineOptions);

      LatLngBounds.Builder bounds = new LatLngBounds.Builder();
      bounds.include(new LatLng(swBoundLatitude, swBoundLongitude));
      bounds.include(new LatLng(neBoundLatitude, neBoundLongitude));
      map.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds.build(), 50));
      buildRoite.buildFinish(finalPolyline);
    }
    catch (Exception e)
    {}
    super.onPostExecute(result);
  }

  @Override
  protected void onPreExecute() {
    super.onPreExecute();
  }

}
