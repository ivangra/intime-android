package intime.map.builds.route;

import com.google.android.gms.maps.model.Polyline;

/**
 * Created by ILYA on 30.01.2017.
 */

public interface IBuildRoute {

  void buildFinish(Polyline polyline);
  void getRoute();
}
