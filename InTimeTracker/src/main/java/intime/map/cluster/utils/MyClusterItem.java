package intime.map.cluster.utils;

import android.content.Context;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

import intime.llc.ua.R;

/**
 * Created by ILYA on 07.10.2016.
 */

public class MyClusterItem implements ClusterItem {

  private LatLng mPosition;
  private String depNumber;
  private String depType;
  private String depAddress;
  private String code;
  private Context context;
  private int weightLimit = 31;
  private boolean added = false;
  private boolean selected = false;

  public void setSelected(boolean selected) {
    this.selected = selected;
  }

  public MyClusterItem(Context context, double lat, double lng,
                       String depType, String depAddress,
                       String code, String depNumber, int weightLimit )
  {
    this.context = context;
    mPosition = new LatLng(lat, lng);
    this.depType = depType;
    this.depAddress = depAddress;
    this.code = code;
    this.depNumber = depNumber;
    this.added = false;
    this.weightLimit = weightLimit;
  }

  @Override
  public LatLng getPosition() {
    return mPosition;
  }

  public boolean isAdded()
  {
    return added;
  }

  public void setAdded(boolean added)
  {
    this.added = added;
  }

  public String getCode() {
    return code;
  }

  public String getDepAddress() {
    return depAddress;
  }

  public String getDepType() {
    return getDepTypeString();
  }

  public int getIcon()
  {
    if (selected==true)
    {
      if (depType.compareTo("store")==0) {
        if (weightLimit==1000)
          return R.drawable.pin_department_1000_active;
        else if (weightLimit==70)
          return R.drawable.pin_department_70_active;
        else
          return R.drawable.pin_department_31_active;
      } else
        return R.drawable.pin_postomat_active;
    }
    else {
      if (depType.compareTo("store")==0) {
        if (weightLimit==1000)
          return R.drawable.pin_department_1000;
        else if (weightLimit==70)
          return R.drawable.pin_department_70;
        else
          return R.drawable.pin_department_31;
      } else
        return R.drawable.pin_postomat;
    }
  }

  private String getDepTypeString()
  {
    if (depType.compareTo("store")==0)
    {
      return context.getResources().getString(R.string.text_department) + " №" + depNumber;
    }
    else
      return context.getResources().getString(R.string.text_postomat) + " №" + depNumber;
  }

}
