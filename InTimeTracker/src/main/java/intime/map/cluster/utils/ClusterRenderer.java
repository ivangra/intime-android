package intime.map.cluster.utils;

import android.content.Context;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;

import intime.llc.ua.R;

/**
 * Created by ILYA on 07.10.2016.
 */

public class ClusterRenderer extends DefaultClusterRenderer<MyClusterItem> {

  private Context context;

  public ClusterRenderer(Context context, GoogleMap map, ClusterManager<MyClusterItem> clusterManager) {
    super(context, map, clusterManager);
    this.context = context;
  }

  @Override
  protected void onBeforeClusterItemRendered(MyClusterItem item, MarkerOptions markerOptions) {
    markerOptions.icon(BitmapDescriptorFactory.fromResource(item.getIcon()));
    //markerOptions.snippet(item.getDepAddress());
    //markerOptions.title(item.getDepType());
    super.onBeforeClusterItemRendered(item, markerOptions);
  }

  @Override
  protected void onBeforeClusterRendered(Cluster<MyClusterItem> cluster, MarkerOptions markerOptions) {
    markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.cluster));
    //super.onBeforeClusterRendered(cluster, markerOptions);
  }
}
