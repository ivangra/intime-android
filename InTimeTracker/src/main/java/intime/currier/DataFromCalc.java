package intime.currier;

/**
 * Created by technomag on 15.12.17.
 */

public class DataFromCalc {

  private String cityCode;
  private String cityName;
  private String width;
  private String length;
  private String height;
  private int selectedWeight;
  private String weightText;
  private String weight;
  private String systemDate;
  private String humanDate;
  private String cargoType;
  private String cargoSubType;
  private double radius;
  private int radiusIndex;
  private String cost;
  private String podSum;
  private int count = 1;

  public String getCargoType() {
    return cargoType;
  }

  public void setCargoType(String cargoType) {
    this.cargoType = cargoType;
  }

  public String getCargoSubType() {
    return cargoSubType;
  }

  public void setCargoSubType(String cargoSubType) {
    this.cargoSubType = cargoSubType;
  }

  public double getRadius() {
    return radius;
  }

  public void setRadius(String radius) {
    String rad = radius.replaceAll("R,", "");
    try {
      this.radius = Double.parseDouble(rad);
    }
    catch(Exception e){}
  }

  public void setRadiusPosition(int position)
  {
    this.radiusIndex = position;
  }

  public int getRadiusPosition()
  {
    return radiusIndex;
  }

  public String getCost() {
    return cost;
  }

  public void setCost(String cost) {
    this.cost = cost;
  }

  public String getPodSum() {
    return podSum;
  }

  public void setPodSum(String podSum) {
    this.podSum = podSum;
  }

  public int getCount() {
    return count;
  }

  public void setCount(String count) {
    try {

      this.count = Integer.parseInt(count);
    }catch (Exception e){}
  }

  public String getCityCode() {
    return cityCode;
  }

  public void setCityCode(String cityCode) {
    this.cityCode = cityCode;
  }

  public String getCityName() {
    return cityName;
  }

  public void setCityName(String cityName) {
    this.cityName = cityName;
  }

  public String getWidth() {
    return width;
  }

  public void setWidth(String width) {
    this.width = width;
  }

  public String getLength() {
    return length;
  }

  public void setLength(String length) {
    this.length = length;
  }

  public String getHeight() {
    return height;
  }

  public void setHeight(String height) {
    this.height = height;
  }

  public int getSelectedWeight() {
    return selectedWeight;
  }

  public void setSelectedWeight(int selectedWeight) {
    this.selectedWeight = selectedWeight;
  }

  public String getWeightText() {
    return weightText;
  }

  public void setWeightText(String weightText) {
    this.weightText = weightText;
  }

  public String getWeight() {
    return weight;
  }

  public void setWeight(String weight) {
    this.weight = weight;
  }

  public String getSystemDate() {
    return systemDate;
  }

  public void setSystemDate(String systemDate) {
    this.systemDate = systemDate;
  }

  public String getHumanDate() {
    return humanDate;
  }

  public void setHumanDate(String humanDate) {
    this.humanDate = humanDate;
  }
}
