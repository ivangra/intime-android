package com.ua.inr0001.intime;


import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesUtil;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import intime.api.model.userinfo.PersonInfo;
import intime.llc.ua.R;
import intime.utils.MyDateFormatSymbols;
import okhttp3.OkHttpClient;

public class GlobalApplicationData {
	
	private Context mContext;
	private static final GlobalApplicationData instance = new GlobalApplicationData();

	// данные для регистрации GCM
	private static final String PROPERTY_DB_LAST_DATE_UPDATE = "db_last_date_update";

	private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
	
	// ID проекта созданного в консоли управления Google
	private static final String SENDER_ID = "77694651431";
	
	//public String loggedPhone = "";

	public boolean welcomeScreenShow = false;
	public boolean confidentialityReaded = false;

	public int filterList[]  = {0, 0, 0, 0, 0};
	public String filterQuery = "StatusId IN (0, 1, 2, 3, 16)"; // "";
	public String filterQueryAPI = "";
	public boolean showTracked = true;
	public boolean showArchived = true;

	private PersonInfo userInfo = new PersonInfo();
	public long lastLocatinGetTime = 0L;
	//private String intimeAPIUserKey = "";  // 98583681410000000040 test
	
  private int notificationId = 2;

	static {
		System.loadLibrary("iconv");
	}

	private GlobalApplicationData()
	{
		mContext = InTimeTrackerApp.getAppContext();
		loadUserInfo();
		loadAppSettings();
	}
	
	public static GlobalApplicationData getInstance()
	{
		return instance;
	}
	
  public Context getContext()
	{
	  return mContext;
	}

	public String getAPIkey()
	{
		return userInfo.getApiKey();
	}

	public PersonInfo getUserInfo()
	{
		return userInfo;
	}

	public void saveAppSettings()
	{
		SharedPreferences localePreferences = getContext().getSharedPreferences("app_settings", Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = localePreferences.edit();
		editor.putBoolean("welcome_screen", welcomeScreenShow);
		editor.commit();
	}

	public void saveFilterSettings()
	{
		SharedPreferences localePreferences = getContext().getSharedPreferences("app_settings", Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = localePreferences.edit();
		editor.putString("db_filter_query", filterQuery);
		editor.putString("api_filter_query", filterQueryAPI);
		editor.putBoolean("show_tracked", showTracked);
		editor.putBoolean("show_archived", showArchived);
		for (int i=0; i<=filterList.length - 1; i++)
			editor.putInt(Integer.toString(i+1), filterList[i]);
		editor.commit();
	}

	public void saveConfidentiality(boolean isRead)
	{
		confidentialityReaded = true;
		SharedPreferences localePreferences = getContext().getSharedPreferences("app_settings", Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = localePreferences.edit();
		editor.putBoolean("confidentiality_readed", isRead);
		editor.commit();
	}

	public void saveLastLocation(Location location)
	{
		SharedPreferences localePreferences = getContext().getSharedPreferences("app_settings", Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = localePreferences.edit();
		editor.putFloat("longitude", (float) location.getLongitude());
		editor.putFloat("latitude", (float) location.getLatitude());
		editor.commit();
	}

	public Location loadLastLocation()
	{
		double longitude;
		double latitude;
		Location location = new Location(LocationManager.GPS_PROVIDER);
		SharedPreferences localePreferences = getContext().getSharedPreferences("app_settings", Context.MODE_PRIVATE);
		longitude = localePreferences.getFloat("longitude", 30.546447f);
		latitude = localePreferences.getFloat("latitude", 50.435383f);
		location.setLatitude(latitude);
		location.setLongitude(longitude);
		return location;
	}

	private void loadAppSettings() {
		SharedPreferences localePreferences = getContext().getSharedPreferences("app_settings", Context.MODE_PRIVATE);
		welcomeScreenShow = localePreferences.getBoolean("welcome_screen", false);
		filterQuery = localePreferences.getString("db_filter_query", "");
		filterQueryAPI = localePreferences.getString("api_filter_query", "");
		showArchived = localePreferences.getBoolean("show_archived", true);
		showTracked = localePreferences.getBoolean("show_tracked", true);
		confidentialityReaded = localePreferences.getBoolean("confidentiality_readed", false);
		for (int i=0; i<=filterList.length - 1; i++)
			filterList[i] = localePreferences.getInt(Integer.toString(i+1), 0);
	}

	public void saveUserInfo(PersonInfo info/*, String loggedPhone*/)
	{
		userInfo = info;
		SharedPreferences localePreferences = getContext().getSharedPreferences("user_info", Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = localePreferences.edit();
		editor.putString("api_key", info.getApiKey());
		editor.putString("name", info.getName());
		editor.putString("sur_name", info.getSurname());
		editor.putString("patronomic_name", info.getPatronymic());
		editor.putString("id", info.getId());
		editor.putString("mail", info.getEmails());
		editor.putString("phones", info.getPhoneNumbers());
		//editor.putString("loggedPhoe", loggedPhone);
		//this.loggedPhone = loggedPhone;
		//editor.putString("tax", (String) info.getTaxNumber());
		editor.commit();
	}

	public void deleteUserInfo()
	{
		SharedPreferences localePreferences = getContext().getSharedPreferences("user_info", Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = localePreferences.edit();
		userInfo = new PersonInfo();
		//loggedPhone = "";
		editor.clear();
		editor.commit();
	}

	private void loadUserInfo()
	{
		SharedPreferences localePreferences = getContext().getSharedPreferences("user_info", Context.MODE_PRIVATE);
		userInfo.setApiKey(localePreferences.getString("api_key", "")/*"98583681410000000040"*/);
		userInfo.setName(localePreferences.getString("name", ""));
		userInfo.setSurname(localePreferences.getString("sur_name", ""));
		userInfo.setPatronymic(localePreferences.getString("patronomic_name", ""));
		userInfo.setId(localePreferences.getString("id", ""));
		userInfo.setPhoneNumbers(localePreferences.getString("phones", ""));
		userInfo.setEmails(localePreferences.getString("mail", ""));
		//loggedPhone = localePreferences.getString("loggedPhone", "");
	}

	public int getNotificationId()
	{
	  return notificationId++;
	}
	
	public boolean isGPSEnabled()
	{
		LocationManager locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
		boolean result = false;
		if (IsOnline() && (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER) ||
				locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))) {
				result = true;
		}
		if (!result)
			Log.d("GPS", "GPS disabled");
		else
			Log.d("GPS", "GPS enabled");
		return result;
	}

	public static String getCurrentLanguage()
	{
		// ru, uk
		String lang = Resources.getSystem().getConfiguration().locale.getLanguage();
		if (lang.compareTo("uk")==0)
			return "ua";
		else
			return "ru";
	}

  public boolean IsOnline()
	{
		boolean haveConnectedWifi = false;
		boolean haveConnectedMobile = false;

		ConnectivityManager cm = (ConnectivityManager)mContext.getSystemService(Context.CONNECTIVITY_SERVICE);

		NetworkInfo[] netInfo = cm.getAllNetworkInfo();
		for (NetworkInfo ni : netInfo) {
		  if (ni.getType() == ConnectivityManager.TYPE_WIFI)
		    if (ni.isConnected())
		      haveConnectedWifi = true;
		    if (ni.getType() == ConnectivityManager.TYPE_MOBILE)
		      if (ni.isConnected())
		          haveConnectedMobile = true;
		}
		return haveConnectedWifi || haveConnectedMobile;
	}

	// поддерживает ли устройство услуги Google
	public boolean checkPlayServices(Activity act) {
		GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
	  int resultCode = googleAPI.isGooglePlayServicesAvailable(mContext);
	  if (resultCode != ConnectionResult.SUCCESS) {
	     if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
	          GooglePlayServicesUtil.getErrorDialog(resultCode, act,
	             PLAY_SERVICES_RESOLUTION_REQUEST).show();
        } else {
          //Log.d("DB", "This device is not supported.");
        }
	      return false;
	    }
	  return true;
	}

	public boolean isLocaleChanged()
	{
		SharedPreferences localePreferences = getContext().getSharedPreferences("locale_saved", Context.MODE_PRIVATE);
		String locale = localePreferences.getString("locale", "");
		return GlobalApplicationData.getCurrentLanguage().compareTo(locale)!=0;
	}

	public void saveLocale()
	{
		SharedPreferences localePreferences = getContext().getSharedPreferences("locale_saved", Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = localePreferences.edit();
		editor.putString("locale", GlobalApplicationData.getCurrentLanguage());
		editor.commit();
	}

	public DepFilterSettings loadDepartmentFilterSettings()
	{
		DepFilterSettings filter = new DepFilterSettings();
		SharedPreferences localePreferences = getContext().getSharedPreferences("dep_filter_settings", Context.MODE_PRIVATE);
		filter.to30kg = localePreferences.getBoolean("to30kg", true);
		filter.to70kg = localePreferences.getBoolean("to70kg", true);
		filter.to1000kg = localePreferences.getBoolean("to1000kg", true);
		filter.postomats = localePreferences.getBoolean("postomats", true);
		return filter;
	}

	public void saveDepartmentFilterSettings(final DepFilterSettings filter)
	{
		SharedPreferences localePreferences = getContext().getSharedPreferences("dep_filter_settings", Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = localePreferences.edit();
		editor.putBoolean("to30kg", filter.to30kg);
		editor.putBoolean("to70kg", filter.to70kg);
		editor.putBoolean("to1000kg", filter.to1000kg);
		editor.putBoolean("postomats", filter.postomats);
		editor.commit();
	}

  public void writeDateUpdateDB()
  {
    SharedPreferences updateSharedPreference = getContext().getSharedPreferences("update_info", Context.MODE_PRIVATE);
    SharedPreferences.Editor mEdit1 = updateSharedPreference.edit();
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
    Calendar cal = Calendar.getInstance();
    mEdit1.clear();
    mEdit1.putString(GlobalApplicationData.PROPERTY_DB_LAST_DATE_UPDATE, sdf.format(cal.getTime()));
    mEdit1.commit();
  }
  public boolean isUpdateDB()
  {
    try
    {
      Calendar cal1 = Calendar.getInstance();
      Calendar cal2 = Calendar.getInstance();
      SharedPreferences updateSharedPreference = getContext().getSharedPreferences("update_info", Context.MODE_PRIVATE);
      String date = updateSharedPreference.getString(PROPERTY_DB_LAST_DATE_UPDATE, "");
      if (date.length() == 0)
			{
				Calendar c = Calendar.getInstance();
				SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");
				date = sdf1.format(c.getTime());
			}
      SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
      cal1.setTime(new Date());
      cal2.setTime(format.parse(date));

      long milis1 = cal1.getTimeInMillis();
      long milis2 = cal2.getTimeInMillis();
      long diff = milis2 - milis1;
      long diffDays = Math.abs(diff / (24 * 60 * 60 * 1000));
      if (diffDays>0)
			{
				return true;
      }
      else
      {
        return false;
      }
    }
    catch(Exception e)
    {
      writeDateUpdateDB();
      return true;
    }
  }

	public static OkHttpClient getUnsafeOkHttpClient() {
		try {
			// Create a trust manager that does not validate certificate chains
			final TrustManager[] trustAllCerts = new TrustManager[] {
				new X509TrustManager() {
					@Override
					public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType)   {
					}

					@Override
					public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) {
					}

					@Override
					public java.security.cert.X509Certificate[] getAcceptedIssuers() {
						return new java.security.cert.X509Certificate[]{};
					}
				}
			};

			// Install the all-trusting trust manager
			final SSLContext sslContext = SSLContext.getInstance("SSL");
			sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
			// Create an ssl socket factory with our all-trusting manager
			final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

			OkHttpClient.Builder builder = new OkHttpClient.Builder();
			builder.sslSocketFactory(sslSocketFactory);
			builder.hostnameVerifier(new HostnameVerifier() {
				@Override
				public boolean verify(String hostname, SSLSession session) {
					return true;
				}
			});

			builder.readTimeout(120, TimeUnit.SECONDS);
			builder.connectTimeout(120, TimeUnit.SECONDS);
			OkHttpClient okHttpClient = builder.build();
			return okHttpClient;
		} catch (Exception e) {
			Log.d("HTTP", "Error: " + e.getMessage());
			throw new RuntimeException(e);
		}
	}
  
}
