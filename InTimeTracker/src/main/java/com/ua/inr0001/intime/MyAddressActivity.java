package com.ua.inr0001.intime;


import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import intime.llc.ua.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class MyAddressActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {

  private AddressAdapter scAdapter;
  private DictionariesDB dictionariesDB;
  private ListView lvMyAddresses;

  private Button btnAddAddress;
  private Context context;

  private void setDepartmentsCursorAdapter(SimpleCursorAdapter adapter, ListView listview)
  {
    String[] from = new String[]{AddressesTable.Column.NAME,
      AddressesTable.Column.CITY};
    int[] to = new int[]{R.id.tvName, R.id.tvCity};
    scAdapter = new AddressAdapter(context,
      R.layout.my_address_item, null, from , to, 0);
    //scAdapter.setFragmentActivity(getActivity());
    listview.setAdapter(scAdapter);

    lvMyAddresses.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (scAdapter!=null)
          scAdapter.setselectedPosition(position);
          scAdapter.notifyDataSetChanged();
      }
    });
  }

  @Override
  public void onResume() {
    super.onResume();
    getSupportLoaderManager().restartLoader(0, null, this);
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.fragment_my_address);
    context = this;
    dictionariesDB = DictionariesDB.getInstance(context);
    lvMyAddresses = (ListView) findViewById(R.id.lvMyAddresses);
    btnAddAddress = (Button) findViewById(R.id.btnAddMyAddress);
    btnAddAddress.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        startActivity(new Intent(context, AddMyAddressActivity.class));
        }
      }
    );
    setDepartmentsCursorAdapter(scAdapter, lvMyAddresses);
    getSupportLoaderManager().initLoader(0, null, this);
    Toolbar toolbar = (Toolbar) findViewById(R.id.myAddressToolbar);
    setSupportActionBar(toolbar);
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.accept_action_menu, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();
    switch (id)
    {
      case R.id.menu_action_accept: onBackPressed(); return true;
      default: return super.onOptionsItemSelected(item);
    }
  }

  @Override
  public Loader<Cursor> onCreateLoader(int id, Bundle args) {
    return new MyAddressActivity.MyCursorLoader(context, dictionariesDB);
  }

  @Override
  public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
    scAdapter.swapCursor(cursor);
  }

  @Override
  public void onLoaderReset(Loader<Cursor> loader) {

  }

  static class MyCursorLoader extends CursorLoader {

    private DictionariesDB db;

    public MyCursorLoader(Context context, DictionariesDB db) {
      super(context);
      this.db = db;
    }

    @Override
    public Cursor loadInBackground() {
      return null;//db.tableAddresses.selectAll();
    }

  }

}
