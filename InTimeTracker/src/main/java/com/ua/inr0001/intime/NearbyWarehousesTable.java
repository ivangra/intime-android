package com.ua.inr0001.intime;

public class NearbyWarehousesTable {

  public static final String NAME = "tblNearbyWarehouse";
  public static final String VIEW_NAME = "vwNearbyWarehouse";
  public static final String JOIN_VIEW_NAME = "vwNearbyWarehouseJoin";
  
  public static final String SQL_DROP_TABLE = "DROP TABLE IF EXISTS " + NAME;
  
  public static final String SQL_DROP_VIEW = "DROP VIEW IF EXISTS " + VIEW_NAME;
  
  public static final String SQL_DROP_VIEW_JOIN = "DROP VIEW IF EXISTS " + JOIN_VIEW_NAME;

}
