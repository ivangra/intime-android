package com.ua.inr0001.intime;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import com.google.android.gms.maps.model.LatLngBounds;

import intime.utils.DbQuickFixUtils;

import static com.ua.inr0001.intime.NearbyWarehousesTable.JOIN_VIEW_NAME;
import static intime.utils.DbQuickFixUtils.*;

public class WarehouseDetailsTable implements ITable {

  private SQLiteDatabase sqlDB;
  private SQLiteStatement insertStatement;
	private SQLiteStatement insertStatementUA;

	public WarehouseDetailsTable(SQLiteDatabase sqldb)
	{
		sqlDB = sqldb;
		insertStatement = sqlDB.compileStatement(INSERT_WAREHOUSE);
		insertStatementUA = sqlDB.compileStatement(INSERT_WAREHOUSE_UA);
		//updateStatement = sqlDB.compileStatement(UPDATE_WAREHOUSE);
	}
	
	public static final class Column
	{
	    public static final String ID = "_id";
		  public static final String CODE = "Code";
		  public static final String DUBLICATE_CITY = "DublicateCity";
			public static final String LAT = "Lat"; // долгота
			public static final String LNG = "Lng"; // широта
			public static final String TYPE = "Type"; // долгота
			public static final String TYPE_ID = "TypeID"; // 1 - транзит
			public static final String NUMBER = "Number"; // широта
			public static final String CAPACITY = "Capacity"; // долгота
			public static final String CITY = "City"; // широта
			public static final String CITY_TYPE = "CityType";
			public static final String STATE = "State";
			public static final String ADDRESS = "Address";
			public static final String AREA = "Area";
			public static final String FULL_ADDRESS = "FullAddress";
			public static final String PHONE = "Phone";
		public static final String MAX_LENGTH = "MaxLength";
			public static final String DAY_IN1 = "DayIn1";
		public static final String DAY_IN2 = "DayIn2";
		public static final String DAY_IN3 = "DayIn3";
		public static final String DAY_IN4 = "DayIn4";
		public static final String DAY_IN5 = "DayIn5";
		public static final String DAY_IN6 = "DayIn6";
		public static final String DAY_IN7 = "DayIn7";
		public static final String OPEN1 = "Open1";
		public static final String CLOSE1 = "Close1";
		public static final String OPEN2 = "Open2";
		public static final String CLOSE2 = "Close2";
		public static final String OPEN3 = "Open3";
		public static final String CLOSE3 = "Close3";
		public static final String OPEN4 = "Open4";
		public static final String CLOSE4 = "Close4";
		public static final String OPEN5 = "Open5";
		public static final String CLOSE5 = "Close5";
		public static final String OPEN6 = "Open6";
		public static final String CLOSE6 = "Close6";
		public static final String OPEN7 = "Open7";
		public static final String CLOSE7 = "Close7";
	}

	public static final class Index
	{
		public static final String ADDRESS = "idx_Address";
		public static final String ADDRESS_UA = "idx_Address_UA";
		public static final String LAT = "idx_Lat";
		public static final String LNG = "idx_Lng";
		public static final String CODE = "idx_Code";
		public static final String CODE_UA = "idx_CodeUA";
	}

	public static final String NAME = "tblWarehouseDetail";
	public static final String VIEW_NAME = "vwWarehouseDetail";
	public static final String NAME_UA = "tblWarehouseDetailUA";
	public static final String VIEW_NAME_UA = "vwWarehouseDetailUA";

	public static final String INSERT_WAREHOUSE_UA =
		"insert into " + NAME_UA + "(" +
			Column.CODE + ", " +
			Column.CITY + " , " +
			Column.STATE + " , " +
			Column.AREA + " , " +
			Column.ADDRESS + ", " +
			Column.FULL_ADDRESS +
			") VALUES (?, ?, ?, ?, ?, ?)";

	public static final String INSERT_WAREHOUSE =
      "insert into " + NAME + "(" +
        Column.CODE + ", " +
				Column.LAT + " , " +
				Column.LNG + " , " +
				Column.TYPE + " , " +
				Column.TYPE_ID + " , " +
				Column.NUMBER + " , " +
				Column.CAPACITY + " , " +
				Column.CITY + " , " +
				Column.CITY_TYPE + " , " +
				Column.STATE + " , " +
				Column.AREA + " , " +
				Column.ADDRESS + " , " +
				Column.FULL_ADDRESS + " , " +
				Column.PHONE + " , " +
				Column.MAX_LENGTH + " , " +
				Column.DAY_IN1 + " , " +
				Column.DAY_IN2 + " , " +
				Column.DAY_IN3 + " , " +
				Column.DAY_IN4 + " , " +
				Column.DAY_IN5 + " , " +
				Column.DAY_IN6 + " , " +
				Column.DAY_IN7 + " , " +
				Column.OPEN1 + " , " +
				Column.CLOSE1 + " , " +
				Column.OPEN2 + " , " +
				Column.CLOSE2 + " , " +
				Column.OPEN3 + " , " +
				Column.CLOSE3 + " , " +
				Column.OPEN4 + " , " +
				Column.CLOSE4 + " , " +
				Column.OPEN5 + " , " +
				Column.CLOSE5 + " , " +
				Column.OPEN6 + " , " +
				Column.CLOSE6 + " , " +
				Column.OPEN7 + " , " +
				Column.CLOSE7 +
        ") VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	
	public static final String SQL_CREATE_TABLE =
            "create table if not exists " + NAME + " ( " +
                    Column.ID + " integer primary key autoincrement, " +
                    Column.CODE + " integer, " +
										Column.DUBLICATE_CITY + " integer default 0, " +
										Column.LAT + " real, " +
										Column.LNG + " real, " +
										Column.TYPE + " text, " +
							Column.TYPE_ID + " integer, " +
										Column.NUMBER + " integer, " +
										Column.CAPACITY + " integer, " +
										Column.CITY + " text, " +
										Column.CITY_TYPE + " integer, " +
										Column.STATE + " text, " +
										Column.AREA + " text, " +
										Column.ADDRESS + " text, " +
										Column.FULL_ADDRESS + " text, " +
										Column.PHONE + " text, " +
							Column.MAX_LENGTH + " text, " +
										Column.DAY_IN1 + " text, " +
							Column.DAY_IN2 + " text, " +
							Column.DAY_IN3 + " text, " +
							Column.DAY_IN4 + " text, " +
							Column.DAY_IN5 + " text, " +
							Column.DAY_IN6 + " text, " +
							Column.DAY_IN7 + " text, " +
							Column.OPEN1 + " text, " +
							Column.CLOSE1 + " text, " +
							Column.OPEN2 + " text, " +
							Column.CLOSE2 + " text, " +
							Column.OPEN3 + " text, " +
							Column.CLOSE3 + " text, " +
							Column.OPEN4 + " text, " +
							Column.CLOSE4 + " text, " +
							Column.OPEN5 + " text, " +
							Column.CLOSE5 + " text, " +
							Column.OPEN6 + " text, " +
							Column.CLOSE6 + " text, " +
							Column.OPEN7 + " text, " +
							Column.CLOSE7 + " text); ";

	public static final String SQL_CREATE_TABLE_UA =
		"create table if not exists " + NAME_UA + " ( " +
			Column.ID + " integer primary key autoincrement, " +
			Column.CODE + " integer, " +
			Column.CITY + " text, " +
			Column.STATE + " text, " +
			Column.AREA + " text, " +
			Column.FULL_ADDRESS + " text, " +
			Column.ADDRESS + " text); ";
	
	public static final String SQL_CREATE_VIEW =
    		"CREATE VIEW if not exists " + VIEW_NAME + " AS" + 
    	    " SELECT " + Column.ID + ", " +
          Column.CODE + ", " +
					Column.DUBLICATE_CITY + ", " +
					Column.LAT + " , " +
					Column.LNG + " , " +
					Column.TYPE + " , " +
					Column.TYPE_ID + " , " +
					Column.NUMBER + " , " +
					Column.CAPACITY + " , " +
					Column.CITY + " , " +
					Column.CITY_TYPE + " , " +
					Column.STATE + " , " +
					Column.AREA + " , " +
					Column.ADDRESS + " , " +
					Column.FULL_ADDRESS + " , " +
					Column.PHONE + " , " +
					Column.MAX_LENGTH + " , " +
					Column.DAY_IN1 + " , " +
					Column.DAY_IN2 + " , " +
					Column.DAY_IN3 + " , " +
					Column.DAY_IN4 + " , " +
					Column.DAY_IN5 + " , " +
					Column.DAY_IN6 + " , " +
					Column.DAY_IN7 + " , " +
					Column.OPEN1 + " , " +
					Column.CLOSE1 + " , " +
					Column.OPEN2 + " , " +
					Column.CLOSE2 + " , " +
					Column.OPEN3 + " , " +
					Column.CLOSE3 + " , " +
					Column.OPEN4 + " , " +
					Column.CLOSE4 + " , " +
					Column.OPEN5 + " , " +
					Column.CLOSE5 + " , " +
					Column.OPEN6 + " , " +
					Column.CLOSE6 + " , " +
					Column.OPEN7 + " , " +
					Column.CLOSE7 +
    	    " FROM " + NAME;

	public static final String SQL_CREATE_VIEW_UA =
		"CREATE VIEW if not exists " + VIEW_NAME_UA + " AS" +
			" SELECT " + Column.ID + ", " +
			Column.CODE + ", " +
			Column.CITY + " , " +
			Column.STATE + " , " +
			Column.AREA + " , " +
			Column.FULL_ADDRESS + " , " +
			Column.ADDRESS +
			" FROM " + NAME_UA;

	public static final String SQL_CREATE_INDEX_ADDRESS_UA =
            String.format("CREATE INDEX if not exists %s ON %s (%s);", Index.ADDRESS_UA, NAME_UA, Column.ADDRESS);

	public static final String SQL_CREATE_INDEX_ADDRESS =
            String.format("CREATE INDEX if not exists %s ON %s (%s);", Index.ADDRESS, NAME, Column.ADDRESS);

	public static final String SQL_CREATE_INDEX_LAT =
            String.format("CREATE INDEX if not exists %s ON %s (%s);", Index.LAT, NAME, Column.LAT);

	public static final String SQL_CREATE_INDEX_LNG =
            String.format("CREATE INDEX if not exists %s ON %s (%s);", Index.LNG, NAME, Column.LNG);

	public static final String SQL_CREATE_INDEX_CODE =
            String.format("CREATE INDEX if not exists %s ON %s (%s);", Index.CODE, NAME, Column.CODE);

	public static final String SQL_CREATE_INDEX_CODE_UA =
            String.format("CREATE INDEX if not exists %s ON %s (%s);", Index.CODE_UA, NAME_UA, Column.CODE);


	
	public static final String SQL_DROP_VIEW_JOIN =
      "DROP VIEW IF EXISTS " + JOIN_VIEW_NAME;

	public static final String SQL_DROP_VIEW =
		"DROP VIEW IF EXISTS " + VIEW_NAME;

	public static final String SQL_DROP_TABLE = "DROP TABLE IF EXISTS " + NAME;

	public static final String SQL_DROP_VIEW_UA =
		"DROP VIEW IF EXISTS " + VIEW_NAME_UA;

	public static final String SQL_DROP_TABLE_UA = "DROP TABLE IF EXISTS " + NAME_UA;

	@Override
	public Cursor selectByID(int Id) {
		return null;
	}

	public String getCode(String city, String storeNumber)
	{
	    String inCity = checkAndEscape(city);
	    String inStoreNumber = checkAndEscape(storeNumber);
		Cursor cursor = sqlDB.rawQuery("select * FROM " + VIEW_NAME + " WHERE " +
			Column.CITY + "=\"" + inCity +"\" AND " + Column.NUMBER + "=" + inStoreNumber
			, null);
		cursor.moveToFirst();
		String code = DictionariesDB.getColumnValue(cursor, WarehouseDetailsTable.Column.ID);
		cursor.close();
		return code;
	}

	@Override
	public Cursor selectByName(String Name) {
		return null;
	}

	@Override
	public Cursor selectAll() {
	  return sqlDB.query(NAME, null, null, null, null, null, null);
	}

	public Cursor selectByCityName(String cityName)
	{
		return sqlDB.query(VIEW_NAME, null,
			Column.CITY + "=?" , new String[]{cityName}
			,null, null, null);
	}

	public Cursor selectByCityNameUA(String cityName)
	{
		return sqlDB.rawQuery("SELECT tua.Code, tua.City, tua.State, tua.Area, t.Lat, t.Lng " +
				"FROM " + NAME_UA + " tua " +
				"JOIN " + NAME + " t WHERE t.CODE=tua.code AND tua." + Column.CITY + "=\"" + cityName + "\"" , null);
	}

	public Cursor selectByDiffLocation(String longitude, String latitude)
	{
		return sqlDB.rawQuery(String.format("select * from %s where (abs(%s - %s)<=0.2) AND (abs(%s - %s)<=0.2)", VIEW_NAME, longitude, Column.LNG, latitude, Column.LAT), null);
	}

	public Cursor selectByDiffLocationUA(String longitude, String latitude)
	{
		return sqlDB.rawQuery(String.format("SELECT tua.Code, tua.City, tua.State, tua.Area, t.Lat, t.Lng FROM %s tua JOIN %s t WHERE t.CODE=tua.code AND (abs(%s - %s)<=0.2) AND (abs(%s - %s)<=0.2)", NAME_UA, NAME, longitude, Column.LNG, latitude, Column.LAT)
			, null);
	}

	public Cursor selectAllUA() {
		return sqlDB.rawQuery(String.format("SELECT tua.Code, tua.City, tua.State, tua.Area, tua.Address, t.DublicateCity, t.Lat, t.lng, t.type, t.TypeId, t.number, t.Capacity,  t.phone, t.MaxLength, t.DayIn1, t.DayIn2, t.DayIn3, t.DayIn4, t.DayIn5, t.DayIn6, t.DayIn7, t.Open1, t.Open2, t.Open3, t.Open4, t.Open5, t.Open6, t.Open7, t.Close1, t.Close2, t.Close3, t.Close4, t.Close5, t.Close6, t.Close7 FROM %s tua JOIN %s t WHERE t.CODE=tua.code", NAME_UA, NAME), null);
	}

	public Cursor selectByAddressUA(String address){
	    address = checkAndEscape(address);
		String searchString;
		if (address != null && address.length() > 0)
			searchString = address.replaceFirst(String.valueOf(address.charAt(0)), String.valueOf(address.charAt(0)).toUpperCase());
		else
			searchString = "";
		return sqlDB.rawQuery("SELECT tua.Code, tua.City, t.DublicateCity, t.CityType, tua.State, tua.Address, t.Lat, t.lng, t.type, t.TypeId, t.number, t.Capacity,  t.phone, " +
			"t.MaxLength, t.DayIn1, t.DayIn2, t.DayIn3, t.DayIn4, t.DayIn5, t.DayIn6, t.DayIn7, " +
			"t.Open1, t.Open2, t.Open3, t.Open4, t.Open5, t.Open6, t.Open7, " +
			"t.Close1, t.Close2, t.Close3, t.Close4, t.Close5, t.Close6, t.Close7 " +
			"FROM " + NAME_UA + " tua " +
			"JOIN " + NAME + " t WHERE t.CODE=tua.code AND tua." + Column.ADDRESS + " like \"%" + searchString + "%\" ORDER BY tua." + Column.ADDRESS, null);
	}

	public Cursor selectWarehouseByCode(String code){
	    code = checkAndEscape(code);
		return sqlDB.query(VIEW_NAME, null,
			Column.CODE + "=?" , new String[]{code}
			,null, null, null);
	}

	public Cursor selectWarehouseByCodeUA(String code){
	    code = checkAndEscape(code);
		return sqlDB.rawQuery("SELECT tua.Code, tua.City, t.DublicateCity, t.CityType, tua.State, tua.Address, t.Lat, t.lng, t.type, t.TypeId, t.number, t.Capacity,  t.phone, " +
			"t.MaxLength, t.DayIn1, t.DayIn2, t.DayIn3, t.DayIn4, t.DayIn5, t.DayIn6, t.DayIn7, " +
			"t.Open1, t.Open2, t.Open3, t.Open4, t.Open5, t.Open6, t.Open7, " +
			"t.Close1, t.Close2, t.Close3, t.Close4, t.Close5, t.Close6, t.Close7 " +
			"FROM " + NAME_UA + " tua " +
			"JOIN " + NAME + " t WHERE t.CODE=tua.code AND tua." + Column.CODE + "=" + code, null);
	}

	public Cursor selectByBounds(LatLngBounds bounds)
	{
		return sqlDB.rawQuery("Select * from " + NAME + " where " +
			"(" + bounds.southwest.latitude + "<=lat and lat<=" + bounds.northeast.latitude + ") " +
			"AND (" + bounds.southwest.longitude + "<=lng and lng<=" + bounds.northeast.longitude + ") ORDER BY number, type", null);
	}

	public Cursor selectByBoundsUA(LatLngBounds bounds)
	{
		return sqlDB.rawQuery("SELECT tua.Code, tua.City, t.DublicateCity, t.CityType, tua.State, tua.Address, tua.Area, t.Lat, t.lng, t.type, t.TypeId,  t.number, t.Capacity,  t.phone, " +
				"t.MaxLength, t.DayIn1, t.DayIn2, t.DayIn3, t.DayIn4, t.DayIn5, t.DayIn6, t.DayIn7, " +
				"t.Open1, t.Open2, t.Open3, t.Open4, t.Open5, t.Open6, t.Open7, " +
				"t.Close1, t.Close2, t.Close3, t.Close4, t.Close5, t.Close6, t.Close7 " +
				"FROM " + NAME_UA + " tua " +
				"JOIN " + NAME + " t WHERE t.CODE=tua.code AND tua." + Column.CODE +
			" AND ((" + bounds.southwest.latitude + "<=lat and lat<=" + bounds.northeast.latitude + ") " +
			"AND (" + bounds.southwest.longitude + "<=lng and lng<=" + bounds.northeast.longitude + ")) ORDER BY number, type", null);
	}

	public Cursor selectByCSA(String city, String state, String area, String notIn)
	{
      return sqlDB.rawQuery("SELECT * FROM " + NAME + " WHERE " + Column.STATE + "=\"" + state+ "\" AND " +
        Column.AREA + "=\"" + area + "\" AND " +
        Column.CITY + "=\"" + city + "\" AND " +
        Column.CODE + notIn + " ORDER BY " + Column.NUMBER + ", " + Column.TYPE
        ,null);
	}

	public Cursor selectByCSAUA(String city, String state, String area, String notIn)
	{
		return sqlDB.rawQuery("SELECT tua.Code, tua.City, t.DublicateCity, t.CityType, tua.State, tua.Address, tua.Area, t.Lat, t.lng, t.type, t.TypeId, t.number, t.Capacity,  t.phone, " +
			"t.MaxLength, t.DayIn1, t.DayIn2, t.DayIn3, t.DayIn4, t.DayIn5, t.DayIn6, t.DayIn7, " +
			"t.Open1, t.Open2, t.Open3, t.Open4, t.Open5, t.Open6, t.Open7, " +
			"t.Close1, t.Close2, t.Close3, t.Close4, t.Close5, t.Close6, t.Close7 " +
			"FROM " + NAME_UA + " tua " +
			"JOIN " + NAME + " t WHERE t.CODE=tua.code AND tua." + Column.CODE + " AND tua." +
			Column.STATE + "=\"" + state+ "\" AND tua." +
			Column.AREA + "=\"" + area + "\" AND tua." +
			Column.CITY + "=\"" + city + "\" AND tua." +
			Column.CODE + notIn + " ORDER BY t." + Column.NUMBER + ", t." + Column.TYPE,
			null);
	}

	public Cursor selectByBoundsMinZoom(LatLngBounds bounds)
	{
		return sqlDB.rawQuery("Select * from " + NAME + " where " +
			Column.AREA + " LIKE '%городское подчинение%' AND " +
			Column.CITY_TYPE + "=0 AND " +
			"(" + bounds.southwest.latitude + "<=lat and lat<=" + bounds.northeast.latitude + ") " +
			"AND (" + bounds.southwest.longitude + "<=lng and lng<=" + bounds.northeast.longitude + ") ORDER BY number, type", null);
	}

	public Cursor selectByBoundsUAMinZoom(LatLngBounds bounds)
	{
		return sqlDB.rawQuery("SELECT tua.Code, tua.City, t.DublicateCity, t.CityType, tua.State, tua.Address, " +
			"tua.Area, t.Lat, t.lng, t.type, t.TypeId, t.number, t.Capacity,  t.phone, " +
			"t.MaxLength, t.DayIn1, t.DayIn2, t.DayIn3, t.DayIn4, t.DayIn5, t.DayIn6, t.DayIn7, " +
			"t.Open1, t.Open2, t.Open3, t.Open4, t.Open5, t.Open6, t.Open7, " +
			"t.Close1, t.Close2, t.Close3, t.Close4, t.Close5, t.Close6, t.Close7 " +
			"FROM " + NAME_UA + " tua " +
			"JOIN " + NAME + " t WHERE t.CODE=tua.code" + /*" AND t." +
			Column.AREA + " LIKE '%городское подчинение%' AND t." +*/
			" AND t." + Column.CITY_TYPE + "=0 " +
			" AND ((" + bounds.southwest.latitude + "<=lat and lat<=" + bounds.northeast.latitude + ") " +
			"AND (" + bounds.southwest.longitude + "<=lng and lng<=" + bounds.northeast.longitude + ")) ORDER BY number, type", null);
	}

	public Cursor selectByCSAMinZoom(String city, String state, String area, String notIn)
	{
		return sqlDB.rawQuery("SELECT * FROM " + NAME + " WHERE " + Column.STATE + "='" + state+ "' AND " +
//				Column.AREA + " LIKE '%городское подчинение%' AND " +
				Column.CITY_TYPE + "=0 AND " +
				Column.CITY + "='" + city + "' AND " +
				Column.CODE + notIn +
				" ORDER BY " + Column.NUMBER + ", " + Column.TYPE
			,null);
	}

	public Cursor selectByCSAUAMinZoom(String city, String state, String area, String notIn)
	{
		return sqlDB.rawQuery("SELECT tua.Code, tua.City, t.DublicateCity, t.CityType, tua.State, tua.Address, " +
				"tua.Area, t.Lat, t.lng, t.type, t.TypeId, t.number, t.Capacity,  t.phone, " +
				"t.MaxLength, t.DayIn1, t.DayIn2, t.DayIn3, t.DayIn4, t.DayIn5, t.DayIn6, t.DayIn7, " +
				"t.Open1, t.Open2, t.Open3, t.Open4, t.Open5, t.Open6, t.Open7, " +
				"t.Close1, t.Close2, t.Close3, t.Close4, t.Close5, t.Close6, t.Close7 " +
				"FROM " + NAME_UA + " tua " +
				"JOIN " + NAME + " t WHERE t.CODE=tua.code AND tua." +
				Column.STATE + "=\"" + state+ "\"" +
//				" AND tua." +	Column.AREA + " LIKE \"%городское подчинение%\" AND " +
				" AND " + Column.CITY_TYPE + "=0 AND tua." +
				Column.CITY + "=\"" + city + "\" AND tua." +
				Column.CODE + notIn + " ORDER BY t." + Column.NUMBER + ", t." + Column.TYPE,
			null);
	}

	public Cursor selectByAddress(String sk[])
	{
		StringBuilder sbQuery = new StringBuilder("SELECT * FROM " + NAME + " WHERE ");
		for (int i = 0 ; i <= sk.length - 1; i++){
			sbQuery.append(Column.FULL_ADDRESS + " like \"%" + checkAndEscape(sk[i]) + "%\"");
			if (i < sk.length - 1)
				sbQuery.append(" AND ");
		}
		sbQuery.append(" AND TypeId!=1 ORDER BY " + Column.CITY_TYPE);
		return sqlDB.rawQuery(sbQuery.toString(),
			null);
	}

	public Cursor selectByAddressSingle(String searchString)
	{
	    searchString = checkAndEscape(searchString);
		StringBuilder sbQuery = new StringBuilder("SELECT * FROM " + NAME + " WHERE TypeId!=1 AND ");
		sbQuery.append(Column.FULL_ADDRESS + " like \"" + searchString + "%\"");
		sbQuery.append(" ORDER BY " + Column.TYPE + " DESC, " + Column.NUMBER);
		return sqlDB.rawQuery(sbQuery.toString(),
			null);
	}

  public Cursor selectByAddressSingleUA(String searchString)
  {
      searchString = checkAndEscape(searchString);
    StringBuilder sbQuery = new StringBuilder("SELECT tua.Code, tua.City, t.DublicateCity, t.CityType, tua.State," +
			"tua.Address, tua.FullAddress, t.Lat, t.lng, t.type, t.TypeId, t.number, t.Capacity,  t.phone, " +
      "t.MaxLength, t.DayIn1, t.DayIn2, t.DayIn3, t.DayIn4, t.DayIn5, t.DayIn6, t.DayIn7, " +
      "t.Open1, t.Open2, t.Open3, t.Open4, t.Open5, t.Open6, t.Open7, " +
      "t.Close1, t.Close2, t.Close3, t.Close4, t.Close5, t.Close6, t.Close7 " +
      "FROM " + NAME_UA + " tua " +
      "JOIN " + NAME + " t WHERE t.CODE=tua.code AND t.TypeID!=1 AND ");
    sbQuery.append("tua." + Column.FULL_ADDRESS + " like \"" + searchString + "%\"");
    sbQuery.append(" ORDER BY t." + Column.TYPE + " DESC, t." + Column.NUMBER);
    return sqlDB.rawQuery(sbQuery.toString(),
      null);
  }

	public Cursor selectByStreetSingle(String searchString)
	{
	    searchString = checkAndEscape(searchString);
		StringBuilder sbQuery = new StringBuilder("SELECT * FROM " + NAME + " WHERE TypeId!=1 AND ");
		sbQuery.append(Column.ADDRESS + " like \"%" + searchString + "%\"");
		sbQuery.append(" ORDER BY " + Column.TYPE + " DESC, " + Column.NUMBER);
		return sqlDB.rawQuery(sbQuery.toString(),
			null);
	}

	public Cursor selectByStreetSingleUA(String searchString)
	{
	    searchString = checkAndEscape(searchString);
		StringBuilder sbQuery = new StringBuilder("SELECT tua.Code, tua.City, t.DublicateCity, t.CityType, tua.State, " +
			"tua.Address, tua.FullAddress, t.Lat, t.lng, t.type, t.TypeId, t.number, t.Capacity,  t.phone, " +
			"t.MaxLength, t.DayIn1, t.DayIn2, t.DayIn3, t.DayIn4, t.DayIn5, t.DayIn6, t.DayIn7, " +
			"t.Open1, t.Open2, t.Open3, t.Open4, t.Open5, t.Open6, t.Open7, " +
			"t.Close1, t.Close2, t.Close3, t.Close4, t.Close5, t.Close6, t.Close7 " +
			"FROM " + NAME_UA + " tua " +
			"JOIN " + NAME + " t WHERE t.CODE=tua.code AND t.TypeID!=1 AND ");
		sbQuery.append("tua." + Column.ADDRESS + " like \"%" + searchString + "%\"");
		sbQuery.append(" ORDER BY t." + Column.TYPE + " DESC, t." + Column.NUMBER);
		return sqlDB.rawQuery(sbQuery.toString(),
			null);
	}

	public Cursor selectByAddressUA(String sk[])
	{
		StringBuilder sbQuery = new StringBuilder("SELECT tua.Code, tua.City, t.DublicateCity, t.CityType, tua.State, " +
			"tua.Address, tua.FullAddress, t.Lat, t.lng, t.type, t.TypeId, t.number, t.Capacity,  t.phone, " +
			"t.MaxLength, t.DayIn1, t.DayIn2, t.DayIn3, t.DayIn4, t.DayIn5, t.DayIn6, t.DayIn7, " +
			"t.Open1, t.Open2, t.Open3, t.Open4, t.Open5, t.Open6, t.Open7, " +
			"t.Close1, t.Close2, t.Close3, t.Close4, t.Close5, t.Close6, t.Close7 " +
			"FROM " + NAME_UA + " tua " +
			"JOIN " + NAME + " t WHERE t.CODE=tua.code AND t.TypeID!=1 AND ");
		for (int i = 0 ; i <= sk.length - 1; i++){
			sbQuery.append("tua." + Column.FULL_ADDRESS + " like \"%" + checkAndEscape(sk[i]) + "%\"");
			if (i < sk.length - 1)
				sbQuery.append(" AND ");
		}
		sbQuery.append(" ORDER BY " + "t." + Column.CITY_TYPE);
		return sqlDB.rawQuery(sbQuery.toString(),
			null);
	}

	public Cursor selectByAddress(String address){
	    address= checkAndEscape(address);
		String searchString;
		if (address != null && address.length() > 0)
			searchString = address.replaceFirst(String.valueOf(address.charAt(0)), String.valueOf(address.charAt(0)).toUpperCase());
		else
			searchString = "";
		return sqlDB.query(VIEW_NAME, null, Column.ADDRESS + " like '%" + searchString + "%'", null, null, null, Column.ADDRESS);
	}

	public Cursor getCount()
	{
		return sqlDB.rawQuery("SELECT count(1) FROM " + NAME, null);
	}

	@Override
	public boolean exists(String fieldName, String fieldValue) {
		Cursor slCursor = sqlDB.query(VIEW_NAME, null, fieldName + "=?", new String[]{fieldValue}, null, null, null);
    	if (slCursor.getCount()>0)
    	{
    		slCursor.close();
    		return true;
    	}
    	else
    	{
    		slCursor.close();
    		return false;
    	}
	}

	public void clearTable()
  {
    sqlDB.delete(NAME, null, null);
		sqlDB.execSQL("DROP INDEX IF EXISTS " + Index.ADDRESS);
		sqlDB.execSQL("DROP INDEX IF EXISTS " + Index.CODE);
		sqlDB.execSQL("DROP INDEX IF EXISTS " + Index.LAT);
		sqlDB.execSQL("DROP INDEX IF EXISTS " + Index.LNG);
  }

	public void clearTableUA()
	{
		sqlDB.delete(NAME_UA, null, null);
		sqlDB.execSQL("DROP INDEX IF EXISTS " + Index.ADDRESS_UA);
		sqlDB.execSQL("DROP INDEX IF EXISTS " + Index.CODE_UA);
	}

	public void creatIndex() {
		sqlDB.execSQL(WarehouseDetailsTable.SQL_CREATE_INDEX_ADDRESS);
		sqlDB.execSQL(WarehouseDetailsTable.SQL_CREATE_INDEX_CODE);
		sqlDB.execSQL(WarehouseDetailsTable.SQL_CREATE_INDEX_LAT);
		sqlDB.execSQL(WarehouseDetailsTable.SQL_CREATE_INDEX_LNG);
	}

	public void creatIndexUA() {
		sqlDB.execSQL(WarehouseDetailsTable.SQL_CREATE_INDEX_ADDRESS_UA);
		sqlDB.execSQL(WarehouseDetailsTable.SQL_CREATE_INDEX_CODE_UA);
	}
	
	public void insert(String code, String Lat, String Lng,
											String type,
											String typeId,
											String number,
											String capacity,
										 	String city,
										 	String city_type,
											String state,
										 String area,
										 	String address,
											String phone,
										 String max_length,
											String dayin1,
										 String dayin2,
										 String dayin3,
										 String dayin4,
										 String dayin5,
										 String dayin6,
										 String dayin7,
											String open1,
											String close1,
										 String open2,
										 String close2,
										 String open3,
										 String close3,
										 String open4,
										 String close4,
										 String open5,
										 String close5,
										 String open6,
										 String close6,
										 String open7,
										 String close7)
    {
    	  try
    	  {
					StringBuilder sbFullAddress = new StringBuilder();
					sbFullAddress.append(city);
					sbFullAddress.append(" ");
					sbFullAddress.append(address);
					sbFullAddress.append(" ");
					if (type.compareTo("store")==0)
						sbFullAddress.append("отделение");
					else
						sbFullAddress.append("почтомат");
					sbFullAddress.append(" ");
					sbFullAddress.append(number);
      	  insertStatement.clearBindings();
          DictionariesDB.bindLong(insertStatement, 1, code);
					DictionariesDB.bindDouble(insertStatement, 2, Lat);
					DictionariesDB.bindDouble(insertStatement, 3, Lng);
          DictionariesDB.bindString(insertStatement, 4, type);
					DictionariesDB.bindLong(insertStatement, 5, typeId);
          DictionariesDB.bindLong(insertStatement, 6, number);
          DictionariesDB.bindLong(insertStatement, 7, capacity);
          DictionariesDB.bindString(insertStatement, 8, city);
					DictionariesDB.bindLong(insertStatement, 9, city_type);
          DictionariesDB.bindString(insertStatement, 10, state);
					DictionariesDB.bindString(insertStatement, 11, area);
          DictionariesDB.bindString(insertStatement, 12, address);
					DictionariesDB.bindString(insertStatement, 13, sbFullAddress.toString().toLowerCase()); // full address
          DictionariesDB.bindString(insertStatement, 14, phone);
					DictionariesDB.bindString(insertStatement, 15, max_length);
          DictionariesDB.bindString(insertStatement, 16, dayin1);
					DictionariesDB.bindString(insertStatement, 17, dayin2);
					DictionariesDB.bindString(insertStatement, 18, dayin3);
					DictionariesDB.bindString(insertStatement, 19, dayin4);
					DictionariesDB.bindString(insertStatement, 20, dayin5);
					DictionariesDB.bindString(insertStatement, 21, dayin6);
					DictionariesDB.bindString(insertStatement, 22, dayin7);
          DictionariesDB.bindString(insertStatement, 23, open1);
          DictionariesDB.bindString(insertStatement, 24, close1);
					DictionariesDB.bindString(insertStatement, 25, open2);
					DictionariesDB.bindString(insertStatement, 26, close2);
					DictionariesDB.bindString(insertStatement, 27, open3);
					DictionariesDB.bindString(insertStatement, 28, close3);
					DictionariesDB.bindString(insertStatement, 29, open4);
					DictionariesDB.bindString(insertStatement, 30, close4);
					DictionariesDB.bindString(insertStatement, 31, open5);
					DictionariesDB.bindString(insertStatement, 32, close5);
					DictionariesDB.bindString(insertStatement, 33, open6);
					DictionariesDB.bindString(insertStatement, 34, close6);
					DictionariesDB.bindString(insertStatement, 35, open7);
					DictionariesDB.bindString(insertStatement, 36, close7);
          insertStatement.executeInsert();
    	  }
    	  catch(Exception e)
    	  {
    	    Log.d("XML", "Error insert wh: " + e.getMessage());
    	  }
    }

	public void insertUA(String code,
										 String city,
										 String state,
											String area,
										 String address,
										 String type,
											 String number
										 )
	{
		try
		{
			StringBuilder sbFullAddress = new StringBuilder();
			sbFullAddress.append(city);
			sbFullAddress.append(" ");
			sbFullAddress.append(address);
			sbFullAddress.append(" ");
			if (type.compareTo("store")==0)
				sbFullAddress.append("відділення");
			else
				sbFullAddress.append("поштомат");
			sbFullAddress.append(" ");
			sbFullAddress.append(number);
			insertStatementUA.clearBindings();
			DictionariesDB.bindLong(insertStatementUA, 1, code);
			DictionariesDB.bindString(insertStatementUA, 2, city);
			DictionariesDB.bindString(insertStatementUA, 3, state);
			DictionariesDB.bindString(insertStatementUA, 4, area);
			DictionariesDB.bindString(insertStatementUA, 5, address);
			DictionariesDB.bindString(insertStatementUA, 6, sbFullAddress.toString().toLowerCase()); // full address
			insertStatementUA.executeInsert();
		}
		catch(Exception e)
		{
			Log.d("XML", "Error insert wh: " + e.getMessage());
		}
	}

	public void markDublicates()
	{
		sqlDB.execSQL("update tblWarehouseDetail " +
		"set " + Column.DUBLICATE_CITY + "=1 " +
		" where _id in(" +
		"select ct._id from tblWarehouseDetail ct " +
		"INNER JOIN " +
		"(SELECT ct2._id, ct2.city, ct2.state FROM tblWarehouseDetail ct2 GROUP BY ct2.city, ct2.State HAVING ( COUNT(*) > 1 )) ct1 ON ct.city = ct1.city AND ct.State=ct1.State " +
		"AND EXISTS (select ct._id, ct.city from tblCities ct3 " +
		"where ct3.city=ct1.city GROUP BY city HAVING COUNT(*) >1));");
	}


//  select for search
//
//	select * from  tblWarehouseDetail where  city like '%Запоро%'  AND  number=1
//	union ALL
//	select * from tblWarehouseDetail where city like '%Запоро%'  AND  number like '%1%'  AND  type='store'
//	union all
//	select * from tblWarehouseDetail where city like '%Запоро%'  AND  ((address like '%1%' OR number  like '%1%') AND type="store" )
//	ORDER BY CITYTYPE, TYPE DESC

// prototype
// select * from tblWarehouseDetail where fullAddress like '%Запоро%'  AND  fullAddress like '%1%'  AND  fullAddress like '%отделение%'

//  select for search 1
//
//	select * from  tblWarehouseDetail where  city like 'Кие%'  AND  number=6 AND  type='store'
//  union all
//	select * from tblWarehouseDetail where city like '%Киев%'  AND  address like '%6%'  AND  type='store'
//	union all
//	select * from tblWarehouseDetail where city like '%Киев%'  AND  ((address like '%6%' OR number  like '%6%') AND type='store' )
//	union all
//	select * from tblWarehouseDetail where fullAddress like '%Киев%'  AND  fullAddress like '%6%' ; if (store) <- add type='store'

}
