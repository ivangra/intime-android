package com.ua.inr0001.intime;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.widget.EditText;

public class MyEditText extends EditText {

  public interface EditTextImeBackListener {
    public abstract void onImeBack(MyEditText ctrl, String text);
  }
  
  private EditTextImeBackListener mOnImeBack;

  @Override
  public boolean onKeyPreIme(int keyCode, KeyEvent event) {
    if (event.getKeyCode() == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
      if (mOnImeBack != null) mOnImeBack.onImeBack(this, this.getText().toString());
    }
    return super.dispatchKeyEvent(event);
  }

  public MyEditText(Context context) {
    super(context);
  }

  public MyEditText(Context context,AttributeSet attr) {
    super(context,attr);
  }

  public void setOnEditTextImeBackListener(EditTextImeBackListener listener) {
      mOnImeBack = listener;
  }

  
}
