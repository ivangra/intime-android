package com.ua.inr0001.intime;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import intime.gps.location.helpers.ISendLocation;
import intime.gps.location.helpers.LocationListenerServices;
import intime.llc.ua.R;

public class CurrierMapActivity extends AppCompatActivity implements ISendLocation, OnMapReadyCallback, GoogleApiClient.OnConnectionFailedListener {

  private SupportMapFragment mapFragment;
  private Button btnSelectAddress;
  private GoogleMap mMap123 = null;
  private Context context;
  private String Lat = "50.435383";
  private String Lng = "30.546447";
  private Marker myLocation;
  private Location locMyLocation = null;
  private LatLng selectedLatLng;
  private Marker selectedLocation;
  private static final String TAG = "SelectOnMap";
  private GoogleApiClient mGoogleApiClient;
  private AddressResultReceiver mResultReceiver;
  private String street;
  private String city;
  private String streetNumber;

  @Override
  protected void onStop() {
    super.onStop();
    mGoogleApiClient.disconnect();
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    mGoogleApiClient =new GoogleApiClient
      .Builder(this)
      .addApi(Places.GEO_DATA_API)
      .addApi(Places.PLACE_DETECTION_API)
      .enableAutoManage(this, this)
      .build();
    mGoogleApiClient.connect();

    setContentView(R.layout.activity_currier_map);
    context = this;
    Toolbar toolbar = (Toolbar) findViewById(R.id.selectCityToolbar);
    toolbar.setTitle("");
    setSupportActionBar(toolbar);
    toolbar.setNavigationOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        onBackPressed();
      }
    });
    btnSelectAddress = (Button) findViewById(R.id.btnSelectAddress);
    btnSelectAddress.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Intent intent = new Intent();
        intent.putExtra("City", city);
        intent.putExtra("Street", street);
        intent.putExtra("StreetNumber", streetNumber);
        setResult(RESULT_OK, intent);
        finish();
      }
    });
    initMap();
  }

  protected void startIntentService(Location selectedLocation) {
    mResultReceiver = new AddressResultReceiver(new Handler());
    Intent intent = new Intent(this, FetchAddressIntentService.class);
    intent.putExtra(Constants.RECEIVER, mResultReceiver);
    intent.putExtra(Constants.LOCATION_DATA_EXTRA, selectedLocation);
    startService(intent);
  }

  @Override
  public void sendLocation(Location location) {
    Log.d(TAG, Double.toString(location.getLongitude()) + " " + Double.toString(location.getLatitude()));

    if (myLocation != null)
      myLocation.remove();

    if (location != null) {
      myLocation = mMap123.addMarker(new MarkerOptions()
        .position(new LatLng(location.getLatitude(), location.getLongitude()))
        .title(context.getResources().getString(R.string.your_location))
        .icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_my_location)));

      myLocation.showInfoWindow();

      gotoMarker(location.getLatitude(), location.getLongitude(), 12);
      locMyLocation = location;
    } else {
      Log.d(TAG, "Location listener: wrong location");
    }
  }

  private void startLocation() {
    new LocationListenerServices(context, this);
  }

  private void gotoMarker(Double latitude, Double longitude, float zoom) {
    CameraPosition cameraPosition = new CameraPosition.Builder()
      .target(new LatLng(latitude, longitude))
      .zoom(zoom)
      .bearing(0)
      .tilt(0)
      .build();
    CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
    mMap123.moveCamera(cameraUpdate);
  }

  private void initMap() {
    try {
      mapFragment = (SupportMapFragment) getSupportFragmentManager()
        .findFragmentById(R.id.frMapDeps);
      mapFragment.getMapAsync(this);
    } catch (Exception e) {
    }
  }

  @Override
  public void onMapReady(GoogleMap map) {
    mMap123 = map;
    map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
    map.getUiSettings().setCompassEnabled(false);
    map.getUiSettings().setZoomControlsEnabled(false);
    map.getUiSettings().setMyLocationButtonEnabled(false);
    map.setMyLocationEnabled(false);
    CameraPosition cameraPosition = new CameraPosition.Builder()
      .target(new LatLng(Double.parseDouble(Lat), Double.parseDouble(Lng)))
      .zoom(13)
      .bearing(0)
      .tilt(0)
      .build();
    CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
    map.moveCamera(cameraUpdate);
    map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
      @Override
      public View getInfoWindow(Marker marker) {
        View v = getLayoutInflater().inflate(R.layout.marker_title, null);
        TextView title = (TextView) v.findViewById(R.id.tvMarkerTitle);
        title.setText(marker.getTitle());
        return v;
      }

      @Override
      public View getInfoContents(Marker marker) {
        return null;
      }
    });
    map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
      @Override
      public void onMapClick(LatLng latLng) {
        selectedLatLng = latLng;
        Location mLocation = new Location("selectedLocation");
        mLocation.setLatitude(latLng.latitude);
        mLocation.setLongitude(latLng.longitude);
        startIntentService(mLocation);
        gotoMarker(latLng.latitude, latLng.longitude, 17);
      }
    });
    if (GlobalApplicationData.getInstance().isGPSEnabled())
      startLocation();
  }

  @Override
  public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    Log.d(TAG, "Conection error: " + connectionResult.getErrorMessage());
  }

  class AddressResultReceiver extends ResultReceiver {
    public AddressResultReceiver(Handler handler) {
      super(handler);
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {

      Log.d(TAG, "Address found!");
      // Display the address string
      // or an error message sent from the intent service.
      String mAddressOutput = resultData.getString("City") +  " " + resultData.getString("Street") + " " + resultData.getString("StreetNumber");
      //displayAddressOutput();

      // Show a toast message if an address was found.
      if (resultCode == Constants.SUCCESS_RESULT) {
        //Log.d(TAG, "Address found:" + mAddressOutput);

        street = resultData.getString("Street");
        streetNumber = resultData.getString("StreetNumber");
        city = resultData.getString("City");

        if (selectedLocation!=null)
          selectedLocation.remove();
        selectedLocation = mMap123.addMarker(new MarkerOptions()
          .position(selectedLatLng)
          .title(mAddressOutput)
          .icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_selected_department)));

        selectedLocation.showInfoWindow();

        //showToast(getString(R.string.address_found));
      }

    }
  }

}
