package com.ua.inr0001.intime;

/**
 * Created by ILYA on 25.01.2017.
 */

public class DepartmentMapItem {

  public String depType;
  public String depNumber;
  public String depWeight;
  public String depAddress;
  public String longitude;
  public String latitude;
  public String workHoursStart;
  public String workHoursEnd;
  public String cityName;
  public String state;
  public int dublicate;
  public double distance;
  public int weightLimit;
  public String code;
  public WorkHoursData workHoursData;
  public String fullAddress;
  public String lengthLimit;

}
