package com.ua.inr0001.intime;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.gsm.SmsMessage;

public class SMSReciever extends BroadcastReceiver {

	final SmsManager sms = SmsManager.getDefault();
	
	private void parseSMS(String message)
	{
  	if (message != null && message.length() >= 4)
    {
      Intent intent = new Intent(RegisterUserActivity.GET_SMS);
      String code = message.substring(0, 4);
      intent.putExtra("sms_code", code.trim());
      GlobalApplicationData.getInstance().getContext().sendBroadcast(intent);
    }
	}
	
	@Override
	public void onReceive(Context arg0, Intent intent) {
		
		// Retrieves a map of extended data from the intent.
        final Bundle bundle = intent.getExtras();
 
        try {
             
            if (bundle != null) {
                 
                final Object[] pdusObj = (Object[]) bundle.get("pdus");

                for (int i = 0; i < pdusObj.length; i++) {
                     
                    SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                    String phoneNumber = currentMessage.getDisplayOriginatingAddress();
                     
                    String senderNum = phoneNumber;
                    String message = currentMessage.getDisplayMessageBody();
                    parseSMS(message);
                     
                } // end for loop
              } // bundle is null
 
        } catch (Exception e) {

        }
		
	}

}
