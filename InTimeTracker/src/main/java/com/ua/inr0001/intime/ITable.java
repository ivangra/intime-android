package com.ua.inr0001.intime;

import android.database.Cursor;

public interface ITable {

	public Cursor selectByID(int Id);
	public Cursor selectByName(String Name);
	public Cursor selectAll();
	public boolean exists(String fieldName, String fieldValue);
}
