package com.ua.inr0001.intime;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import intime.llc.ua.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class MainMenuFragment extends Fragment {

  private FragmentActivity activity;
  private Context activityContext;
  private TextView tvNotifyCount;
  private LinearLayout layoutMenuCallback;
  private LinearLayout layoutMenuProfile;
  private LinearLayout layoutPushHistory;
  private LinearLayout layoutMyAddress;
  private LinearLayout layoutAbout;
  private Button btnSignUp;
  private TextView tvUserName;
  private TextView tvUserStatus;
  private TextView tvLogout;
  //private TextView tvMenuMyAddress;


  public MainMenuFragment() {
    // Required empty public constructor
  }


  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    activityContext = context;
    //initNotifyCount();
  }

  public void configureMenu()
  {
    if (GlobalApplicationData.getInstance().getUserInfo().getName().length()>0) {
      btnSignUp.setVisibility(View.GONE);
      tvUserName.setText(GlobalApplicationData.getInstance().getUserInfo().getName());
      layoutMyAddress.setVisibility(View.VISIBLE);
      layoutMenuProfile.setVisibility(View.VISIBLE);
      tvLogout.setVisibility(View.VISIBLE);
    }
    else
    {
      btnSignUp.setVisibility(View.VISIBLE);
      layoutMyAddress.setVisibility(View.GONE);
      layoutMenuProfile.setVisibility(View.GONE);
      tvLogout.setVisibility(View.INVISIBLE);
      tvUserName.setVisibility(View.VISIBLE);
    }
  }

  @Override
  public void onResume() {
    super.onResume();
    configureMenu();
  }

  private void initNotifyCount()
  {
    int count = InTimeDB.getInstance(activity).tablePushHistory.getCountPush();
    if (count>0) {
      tvNotifyCount.setText(Integer.toString(count));
      tvNotifyCount.setVisibility(View.VISIBLE);
    }
    else
      tvNotifyCount.setVisibility(View.INVISIBLE);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {

    activity = getActivity();

    View view = inflater.inflate(R.layout.activity_main_menu, container, false);

    layoutMenuCallback = (LinearLayout) view.findViewById(R.id.layoutCallBack);
    layoutMenuCallback.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        startActivity(new Intent(activityContext, ContactsActivity.class));
        //MainFragmentManager.pushFragment(activity, new ContactsFragment(), "ContactsFragment");
      }
    });

    layoutMenuProfile = (LinearLayout) view.findViewById(R.id.layoutProfile);
    layoutMenuProfile.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        startActivity(new Intent(activityContext, ProfileActivity.class));
        //MainFragmentManager.pushFragment(activity, new ProfileFragment(), "ProfileFragment");
      }
    });

    layoutPushHistory = (LinearLayout) view.findViewById(R.id.layoutPushHistory);
    layoutPushHistory.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        startActivity(new Intent(activityContext, PushHistoryActivity.class));
        //MainFragmentManager.pushFragment(activity, new PushHistoryFragment(), "PushHistoryFragment");
      }
    });

    layoutMyAddress = (LinearLayout) view.findViewById(R.id.layoutMyAddress);
    layoutMyAddress.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        startActivity(new Intent(activityContext, MyAddressActivity.class));
        //MainFragmentManager.pushFragment(activity, new MyAddressFragment(), "MyAddressFragment");
      }
    });

    layoutAbout = (LinearLayout) view.findViewById(R.id.layoutMenuAbout);
    layoutAbout.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        startActivity(new Intent(activityContext, AboutActivity.class));
      }
    });

    tvNotifyCount = (TextView) view.findViewById(R.id.tvPushNumber);
    tvUserName = (TextView) view.findViewById(R.id.tvUserName);
    tvUserStatus = (TextView) view.findViewById(R.id.tvUserStatus);
    tvLogout = (TextView) view.findViewById(R.id.tvLogout);

    tvLogout.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
//        GlobalApplicationData.getInstance().setContragentID("");
//        GlobalApplicationData.getInstance().setContragentKEY("");
//        GlobalApplicationData.getInstance().setContragentNAME("");
//        GlobalApplicationData.getInstance().saveAuthorizationData();
        configureMenu();
      }
    });

    /*tvMenuMyAddress = (TextView) view.findViewById(R.id.tvMenuProfule);
    tvMenuMyAddress.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        pushFragment(new ProfileFragment(), "ProfileFragment");
      }
    });*/

    initNotifyCount();

    btnSignUp = (Button) view.findViewById(R.id.btnSignUp);

    btnSignUp.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        /*Fragment tracker = new AutorizationActivity();
        FragmentTransaction fTrans = getActivity().getSupportFragmentManager().beginTransaction();
        fTrans.add(R.id.frActivities, tracker);
        fTrans.commit();*/
        startActivity(new Intent(activityContext, AutorizationActivity.class));
      }
    });

    //configureMenu();

    return view;
  }

  //@FromXML
  //public void btnSignUpClick(View v)
  //{
  //  startActivity(new Intent(activityContext, AutorizationActivity.class));
  //}

}
