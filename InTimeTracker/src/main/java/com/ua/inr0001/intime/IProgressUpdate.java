package com.ua.inr0001.intime;

/**
 * Created by ILYA on 18.10.2016.
 */

public interface IProgressUpdate {

  void update(final String... values);
  void setMaxProgress(int max);

}
