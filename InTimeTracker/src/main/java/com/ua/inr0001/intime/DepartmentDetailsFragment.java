package com.ua.inr0001.intime;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import intime.llc.ua.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class DepartmentDetailsFragment extends Fragment {

  private Context context;
  private Toolbar toolbar;

  public void setCode(String code) {
    this.code = code;
  }

  private String code;

  public DepartmentDetailsFragment() {
    // Required empty public constructor
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    context = getActivity();
    View view = inflater.inflate(R.layout.fragment_department_details, container, false);
    toolbar = (Toolbar) view.findViewById(R.id.toolbarDepDetails);
    ((AppCompatActivity)getActivity()).setTitle("");
    //((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
    toolbar.setNavigationOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        getActivity().onBackPressed();
      }
    });
    DepartmentDetailsView depView = new DepartmentDetailsView(context, null, view);
    depView.selectDepDetails(code);
    return view;
  }

}
