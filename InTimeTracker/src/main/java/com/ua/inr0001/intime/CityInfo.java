package com.ua.inr0001.intime;

public class CityInfo {

  private String mCityCode = "";
	private String mCityName = ""; // название города
	private String mDeliveryCode = ""; 
	private String mPresenceWarehouse = ""; // признак наличия склада
	private String mState = ""; // область
	private String mArea = ""; // район
	
	public String getCityCode() {
    return mCityCode;
  }

  public void setCityCode(String mCityCode) {
    this.mCityCode = mCityCode;
  }

  public String getState() {
    return mState;
  }

  public void setState(String mState) {
    this.mState = mState;
  }

  public String getArea() {
    return mArea;
  }

  public void setArea(String mArea) {
    this.mArea = mArea;
  }
  
  public String getPresenceWarehouse() {
    return mPresenceWarehouse;
  }

  public void setPresenceWarehouse(String mPresenceWarehouse) {
    this.mPresenceWarehouse = mPresenceWarehouse;
  }

  public String getCityName() {
		return mCityName;
	}

	public void setCityName(String cityName) {
		this.mCityName = cityName;
	}

	public String getDeliveryCode() {
		return mDeliveryCode;
	}

	public void setDeliveryCode(String mDeliveryCode) {
		this.mDeliveryCode = mDeliveryCode;
	}

}
