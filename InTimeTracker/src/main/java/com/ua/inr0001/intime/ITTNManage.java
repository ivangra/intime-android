package com.ua.inr0001.intime;

/**
 * Created by technomag on 15.11.17.
 */

public interface ITTNManage {

  void showDeleteHint(int deleteType);
  void initTTNHistoryRecyclerView();

}
