package com.ua.inr0001.intime;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.CheckBox;

import intime.llc.ua.R;

public class ConfidentialityActivity extends AppCompatActivity {

  private CheckBox chAccept;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_confidentiality);
    Toolbar toolbar = (Toolbar) findViewById(R.id.confidentialityToolbar);
    setSupportActionBar(toolbar);

    WebView webView = (WebView) findViewById(R.id.wvConfidentiality);
    webView.loadDataWithBaseURL(null, getResources().getString(R.string.canfidentiality_text), "text/html", "utf-8", null);
    Button btnAccept = (Button) findViewById(R.id.btnAccept);
    chAccept = (CheckBox) findViewById(R.id.chAccept);
    chAccept.setChecked(false);
    if (GlobalApplicationData.getInstance().confidentialityReaded) {
      btnAccept.setVisibility(View.GONE);
      chAccept.setVisibility(View.GONE);
    }
    btnAccept.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (chAccept.isChecked()) {
          GlobalApplicationData.getInstance().saveConfidentiality(true);
          finish();
        }
      }
    });
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();
    switch (id)
    {
      case R.id.action_close: onBackPressed(); return true;
      default: return super.onOptionsItemSelected(item);
    }
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.close_action_menu, menu);
    return true; //super.onCreateOptionsMenu(menu);
  }

}
