package com.ua.inr0001.intime;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import intime.api.model.currier.Currier;
import intime.llc.ua.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CallCurrierActivity extends AppCompatActivity {

  private Context context;
  private Button btnCallCurrierBack;
  private Button btnSendCallCurrier;

  private MailCallCurrier mailCallCurrier;

  //private TextView tvAddress;
  private Call<Currier> call;

  //private TextView tvWeight;
  private TextView tvDate;
  private TextView tvPhone;

  private void showMessage(String caption, String message)
  {
    MessageDialog dlgWarning = new MessageDialog();
    dlgWarning.setParams(caption, message, false, null);
    dlgWarning.show(getSupportFragmentManager(), "Warning");
  }

  private void doRequest()
  {
    btnSendCallCurrier.setEnabled(false);
    btnCallCurrierBack.setEnabled(false);
    Retrofit retrofit = new Retrofit.Builder()
      .baseUrl("https://newapi.intime.ua/")
      .addConverterFactory(GsonConverterFactory.create())
      //.addConverterFactory(createGsonConverter())
      .client(GlobalApplicationData.getUnsafeOkHttpClient())
      .build();

    IRtfAPI service = retrofit.create(IRtfAPI.class);

    mailCallCurrier = MailCallCurrier.getInstance();

    call = service.currier(GlobalApplicationData.getCurrentLanguage(),
      mailCallCurrier.getFullName(),
      mailCallCurrier.getPhone(),
      mailCallCurrier.getDateSend(),
      mailCallCurrier.getComfortableTime(),
      mailCallCurrier.getCityCode(),
      mailCallCurrier.getCurrierFromStreet(),
      mailCallCurrier.getCurrierFromHouse(),
      mailCallCurrier.getCurrierFromFloor(),
      mailCallCurrier.getCurrierFromRoom(),
      mailCallCurrier.getListToStage(),
      mailCallCurrier.getWeight(),
      mailCallCurrier.getComment(),
      mailCallCurrier.getCargoType(),
      mailCallCurrier.getRadius(),
      mailCallCurrier.getWidth(),
      mailCallCurrier.getHeight(),
      mailCallCurrier.getLength(),
      mailCallCurrier.getCost(),
      mailCallCurrier.getCount(),
      mailCallCurrier.getPodSum(),
      mailCallCurrier.getCargoSubType()
      );
    //Log.d("TrackTTN", call.request().url().encodedQuery());
    call.enqueue(
      new Callback<Currier>() {
        @Override
        public void onResponse(Call<Currier> call, Response<Currier> response) {

          btnSendCallCurrier.setEnabled(true);
          btnCallCurrierBack.setEnabled(true);

          Currier currier = response.body();
          if (currier!=null) {
            if (!currier.isError()) {
              mailCallCurrier.setSended(true);
              mailCallCurrier.setNumberOrder(currier.getData());
              startActivity(new Intent(context, CallCurrierConfirmActivity.class));
              finish();
            } else {
              Log.d("Currier", "CallCurrier error: " + currier.getErrorMessage());
              showMessage(getResources().getString(R.string.dialog_title),
                getResources().getString(R.string.request_error));
            }
          }
          else
          {
            showMessage(getResources().getString(R.string.dialog_title),
              getResources().getString(R.string.try_later));
          }

        }

        @Override
        public void onFailure(Call<Currier> call, Throwable t) {
          //call.request().
          Log.d("Currier", "CallCurrier error: " + t.getMessage());
          showMessage(getResources().getString(R.string.dialog_title),
            getResources().getString(R.string.try_later));
          btnSendCallCurrier.setEnabled(true);
          btnCallCurrierBack.setEnabled(true);
        }

      }
    );
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_call_currier);
    context = this;
    btnCallCurrierBack = (Button) findViewById(R.id.btnCallCurrierBack);
    btnCallCurrierBack.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (call!=null && call.isExecuted())
          call.cancel();
        finish();
      }
    });

    btnSendCallCurrier = (Button) findViewById(R.id.btnSendCallCurrier);
    btnSendCallCurrier.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {

        if (!GlobalApplicationData.getInstance().IsOnline())
        {
          // есть ли доступ к интернет на устройстве
          showMessage(getResources().getString(R.string.dialog_warning_caption),
            getResources().getString(R.string.dialog_message_no_network_access));
        }
        else
          doRequest();
      }
    });

    //tvAddress = (TextView) findViewById(R.id.tvCurrierAddress);
    //tvCargoType = (TextView) findViewById(R.id.tvCurrierCargoType);
    //tvWeight = (TextView) findViewById(R.id.tvCurrierWeight);

    tvDate = (TextView) findViewById(R.id.tvCurrierDateSend);
    tvPhone = (TextView) findViewById(R.id.tvPhone);
  }

  @Override
  protected void onPause() {
    super.onPause();
    if (call!=null && call.isExecuted())
      call.cancel();
  }

  @Override
  protected void onResume() {
    super.onResume();
    MailCallCurrier mailCallCurrier = MailCallCurrier.getInstance();
//    tvAddress.setText(mailCallCurrier.currierFromCity + ", " +
//      mailCallCurrier.currierFromStreet + ", " + getResources().getString(R.string.house_short) +
//      mailCallCurrier.currierFromHouse + ", кв." +
//      mailCallCurrier.currierFromRoom
//    );
    //tvAddress.setText(mailCallCurrier.fullName);
    //tvCargoType.setText(mailCallCurrier.cargoType);
    // tvWeight.setText("до " + mailCallCurrier.weight + " кг");
    tvPhone.setText(mailCallCurrier.getPhone());
    tvDate.setText(mailCallCurrier.getHumanDate());
  }
}
