package com.ua.inr0001.intime;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.FilterQueryProvider;
import android.widget.SimpleCursorAdapter;
import android.widget.SimpleCursorAdapter.CursorToStringConverter;
import android.widget.TextView;

import intime.llc.ua.R;

public class AddMyAddressActivity extends AppCompatActivity {

  private EditText editName;
  private EditText editPhone;
  private EditText editMail;

  private TextView editCity;
  private TextView editStreet;
  private EditText editHouse;
  private EditText editRoom;
  private EditText editStage;

  private TextView textName;
  private TextView textPhone;
  private TextView textMail;

  private TextView textCity;
  private TextView textStreet;
  private TextView textHouse;
  private TextView textRoom;
  private TextView textStage;

  private Context context;

  private Cursor selectCityCursor;
  private SimpleCursorAdapter mCityRecieverAdapter;

  private String editStatus = "add";

  private String id = "";

  public void setEditStatus(String status, String id)
  {
    editStatus = status;
    this.id = id;
  }

  private boolean validateForm()
  {
    boolean result = true;
    if (editName.getText().toString().length()==0) {
      textName.setTextColor(getResources().getColor(R.color.error_massage));
      result = false;
    }

    if (editPhone.getText().toString().length()==0) {
      textPhone.setTextColor(getResources().getColor(R.color.error_massage));
      result = false;
    }

    if (editMail.getText().toString().length()==0) {
      textMail.setTextColor(getResources().getColor(R.color.error_massage));
      result = false;
    }

    if (editCity.getText().toString().length()==0) {
      textCity.setTextColor(getResources().getColor(R.color.error_massage));
      result = false;
    }

    if (editStreet.getText().toString().length()==0) {
      textStreet.setTextColor(getResources().getColor(R.color.error_massage));
      result = false;
    }

    if (editHouse.getText().toString().length()==0) {
      textHouse.setTextColor(getResources().getColor(R.color.error_massage));
      result = false;
    }

    if (editRoom.getText().toString().length()==0) {
      textRoom.setTextColor(getResources().getColor(R.color.error_massage));
      result = false;
    }

    if (editStage.getText().toString().length()==0) {
      textStage.setTextColor(getResources().getColor(R.color.error_massage));
      result = false;
    }

    return result;
  }

  /*private void insertAddress()
  {
    DictionariesDB.getInstance(context).tableAddresses.insert(
      editName.getText().toString(),
      editPhone.getText().toString(),
      editMail.getText().toString(),
      editCity.getText().toString(),
      editStreet.getText().toString(),
      editHouse.getText().toString(),
      editRoom.getText().toString(),
      editStage.getText().toString()
    );
  }*/

  /*private void updateAddress()
  {
    DictionariesDB.getInstance(context).tableAddresses.update(
      editName.getText().toString(),
      editPhone.getText().toString(),
      editMail.getText().toString(),
      editCity.getText().toString(),
      editStreet.getText().toString(),
      editHouse.getText().toString(),
      editRoom.getText().toString(),
      editStage.getText().toString(),
      id
    );
  }*/

  private CursorToStringConverter setCityCursorConverter() {
    return new CursorToStringConverter() {
      public CharSequence convertToString(Cursor cur) {
        return DictionariesDB.getColumnValue(cur, CityTable.Column.CITY);
      }
    };
  }

  private FilterQueryProvider setCityFilterQueryProvider() {
    return new FilterQueryProvider() {
      public Cursor runQuery(CharSequence str) {
        return DictionariesDB.getInstance(context).tableCity.selectByName(str.toString());
      }
    };
  }

  private void setCityCursorAdapter(SimpleCursorAdapter adapter,
                                    AutoCompleteTextView autoCompleteTextView)
  {
    adapter = new SimpleCursorAdapter(context,
      R.layout.dialog_list_item_ext, selectCityCursor, new String[] {
      CityTable.Column.CITY, CityTable.Column.STATE,
      CityTable.Column.AREA },
      new int[] { R.id.textCityName, R.id.textRegionName, R.id.textAreaName });
    adapter.setFilterQueryProvider(setCityFilterQueryProvider());
    adapter.setCursorToStringConverter(setCityCursorConverter());
    autoCompleteTextView.setAdapter(adapter);
    autoCompleteTextView.setText("");
  }

  /*private void fillFields()
  {
    Cursor cursor = DictionariesDB.getInstance(context).tableAddresses.selectByID(Integer.parseInt(id));
    cursor.moveToFirst();
    editName.setText(DictionariesDB.getColumnValue(cursor, AddressesTable.Column.NAME));
    editPhone.setText(DictionariesDB.getColumnValue(cursor, AddressesTable.Column.PHONE));
    editMail.setText(DictionariesDB.getColumnValue(cursor, AddressesTable.Column.MAIL));

    editCity.setText(DictionariesDB.getColumnValue(cursor, AddressesTable.Column.CITY));
    editStreet.setText(DictionariesDB.getColumnValue(cursor, AddressesTable.Column.STREET));
    editHouse.setText(DictionariesDB.getColumnValue(cursor, AddressesTable.Column.HOUSE));
    editRoom.setText(DictionariesDB.getColumnValue(cursor, AddressesTable.Column.ROOM));
    editStage.setText(DictionariesDB.getColumnValue(cursor, AddressesTable.Column.STAGE));
    cursor.close();
  }*/

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.fragment_edit_profile);
    context = this;

    editName = (EditText) findViewById(R.id.editName);
    editPhone = (EditText) findViewById(R.id.editPhone);
    editMail = (EditText) findViewById(R.id.editMail);

    editCity = (TextView) findViewById(R.id.editCity);
    editStreet = (TextView) findViewById(R.id.editStreet);
    editHouse = (EditText) findViewById(R.id.editHouse);
    editRoom = (EditText) findViewById(R.id.editRoom);
    editStage = (EditText) findViewById(R.id.editStage);

    textName = (TextView) findViewById(R.id.textName);
    textPhone = (TextView) findViewById(R.id.textPhone);
    textMail = (TextView) findViewById(R.id.textMail);

    textCity = (TextView) findViewById(R.id.textCity);
    textStreet = (TextView) findViewById(R.id.textStreet);
    textHouse = (TextView) findViewById(R.id.textHouse);
    textRoom = (TextView) findViewById(R.id.textRoom);
    textStage = (TextView) findViewById(R.id.textStage);

    editName.addTextChangedListener(new TextChangeListener(context, null, textName, null));
    editPhone.addTextChangedListener(new TextChangeListener(context, null, textPhone, null));
    editMail.addTextChangedListener(new TextChangeListener(context, null, textMail, null));

    /*editCity.addTextChangedListener(new TextChangeListener(context, null, textCity, null));
    editStreet.addTextChangedListener(new TextChangeListener(context, null, textStreet, null));
    editHouse.addTextChangedListener(new TextChangeListener(context, null, textHouse, null));
    editRoom.addTextChangedListener(new TextChangeListener(context, null, textRoom, null));
    editStage.addTextChangedListener(new TextChangeListener(context, null, textStage, null));*/

    //editCity.setKeyListener(null);
    //editStreet.setKeyListener(null);

    editCity.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        startActivity(new Intent(context, SelectCityActivity.class));
      }
    });

    editStreet.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        startActivity(new Intent(context, SelectCityActivity.class));
      }
    });

    /*selectCityCursor = DictionariesDB.getInstance(context).tableCity.selectAll();
    startManagingCursor(selectCityCursor);
    //setCityCursorAdapter(mCityRecieverAdapter, editCity);*/

    //if (editStatus.equals("edit"))
    //  fillFields();
    Toolbar toolbar = (Toolbar) findViewById(R.id.editProfileToolbar);
    setSupportActionBar(toolbar);
    toolbar.setNavigationOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        onBackPressed();
      }
    });
  }

  @Override
  public void onStop()
  {
    super.onStop();
    //stopManagingCursor(selectCityCursor);
  }

  @Override
  public void onPause()
  {
    super.onPause();
    //stopManagingCursor(selectCityCursor);
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.accept_action_menu, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();
    switch (id)
    {
      case R.id.menu_action_accept: onBackPressed(); return true;
      default: return super.onOptionsItemSelected(item);
    }
  }

}
