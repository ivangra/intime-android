package com.ua.inr0001.intime;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.Random;

import intime.api.model.recovery.Recovery;
import intime.llc.ua.R;
import intime.maskedtextedit.MaskedInputFilter;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RecoveryPasswordActivity extends AppCompatActivity {

  private final static String TAG = "Recovery";
  private final int SMS_GRANT_REQUEST = 23;
  private MaskedInputFilter maskedInputFilter;
  private BroadcastReceiver brRegisterUserSMS;
  private FrameLayout frameRecWrongLoginHint;
  private int code = -1;
  private EditText editPhone;
  private EditText editRecSMS;
  private EditText editRecPassword;
  private TextView tvPhone;
  private TextView tvRecSMS;
  private TextView tvRecPassword;
  private Button btnRecoveryPassword;
  private CheckBox chLoginShowPassword;
  private Context context;
  private boolean activityPaused = false;
  private Call<ResponseBody> sendSMSCall;
  private Call<Recovery> recoveryPasswordCall;
  private Call<ResponseBody> userExistsCall;
  private TextWatcher textWatcher;
  private boolean mToggle = false;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_recovery_password);
    context = this;
    Toolbar mActionBarToolbar = (Toolbar) findViewById(R.id.authToolbar1);
    mActionBarToolbar.setTitle("");
    setSupportActionBar(mActionBarToolbar);
    frameRecWrongLoginHint = (FrameLayout) findViewById(R.id.frameRecWrongLoginHint);
    chLoginShowPassword = (CheckBox) findViewById(R.id.chLoginShowPasswordRec);
    editPhone = (EditText) findViewById(R.id.editRecoveryPhone);
    editRecSMS = (EditText) findViewById(R.id.editRecSMS);
    editRecPassword = (EditText) findViewById(R.id.editRecPassword);
    tvPhone = (TextView) findViewById(R.id.tvRecPhone);
    tvRecPassword = (TextView) findViewById(R.id.tvRecPassword);
    tvRecSMS = (TextView) findViewById(R.id.tvRecSMS);
    editRecSMS.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {

      }

      @Override
      public void afterTextChanged(Editable s) {
        tvRecSMS.setTextColor(getResources().getColor(R.color.text_hint));
      }
    });
    editRecPassword.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {

      }

      @Override
      public void afterTextChanged(Editable s) {
        tvRecPassword.setTextColor(getResources().getColor(R.color.text_hint));
      }
    });
    initShowPassword(editRecPassword, chLoginShowPassword);
    btnRecoveryPassword = (Button) findViewById(R.id.btnRecovery);
    btnRecoveryPassword.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (validateForm()) {
          btnRecoveryPassword.setEnabled(false);
          recoveryPassword(editPhone.getText().toString(), editRecPassword.getText().toString());
        }
      }
    });

    textWatcher = new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {
        frameRecWrongLoginHint.setVisibility(View.INVISIBLE);
      }

      @Override
      public void afterTextChanged(Editable s) {
        tvPhone.setTextColor(getResources().getColor(R.color.text_hint));
        if (mToggle) {
          String phone = editPhone.getText().toString().trim();
          if (phone.length() >= 17) {
            if (code == -1) {
              isUserExists(phone);
            }
          } else
            code = -1;
        }
        mToggle = !mToggle;
      }
    };
    //editPhone.addTextChangedListener(textWatcher);
    brRegisterUserSMS = new BroadcastReceiver() {
      @Override
      public void onReceive(Context context, Intent intent) {
        String strCode = intent.getStringExtra("sms_code");
        editRecSMS.setText(strCode);
      }
    };
    Intent intent = getIntent();
    editPhone.setText(intent.getStringExtra("phone"));
    if (editPhone.getText().length()==0)
      editPhone.setText("+38 0");
    editPhone.setSelection(editPhone.length());
    maskedInputFilter = new MaskedInputFilter("+38 0## ### ## ##", editPhone, '#');
    editPhone.setFilters(new InputFilter[]{maskedInputFilter});
    if (code==-1 && editPhone.getText().toString().trim().length()>=17) {
      String phone = editPhone.getText().toString().trim();
      isUserExists(phone);
    }
    editPhone.addTextChangedListener(textWatcher);
    isSMSReceivePermissionGranted();
  }

  private boolean validateForm()
  {
    boolean result = true;
    if (editPhone.getText().toString().trim().length()<17){
      tvPhone.setTextColor(getResources().getColor(R.color.error_massage));
      result = false;
    }
    if (editRecSMS.getText().toString().compareTo(Integer.toString(code))!=0){
      tvRecSMS.setTextColor(getResources().getColor(R.color.error_massage));
      result = false;
    }
    if (editRecPassword.getText().length()==0){
      tvRecPassword.setTextColor(getResources().getColor(R.color.error_massage));
      result = false;
    }
    return result;
  }

  private void initShowPassword(final EditText edPassword, final CheckBox chPassword)
  {
    chPassword.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(isChecked) {
          edPassword.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
          //edPassword.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
          edPassword.setTextColor(getResources().getColor(R.color.base_text_color));
        } else {
          edPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
          //edPassword.setTextSize(TypedValue.COMPLEX_UNIT_SP, 32);
          edPassword.setTextColor(getResources().getColor(R.color.paper_caption));
        }
      }
    });
  }

  private void showMessage(String caption, String message)
  {
    if (activityPaused==false) {
      MessageDialog dlgWarning = new MessageDialog();
      dlgWarning.setParams(caption, message, false, null);
      dlgWarning.show(getSupportFragmentManager(), "Warning");
    }
  }

  private void recoveryPassword(final String inPhone, final String password) {

    String phone = inPhone.replace(" ", "").replace("+", "");

    Retrofit retrofit = new Retrofit.Builder()
      .baseUrl("https://esb.intime.ua:4443/services/intime_auth_rest/")
      .addConverterFactory(GsonConverterFactory.create())
      .client(GlobalApplicationData.getUnsafeOkHttpClient())
      .build();

    IRtfAPI service = retrofit.create(IRtfAPI.class);
    recoveryPasswordCall = service.changePassword("application/json",
      RequestBody.create(MediaType.parse("application/json"), "{\"_post_change_password_rest\":{\"login\":\""
        + phone + "\", \"password\":\"" + password + "\"}}"));
    recoveryPasswordCall.enqueue(
      new Callback<Recovery>() {
        @Override
        public void onResponse(Call<Recovery> call, Response<Recovery> response) {
          //ResponseBody body = response.body();
          Recovery auth = response.body();
          if (auth!=null)
          {
            String result = auth.getEntriesChangePassword().getEntryChangePassword().get(0).getResult();
            if (result.compareTo("OK")==0) {
              Toast.makeText(context, getResources().getString(R.string.password_recovered), Toast.LENGTH_LONG).show();
              finish();
              //GlobalApplicationData.getInstance().saveAPIKey(result);
            }
            else
            {
              showMessage(getResources().getString(R.string.dialog_title),
                getResources().getString(R.string.error_recovery_password));
            }
          }
          else
          {
            showMessage(getResources().getString(R.string.dialog_title),
              getResources().getString(R.string.error_recovery_password));
          }
          btnRecoveryPassword.setEnabled(true);
        }

        @Override
        public void onFailure(Call<Recovery> call, Throwable t) {
          //frPreCalcSum1.setVisibility(View.GONE);
          btnRecoveryPassword.setEnabled(true);
          Log.d(TAG, "Auth Error: " + t.getMessage());
          showMessage(getResources().getString(R.string.dialog_title),
            getResources().getString(R.string.error_recovery_password));
        }

      }
    );

  }

  @Override
  protected void onPause() {
    super.onPause();
    activityPaused = true;
  }

  @Override
  public void onStart()
  {
    super.onStart();
    editPhone.addTextChangedListener(textWatcher);
  }

  @Override
  public void onStop()
  {
    super.onStop();
    editPhone.removeTextChangedListener(textWatcher);
    if (sendSMSCall!=null)
      sendSMSCall.cancel();
    if (recoveryPasswordCall!=null)
      recoveryPasswordCall.cancel();
    if (userExistsCall!=null)
      userExistsCall.cancel();
    try {
      if (brRegisterUserSMS != null)
        context.unregisterReceiver(brRegisterUserSMS);
    }
    catch (Exception e)
    {

    }
  }

  @Override
  public void onResume() {
    super.onResume();
    activityPaused = false;
  }

  private void smsPremissionRequest()
  {
    ActivityCompat.requestPermissions(this,
      new String[]{
        Manifest.permission.RECEIVE_SMS
      },
      SMS_GRANT_REQUEST);
  }

  private  boolean isSMSReceivePermissionGranted() {
    if (Build.VERSION.SDK_INT >= 23) {
      if (checkSelfPermission(Manifest.permission.RECEIVE_SMS)
        == PackageManager.PERMISSION_GRANTED) {
        // Log.v("TAG","Permission is granted");
        IntentFilter intFilt = new IntentFilter(RegisterUserActivity.GET_SMS);
        context.registerReceiver(brRegisterUserSMS, intFilt);
        return true;
      } else {
        // Log.v("TAG","Permission is revoked");
        smsPremissionRequest();
        return false;
      }
    }
    else { //permission is automatically granted on sdk<23 upon installation
      // Log.v("TAG", "Permission is granted");
      IntentFilter intFilt = new IntentFilter(RegisterUserActivity.GET_SMS);
      context.registerReceiver(brRegisterUserSMS, intFilt);
      return true;
    }
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
    if (requestCode == SMS_GRANT_REQUEST) {
      IntentFilter intFilt = new IntentFilter(RegisterUserActivity.GET_SMS);
      context.registerReceiver(brRegisterUserSMS, intFilt);
    }
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
  }

  private void sendSMS(final String inPhone)
  {
    String phone = inPhone.replace(" ", "").replace("+", "");
    Retrofit retrofit = new Retrofit.Builder()
      .baseUrl("https://newapi.intime.ua/")
      .addConverterFactory(GsonConverterFactory.create())
      //.client(GlobalApplicationData.getUnsafeOkHttpClient())
      .build();

    IRtfAPI service = retrofit.create(IRtfAPI.class);
    Random rand = new Random();
    code = rand.nextInt(9999);
    if (code<1000)
      code += 1000;
    sendSMSCall = service.sendSMS(phone, Integer.toString(code) + " - код підтвердження відновлення пароля",
      Integer.toString(code) + " - kod pidtverdzhennya vidnovlennya parolya");
    sendSMSCall.enqueue(
      new Callback<ResponseBody>() {
        @Override
        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
          //ResponseBody body = response.body();
        }

        @Override
        public void onFailure(Call<ResponseBody> call, Throwable t) {
          //frPreCalcSum1.setVisibility(View.GONE);
          code = -1;
          Log.d(TAG, "Auth Error: " + t.getMessage());
        }

      }
    );
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();
    switch (id)
    {
      case R.id.action_close: onBackPressed(); return true;
      default: return super.onOptionsItemSelected(item);
    }
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.close_action_menu, menu);
    return true; //super.onCreateOptionsMenu(menu);
  }

  private void isUserExists(final String inPhone) {

    String phone = inPhone.replace(" ", "").replace("+", "");

    Retrofit retrofit = new Retrofit.Builder()
      .baseUrl("https://esb.intime.ua:4443/services/intime_auth_rest/")
      //.addConverterFactory(GsonConverterFactory.create())
      .client(GlobalApplicationData.getUnsafeOkHttpClient())
      .build();

    IRtfAPI service = retrofit.create(IRtfAPI.class);

    userExistsCall = service.checkPeopleExists("application/json",
      RequestBody.create(MediaType.parse("application/json"), "{\"_post_get_people_by_phone_rest\":{\"phone\":\"" + phone + "\"}}"));

    userExistsCall.enqueue(
      new Callback<ResponseBody>() {
        @Override
        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

          if (response.body()!=null) {
            String body = "";
            try {
              body = new String(response.body().bytes());
            } catch (IOException e) {

            }
            if (body.contains("Name")) {
              frameRecWrongLoginHint.setVisibility(View.GONE);
              btnRecoveryPassword.setVisibility(View.VISIBLE);
              sendSMS(inPhone);
            }
            else
            {
              frameRecWrongLoginHint.setVisibility(View.VISIBLE);
              btnRecoveryPassword.setVisibility(View.GONE);
            }
          }
        }

        @Override
        public void onFailure(Call<ResponseBody> call, Throwable t) {
          //frPreCalcSum1.setVisibility(View.GONE);
          Log.d(TAG, "Check phone error: " + t.getMessage());
        }

      }
    );

  }

}
