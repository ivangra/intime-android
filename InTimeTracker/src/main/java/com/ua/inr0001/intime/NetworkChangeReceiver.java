package com.ua.inr0001.intime;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkChangeReceiver extends BroadcastReceiver {

  private INetworkState networkState;

  public void setNetworkState(INetworkState networkState)
  {
    this.networkState = networkState;
  }

  @Override
  public void onReceive(Context context, Intent intent) {
    if (networkState != null)
      networkState.networkConnected(isOnline(context));
  }

  private boolean isOnline(Context context)
  {
    ConnectivityManager cm =
      (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

    NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
    //return activeNetwork.isConnectedOrConnecting();

    android.net.NetworkInfo wifi = cm
      .getNetworkInfo(ConnectivityManager.TYPE_WIFI);

    android.net.NetworkInfo mobile = cm
      .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

    return wifi.isConnected() || mobile.isConnected();
  }

}
