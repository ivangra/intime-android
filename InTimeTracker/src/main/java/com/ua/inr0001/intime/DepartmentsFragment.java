package com.ua.inr0001.intime;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.view.inputmethod.InputMethodSubtype;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterItem;
import com.google.maps.android.clustering.ClusterManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import intime.departmentlist.DepRcLayoutManager;
import intime.googleapiclientutils.AutocompleteHelper;
import intime.gps.location.helpers.ISendLocation;
import intime.gps.location.helpers.LocationListenerServices;
import intime.intime.mybottomsheet.MyBottomSheetBehavior;
import intime.llc.ua.R;
import intime.map.builds.route.GetRoute;
import intime.map.builds.route.IBuildRoute;
import intime.map.cluster.utils.ClusterRenderer;
import intime.map.cluster.utils.MyClusterItem;
import intime.utils.DisplayUnits;
import intime.utils.MyDateUtils;

import static android.app.Activity.RESULT_OK;
import static com.ua.inr0001.intime.DictionariesDB.getColumnValue;

public class DepartmentsFragment extends Fragment implements ISendLocation, IBuildRoute,
  OnMapReadyCallback, GoogleMap.OnCameraMoveListener,
  ClusterManager.OnClusterClickListener<MyClusterItem>,
  ClusterManager.OnClusterItemClickListener<MyClusterItem>,
  GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
  INetworkState {

  private static final String TAG = "MapDeps";
  private final String KIEV_LAT = "50.435383";
  private final String KIEV_LNG = "30.546447";
  private Toolbar toolBar;
  private TextView tvDeplistTitle;
  private MenuItem miClose;
  private MenuItem miFilter;
  private MenuItem miSearch;
  private String lang = "ru";
  private String searchedDepCode = null;

  private LinearLayout llSearchDepartments;
  private EditText editSearchDep;
  private DepartmentsFragment mDepActivity;
  private FloatingActionButton fabLocateMe;
  private SupportMapFragment mapFragment;
  private GoogleMap mMap123 = null;
  private ClusterManager<MyClusterItem> mClusterManager;
  private MyClusterItem oldItemSelected;

  private DictionariesDB dictionariesDB;
  private float currentZoomLevel = 13;

  private Context context;
  private String Lat = KIEV_LAT;
  private String Lng = KIEV_LNG;
  private String depCode;
  private String depType;
  private boolean clusterShow = false;

  private Location locMyLocation = null;
  private Location selectedDepLocation = null;

  private Marker selectedPlace;
  private Marker myLocation;
  private Circle circleLocation;

  private RecyclerView rcSearchDeps;
  private DepartmentsSearchRvAdapter rcSearchAdapter;
  private SearchDepData searchDepData;
  //private DepartmentsRvCursorAdapter rcSearchAdapter;

  private RecyclerView lvCities;
  private RecyclerView lvCitiesOffline;
  private DepartmentsRvAdapter rvAdapter;

  private List<DepartmentMapItem> depList = new ArrayList<DepartmentMapItem>();
  private List<MyClusterItem> markers = new ArrayList<MyClusterItem>();
  private List<DepartmentMapItem> searchDepList = new ArrayList<DepartmentMapItem>();
  private GoogleApiClient mGoogleApiClient;

  //private CoordinatorLayout.LayoutParams coordinatorLayoutParams;

  //private int screenHeightDp;
  //private int coordinatorLayoutHeight;
  //private int oldHeight = 160;
  private CoordinatorLayout coordinatorLayout;
  private DepRcLayoutManager mLayoutManager;
  private DepRcLayoutManager mLayoutManagerOffline;

  private Polyline finalPolyline;
  private boolean mapActive = false;

  private ScheduledExecutorService service;
  private ScheduledExecutorService searchService;
  private LoadDeps loadDeps;
  private SearchDeps searchDeps;
  public LatLngBounds bounds;
  private boolean markerListBusy = false;
  private boolean messageGPSIsShowed = false;

  // Dep list behavior

  private LinearLayout toplayerInfo;
  private LinearLayout bottomSheet;
  private BottomSheetBehavior bottomSheetBehavior;

  // Dep Details behavior
  private LinearLayout depDetailsTopLayerInfo;
  private LinearLayout depBottomSheet;
  private BottomSheetBehavior depBottomSheetBehavior;

  // Dep Details View

  private DepartmentDetailsView depView;
  private NestedScrollView departmentDetailsNestedScroll;
  // End dep details View

  /**
   * The bottom sheet is dragging.
   */
  public static final int STATE_DRAGGING = 1;

  /**
   * The bottom sheet is settling.
   */
  public static final int STATE_SETTLING = 2;

  /**
   * The bottom sheet is expanded_half_way.
   */
  public static final int STATE_ANCHOR_POINT = 3;

  /**
   * The bottom sheet is expanded.
   */
  public static final int STATE_EXPANDED = 4;

  /**
   * The bottom sheet is collapsed.
   */
  public static final int STATE_COLLAPSED = 5;

  /**
   * The bottom sheet is hidden.
   */
  public static final int STATE_HIDDEN = 6;

  // Dep list behavior

  DepFilterSettings depFilterSettingds;
  private boolean clusterClick = false;

  private boolean showOnMap = false;

  private NetworkChangeReceiver networkChangeReceiver;
  private FrameLayout frOfflineBack;

  public void setDepCodeDepType(final String depCode, final String depType, final boolean showOnMap)
  {
    this.depCode = depCode;
    this.depType = depType;
    this.showOnMap = showOnMap;
  }

  private MyClusterItem addDepMarker(String longitude, String latitude,
                            String depType,
                            String depDesc,
                            String depNumber,
                            String code,
                            String weightLimit) {
    if (longitude != null && latitude != null) {
      try {
        int weight = 31;
        if (weightLimit!=null)
          weight = Integer.parseInt(weightLimit);
        MyClusterItem clusterItem = new MyClusterItem(context,
          Double.parseDouble(latitude), Double.parseDouble(longitude),
          depType,
          depDesc,
          code,
          depNumber,
          weight
        );
        markers.add(clusterItem);
        return clusterItem;
      } catch (Exception e) {
        Log.d(TAG, e.getMessage());
        return null;
      }
    }
    return null;
  }

  private void addDepItem(String longitude, String latitude,
                          String depType,
                          String depDesc,
                          String depNumber,
                          String code,
                          String workHoursStart,
                          String workHoursEnd,
                          String weight,
                          String cityName,
                          String state,
                          int dublicate,
                          double distance,
                          String lengthLimit,
                          WorkHoursData workHoursData
                          )
  {
    if (longitude != null && latitude != null) {
      DepartmentMapItem depMapItem = new DepartmentMapItem();
      depMapItem.depNumber = depNumber;
      depMapItem.depType = depType;
      depMapItem.longitude = longitude;
      depMapItem.latitude = latitude;
      depMapItem.depAddress = depDesc;
      depMapItem.workHoursStart = workHoursStart;
      depMapItem.workHoursEnd = workHoursEnd;
      depMapItem.depWeight = weight;
      depMapItem.cityName = cityName;
      depMapItem.code = code;
      depMapItem.state = state;
      depMapItem.dublicate = dublicate;
      depMapItem.distance = distance;
      depMapItem.workHoursData = workHoursData;
      depMapItem.lengthLimit = lengthLimit;
      if (weight != null)
        depMapItem.weightLimit = Integer.parseInt(weight);
      else if (depMapItem.depType.compareTo("store") == 0)
          depMapItem.weightLimit = 31;
      depList.add(depMapItem);
    }
  }

  private void addSearchDepItem(String longitude, String latitude,
                          String depType,
                          String depDesc,
                          String depNumber,
                          String code,
                          String workHoursStart,
                          String workHoursEnd,
                          String weight,
                          String cityName,
                          String state,
                          int dublicate,
                          double distance,
                          String lengthLimit,
                          WorkHoursData workHoursData,
                          String fullAddress,
                          HashSet<DepartmentMapItem> list
  )
  {
    if (longitude != null && latitude != null) {
      DepartmentMapItem depMapItem = new DepartmentMapItem();
      depMapItem.depNumber = depNumber;
      depMapItem.depType = depType;
      depMapItem.longitude = longitude;
      depMapItem.latitude = latitude;
      depMapItem.depAddress = depDesc;
      depMapItem.workHoursStart = workHoursStart;
      depMapItem.workHoursEnd = workHoursEnd;
      depMapItem.depWeight = weight;
      depMapItem.cityName = cityName;
      depMapItem.code = code;
      depMapItem.state = state;
      depMapItem.dublicate = dublicate;
      depMapItem.distance = distance;
      depMapItem.workHoursData = workHoursData;
      depMapItem.fullAddress = fullAddress;
      depMapItem.lengthLimit = lengthLimit;
      if (weight!=null)
        depMapItem.weightLimit = Integer.parseInt(weight);
      else if (depMapItem.depType.compareTo("store") == 0 )
        depMapItem.weightLimit = 31;
      list.add(depMapItem);
    }
  }

  private void addSearchPlaceDepItem(
                                String depType,
                                String placeId,
                                String cityName,
                                String region,
                                String fullAddress,
                                HashSet<DepartmentMapItem> list
  )
  {
      DepartmentMapItem depMapItem = new DepartmentMapItem();
      depMapItem.depType = depType;
      depMapItem.depAddress = region;
      depMapItem.cityName = cityName;
      depMapItem.code = placeId;
      depMapItem.fullAddress = fullAddress;
      list.add(depMapItem);
  }

  private void loadAllMarkers() {
    mClusterManager.clearItems();
    markers.clear();
    Thread nr = new Thread(new Runnable() {
      @Override
      public void run() {
        Cursor cursor;
        if (lang.compareTo("ru") == 0)
          cursor = dictionariesDB.tableWarehouseDetails.selectAll();
        else
          cursor = dictionariesDB.tableWarehouseDetails.selectAllUA();
        try {
          cursor.moveToFirst();
          markerListBusy = true;
          Log.d(TAG, "LoadAllMarkers");
          while (!cursor.isAfterLast()) {
            try {
              addDepMarker(getColumnValue(cursor, WarehouseDetailsTable.Column.LNG),
                getColumnValue(cursor, WarehouseDetailsTable.Column.LAT),
                getColumnValue(cursor, WarehouseDetailsTable.Column.TYPE),
                getColumnValue(cursor, WarehouseDetailsTable.Column.ADDRESS),
                getColumnValue(cursor, WarehouseDetailsTable.Column.NUMBER),
                getColumnValue(cursor, WarehouseDetailsTable.Column.CODE),
                getColumnValue(cursor, WarehouseDetailsTable.Column.CAPACITY));
            } finally {
              cursor.moveToNext();
            }
          }
          markerListBusy = false;
        }
        finally {
          if (cursor!=null)
            cursor.close();
        }
      }
    });
    nr.start();
  }

  private void gotoMarker(Double latitude, Double longitude, float zoom) {
    CameraPosition cameraPosition = new CameraPosition.Builder()
      .target(new LatLng(latitude, longitude))
      .zoom(zoom)
      .bearing(0)
      .tilt(0)
      .build();
    CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
    mMap123.moveCamera(cameraUpdate);
  }

  @Override
  public void onMapReady(GoogleMap map) {

    Log.d(TAG, "Map init complette");
    mMap123 = map;
    map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
    map.getUiSettings().setCompassEnabled(false);
    map.getUiSettings().setZoomControlsEnabled(false);
    map.getUiSettings().setMyLocationButtonEnabled(false);
    map.setMyLocationEnabled(false);

    if (depCode==null && depType==null) {
      CameraPosition cameraPosition = new CameraPosition.Builder()
        .target(new LatLng(Double.parseDouble(Lat), Double.parseDouble(Lng)))
        .zoom(currentZoomLevel)
        .bearing(0)
        .tilt(0)
        .build();
      CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
      mMap123.moveCamera(cameraUpdate);
      onCameraMove();
    }

    mClusterManager = new ClusterManager<MyClusterItem>(context, map);
    mClusterManager.setRenderer(new ClusterRenderer(context, map, mClusterManager));
    map.setOnCameraIdleListener(mClusterManager);
    map.setOnMarkerClickListener(mClusterManager);
    map.setOnInfoWindowClickListener(mClusterManager);
    map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
      @Override
      public void onMapClick(LatLng latLng) {
        clusterClick = false;
        rvAdapter.setDataList(depList);
        rvAdapter.notifyDataSetChanged();
        bottomSheet.setVisibility(View.VISIBLE);
        depBottomSheet.setVisibility(View.GONE);
        //setTitle(getResources().getString(R.string.text_title_departments));
        if (oldItemSelected != null) {
          oldItemSelected.setSelected(false);
          try {
            ((ClusterRenderer) mClusterManager.getRenderer()).getMarker(oldItemSelected).setIcon(BitmapDescriptorFactory.fromResource(oldItemSelected.getIcon()));
          }
          catch (Exception e)
          {
            Log.d(TAG, "Error :" + e.getMessage());
          }
        }
        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) fabLocateMe.getLayoutParams();
        params.setAnchorId(R.id.nestedScroll);
        fabLocateMe.setLayoutParams(params);
        removeRoute();
      }
    });

    //loadAllMarkers();

    mMap123.setOnCameraMoveListener(this);
    mClusterManager.setOnClusterClickListener(this);
    mClusterManager.setOnClusterItemClickListener(this);

    if (depCode!=null && depType!=null)
    {
      Log.d(TAG, "Go to department");
      Cursor toDep;
      if (lang.compareTo("ru")==0)
        toDep = DictionariesDB.getInstance(context).tableWarehouseDetails.selectWarehouseByCode(depCode);
      else
        toDep = DictionariesDB.getInstance(context).tableWarehouseDetails.selectWarehouseByCodeUA(depCode);
      try {
        toDep.moveToFirst();
        String lat = DictionariesDB.getColumnValue(toDep, WarehouseDetailsTable.Column.LAT);
        String lng = DictionariesDB.getColumnValue(toDep, WarehouseDetailsTable.Column.LNG);
        if (lat != null && lng != null) {
          gotoMarker(Double.parseDouble(lat), Double.parseDouble(lng), 18);
          onCameraMove();
        }
      }
      catch(Exception e)
      {
        Log.d(TAG, "Error :" + e.getMessage());
      }
      finally {
        if (toDep!=null)
          toDep.close();
      }
    }
    else
    if (GlobalApplicationData.getInstance().isGPSEnabled()) {
      startLocation();
    }
//    else {
//      runSearchStart();
//    }
  }

  private void runSearchStart()
  {
    bounds = mMap123.getProjection().getVisibleRegion().latLngBounds;
    service = Executors.newSingleThreadScheduledExecutor();
    service.schedule(new Runnable() {
      @Override
      public void run() {
        if (mapActive && markerListBusy==false) {
          Log.d(TAG, "runSearch");
          loadDeps = new LoadDeps(locMyLocation);
          loadDeps.linkMain(mDepActivity);
          loadDeps.execute();
        }
      }
    }, 800, TimeUnit.MILLISECONDS);
  }

  private void runSearchDeps(final String searchString)
  {
    searchService = Executors.newSingleThreadScheduledExecutor();
    searchService.schedule(new Runnable() {
      @Override
      public void run() {
        while (true) {

            Log.d(TAG, "renSearchStart" + searchString);
            searchDeps = new SearchDeps(locMyLocation, searchString, mGoogleApiClient, searchDepData);
            searchDeps.linkMain(mDepActivity);
            searchDeps.execute();
            break;

        }
      }
    }, 800, TimeUnit.MILLISECONDS);
  }

  private void initMap() {
    try {
      mapFragment = (SupportMapFragment) getChildFragmentManager()
        .findFragmentById(R.id.frDepsList);
      mapFragment.getMapAsync(this);
    } catch (Exception e) {
    }
  }

  private BitmapDescriptor getMarkerIconFromDrawable(Drawable drawable) {
    Canvas canvas = new Canvas();
    Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
    canvas.setBitmap(bitmap);
    drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
    drawable.draw(canvas);
    return BitmapDescriptorFactory.fromBitmap(bitmap);
  }

  @Override
  public void sendLocation(Location location) {
    if (mMap123 != null && mapActive) {
      Log.d(TAG, Double.toString(location.getLongitude()) + " " + Double.toString(location.getLatitude()));

      if (myLocation != null)
        myLocation.remove();

      if (circleLocation != null)
        circleLocation.remove();

      if (mMap123!=null && location != null) {
        Lat = Double.toString(location.getLatitude());
        Lng = Double.toString(location.getLongitude());
        LatLng center = new LatLng(location.getLatitude(), location.getLongitude());
        Drawable circleDrawable = getResources().getDrawable(R.drawable.geo_location);
        BitmapDescriptor markerIcon = getMarkerIconFromDrawable(circleDrawable);

        myLocation = mMap123.addMarker(new MarkerOptions()
          .position(center)
          .title(context.getResources().getString(R.string.your_location))
          .anchor(0.5f, 0.5f)
          .icon(/*BitmapDescriptorFactory.fromResource(R.drawable.pin_my_location))*/markerIcon));
        circleLocation = mMap123.addCircle(new CircleOptions()
          .center(center)
          .radius(300)
          .strokeWidth(1)
          .strokeColor(getResources().getColor(R.color.map_circle_stroke))
          .fillColor(getResources().getColor(R.color.map_circle_fill)));
        if (depCode == null && depType == null) {
          if (bottomSheet.getVisibility() == View.VISIBLE)
            gotoMarker(location.getLatitude() - 0.0140d, location.getLongitude(), 13);
          else
            gotoMarker(location.getLatitude(), location.getLongitude(), 13);
        }
        locMyLocation = location;
//        GlobalApplicationData.getInstance().saveLastLocation(location);
        depView.setMyLocation(locMyLocation.getLongitude(), locMyLocation.getLatitude());
        rvAdapter.setMyLocation(location);
        rcSearchAdapter.setMyLocation(location);
        stopTask();
        runSearchStart();
      } else {
        Log.d(TAG, "Location listener: wrong location");
      }
    }
  }

  private void startLocation() {
    new LocationListenerServices(context, this);
  }

  private void findAndSelectMarker(int position)
  {
    //Log.d(TAG, "findAndSelectMarker");
    for (MyClusterItem myClusterItem : markers) {
      //Log.d(TAG, "Marker " + myClusterItem.getCode());
      if (myClusterItem.getCode().compareTo(depList.get(position).code)==0) {
        //Log.d(TAG, "Marker finded");
        //onCameraMove(); // desabled for selected marker is not switched
        switchClisterItemSelected(myClusterItem);
        break;
      }
    }
  }

  private void findAndSelectMarkerByCode(String code)
  {
    //Log.d(TAG, "findAndSelectMarkerByCode");
    for (MyClusterItem myClusterItem : markers) {
      //Log.d(TAG, "Marker " + myClusterItem.getCode());
      if (myClusterItem.getCode().compareTo(code)==0) {
        //Log.d(TAG, "Marker finded");
        // onCameraMove(); // desabled for selected marker is not switched
        switchClisterItemSelected(myClusterItem);
        break;
      }
    }
  }

  private void initListView(final View view) {
    //lvCities = (RecyclerView) view.findViewById(R.id.lvCities);
    mLayoutManager = new DepRcLayoutManager(context);
    lvCities.addOnScrollListener(new RecyclerView.OnScrollListener() {
      @Override
      public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
        super.onScrollStateChanged(recyclerView, newState);
        //Log.d(TAG, "Scrolled");
      }

      @Override
      public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        if (dy>50)
        {
          //Log.d(TAG, "Swipe Up");
          ViewGroup.LayoutParams lp = bottomSheet.getLayoutParams();
          lp.height = coordinatorLayout.getHeight();
          bottomSheet.setLayoutParams(lp);

//          ViewGroup.LayoutParams lp = bottomSheet.getLayoutParams();
//          //lp.height = coordinatorLayout.getHeight();
////          bottomSheet.setLayoutParams(lp);
//          //bottomSheet.animate().scaleY(-lp.height).setDuration(1500);
//          ViewGroup.LayoutParams lp1 = coordinatorLayout.getLayoutParams();
//          ValueAnimator anim = ValueAnimator.ofInt(lp.height,100);
//          anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
//            @Override
//            public void onAnimationUpdate(ValueAnimator valueAnimator) {
//              int val = (Integer) valueAnimator.getAnimatedValue();
//              ViewGroup.LayoutParams lp = bottomSheet.getLayoutParams();
//              lp.height = val;
//              bottomSheet.setLayoutParams(lp);
//            }
//          });
//          anim.setDuration(1000);
//          anim.start();

        }
        if (dy<-50)
        {
          //Log.d(TAG, "Swipe down");
        }
      }
    });
    lvCities.setLayoutManager(mLayoutManager);

    //rvAdapter = new DepartmentsRvAdapter(context, depList);
//    rvAdapter.setHasStableIds(true);
    rvAdapter.setDataList(depList);
    rvAdapter.setmOnItemClickListener(new DepartmentsRvAdapter.OnItemClickListener() {
      @Override
      public void onItemClick(View view, final int position) {
        try {
          if (GlobalApplicationData.getInstance().IsOnline()) {
            gotoMarker(Double.parseDouble(depList.get(position).latitude), Double.parseDouble(depList.get(position).longitude), 13);
            findAndSelectMarker(position);
            initDepDetailsItem(depList.get(position).code);
          }
          else
            initDepDetailsItemOffline(depList.get(position).code);
        }
        catch(Exception e)
        {
          Log.d(TAG, e.toString());
        }
      }
    });
    lvCities.setAdapter(rvAdapter);
//    lvCities.setNestedScrollingEnabled(false);
//    lvCities.setHasFixedSize(true);
  }

  private void initListViewOffline(final View view) {
//    lvCitiesOffline = (RecyclerView) view.findViewById(R.id.rvCitiesOffline);
    mLayoutManagerOffline = new DepRcLayoutManager(context);
    lvCitiesOffline.setLayoutManager(mLayoutManagerOffline);

    //rvAdapter = new DepartmentsRvAdapter(context, depList);
    //rvAdapter.setHasStableIds(true);
//    rvAdapter.setDataList(depList);
//    rvAdapter.setmOnItemClickListener(new DepartmentsRvAdapter.OnItemClickListener() {
//      @Override
//      public void onItemClick(View view, final int position) {
//        try {
//          initDepDetailsItemOffline(depList.get(position).code);
//        }
//        catch(Exception e)
//        {
//          Log.d(TAG, e.toString());
//        }
//      }
//    });
    lvCitiesOffline.setAdapter(rvAdapter);
    lvCitiesOffline.setNestedScrollingEnabled(false);
    lvCitiesOffline.setHasFixedSize(true);
  }

  private void selectSearchedMarker(){
    if (searchedDepCode != null)
      findAndSelectMarkerByCode(searchedDepCode);
    searchedDepCode = null;
  }

  private void initSesarchDepartmentListView(final View view)
  {
    rcSearchDeps = (RecyclerView) view.findViewById(R.id.rcSearchDeps);
    editSearchDep = (EditText) view.findViewById(R.id.editSearchDep);
    editSearchDep.setOnFocusChangeListener(new View.OnFocusChangeListener() {
      @Override
      public void onFocusChange(View v, boolean hasFocus) {
        if (hasFocus) {
          editSearchDep.post(new Runnable() {
            @Override
            public void run() {
              InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
              imm.showSoftInput(editSearchDep, InputMethodManager.SHOW_IMPLICIT);
            }
          });
        }
      }
    });
    DepRcLayoutManager mLayoutManager1 = new DepRcLayoutManager(context);
    rcSearchDeps.setLayoutManager(mLayoutManager1);
    rcSearchAdapter = new DepartmentsSearchRvAdapter(context, searchDepList);
    rcSearchDeps.setAdapter(rcSearchAdapter);

    rcSearchAdapter.setmOnItemClickListener(new DepartmentsSearchRvAdapter.OnItemClickListener() {
      @Override
      public void onItemClick(View view, final int position) {
        if (searchDepList.get(position).depType.compareTo("place") == 0) {

          Places.GeoDataApi.getPlaceById(mGoogleApiClient, searchDepList.get(position).code)
            .setResultCallback(new ResultCallback<PlaceBuffer>() {
              @Override
              public void onResult(PlaceBuffer places) {
                if (places.getStatus().isSuccess()) {
                  final Place myPlace = places.get(0);
                  LatLng queriedLocation = myPlace.getLatLng();
                  if (selectedPlace != null)
                    selectedPlace.remove();
                  if (queriedLocation.latitude != 0 && queriedLocation.longitude != 0) {
                    if (bottomSheet.getVisibility() == View.VISIBLE)
                      gotoMarker(queriedLocation.latitude - 0.0140d, queriedLocation.longitude, 13);
                    else
                      gotoMarker(queriedLocation.latitude, queriedLocation.longitude, 13);
                    LatLng center = new LatLng(queriedLocation.latitude, queriedLocation.longitude);
                    selectedPlace = mMap123.addMarker(new MarkerOptions()
                      .position(center)
                      .title(searchDepList.get(position).depAddress)
                      .anchor(0.5f, 0.5f)
                      .icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_selected_department)));
                    backFromSearch();
                    lvCities.postDelayed(new Runnable() {
                      @Override
                      public void run() {
                        onCameraMove();
                      }
                    }, 500);
                  }
                }
                places.release();
              }
            });
        }
        else
        {
          String latitude = searchDepList.get(position).latitude;
          String longitude = searchDepList.get(position).longitude;
          String code = searchDepList.get(position).code;
          searchedDepCode = code;
          gotoMarker(Double.parseDouble(latitude), Double.parseDouble(longitude), 13);
          initDepDetailsItem(code);
          backFromSearch();
          lvCities.postDelayed(new Runnable() {
            @Override
            public void run() {
              onCameraMove();
            }
          }, 500);
        }
      }
    });
    editSearchDep.addTextChangedListener(new TextWatcher() {

      public void afterTextChanged(Editable s) {

      }

      public void beforeTextChanged(CharSequence s, int start,
                                    int count, int after) {
        stopSearchTask();
      }

      public void onTextChanged(CharSequence s, int start,
                                int before, int count)
      {
        if (s.length()==0) {
          //if (isUaString())
          //  rcSearchAdapter.updateRequest(DictionariesDB.getInstance(context).tableWarehouseDetails.selectAllUA());
          //else
            //rcSearchAdapter.updateRequest(DictionariesDB.getInstance(context).tableWarehouseDetails.selectAll());
        }
        else {
//          if (isUaString())
//            rcSearchAdapter.updateRequest(DictionariesDB.getInstance(context).tableWarehouseDetails.selectByAddressUA(s.toString()));
//          else
//            rcSearchAdapter.updateRequest(DictionariesDB.getInstance(context).tableWarehouseDetails.selectByAddress(s.toString()));
          runSearchDeps(s.toString());
          //buildSearchSQL(s.toString());
        }
      }
    });

  }

  private void buildSearchSQL(String searchRequest)
  {
    String cleanRequst = searchRequest.replaceAll("[.,-]", "");
    cleanRequst = cleanRequst.toLowerCase();
    String sk[] = cleanRequst.split(" ");

    //rcSearchAdapter.updateRequest(DictionariesDB.getInstance(context).tableWarehouseDetails.selectByAddress(sk));
    //rcSearchAdapter.updateRequest(DictionariesDB.getInstance(context).tableWarehouseDetails.selectByAddress(sk));
//    if (cleanRequst.contains("отделение") || cleanRequst.contains("почтомат"))
//    {
//      Log.d("SearchDeps", "Ищем Отделение или почтомат");
//    }
  }

  private boolean isInterfceUA()
  {
    String curLang = GlobalApplicationData.getCurrentLanguage();
    if (curLang.compareTo("ua") == 0)
      return true;
    else
      return false;
  }

  private boolean isUaString()
  {
    InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
    InputMethodSubtype ims = imm.getCurrentInputMethodSubtype();
    if (ims == null) {
      return isInterfceUA();
    }
    else {
      String locale = ims.getLocale();
      if (locale == null || locale.length() == 0)
      {
        return isInterfceUA();
      }
      else {
        if (locale.compareTo("uk") == 0)
          return true;
        else
          return false;
      }
    }
  }

  private void initDepListBottomSheetBehaviour()
  {
    bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
      @Override
      public void onStateChanged(@NonNull View bottomSheet, int newState) {

        /*if (BottomSheetBehavior.STATE_DRAGGING == newState) {
          fabLocateMe.animate().scaleX(0).scaleY(0).setDuration(300).start();
        } else if (BottomSheetBehavior.STATE_COLLAPSED == newState) {
          fabLocateMe.animate().scaleX(1).scaleY(1).setDuration(300).start();
        }*/

        switch (newState) {
          case DepartmentsFragment.STATE_COLLAPSED:
            Log.d("bottomsheet-", "STATE_COLLAPSED");
            toplayerInfo.setVisibility(View.VISIBLE);
            toplayerInfo.postDelayed(new Runnable() {
              @Override
              public void run() {
                int topBarHeight = (int) DisplayUnits.pxToDp(context, (int ) DisplayUnits.dpToPx(context, toplayerInfo.getMeasuredHeight()));
                bottomSheetBehavior.setPeekHeight(topBarHeight);
              }
            }, 100);
            break;
          case DepartmentsFragment.STATE_DRAGGING:
            Log.d("bottomsheet-", "STATE_DRAGGING");
            break;
          case DepartmentsFragment.STATE_EXPANDED:
            Log.d("bottomsheet-", "STATE_EXPANDED");
            toplayerInfo.setVisibility(View.VISIBLE);
            ViewGroup.LayoutParams lp = bottomSheet.getLayoutParams();
            lp.height = Math.round(coordinatorLayout.getHeight()/2);
            bottomSheet.setLayoutParams(lp);
            toplayerInfo.postDelayed(new Runnable() {
              @Override
              public void run() {
                int topBarHeight = (int) DisplayUnits.pxToDp(context, (int ) DisplayUnits.dpToPx(context, toplayerInfo.getMeasuredHeight()));
                bottomSheetBehavior.setPeekHeight(topBarHeight);
              }
            }, 100);
            break;
          case DepartmentsFragment.STATE_ANCHOR_POINT: {
            Log.d("bottomsheet-", "STATE_ANCHOR_POINT");
            toplayerInfo.setVisibility(View.GONE);
            break;
          }
          case DepartmentsFragment.STATE_HIDDEN:
            Log.d("bottomsheet-", "STATE_HIDDEN");
            break;
          default:
            Log.d("bottomsheet-", "STATE_SETTLING");
            break;
        }
      }

      @Override
      public void onSlide(@NonNull View bottomSheet, float slideOffset) {
        //Log.d(TAG, "Top: " +  bottomSheet.getTop() + " height: " + coordinatorLayout.getHeight());
        //if (bottomSheet.getTop()>Math.round(coordinatorLayout.getHeight()/2));
        //  fabLocateMe.animate().scaleX(1 - slideOffset).scaleY(1 - slideOffset).setDuration(0).start();
      }
    });
    bottomSheetBehavior.setState(DepartmentsFragment.STATE_ANCHOR_POINT);
  }

  private void initDepListLayot(final View view)
  {
//    coordinatorLayout = (CoordinatorLayout) view.findViewById(R.id.coordinatorlayout);
//    //NestedScrollView bottomSheet = (NestedScrollView) coordinatorLayout.findViewById(R.id.nestedScroll);
//
//    bottomSheet = (LinearLayout) coordinatorLayout.findViewById(R.id.nestedScroll);
//    bottomSheet.setVisibility(View.GONE);

//    NestedScrollView paramsScroll = null; //(NestedScrollView) coordinatorLayout.findViewById(R.id.paramsScroll);
//    //bottomSheet.state
//    paramsScroll.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
//      @Override
//      public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
//
//        if (scrollY > oldScrollY) {
//          //fabLocateMe.animate().scaleX(1 - v.getMeasuredHeight()).scaleY(1 - v.getMeasuredHeight()).setDuration(0).start();
//          ViewGroup.LayoutParams lp = bottomSheet.getLayoutParams();
//          lp.height = coordinatorLayout.getHeight();
//          bottomSheet.setLayoutParams(lp);
//          //bottomSheetBehavior.setState(BottomSheetBehaviorGoogleMapsLike.STATE_EXPANDED);
//          Log.d("BottomSheet", "Scroll DOWN");
//        }
//        if (scrollY < oldScrollY) {
//
//          Log.d("BottomSheet", "Scroll UP");
//        }
//
//        if (scrollY == 0) {
//          Log.d("BottomSheet", "TOP SCROLL");
//        }
//
//        if (scrollY == (v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight())) {
//          Log.d("BottomSheet", "BOTTOM SCROLL");
//        }
//      }
//    });

    //bottomSheetBehavior = BottomSheetBehavior.from(view.findViewById(R.id.nestedScroll));

    toplayerInfo = (LinearLayout) view.findViewById(R.id.toplayerInfo);
    toplayerInfo.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        //toplayerInfo.setVisibility(View.GONE);
        /*ViewGroup.LayoutParams lp = bottomSheet.getLayoutParams();
        lp.height = 260;
        bottomSheet.setLayoutParams(lp);*/
        bottomSheetBehavior.setState(DepartmentsFragment.STATE_ANCHOR_POINT);
      }
    });
    initDepListBottomSheetBehaviour();
  }

  private void initDepDetailsLayot(final View view)
  {
    coordinatorLayout = (CoordinatorLayout) view.findViewById(R.id.coordinatorlayout);

    depBottomSheet = (LinearLayout) coordinatorLayout.findViewById(R.id.depNestedScroll);
    departmentDetailsNestedScroll = (NestedScrollView) coordinatorLayout.findViewById(R.id.depDetailsParamsScroll);
    //bottomSheet.state
    departmentDetailsNestedScroll.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
      @Override
      public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {

        if (scrollY > oldScrollY) {
          ViewGroup.LayoutParams lp = depBottomSheet.getLayoutParams();
          lp.height = coordinatorLayout.getHeight();
          depBottomSheet.setLayoutParams(lp);
          //bottomSheetBehavior.setState(BottomSheetBehaviorGoogleMapsLike.STATE_EXPANDED);
          Log.d("BottomSheet", "Scroll DOWN");
        }
        if (scrollY < oldScrollY) {

          Log.d("BottomSheet", "Scroll UP");
        }

        if (scrollY == 0) {
          Log.d("BottomSheet", "TOP SCROLL");
        }

        if (scrollY == (v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight())) {
          Log.d("BottomSheet", "BOTTOM SCROLL");
        }
      }
    });

    depBottomSheetBehavior = MyBottomSheetBehavior.from(view.findViewById(R.id.depNestedScroll));
    depDetailsTopLayerInfo = (LinearLayout) view.findViewById(R.id.depDetailsTopLayerInfo);
    depBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
      @Override
      public void onStateChanged(@NonNull View bottomSheet, int newState) {

        /*if (BottomSheetBehavior.STATE_DRAGGING == newState) {
          fabLocateMe.animate().scaleX(0).scaleY(0).setDuration(300).start();
        } else if (BottomSheetBehavior.STATE_COLLAPSED == newState) {
          fabLocateMe.animate().scaleX(1).scaleY(1).setDuration(300).start();
        }*/

        switch (newState) {
          case DepartmentsFragment.STATE_COLLAPSED:
            Log.d("bottomsheet-", "STATE_COLLAPSED");
            //depDetailsTopLayerInfo.setVisibility(View.VISIBLE);
            break;
          case DepartmentsFragment.STATE_DRAGGING:
            Log.d("bottomsheet-", "STATE_DRAGGING");
            break;
          case DepartmentsFragment.STATE_EXPANDED:
            Log.d("bottomsheet-", "STATE_EXPANDED");
            //depDetailsTopLayerInfo.setVisibility(View.VISIBLE);
//            ViewGroup.LayoutParams lp = bottomSheet.getLayoutParams();
//            lp.height = Math.round(coordinatorLayout.getHeight()/2);
//            bottomSheet.setLayoutParams(lp);
            departmentDetailsNestedScroll.scrollTo(0, 0);
//            bottomSheet.postDelayed(new Runnable() {
//              @Override
//              public void run() {
//                departmentDetailsNestedScroll.scrollTo(0, 0);
//              }
//            }, 100);
            break;
          case DepartmentsFragment.STATE_ANCHOR_POINT: {
            Log.d("bottomsheet-", "STATE_ANCHOR_POINT");
            //depDetailsTopLayerInfo.setVisibility(View.GONE);
            break;
          }
          case DepartmentsFragment.STATE_HIDDEN:
            Log.d("bottomsheet-", "STATE_HIDDEN");
            break;
          default:
            Log.d("bottomsheet-", "STATE_SETTLING");
            break;
        }
      }

      @Override
      public void onSlide(@NonNull View bottomSheet, float slideOffset) {
        fabLocateMe.animate().scaleX(1 - slideOffset).scaleY(1 - slideOffset).setDuration(0).start();
      }
    });
    //depBottomSheetBehavior.setState(DepartmentsActivity.STATE_ANCHOR_POINT);
    depBottomSheet.setVisibility(View.GONE);
    depView = new DepartmentDetailsView(context, this, view);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    lang = GlobalApplicationData.getCurrentLanguage();
    View view = inflater.inflate(R.layout.activity_department_list, container, false);
    context = getContext();
    toolBar = (Toolbar) view.findViewById(R.id.mapToolbar1);
    setHasOptionsMenu(true);
    locMyLocation = GlobalApplicationData.getInstance().loadLastLocation();
    Lat = Double.toString(locMyLocation.getLatitude());
    Lng = Double.toString(locMyLocation.getLongitude());
    searchDepData = new SearchDepData();
    networkChangeReceiver = new NetworkChangeReceiver();
    networkChangeReceiver.setNetworkState(this);
    context.registerReceiver(networkChangeReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    initGooglePlaces();
//    if (GlobalApplicationData.getInstance().isGPSEnabled()) {
//      startLocation();
//    }
    return view;
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    mDepActivity = this;
    depFilterSettingds = GlobalApplicationData.getInstance().loadDepartmentFilterSettings();
    dictionariesDB = DictionariesDB.getInstance(context);
    final DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
    //screenHeightDp = (int) DisplayUnits.pxToDp(context, displayMetrics.heightPixels);

    Toolbar toolbar = (Toolbar) view.findViewById(R.id.mapToolbar1);
    toolbar.setTitle("");
//    toolbar.setVisibility(View.GONE);
    ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
    fabLocateMe = (FloatingActionButton) view.findViewById(R.id.fabLocateMe);
    fabLocateMe.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Log.d(TAG, "Start Location");
        startLocation();
      }
    });
    frOfflineBack = (FrameLayout) view.findViewById(R.id.frOfflineBack);
    tvDeplistTitle = (TextView) view.findViewById(R.id.tvDeplistTitle);
    coordinatorLayout = (CoordinatorLayout) view.findViewById(R.id.coordinatorlayout);
    bottomSheet = (LinearLayout) coordinatorLayout.findViewById(R.id.nestedScroll);
    bottomSheetBehavior = BottomSheetBehavior.from(view.findViewById(R.id.nestedScroll));
    llSearchDepartments = (LinearLayout) view.findViewById(R.id.llSearchDepartments);
    lvCities = (RecyclerView) view.findViewById(R.id.lvCities);
    lvCitiesOffline = (RecyclerView) view.findViewById(R.id.rvCitiesOffline);
    rvAdapter = new DepartmentsRvAdapter(context, depList);
    //AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.main_appbar);

    /*coordinatorLayoutParams = (CoordinatorLayout.LayoutParams) appBarLayout.getLayoutParams();
    coordinatorLayoutHeight = coordinatorLayoutParams.height;

    AppBarLayout.Behavior behavior = new AppBarLayout.Behavior();
    behavior.setDragCallback(new AppBarLayout.Behavior.DragCallback() {
      @Override
      public boolean canDrag(AppBarLayout appBarLayout) {
        return false;
      }
    });
    coordinatorLayoutParams.setBehavior(behavior);*/
    networkConnected(GlobalApplicationData.getInstance().IsOnline());
    initListView(view);
    initListViewOffline(view);
    initMap();
    initDepListLayot(view);
    initDepDetailsLayot(view);
    initSesarchDepartmentListView(view);
    //initDepetailsView();
  }

  @Override
  public void onDestroyView() {
    super.onDestroyView();
    try {
      if (mapFragment != null)
        getFragmentManager().beginTransaction().remove(mapFragment).commit();
    }
    catch (Exception e) { }
    try {
      ((MainActivity) getActivity()).setToolbar(null);
    } catch (Exception e) {}
  }

  @Override
  public void onActivityCreated(@Nullable Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
    try {
      ((MainActivity) getActivity()).setToolbar(toolBar);
    } catch (Exception e) {}
  }

//  @Override
//  public void onCreate(Bundle savedInstanceState) {
//    super.onCreate(savedInstanceState);
//    setContentView(R.layout.activity_department_list);
//    context = this;
//    mDepActivity = this;
//    depFilterSettingds = GlobalApplicationData.getInstance().loadDepartmentFilterSettings();
//    dictionariesDB = DictionariesDB.getInstance(context);
//    final DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
//    //screenHeightDp = (int) DisplayUnits.pxToDp(context, displayMetrics.heightPixels);
//
//    Toolbar toolbar = (Toolbar) findViewById(R.id.mapToolbar1);
//    setSupportActionBar(toolbar);
//    fabLocateMe = (FloatingActionButton) findViewById(R.id.fabLocateMe);
//    fabLocateMe.setOnClickListener(new View.OnClickListener() {
//      @Override
//      public void onClick(View v) {
//        Log.d(TAG, "Start Location");
//        startLocation();
//      }
//    });
//
//    Intent depIntent = getIntent();
//    depCode = depIntent.getStringExtra("code");
//    depType = depIntent.getStringExtra("depType");
//    //AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.main_appbar);
//
//    /*coordinatorLayoutParams = (CoordinatorLayout.LayoutParams) appBarLayout.getLayoutParams();
//    coordinatorLayoutHeight = coordinatorLayoutParams.height;
//
//    AppBarLayout.Behavior behavior = new AppBarLayout.Behavior();
//    behavior.setDragCallback(new AppBarLayout.Behavior.DragCallback() {
//      @Override
//      public boolean canDrag(AppBarLayout appBarLayout) {
//        return false;
//      }
//    });
//    coordinatorLayoutParams.setBehavior(behavior);*/
//    initListView();
//    initMap();
//    initDepListLayot();
//    initDepDetailsLayot();
//    //initDepetailsView();
//  }

  private void showMessage(String caption, String message) {
    MessageDialog dlgWarning = new MessageDialog();
    dlgWarning.setParams(caption, message, false, null);
    dlgWarning.show(getChildFragmentManager(), "Warning");
  }

  @Override
  public void onResume() {
    super.onResume();
    if (mMap123!=null)
      mMap123.setOnCameraMoveListener(this);
    mapActive = true;
  }

  @Override
  public void onPause() {
    super.onPause();
    Log.d("Dep123", "Departments fragment pause");
    stopTask();
    stopSearchTask();
    if (mMap123 != null) {
      Location oldLocation = new Location(LocationManager.GPS_PROVIDER);
      oldLocation.setLongitude(mMap123.getCameraPosition().target.longitude);
      oldLocation.setLatitude(mMap123.getCameraPosition().target.latitude);
      GlobalApplicationData.getInstance().saveLastLocation(oldLocation);
    }
    else
      if (locMyLocation != null)
        GlobalApplicationData.getInstance().saveLastLocation(locMyLocation);
    if (mMap123!=null)
      mMap123.setOnCameraMoveListener(null);
    mapActive = false;
  }

  private void stopTask(){
    if (service!=null)
      service.shutdownNow();
    if (loadDeps!=null) {
      loadDeps.cancel(false);
      loadDeps.unlink();
      loadDeps = null;
    }
  }

  private void stopSearchTask(){
    if (searchService != null)
      searchService.shutdownNow();
    if (searchDeps != null) {
      searchDeps.cancel(false);
      searchDeps.unlink();
      searchDeps = null;
    }
  }

  @Override
  public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    if (showOnMap)
    {
      inflater.inflate(R.menu.departments_list_menu, menu);
      miClose = menu.findItem(R.id.action_close_map);
    }
    else {
      inflater.inflate(R.menu.departments_list_fragment_menu, menu);
      miSearch = menu.findItem(R.id.action_search_deps);
      miClose = menu.findItem(R.id.action_dep_search_close);
      miFilter = menu.findItem(R.id.action_dep_filter);
    }
    super.onCreateOptionsMenu(menu,inflater);
  }

  private void removeRoute()
  {
    if (finalPolyline != null)
      finalPolyline.remove();
  }

  private void searchDeps()
  {
    llSearchDepartments.setVisibility(View.VISIBLE);
    miSearch.setVisible(false);
    miFilter.setVisible(false);
    miClose.setVisible(true);
    tvDeplistTitle.setText(context.getResources().getString(R.string.search_deps));
    editSearchDep.requestFocus();
  }

  public void hideSoftKeyboard() {
    try {
      InputMethodManager inputMethodManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
      inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
    } catch (Exception e) { }
  }

  private void backFromSearch()
  {
    hideSoftKeyboard();
    llSearchDepartments.setVisibility(View.GONE);
    miSearch.setVisible(true);
    miFilter.setVisible(true);
    miClose.setVisible(false);
    tvDeplistTitle.setText(context.getResources().getString(R.string.text_title_departments));
  }

  public void getRoute()
  {
    removeRoute();
    if (locMyLocation!=null && selectedDepLocation!=null) {
      new GetRoute(mMap123, this, Double.toString(locMyLocation.getLatitude()),
        Double.toString(locMyLocation.getLongitude()),
        Double.toString(selectedDepLocation.getLatitude()),
        Double.toString(selectedDepLocation.getLongitude())).execute();
    }
    else
    {
      showMessage(getResources().getString(R.string.dialog_warning_caption),
        getResources().getString(R.string.build_route_tip));
    }
  }

  public void onBackPressed()
  {
    ((DepartmentsActivity) getActivity()).onBackPressed();
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();
    switch (id) {
      case R.id.action_search_deps:
        searchDeps();
        return true;
      case R.id.action_close_map:
        onBackPressed();
        return true;
      case R.id.action_dep_search_close:
        backFromSearch();
        return true;
//      case R.id.action_build_road:
//        getRoute();
//        return true;
      case R.id.action_dep_filter:
        startActivityForResult(new Intent(context, DepartmentsFilterActivity.class), 3011);
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (data == null) { return; }
    if (resultCode == RESULT_OK) {
      Log.d(TAG, "Filter saved");
      stopTask();
      depFilterSettingds = GlobalApplicationData.getInstance().loadDepartmentFilterSettings();
      mMap123.clear();
      mClusterManager.clearItems();
      for (MyClusterItem entry : markers) {
        entry.setAdded(false);
      }
      stopTask();
      runSearchStart();
    }
  }

  @Override
  public boolean onClusterClick(Cluster<MyClusterItem> cluster) {
    Log.d(TAG, "ClusterClick");
    currentZoomLevel = currentZoomLevel + 2;
    if (currentZoomLevel>=18)
      currentZoomLevel=18;
    gotoMarker(cluster.getPosition().latitude, cluster.getPosition().longitude, currentZoomLevel);
    LatLngBounds.Builder builder = LatLngBounds.builder();
    for (ClusterItem item : cluster.getItems()) {
      builder.include(item.getPosition());
    }
    final LatLngBounds bounds = builder.build();
    mMap123.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 1));
    return false;
  }

  /*private void setSingleDepItem(String code) {
    singleDepList.clear();
    for (DepartmentMapItem di : depList) {
      if (di.code.compareTo(code) == 0) {

        //coordinatorLayoutParams.height = (int) DisplayUnits.dpToPx(context,
        //  screenHeightDp - (int) DisplayUnits.pxToDp(context, oldHeight + 60));

        singleDepList.add(di);
        rvAdapter.setDataList(singleDepList);
        rvAdapter.notifyDataSetChanged();
        //oldHeight = lvCities.getChildAt(0).getMeasuredHeight(); //(int) DisplayUnits.dpToPx(context,
        Log.d(TAG, "Height " + lvCities.getChildAt(0).getMeasuredHeight());
      }
    }
  }*/

  private void initDepDetailsItem(String code)
  {
    depView.selectDepDetails(code);
    //departmentDetailsNestedScroll.scrollTo(0, 0);
    bottomSheet.setVisibility(View.GONE);
    depBottomSheet.setVisibility(View.VISIBLE);
    CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) fabLocateMe.getLayoutParams();
    params.setAnchorId(R.id.depNestedScroll);
    fabLocateMe.setLayoutParams(params);
    bottomSheet.postDelayed(new Runnable() {
      @Override
      public void run() {
        depBottomSheetBehavior.setPeekHeight(depView.getTopBarHeight());
        departmentDetailsNestedScroll.scrollTo(0, 0);
      }
    }, 100);
  }

  private void initDepDetailsItemOffline(String code)
  {
    DepartmentDetailsFragment frDepDetails = new DepartmentDetailsFragment();
    frDepDetails.setCode(code);
    MainFragmentManager.pushChildFragment(getActivity(), frDepDetails, "DepDetails");
  }

  private void switchClisterItemSelected(final MyClusterItem myClusterItem)
  {
    selectedDepLocation = new Location("");
    selectedDepLocation.setLatitude(myClusterItem.getPosition().latitude);
    selectedDepLocation.setLongitude(myClusterItem.getPosition().longitude);
    if (oldItemSelected != null) {
      oldItemSelected.setSelected(false);
      try {
        ((ClusterRenderer) mClusterManager.getRenderer()).getMarker(oldItemSelected).setIcon(BitmapDescriptorFactory.fromResource(oldItemSelected.getIcon()));
      }
      catch (Exception e)
      {
        Log.d(TAG, "Error 1: " + e.getMessage());
      }
    }
    myClusterItem.setSelected(true);
    try {
      ((ClusterRenderer) mClusterManager.getRenderer()).getMarker(myClusterItem).setIcon(BitmapDescriptorFactory.fromResource(myClusterItem.getIcon()));
    }
    catch (Exception e)
    {
      Log.d(TAG, "Error 2: " + e.getMessage());
    }
    mClusterManager.cluster();
    oldItemSelected = myClusterItem;
  }

  @Override
  public boolean onClusterItemClick(MyClusterItem myClusterItem) {
    Log.d(TAG, "Cluster 1 click");
    clusterClick = true;
    //setSingleDepItem(myClusterItem.getCode());
    initDepDetailsItem(myClusterItem.getCode());
    switchClisterItemSelected(myClusterItem);
    return false;
  }

  @Override
  public void buildFinish(Polyline polyline) {
    finalPolyline = polyline;
  }

  @Override
  public void onStart() {
    super.onStart();
    mGoogleApiClient.connect();
  }

  @Override
  public void onStop() {
    super.onStop();
    mGoogleApiClient.disconnect();
  }

  @Override
  public void onDestroy()
  {
    if (networkChangeReceiver != null) {
      context.unregisterReceiver(networkChangeReceiver);
      if (networkChangeReceiver != null)
        networkChangeReceiver.setNetworkState(null);
    }
    super.onDestroy();
  }

  @Override
  public void onCameraMove() {
    Log.d(TAG, "Camera move");
    Log.d(TAG, "Camera Zoom: " + Float.toString(mMap123.getCameraPosition().zoom));
    if (clusterClick == false) {
      currentZoomLevel = mMap123.getCameraPosition().zoom;
      stopTask();
      runSearchStart();
    }
  }

  @Override
  public void onConnected(@Nullable Bundle bundle) {

  }

  @Override
  public void onConnectionSuspended(int i) {

  }

  @Override
  public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

  }

  @Override
  public void networkConnected(boolean connect) {
     if (connect)
     {
       frOfflineBack.setVisibility(View.GONE);
       fabLocateMe.setVisibility(View.VISIBLE);
       bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
     }
     else
     {
       frOfflineBack.setVisibility(View.VISIBLE);
       fabLocateMe.setVisibility(View.GONE);
     }
  }

  private static class LoadDeps extends AsyncTask<Void, MyClusterItem, Void>{

    private Location myLocation;
    private double humanDist = 0;
    private DepartmentsFragment depActivity;
    private Set<String> codeList;
    private String state;
    private String city;
    private String area;

    public void linkMain(DepartmentsFragment activity)
    {
      depActivity = activity;
    }

    public void unlink()
    {
      depActivity = null;
    }

    private boolean checkForFilter(final Cursor cursor)
    {
      try {
        if (depActivity.depType == null && depActivity.depCode == null &&
          (Integer.parseInt(getColumnValue(cursor, WarehouseDetailsTable.Column.TYPE_ID)) == 1)) {
          return false;
        }
      }
      catch (Exception e)
      {

      }
      try {
        if ((Integer.parseInt(getColumnValue(cursor, WarehouseDetailsTable.Column.CAPACITY))==31) && depActivity.depFilterSettingds.to30kg) {
          return true;
        }
      }
      catch (Exception e)
      {
        //Log.d(TAG, e.getMessage());
      }
      try{
        if ((Integer.parseInt(getColumnValue(cursor, WarehouseDetailsTable.Column.CAPACITY))==70) && depActivity.depFilterSettingds.to70kg) {
          return true;
        }
      }
      catch (Exception e)
      {
        //Log.d(TAG, e.getMessage());
      }
      try{
        if ((Integer.parseInt(getColumnValue(cursor, WarehouseDetailsTable.Column.CAPACITY))==1000) && depActivity.depFilterSettingds.to1000kg) {
          return true;
        }
      }
      catch (Exception e)
      {
        //Log.d(TAG, e.getMessage());
      }
      try{
        if ((getColumnValue(cursor, WarehouseDetailsTable.Column.TYPE).compareTo("postmat")==0) && depActivity.depFilterSettingds.postomats) {
          return true;
        }
      }
      catch (Exception e)
      {
        //Log.d(TAG, e.getMessage());
      }
      return false;
    }

    public LoadDeps(Location myLocation)
    {
      this.myLocation = myLocation;
    }

    private String buildNotInList()
    {
      StringBuilder notIn = new StringBuilder();
      notIn.append(" NOT IN (");

      int i = 0;
      for (String item : codeList)
      {
          if (i!=codeList.size() - 1 )
            notIn.append(item + ", ");
          else
            notIn.append(item + ") ");
          i++;
      }
      //Log.d(TAG, notIn.toString());
      return notIn.toString();
    }

    private void orderNearest()
    {
      Collections.sort(depActivity.depList, new Comparator<DepartmentMapItem>() {
          @Override
          public int compare(DepartmentMapItem o1, DepartmentMapItem o2) {
            if (o1.distance < o2.distance)
              return -1;
            else
              return 1;
          }
        });
    }

    private void findNearestDepartment()
    {
      int count = 1;
      Cursor cursor;

      if (depActivity.lang.compareTo("ru") == 0)
        cursor = DictionariesDB.getInstance(depActivity.context).tableWarehouseDetails.selectAll();
      else
        cursor = DictionariesDB.getInstance(depActivity.context).tableWarehouseDetails.selectAllUA();

      cursor.moveToFirst();
      while (!cursor.isAfterLast()) {
        if (!depActivity.mapActive || isCancelled()) {
          Log.d(TAG, "Terimate task add markers");
          cursor.close();
          return;
        }
            if (myLocation != null) {
              double dist = DistanceCalculator.distance(Double.parseDouble(getColumnValue(cursor, WarehouseDetailsTable.Column.LAT)),
                Double.parseDouble(getColumnValue(cursor, WarehouseDetailsTable.Column.LNG)),
                myLocation.getLatitude(), myLocation.getLongitude());
              humanDist = dist / 1000;
            }

            try {

              if (humanDist <= 5 && checkForFilter(cursor)) {
                  if (city == null) {
                    state = getColumnValue(cursor, WarehouseDetailsTable.Column.STATE);
                    area = getColumnValue(cursor, WarehouseDetailsTable.Column.AREA);
                    city = getColumnValue(cursor, WarehouseDetailsTable.Column.CITY);
                  }

                  try {

                    MyClusterItem mc1 = depActivity.addDepMarker(getColumnValue(cursor, WarehouseDetailsTable.Column.LNG),
                      getColumnValue(cursor, WarehouseDetailsTable.Column.LAT),
                      getColumnValue(cursor, WarehouseDetailsTable.Column.TYPE),
                      getColumnValue(cursor, WarehouseDetailsTable.Column.ADDRESS),
                      getColumnValue(cursor, WarehouseDetailsTable.Column.NUMBER),
                      getColumnValue(cursor, WarehouseDetailsTable.Column.CODE),
                      getColumnValue(cursor, WarehouseDetailsTable.Column.CAPACITY));

                    codeList.add(getColumnValue(cursor, WarehouseDetailsTable.Column.CODE));
                    //Log.d(TAG, "ID: " + getColumnValue(cursor, WarehouseDetailsTable.Column.ID));
                    depActivity.addDepItem(getColumnValue(cursor, WarehouseDetailsTable.Column.LNG),
                      getColumnValue(cursor, WarehouseDetailsTable.Column.LAT),
                      getColumnValue(cursor, WarehouseDetailsTable.Column.TYPE),
                      getColumnValue(cursor, WarehouseDetailsTable.Column.ADDRESS),
                      getColumnValue(cursor, WarehouseDetailsTable.Column.NUMBER),
                      getColumnValue(cursor, WarehouseDetailsTable.Column.CODE),
                      MyDateUtils.getWorhHoursStart(cursor),
                      MyDateUtils.getWorhHoursEnd(cursor),
                      getColumnValue(cursor, WarehouseDetailsTable.Column.CAPACITY),
                      getColumnValue(cursor, WarehouseDetailsTable.Column.CITY),
                      getColumnValue(cursor, WarehouseDetailsTable.Column.STATE),
                      Integer.parseInt(getColumnValue(cursor, WarehouseDetailsTable.Column.DUBLICATE_CITY)),
                      humanDist,
                      getColumnValue(cursor, WarehouseDetailsTable.Column.MAX_LENGTH),
                      MyDateUtils.getNextWorhHours(cursor)
                    );

                    if (mc1 != null)
                      publishProgress(mc1);

                  } catch (Exception e) {
                    Log.d(TAG, "Load MapDeps error: " + e.getMessage());
                  }


                  count++;
                  if (count >= 5) {
                    orderNearest();
                    if (cursor != null && !cursor.isClosed())
                      cursor.close();
                    return;
                  }
                }
              } catch(Exception e){
                Log.d(TAG, "Load MapDeps error: " + e.getMessage());
              }

        cursor.moveToNext();
      }
      if (depActivity.depList.size()>1)
        orderNearest();
      if (cursor!=null && !cursor.isClosed())
        cursor.close();
      return;
    }

    private boolean addOtherDepsInCity()
    {

      Cursor cursor;
      String notInList = buildNotInList();
//      if (depActivity.currentZoomLevel <= 9f) {
//        Log.d(TAG, "Load few Deps");
//        if (depActivity.lang.compareTo("ru") == 0)
//          cursor = DictionariesDB.getInstance(depActivity.context).tableWarehouseDetails.selectByCSAMinZoom(city, state, area, notInList);
//        else
//          cursor = DictionariesDB.getInstance(depActivity.context).tableWarehouseDetails.selectByCSAUAMinZoom(city, state, area, notInList);
//      }
//      else {
        if (depActivity.lang.compareTo("ru") == 0)
          cursor = DictionariesDB.getInstance(depActivity.context).tableWarehouseDetails.selectByCSA(city, state, area, notInList);
        else
          cursor = DictionariesDB.getInstance(depActivity.context).tableWarehouseDetails.selectByCSAUA(city, state, area, notInList);
//      }
      cursor.moveToFirst();
      while (!cursor.isAfterLast()) {
        if (!depActivity.mapActive || isCancelled()) {
          Log.d(TAG, "Terimate task add markers");
          cursor.close();
          return false;
        }
        if (checkForFilter(cursor))
        {
          if (myLocation != null) {
            double dist = DistanceCalculator.distance(Double.parseDouble(getColumnValue(cursor, WarehouseDetailsTable.Column.LAT)),
              Double.parseDouble(getColumnValue(cursor, WarehouseDetailsTable.Column.LNG)),
              myLocation.getLatitude(), myLocation.getLongitude());
            humanDist = dist / 1000;
          }

          try {

            MyClusterItem mc1 = depActivity.addDepMarker(getColumnValue(cursor, WarehouseDetailsTable.Column.LNG),
              getColumnValue(cursor, WarehouseDetailsTable.Column.LAT),
              getColumnValue(cursor, WarehouseDetailsTable.Column.TYPE),
              getColumnValue(cursor, WarehouseDetailsTable.Column.ADDRESS),
              getColumnValue(cursor, WarehouseDetailsTable.Column.NUMBER),
              getColumnValue(cursor, WarehouseDetailsTable.Column.CODE),
              getColumnValue(cursor, WarehouseDetailsTable.Column.CAPACITY));

            //Log.d(TAG, "ID: " + getColumnValue(cursor, WarehouseDetailsTable.Column.ID));
            depActivity.addDepItem(getColumnValue(cursor, WarehouseDetailsTable.Column.LNG),
              getColumnValue(cursor, WarehouseDetailsTable.Column.LAT),
              getColumnValue(cursor, WarehouseDetailsTable.Column.TYPE),
              getColumnValue(cursor, WarehouseDetailsTable.Column.ADDRESS),
              getColumnValue(cursor, WarehouseDetailsTable.Column.NUMBER),
              getColumnValue(cursor, WarehouseDetailsTable.Column.CODE),
              MyDateUtils.getWorhHoursStart(cursor),
              MyDateUtils.getWorhHoursEnd(cursor),
              getColumnValue(cursor, WarehouseDetailsTable.Column.CAPACITY),
              getColumnValue(cursor, WarehouseDetailsTable.Column.CITY),
              getColumnValue(cursor, WarehouseDetailsTable.Column.STATE),
              Integer.parseInt(getColumnValue(cursor, WarehouseDetailsTable.Column.DUBLICATE_CITY)),
              humanDist,
              getColumnValue(cursor, WarehouseDetailsTable.Column.MAX_LENGTH),
              MyDateUtils.getNextWorhHours(cursor)
            );

            if (mc1 != null)
              publishProgress(mc1);

          } catch (Exception e) {
            Log.d(TAG, "Load MapDeps error: " + e.getMessage());
          }
        }
        cursor.moveToNext();
      }
      if (cursor!=null && !cursor.isClosed())
        cursor.close();
      return true;
    }

    private boolean addDepsInBounds()
    {
      Cursor cursor;
//      if (depActivity.currentZoomLevel <= 9f) {
//        Log.d(TAG, "Load few markers");
//        if (depActivity.lang.compareTo("ru") == 0)
//          cursor = DictionariesDB.getInstance(depActivity.context).tableWarehouseDetails.selectByBoundsMinZoom(depActivity.bounds);
//        else
//          cursor = DictionariesDB.getInstance(depActivity.context).tableWarehouseDetails.selectByBoundsUAMinZoom(depActivity.bounds);
//      }
//      else {
        if (depActivity.lang.compareTo("ru") == 0)
          cursor = DictionariesDB.getInstance(depActivity.context).tableWarehouseDetails.selectByBounds(depActivity.bounds);
        else
          cursor = DictionariesDB.getInstance(depActivity.context).tableWarehouseDetails.selectByBoundsUA(depActivity.bounds);
//      }
      cursor.moveToFirst();

      if (cursor.getCount() > 0) {
        if (city == null) {
          state = getColumnValue(cursor, WarehouseDetailsTable.Column.STATE);
          area = getColumnValue(cursor, WarehouseDetailsTable.Column.AREA);
          city = getColumnValue(cursor, WarehouseDetailsTable.Column.CITY);
        }

        while (!cursor.isAfterLast()) {
          if (!depActivity.mapActive || isCancelled()) {
            Log.d(TAG, "Terimate task add markers");
            cursor.close();
            return false;
          }
          if (checkForFilter(cursor) && !codeList.contains(getColumnValue(cursor, WarehouseDetailsTable.Column.CODE))) {
            if (myLocation != null) {
              double dist = DistanceCalculator.distance(Double.parseDouble(getColumnValue(cursor, WarehouseDetailsTable.Column.LAT)),
                Double.parseDouble(getColumnValue(cursor, WarehouseDetailsTable.Column.LNG)),
                myLocation.getLatitude(), myLocation.getLongitude());
              humanDist = dist / 1000;
            }

            try {

              MyClusterItem mc1 = depActivity.addDepMarker(getColumnValue(cursor, WarehouseDetailsTable.Column.LNG),
                getColumnValue(cursor, WarehouseDetailsTable.Column.LAT),
                getColumnValue(cursor, WarehouseDetailsTable.Column.TYPE),
                getColumnValue(cursor, WarehouseDetailsTable.Column.ADDRESS),
                getColumnValue(cursor, WarehouseDetailsTable.Column.NUMBER),
                getColumnValue(cursor, WarehouseDetailsTable.Column.CODE),
                getColumnValue(cursor, WarehouseDetailsTable.Column.CAPACITY));

              codeList.add(getColumnValue(cursor, WarehouseDetailsTable.Column.CODE));

              //Log.d(TAG, "ID: " + getColumnValue(cursor, WarehouseDetailsTable.Column.ID));
              depActivity.addDepItem(getColumnValue(cursor, WarehouseDetailsTable.Column.LNG),
                getColumnValue(cursor, WarehouseDetailsTable.Column.LAT),
                getColumnValue(cursor, WarehouseDetailsTable.Column.TYPE),
                getColumnValue(cursor, WarehouseDetailsTable.Column.ADDRESS),
                getColumnValue(cursor, WarehouseDetailsTable.Column.NUMBER),
                getColumnValue(cursor, WarehouseDetailsTable.Column.CODE),
                MyDateUtils.getWorhHoursStart(cursor),
                MyDateUtils.getWorhHoursEnd(cursor),
                getColumnValue(cursor, WarehouseDetailsTable.Column.CAPACITY),
                getColumnValue(cursor, WarehouseDetailsTable.Column.CITY),
                getColumnValue(cursor, WarehouseDetailsTable.Column.STATE),
                Integer.parseInt(getColumnValue(cursor, WarehouseDetailsTable.Column.DUBLICATE_CITY)),
                humanDist,
                getColumnValue(cursor, WarehouseDetailsTable.Column.MAX_LENGTH),
                MyDateUtils.getNextWorhHours(cursor)
              );

              if (mc1 != null)
                publishProgress(mc1);

            } catch (Exception e) {
              Log.d(TAG, "Load MapDeps error: " + e.getMessage());
            }
          }
          cursor.moveToNext();
        }
      }
      if (cursor!=null && !cursor.isClosed())
        cursor.close();
      return true;
    }

    @Override
    protected Void doInBackground(Void... params) {

      Log.d(TAG, "Run add markers");
      Log.d(TAG, "Markers count: " + depActivity.markers.size() );
      codeList = new LinkedHashSet<String>();
      findNearestDepartment();
      if (addDepsInBounds() && codeList.size() > 0/* && depActivity.currentZoomLevel > 9f*/)
        addOtherDepsInCity();
      return null;
    }

    @Override
    protected void onPreExecute() {
      super.onPreExecute();
      depActivity.mClusterManager.clearItems();
      depActivity.depList.clear();
      depActivity.markers.clear();
    }

    @Override
    protected void onPostExecute(Void aVoid) {
      super.onPostExecute(aVoid);
      if (depActivity != null) {
        depActivity.mClusterManager.cluster();
        //if (singleDepList!=null && singleDepList.size()==0) {
        depActivity.rvAdapter.setDataList(depActivity.depList);
        depActivity.rvAdapter.notifyDataSetChanged();
        //}
        if (depActivity.depCode != null && depActivity.depType != null && depActivity.clusterShow == false) {
          depActivity.clusterShow = true;
          for (MyClusterItem mc : depActivity.markers) {
            if (mc != null && mc.getCode().compareTo(depActivity.depCode) == 0)
              depActivity.onClusterItemClick(mc);
          }
        }
        depActivity.selectSearchedMarker();
      }
      Log.d(TAG, "Task is completted");
    }

    @Override
    protected void onProgressUpdate(MyClusterItem... entry) {
      super.onProgressUpdate(entry);
      if (depActivity!=null && entry!=null) {
        if (!entry[0].isAdded()) {
          depActivity.mClusterManager.addItem(entry[0]);
          entry[0].setAdded(true);
        }
      }
    }
  }

  private void initGooglePlaces()
  {
    mGoogleApiClient = new GoogleApiClient
      .Builder(context)
      .addApi(Places.GEO_DATA_API)
      .addConnectionCallbacks(this)
      .addOnConnectionFailedListener(this)
      .build();
  }

  private static class SearchDeps extends AsyncTask<Void, Void, Void>{

    private Location myLocation;
    private double humanDist = 0;
    private DepartmentsFragment depActivity;
    private SearchDepData searchDepData;
    private String sk[];
    private String searchString;
    private ArrayList<Integer> numbers;


    //private LatLngBounds mBounds;
    private GoogleApiClient mGoogleApiClient;
    private ArrayList<AutocompletePrediction> filterData;
    //private AutocompleteFilter mPlaceFilter;

    public void linkMain(DepartmentsFragment activity)
    {
      depActivity = activity;
    }

    public void unlink()
    {
      depActivity = null;
    }

    public SearchDeps(Location myLocation, String searchString, GoogleApiClient mGoogleApiClient, SearchDepData searchDepData)
    {
      this.mGoogleApiClient = mGoogleApiClient;
      //this.mBounds = new LatLngBounds(new LatLng(-34.041458, 150.790100), new LatLng(-33.682247, 151.383362));
      this.myLocation = myLocation;
      this.searchString = searchString;
      this.numbers = new ArrayList<Integer>();
      this.searchDepData = searchDepData;
      this.filterData = new ArrayList<>();
      //this.mPlaceFilter = new AutocompleteFilter.Builder()
      //  .setTypeFilter(AutocompleteFilter.TYPE_FILTER_ADDRESS | AutocompleteFilter.TYPE_FILTER_GEOCODE)
      //  .build();
    }

    @Override
    protected Void doInBackground(Void... params) {

      Log.d(TAG, "Add search depitems ");

      String cleanRequst = searchString.replaceAll("[.,-]", "");
      cleanRequst = cleanRequst.replaceAll("№", "");
      cleanRequst = cleanRequst.toLowerCase();
      sk = cleanRequst.split(" ");

      // получаем только цифры из строки поиска
      for (String str1 : sk)
      {
        try{
          String str = str1.replaceAll("\\D+","");
          if (str.length()>0) {
            numbers.add(Integer.parseInt(str));
            //Log.d(TAG, "STR: " + str);
          }
        }
        catch(Exception e)
        {

        }
      }

      // получение мест
      filterData = new AutocompleteHelper(mGoogleApiClient).getAutocomplete("Украина " + cleanRequst + " ");
      if (filterData != null && filterData.size() > 0)
      {
        for (AutocompletePrediction aup : filterData)
        {
          depActivity.addSearchPlaceDepItem(
            "place",
            aup.getPlaceId(), //code
            aup.getPrimaryText(null).toString(), // city
            aup.getSecondaryText(null).toString(), // address
            aup.getFullText(null).toString(), // full address
            searchDepData.placesList
          );
        }
      }

      if (sk.length>1) {
        Cursor cursor;
        if (depActivity.isUaString())
          cursor = DictionariesDB.getInstance(depActivity.context).tableWarehouseDetails.selectByAddressUA(sk);
        else
          cursor = DictionariesDB.getInstance(depActivity.context).tableWarehouseDetails.selectByAddress(sk);
        loadPrimaryList(cursor);
        advancedSearch();
      }
      else if (sk.length==1)
      {
        Cursor cursor;
        if (depActivity.isUaString())
          cursor = DictionariesDB.getInstance(depActivity.context).tableWarehouseDetails.selectByAddressSingleUA(sk[0]);
        else
          cursor = DictionariesDB.getInstance(depActivity.context).tableWarehouseDetails.selectByAddressSingle(sk[0]);
        cursor.moveToFirst();
        if (cursor.getCount()==0)
        {
          cursor.close();
          if (depActivity.isUaString())
            cursor = DictionariesDB.getInstance(depActivity.context).tableWarehouseDetails.selectByStreetSingleUA(sk[0]);
          else
            cursor = DictionariesDB.getInstance(depActivity.context).tableWarehouseDetails.selectByStreetSingle(sk[0]);
        }
        loadPrimaryList(cursor);
        singleSearch();
      }

//      for (DepartmentMapItem di : filteredList)
//      {
//        Log.d(TAG, di.fullAddress);
//      }
//
//      // другие города где есть слово "киев" и цифра "6" в адресе или номере отделения
//
//      for (DepartmentMapItem di : nonCityList)
//      {
//        Log.d(TAG, di.fullAddress);
//      }

      //Log.d(TAG, "Dep Count: " + filteredList.size());

//      if (depActivity.searchDepList.isEmpty()) {
//        depActivity.searchDepList.addAll(list);
//        Collections.sort(depActivity.searchDepList, new Comparator<DepartmentMapItem>() {
//          public int compare(DepartmentMapItem o1, DepartmentMapItem o2) {
//            int depNum1 = Integer.parseInt(o1.depNumber);
//            int depNum2 = Integer.parseInt(o2.depNumber);
//            if (depNum1 < depNum2)
//              return -1;
//            else
//              return 1;
//          }
//        });
//      }

//      for (DepartmentMapItem di : depActivity.searchDepList)
//      {
//        Log.d(TAG, di.fullAddress + " deptype= " + di.depType);
//      }

      searchDepData.clearStructures();
      return null;
    }

    private void advancedSearch()
    {
      strongSelect(); // отделения с города "киев" и с отделением "№6"
      select1(); // ниже с города "киев" и наличием цифры "6" в адресе отделения
      select2(); // отделения киева у которых в номере или адресе есть цифра "6"
      select3(); // другие города где есть слово "киев" и цифра "6" в адресе или номере отделения
      sortFilteredList();
      addFirstPlace();
      orginizeList();
      addLastPlaces();
    }

    private void sortByDepNumber(ArrayList<DepartmentMapItem> list)
    {
      //depActivity.searchDepList.addAll(list);
        Collections.sort(list, new Comparator<DepartmentMapItem>() {
          public int compare(DepartmentMapItem o1, DepartmentMapItem o2) {
            int depNum1 = Integer.parseInt(o1.depNumber);
            int depNum2 = Integer.parseInt(o2.depNumber);
            if (depNum1 < depNum2)
              return -1;
            else
              return 0;
          }
        });
    }

    private void singleSearch()
    {
      addFirstPlace();
      depActivity.searchDepList.addAll(searchDepData.list);
      addLastPlaces();
    }

    private void select1()
    {
      // ниже с города "киев" и наличием цифры "6" в адресе отделения
      // группа для сортировки по типу отделения

      for (DepartmentMapItem di : searchDepData.list)
      {
        if (!isExists(di.code)) {
          for (String city : sk) {
            if (di.cityName.compareToIgnoreCase(city) == 0) {
              for (int num : numbers) {
                if (di.depAddress.contains(Integer.toString(num))) {
                  //depActivity.searchDepList.add(di);
                  searchDepData.filteredList.add(di);
                  //Log.d(TAG, di.fullAddress);
                }
              }
            }
          }
        }
      }

    }

    private void select2()
    {
      // отделения киева у которых в номере или адресе есть цифра "6"
      for (DepartmentMapItem di : searchDepData.list)
      {
        if (!isExists(di.code)) {
          for (int num : numbers) {
            if (di.depAddress.contains(Integer.toString(num)) ||
              di.depNumber.contains(Integer.toString(num))) {
              //depActivity.searchDepList.add(di);
              if (isCity(di.cityName, sk)) {
                searchDepData.filteredList.add(di);
              } else
                searchDepData.nonCityList.add(di); // другие города где есть слово "киев" и цифра "6" в адресе или номере отделения
              //Log.d(TAG, di.fullAddress);
            }
          }
        }
      }
    }

    private void select3()
    {
      // другие города где есть слово "киев" и цифра "6" в адресе или номере отделения
      for (DepartmentMapItem di : searchDepData.list) {
        if (!isExists(di.code)) {
          if (isContainsDepNumber(di.fullAddress, numbers) ||
            isCity(di.fullAddress, sk)) {
            searchDepData.filteredList.add(di);
          }
        }
      }
    }

    private void sortFilteredList()
    {
      searchDepData.filteredArrayList.addAll(searchDepData.filteredList);
      //depActivity.searchDepList.addAll(filteredList);

      Collections.sort(searchDepData.filteredArrayList, new Comparator<DepartmentMapItem>() {
        public int compare(DepartmentMapItem o1, DepartmentMapItem o2) {
          try{
            if (o1.depType != null && o2.depType != null) {
              if (o1.depType.compareTo(o2.depType) == 0) {
                //Log.d(TAG, o1.fullAddress + "  ===  " + o2.fullAddress);
                return -1;
              } else
                return 0;
            }
            else
              return 0;
          }
          catch (Exception e)
          {
            Log.d(TAG, "Compare error: " + e.getMessage());
            return 0;
          }
        }
      });
    }

    private void strongSelect()
    {
      // отделения с города "киев" и с отделением "№6"

      for (DepartmentMapItem di : searchDepData.list)
      {
        try {
          if (isCity(di.cityName, sk) && isDepNumber(di, numbers))
            searchDepData.cityAndeNumberList.add(di);
//          int depNm1 = Integer.parseInt(di.depNumber);
//          for (int num : numbers ) {
//            if (num == depNm1){
//              //depActivity.searchDepList.add(di);
//              filteredList.add(di);
//              //Log.d(TAG, di.fullAddress);
//            }
//          }
        }
        catch (Exception e)
        {
          Log.d(TAG, "Error filer: " + e.getMessage());
        }
      }
      searchDepData.cityAndeNumberArrayList.addAll(searchDepData.cityAndeNumberList);
      sortByDepNumber(searchDepData.cityAndeNumberArrayList);
    }

    private void addFirstPlace()
    {
      searchDepData.placesArrayList.addAll(searchDepData.placesList);
      if (searchDepData.placesList.size() > 0) {
        depActivity.searchDepList.add(searchDepData.placesArrayList.get(0));
      }
    }

    private void orginizeList()
    {
      depActivity.searchDepList.addAll(searchDepData.cityAndeNumberArrayList);
      depActivity.searchDepList.addAll(searchDepData.filteredArrayList);
      depActivity.searchDepList.addAll(searchDepData.nonCityList);
    }

    private void addLastPlaces()
    {
      if (searchDepData.placesList.size() > 1) {
        for (int i=1; i<=searchDepData.placesList.size()-1; i++) {
          depActivity.searchDepList.add(searchDepData.placesArrayList.get(i));
          if (i>=4)
            break;
        }
      }
    }

    private void loadPrimaryList(Cursor cursor)
    {
      cursor.moveToFirst();
      while (!cursor.isAfterLast()){

        if (!depActivity.mapActive || isCancelled()) {
          if (cursor != null)
            cursor.close();
          Log.d(TAG, "Terimate task add markers");
          break;
        }

        if (myLocation != null) {
          double dist = DistanceCalculator.distance(Double.parseDouble(getColumnValue(cursor, WarehouseDetailsTable.Column.LAT)),
            Double.parseDouble(getColumnValue(cursor, WarehouseDetailsTable.Column.LNG)),
            myLocation.getLatitude(), myLocation.getLongitude());
          humanDist = dist / 1000;
        }

        // создаем первичный список - несортированный
        try {
          //Log.d(TAG, "ID: " + getColumnValue(cursor, WarehouseDetailsTable.Column.ID));
          depActivity.addSearchDepItem(getColumnValue(cursor, WarehouseDetailsTable.Column.LNG),
            getColumnValue(cursor, WarehouseDetailsTable.Column.LAT),
            getColumnValue(cursor, WarehouseDetailsTable.Column.TYPE),
            getColumnValue(cursor, WarehouseDetailsTable.Column.ADDRESS),
            getColumnValue(cursor, WarehouseDetailsTable.Column.NUMBER),
            getColumnValue(cursor, WarehouseDetailsTable.Column.CODE),
            MyDateUtils.getWorhHoursStart(cursor),
            MyDateUtils.getWorhHoursEnd(cursor),
            getColumnValue(cursor, WarehouseDetailsTable.Column.CAPACITY),
            getColumnValue(cursor, WarehouseDetailsTable.Column.CITY),
            getColumnValue(cursor, WarehouseDetailsTable.Column.STATE),
            Integer.parseInt(getColumnValue(cursor, WarehouseDetailsTable.Column.DUBLICATE_CITY)),
            humanDist,
            getColumnValue(cursor, WarehouseDetailsTable.Column.MAX_LENGTH),
            MyDateUtils.getNextWorhHours(cursor),
            getColumnValue(cursor, WarehouseDetailsTable.Column.FULL_ADDRESS),
            searchDepData.list
          );
        } catch (Exception e) {
          Log.d(TAG, "Load MapDeps error: " + e.getMessage());
        } finally {
          if (cursor != null)
            cursor.moveToNext();
        }
      } // while
      if (cursor != null)
        cursor.close();
    }

    private boolean isExists(String code)
    {
      for (DepartmentMapItem di: searchDepData.cityAndeNumberList)
      {
        if (di.code.compareToIgnoreCase(code)==0)
          return true;
      }
      return false;
    }

    private boolean isDepNumber(final DepartmentMapItem di, final ArrayList<Integer> numbers)
    {
      boolean result = false;
      int depNm1 = Integer.parseInt(di.depNumber);
      for (int num : numbers ) {
        if (num == depNm1){
          result = true;
        }
      }
      return result;
    }

    private boolean isContainsDepNumber(final String address, final ArrayList<Integer> numbers)
    {
      boolean result = false;

      for (int num : numbers ) {
        if (address.contains(Integer.toString(num))){
          result = true;
        }
      }
      return result;
    }

    private boolean isStrongCity(final String cityName, final String sk[])
    {
      boolean result = false;
      for (String city : sk)
      {
        if (cityName.compareToIgnoreCase(city) == 0)
          result = true;
      }
      return result;
    }

    private boolean isCity(final String cityName, final String sk[])
    {
      boolean result = false;
      for (String city : sk)
      {
        if (cityName.toLowerCase().startsWith(city))
          result = true;
      }
      return result;
    }

    @Override
    protected void onPreExecute() {
      super.onPreExecute();
      depActivity.searchDepList.clear();
    }

    @Override
    protected void onPostExecute(Void aVoid) {
      super.onPostExecute(aVoid);
      depActivity.rcSearchAdapter.setDataList(depActivity.searchDepList);
      depActivity.rcSearchAdapter.notifyDataSetChanged();
      Log.d(TAG, "Search Task is completted");
    }

  }

}
