package com.ua.inr0001.intime;

import java.util.ArrayList;
import java.util.LinkedHashSet;

/**
 * Created by technomag on 23.01.18.
 */

public class SearchDepData {

  public LinkedHashSet<DepartmentMapItem> list;
  public ArrayList<DepartmentMapItem> cityAndeNumberArrayList;
  public LinkedHashSet<DepartmentMapItem> cityAndeNumberList;
  public LinkedHashSet<DepartmentMapItem> filteredList;
  public ArrayList<DepartmentMapItem> filteredArrayList;
  public LinkedHashSet<DepartmentMapItem> placesList;
  public LinkedHashSet<DepartmentMapItem> nonCityList; // Список где город не соответсвует поиску
  public ArrayList<DepartmentMapItem> placesArrayList;
  public ArrayList<Integer> numbers;

  public SearchDepData() {
    numbers = new ArrayList<Integer>();
    list = new LinkedHashSet<DepartmentMapItem>();
    cityAndeNumberArrayList = new ArrayList<DepartmentMapItem>();
    filteredArrayList = new ArrayList<DepartmentMapItem>();
    cityAndeNumberList = new LinkedHashSet<DepartmentMapItem>();
    filteredList = new LinkedHashSet<DepartmentMapItem>();
    nonCityList = new LinkedHashSet<DepartmentMapItem>();
    placesList = new LinkedHashSet<DepartmentMapItem>();
    placesArrayList = new ArrayList<DepartmentMapItem>();
  }

  public void clearStructures()
  {
    list.clear();
    cityAndeNumberList.clear();
    cityAndeNumberArrayList.clear();
    filteredList.clear();
    filteredArrayList.clear();
    placesList.clear();
    nonCityList.clear();
    placesArrayList.clear();
    numbers.clear();
  }

}
