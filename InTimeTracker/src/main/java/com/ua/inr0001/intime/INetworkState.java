package com.ua.inr0001.intime;

public interface INetworkState {

  void networkConnected(boolean connect);

}
