package com.ua.inr0001.intime;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import intime.llc.ua.R;

public class ContactsFragment extends Fragment {

  public ContactsFragment()
  {

  }

  public void onBackPressed()
  {
    FragmentManager fm = getActivity().getSupportFragmentManager();
    fm.popBackStack();
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.activity_contacts, container, false);

    return view;
  }
}
