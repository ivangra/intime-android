package com.ua.inr0001.intime;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import intime.llc.ua.R;

/**
 * Created by ILYA on 12.10.2016.
 */

public class AddressAdapter extends SimpleCursorAdapter {

  private int selectedPosition;
  //private FragmentActivity activity;

  private Context context;

  public void setselectedPosition(int position)
  {
    selectedPosition = position;
  }

  /*public void setFragmentActivity(FragmentActivity frAct)
  {
    this.activity = frAct;
  }*/

  public AddressAdapter(Context context, int layout, Cursor c, String[] from, int[] to, int flags) {
    super(context, layout, c, from, to, flags);
    this.context = context;
  }

  public View getView(int position, View convertView, ViewGroup parent) {
    final View view = super.getView(position, convertView, parent);
    if (position == selectedPosition) {
      view.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.item_filter_selected));
    }
    else {
      view.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.item_filter_unselected));
    }
    return view;
  }

  private void editAddress(String id)
  {

  }

  @Override
  public void bindView(View view, final Context context, Cursor cursor) {
    super.bindView(view, context, cursor);

    TextView tvText = (TextView) view.findViewById(R.id.tvActive);

    String active = DictionariesDB.getColumnValue(cursor, AddressesTable.Column.ACTIVE);
    if (active.equals("0"))
      tvText.setVisibility(View.GONE);

    tvText = (TextView) view.findViewById(R.id.tvAddress);
    tvText.setText(DictionariesDB.getColumnValue(cursor, AddressesTable.Column.STREET) + " " +
      DictionariesDB.getColumnValue(cursor, AddressesTable.Column.HOUSE) + ", кв." +
      DictionariesDB.getColumnValue(cursor, AddressesTable.Column.ROOM));

    ImageView imgEditAddress = (ImageView) view.findViewById(R.id.imgEditAddress);
    imgEditAddress.setTag(DictionariesDB.getColumnValue(cursor, AddressesTable.Column.ID));
    imgEditAddress.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        editAddress((String)v.getTag());
      }
    });
  }

}
