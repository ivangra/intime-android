package com.ua.inr0001.intime;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.microsoft.appcenter.AppCenter;
import com.microsoft.appcenter.analytics.Analytics;
import com.microsoft.appcenter.crashes.Crashes;
import com.tbruyelle.rxpermissions2.RxPermissions;

import intime.currier.DataFromCalc;
import intime.llc.ua.R;
import intime.utils.MyStringUtils;
import intime.utils.PermissionUtils;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

import static com.balsikandar.crashreporter.CrashReporter.getContext;

public class MainActivity extends AppCompatActivity implements IShowCurrier, INetworkState {

    private final int CAMERA_PERMISSION_REQUEST = 10;
    private final int LOCATION_PERMISSION_REQUEST = 11;

    private GlobalApplicationData appData;
    private MainActivity activity;
    private DrawerLayout drawer;
    private FrameLayout frMainMenu;
    private int rootHeight = -1;
    private Context mContext;

    private ImageView btnDelivery;
    private ImageView btnCalc;
    private ImageView btnCurrier;
    private ImageView btnDepartments;

    private LinearLayout lvDelivery;
    private LinearLayout lvCalc;
    private LinearLayout lvCurrier;
    private LinearLayout lvDepartments;

    private TextView tvDelivery;
    private TextView tvCalc;
    private TextView tvCurrier;
    private TextView tvDepartments;
    private LinearLayout llMainContent;
    private IShowHideKeyboard iKeyboard;

    private String prevName = "Delivery";

    private LinearLayout llNoNetworkHint;
    private boolean noNetWorkHintShowed = false;
    private Button btnNoNetwork;
    private Animation animationShow1;
    private Animation animationHide1;

    private FrameLayout frTopMessage;

    private NetworkChangeReceiver networkChangeReceiver;
    private RxPermissions rxPermissions;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

//    private Handler mResultHandler = new Handler(){
//      @Override
//      public void handleMessage(Message m){
//        Bundle b = m.getData();
//        if (b.getString("result").equals(getResources().getString(android.R.string.ok)))
//        {
//            browseLink();
//        }
//      }
//    };
//
//    private void browseLink()
//    {
//      Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://intime.ua/upload/images/document/intime_privacy_policy.pdf"));
//      intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//      intent.addFlags(Intent.FLAG_ACTIVITY_PREVIOUS_IS_TOP);
//      startActivity(intent);
//    }
//
//    private void dialogConfidentiality(final String caption, final String message)
//    {
//      MessageDialog dlgWarning = new MessageDialog();
//      dlgWarning.setParams(caption, message, true, mResultHandler);
//      dlgWarning.show(getSupportFragmentManager(), "Warning");
//    }

//  public void showNoNetworkHint() {
//    if (llNoNetworkHint != null && !noNetWorkHintShowed) {
//
//      llNoNetworkHint.setVisibility(View.INVISIBLE);
//      llNoNetworkHint.startAnimation(animationShow1);
//      llNoNetworkHint.postDelayed(new Runnable() {
//        @Override
//        public void run() {
//          if (llNoNetworkHint!= null && noNetWorkHintShowed)
//            llNoNetworkHint.startAnimation(animationHide1);
//        }
//      }, 7000);
//    }
//  }

    private void initNoNetworkHint() {
        frTopMessage = (FrameLayout) findViewById(R.id.frTopMessage);
        llNoNetworkHint = (LinearLayout) findViewById(R.id.llNoNetworkHint);
        btnNoNetwork = (Button) findViewById(R.id.btnNoNetwork);
        btnNoNetwork.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                noNetWorkHintShowed = false;
                llNoNetworkHint.setVisibility(View.GONE);
            }
        });
        animationShow1 = AnimationUtils
                .loadAnimation(GlobalApplicationData.getInstance().getContext(), R.anim.fade_in);
        animationShow1.setFillAfter(true);
        animationShow1.setFillEnabled(true);
        animationHide1 = AnimationUtils
                .loadAnimation(GlobalApplicationData.getInstance().getContext(), R.anim.fade_out);
        animationHide1.setFillAfter(true);
        animationHide1.setFillEnabled(true);
        animationShow1.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                llNoNetworkHint.setVisibility(View.VISIBLE);
                llNoNetworkHint.clearAnimation();
                animationShow1.cancel();
                animationShow1.reset();
                noNetWorkHintShowed = true;
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        animationHide1.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                llNoNetworkHint.setVisibility(View.INVISIBLE);
                llNoNetworkHint.clearAnimation();
                animationHide1.cancel();
                animationHide1.reset();
                noNetWorkHintShowed = false;
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private void resetPreviewItem(final String curItem) {
        if (!curItem.equals(prevName)) {
            if (prevName.equals(mContext.getResources().getString(R.string.tab_delivery))) {
                btnDelivery.setImageDrawable(
                        getResources().getDrawable(R.drawable.menu_delivery_inactive));
                tvDelivery.setTextColor(getResources().getColor(R.color.text_hint));
            }
            if (prevName.equals(mContext.getResources().getString(R.string.tab_calc))) {
                btnCalc.setImageDrawable(getResources().getDrawable(R.drawable.menu_calc_inactive));
                tvCalc.setTextColor(getResources().getColor(R.color.text_hint));
            }
            if (prevName.equals(mContext.getResources().getString(R.string.tab_currier))) {
                btnCurrier.setImageDrawable(
                        getResources().getDrawable(R.drawable.menu_currier_inactive));
                tvCurrier.setTextColor(getResources().getColor(R.color.text_hint));
            }
            if (prevName.equals(mContext.getResources().getString(R.string.tab_departments))) {
                btnDepartments.setImageDrawable(
                        getResources().getDrawable(R.drawable.menu_department_inactive));
                tvDepartments.setTextColor(getResources().getColor(R.color.text_hint));
            }
            prevName = curItem;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        appData.checkPlayServices(this);


        // если регистрационного номера нет или дата истекла
//      if (DictionariesDB.getInstance(mContext).isEmptyDB() && appData.IsOnline())
//        startActivity(new Intent(this, UpdateDBActivity.class));
//      else
//      if (appData.isUpdateDB() && appData.IsOnline())
//         startActivity(new Intent(this, UpdateDBActivity.class));
//      else
//        if (GlobalApplicationData.getInstance().isLocaleChanged() && appData.IsOnline()) {
//          GlobalApplicationData.getInstance().saveLocale();
//          startActivity(new Intent(this, UpdateDBActivity.class));
//        }
        setupMainMenu(GlobalApplicationData.getInstance().getUserInfo().getName().length() > 0);
//      else if (!GlobalApplicationData.getInstance().isConfidentialityReaded()) {
//          GlobalApplicationData.getInstance().saveConfRead();
//          dialogConfidentiality(getResources().getString(R.string.dialog_warning_caption),
//            getResources().getString(R.string.text_confidentiality));
//        }

        //initNotifyCount();

      /*if (appData.getRegistrationId().equals("")==true)
      {
        new Thread (
          new Runnable()
          {
            @Override
            public void run() {
              try {
                // просто отправляем для регистрации
                appData.registerGCMApp();
              } catch (IOException e) {
                Log.d("GCMDemo", e.getMessage());
              }
            }
          }
        ).start();
      }*/

    }

//  private void initNotifyCount()
//  {
//    LinearLayout ll = (LinearLayout) navigationView.getMenu().findItem(R.id.nav_notify).getActionView();
//    TextView tvNotifyCount = (TextView )ll.findViewById(R.id.tvNotifyCount);
//    //TextView tvNotify = (TextView) navigationView.getMenu().findItem(R.id.nav_notify).getActionView();
//    int count = DictionariesDB.getInstance(this).tablePushHistory.getCountPush();
//    if (count>0) {
//      tvNotifyCount.setText(Integer.toString(count));
//      tvNotifyCount.setVisibility(View.VISIBLE);
//    }
//    else
//      tvNotifyCount.setVisibility(View.GONE);
//  }

    private void setVisible(final View view, boolean visible) {
        if (!visible) {
            view.setVisibility(view.GONE);
        } else {
            view.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        GlobalApplicationData.getInstance().saveLocale();
    }

    private void setupMainMenu(boolean logged) {
        Button loginButton = (Button) drawer.findViewById(R.id.button_main_menu_login);
        LinearLayout logoutButton = (LinearLayout) drawer.findViewById(R.id.llProfileLogout);
        if (logged) {
            if (!logoutButton.hasOnClickListeners()) {
                logoutButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        GlobalApplicationData.getInstance().deleteUserInfo();
                        InTimeDB.getInstance(mContext).tableTTNHistory.clearTable();
                        setupMainMenu(GlobalApplicationData.getInstance().getUserInfo().getName()
                                .length() > 0);
                        Intent intent = new Intent(DeliveryActivity.LOGIN_STATUS_CHANGED);
                        intent.putExtra("login", "logout");
                        appData.getContext().sendBroadcast(intent);
                        drawer.closeDrawer(GravityCompat.START);
                    }
                });
            }
        }
        if (!logged) {
            if (!loginButton.hasOnClickListeners()) {
                loginButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(mContext, AutorizationActivity.class));
                        drawer.closeDrawer(GravityCompat.START);
                    }
                });
            }
        }

        TextView tvClientName = (TextView) drawer.findViewById(R.id.tvMenuClientName);
        TextView tvClientStatus = (TextView) drawer.findViewById(R.id.tvMenuClientStatus);
        ImageView imgLogo = (ImageView) drawer.findViewById(R.id.imgMenuLogo);
        FrameLayout frHeaderBack = (FrameLayout) drawer.findViewById(R.id.frHeaderBack);

        if (!logged) {
            imgLogo.setImageResource(R.drawable.logo_invert_small);
            frHeaderBack.setBackgroundResource(R.drawable.layout_menu_buttom);
        } else {
            imgLogo.setImageResource(R.drawable.logo_small);
            frHeaderBack.setBackgroundColor(getResources().getColor(R.color.action_bar_color));
        }

        setVisible(loginButton, !logged);
        setVisible(tvClientName, logged);
        setVisible(tvClientStatus, logged);
        setVisible(logoutButton, logged);
        tvClientStatus.setText(
                MyStringUtils.getFormattedPhone(
                        GlobalApplicationData.getInstance().getUserInfo().getPhoneNumbers()
                )
        );
        tvClientName.setText(
                GlobalApplicationData.getInstance().getUserInfo().getSurname() + " " +
                        GlobalApplicationData.getInstance().getUserInfo().getName() + " " +
                        GlobalApplicationData.getInstance().getUserInfo().getPatronymic()
        );
//    initNotifyCount();
    }

    private void initContactsLoginMenu() {
        LinearLayout llContactsMail = (LinearLayout) drawer.findViewById(R.id.llContactsMail);
        llContactsMail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_SENDTO);
                intent.setData(
                        Uri.parse("mailto:" + getResources().getString(R.string.contacts_mail)));
                startActivity(intent);
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        LinearLayout llContactsPhone = (LinearLayout) drawer.findViewById(R.id.llContactsPhone);
        llContactsPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse(
                        "tel:" + getResources().getString(R.string.contacts_call_center)));
                startActivity(intent);
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        LinearLayout llContactsAddress = (LinearLayout) drawer.findViewById(R.id.llContactsAddress);
        llContactsAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                browserIntent.setData(Uri.parse("https://www.intime.ua"));
                startActivity(browserIntent);
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        LinearLayout llProfileAbout = (LinearLayout) drawer.findViewById(R.id.llProfileAbout);
        llProfileAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(mContext, AboutActivity.class));
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        LinearLayout llProfileLanguage = (LinearLayout) drawer.findViewById(R.id.llProfileLanguage);
        llProfileLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(mContext, LanguageActivity.class));
                drawer.closeDrawer(GravityCompat.START);
            }
        });
    }

//  private void hideSoftKeyboard() {
//    try {
//      InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//      inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
//    } catch (Exception e) { }
//  }

    private void initMainMenu() {
        prevName = mContext.getResources().getString(R.string.tab_delivery);
        llMainContent = (LinearLayout) findViewById(R.id.main_content);
        frMainMenu = (FrameLayout) findViewById(R.id.frMainMenu);
        tvDelivery = (TextView) findViewById(R.id.tvDelivery);
        tvCalc = (TextView) findViewById(R.id.tvCalc);
        tvCurrier = (TextView) findViewById(R.id.tvCurrier);
        tvDepartments = (TextView) findViewById(R.id.tvDepartments);

        btnDelivery = (ImageView) findViewById(R.id.btnDelivery);
        btnCalc = (ImageView) findViewById(R.id.btnCalc);
        btnCurrier = (ImageView) findViewById(R.id.btnCurrier);
        btnDepartments = (ImageView) findViewById(R.id.btnDepartments);

        lvDelivery = (LinearLayout) findViewById(R.id.lvDelivery);
        lvCalc = (LinearLayout) findViewById(R.id.lvCalc);
        lvCurrier = (LinearLayout) findViewById(R.id.lvCurrier);
        lvDepartments = (LinearLayout) findViewById(R.id.lvDepartments);

        lvDelivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String selectedName = mContext.getResources().getString(R.string.tab_delivery);
                if (!prevName.equals(selectedName)) {
                    btnDelivery.setImageDrawable(
                            getResources().getDrawable(R.drawable.menu_delivery_active));
                    tvDelivery.setTextColor(getResources().getColor(R.color.action_bar_color));
                    resetPreviewItem(selectedName);
                    iKeyboard = null;
                    DeliveryActivity frDelivery = new DeliveryActivity();
                    MainFragmentManager.replaceFragment(activity, frDelivery);
                }
            }
        });

        llMainContent.getViewTreeObserver()
                .addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        if (rootHeight == -1) {
                            rootHeight = llMainContent.getHeight();
                        }
                        if (rootHeight != llMainContent.getHeight()) {
                            frMainMenu.setVisibility(View.GONE);
                            if (iKeyboard != null) {
                                iKeyboard.keyboardSHow(true);
                            }
                        } else {
                            frMainMenu.setVisibility(View.VISIBLE);
                            if (iKeyboard != null) {
                                iKeyboard.keyboardSHow(false);
                            }
                        }
                    }
                });

        lvCalc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String selectedName = mContext.getResources().getString(R.string.tab_calc);
                if (!prevName.equals(selectedName)) {
                    btnCalc.setImageDrawable(
                            getResources().getDrawable(R.drawable.menu_calc_active));
                    tvCalc.setTextColor(getResources().getColor(R.color.action_bar_color));
                    resetPreviewItem(selectedName);
                    CalculationOfShippingCosts frCalc = new CalculationOfShippingCosts();
                    frCalc.setShowCurrier(activity);
                    iKeyboard = frCalc;
                    MainFragmentManager.replaceFragment(activity, frCalc);
                }
            }
        });

        lvCurrier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCurrierFragment(null);
            }
        });

        lvDepartments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String selectedName = mContext.getResources().getString(R.string.tab_departments);
                if (!prevName.equals(selectedName)) {
                    isLocationPermissionGranted();
                    btnDepartments.setImageDrawable(
                            getResources().getDrawable(R.drawable.menu_departmnet_active));
                    tvDepartments.setTextColor(getResources().getColor(R.color.action_bar_color));
                    resetPreviewItem(selectedName);
                    DepartmentsFragment frDepartments = new DepartmentsFragment();
                    iKeyboard = null;
                    MainFragmentManager.replaceFragment(activity, frDepartments);
                }
            }
        });

        String selectedName = mContext.getResources().getString(R.string.tab_delivery);
        tvDelivery.setTextColor(getResources().getColor(R.color.action_bar_color));
        btnDelivery.setImageDrawable(getResources().getDrawable(R.drawable.menu_delivery_active));
        resetPreviewItem(selectedName);
        DeliveryActivity frDelivery = new DeliveryActivity();
        iKeyboard = null;
        MainFragmentManager.replaceFragment(activity, frDelivery);
    }

    private void cameraPremissionRequest() {
        ActivityCompat.requestPermissions(this,
                new String[]{
                        Manifest.permission.CAMERA,
                        Manifest.permission.CALL_PHONE
                },
                CAMERA_PERMISSION_REQUEST);
    }

    private void locationPremissionRequest() {
        ActivityCompat.requestPermissions(this,
                new String[]{
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                },
                LOCATION_PERMISSION_REQUEST);
    }

    private boolean isCameraPermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.CAMERA)
                    == PackageManager.PERMISSION_GRANTED) {
                // Log.v("TAG","Permission is granted");
                return true;
            } else {

                // Log.v("TAG","Permission is revoked");
                cameraPremissionRequest();
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            // Log.v("TAG", "Permission is granted");
            return true;
        }
    }

    private boolean isLocationPermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED
                    &&
                    checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED
            ) {
//      Log.v("Permissions","Permission is granted");
                return true;
            } else {
//      Log.v("Permissions","Permission is revoked");
                locationPremissionRequest();
                return false;
            }
        } else {
            //permission is automatically granted on sdk<23 upon installation
//      Log.v("Permissions", "Permission is automatically granted");
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(
            int requestCode,
            @NonNull String[] permissions,
            @NonNull int[] grantResults
    ) {
        if (requestCode == CAMERA_PERMISSION_REQUEST) {

        }
        if (requestCode == LOCATION_PERMISSION_REQUEST) {

        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCenter.start(getApplication(), "ebad3ce9-fe56-4ac5-a721-87396cd434b2", Analytics.class,
                Crashes.class);
        networkChangeReceiver = new NetworkChangeReceiver();
        networkChangeReceiver.setNetworkState(this);
        setContentView(R.layout.activity_main);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        activity = this;
        mContext = this;
        mContext.registerReceiver(networkChangeReceiver,
                new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        // глобальная информация о программе
        appData = GlobalApplicationData.getInstance();
        initNoNetworkHint();
        // есть ли доступ к интернет на устройстве
        initMainMenu();
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        initContactsLoginMenu();

        rxPermissions = new RxPermissions(this);
        checkPermissions();
        isCameraPermissionGranted();
    }

    private void checkPermissions() {
        compositeDisposable.clear();
        compositeDisposable.add(
                rxPermissions.request(
                        Manifest.permission.CAMERA,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.RECEIVE_SMS,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                        .subscribe(new Consumer<Boolean>() {
                            @Override
                            public void accept(Boolean permission) throws Exception {
                                if (permission) {

                                } else {
                                    PermissionUtils.showPermissionDialog(MainActivity.this,
                                            new PermissionUtils.PermissionDialogCallback() {
                                                @Override
                                                public void ok() {
                                                    checkPermissions();
                                                }

                                                @Override
                                                public void exit() {
                                                    finish();
                                                }
                                            });
                                }
                            }
                        })
        );
    }


    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onDestroy() {
        if (networkChangeReceiver != null) {
            mContext.unregisterReceiver(networkChangeReceiver);
            if (networkChangeReceiver != null) {
                networkChangeReceiver.setNetworkState(null);
            }
        }
        super.onDestroy();
    }

    public void setToolbar(Toolbar toolbar) {
        if (toolbar != null) {
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawer, toolbar, R.string.navigation_drawer_open,
                    R.string.navigation_drawer_close);
            drawer.setDrawerListener(toggle);
            toggle.setHomeAsUpIndicator(R.drawable.ic_main_menu);
            toggle.syncState();
            toggle.getDrawerArrowDrawable()
                    .setColor(getResources().getColor(R.color.additional_action));
        } else {
            drawer.setDrawerListener(null);
        }
    }

    @Override
    public void showCurrierFragment(DataFromCalc dataFromCalc) {
        String selectedName = mContext.getResources().getString(R.string.tab_currier);
        if (!prevName.equals(selectedName)) {
            btnCurrier.setImageDrawable(getResources().getDrawable(R.drawable.menu_currier_active));
            tvCurrier.setTextColor(getResources().getColor(R.color.action_bar_color));
            resetPreviewItem(selectedName);
            CurrierFragment frCurrier = new CurrierFragment();
            frCurrier.setDataFromCalc(dataFromCalc);
            iKeyboard = null;
            MainFragmentManager.replaceFragment(activity, frCurrier);
        }
    }

    @Override
    public void networkConnected(boolean connect) {
        if (connect) {
            frTopMessage.setVisibility(View.GONE);
        } else {
            frTopMessage.setVisibility(View.VISIBLE);
//      showNoNetworkHint();
        }
    }

//  @Override
//  public boolean onNavigationItemSelected(MenuItem item) {
//    int id = item.getItemId();
//
//    if (id == R.id.nav_about)
//      startActivity(new Intent(this, AboutActivity.class));
//
//    if (id == R.id.nav_callback)
//      startActivity(new Intent(this, ContactsActivity.class));
//
////    if (id == R.id.nav_profile)
////      startActivity(new Intent(this, ProfileActivity.class));
////
////    if (id == R.id.nav_my_address)
////      startActivity(new Intent(this, MyAddressActivity.class));
//
//    if (id == R.id.nav_notify)
//      startActivity(new Intent(this, PushHistoryActivity.class));
//
//    if (id == R.id.nav_logout) {
//      GlobalApplicationData.getInstance().deleteUserInfo();
//      DictionariesDB.getInstance(this).tableTTNHistory.clearTable();
//      setupMainMenu(GlobalApplicationData.getInstance().getUserInfo().getName().length()>0);
//      Intent intent = new Intent(DeliveryActivity.LOGIN_STATUS_CHANGED);
//      intent.putExtra("login", "logout");
//      appData.getContext().sendBroadcast(intent);
//    }
//
//    drawer.closeDrawer(GravityCompat.START);
//    return true;
//  }

    private class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0: {
                    return new DeliveryActivity();
                }
                case 1: {
                    CalculationOfShippingCosts calc = new CalculationOfShippingCosts()
                            .getInstance();
                    return calc;
                }
                case 2:
                    return new CurrierFragment();
                case 3:
                    return new DepartmentsFragment();
                default:
                    return new DeliveryActivity();
            }
        }

        @Override
        public int getCount() {
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getResources().getString(R.string.tab_delivery);
                case 1:
                    return getResources().getString(R.string.tab_calc);
                case 2:
                    return getResources().getString(R.string.tab_currier);
                case 3:
                    return getResources().getString(R.string.tab_departments);
            }
            return null;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

//  @Override
//  public boolean onOptionsItemSelected(MenuItem item) {
//    int id = item.getItemId();
//    switch (id)
//    {
//      case R.id.menu_departments:
//        if (appData.IsOnline())
//          startActivity(new Intent(mContext, DepartmentsActivity.class));
//        return true;
//      case R.id.menu_filters: startActivity(new Intent(mContext, FilterActivity.class)); return true;
//      default: return super.onOptionsItemSelected(item);
//    }
//  }

//  @Override
//  public boolean onCreateOptionsMenu(Menu menu) {
//    MenuInflater inflater = getMenuInflater();
//    inflater.inflate(R.menu.main_activity_menu, menu);
//    filterMenuItem = (MenuItem) menu.findItem(R.id.menu_filters);
//    setMenuItemVisible(isTTNListExists());
//    return true;
//  }

//  private void showMessage(String caption, String message)
//  {
//    MessageDialog dlgWarning = new MessageDialog();
//    dlgWarning.setParams(caption, message, false, null);
//    dlgWarning.show(getSupportFragmentManager(), "Warning");
//  }

}
