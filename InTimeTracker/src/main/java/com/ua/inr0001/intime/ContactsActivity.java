package com.ua.inr0001.intime;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import intime.llc.ua.R;

public class ContactsActivity extends AppCompatActivity {

//  private TextView tvCallBackPhone1;
  private TextView tvCallBackPhone2;
  private TextView tvMailTo;
//  private ImageView imgCallBackPhone1;
  private ImageView imgCallBackPhone2;
  private ImageView imgMailTo;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_contacts);
    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar2);
    setSupportActionBar(toolbar);

//    tvCallBackPhone1 = (TextView) findViewById(tvCallBackPhone1);
    tvCallBackPhone2 = (TextView) findViewById(R.id.tvCallBackPhone2);
    tvMailTo = (TextView) findViewById(R.id.tvMailTo);

//    imgCallBackPhone1 = (ImageView) findViewById(imgCallBackPhone1);
//    imgCallBackPhone1.setOnClickListener(new View.OnClickListener() {
//      @Override
//      public void onClick(View v) {
//        Intent intent = new Intent(Intent.ACTION_DIAL);
//        intent.setData(Uri.parse("tel:" + tvCallBackPhone1.getText().toString()));
//        startActivity(intent);
//      }
//    });
    imgCallBackPhone2 = (ImageView) findViewById(R.id.imgCallBackPhone2);
    imgCallBackPhone2.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + tvCallBackPhone2.getText().toString()));
        startActivity(intent);
      }
    });
    imgMailTo = (ImageView) findViewById(R.id.imgMailTo);
    imgMailTo.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:" + tvMailTo.getText().toString()));
        startActivity(intent);
      }
    });


  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.accept_action_menu, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();
    switch (id)
    {
      case R.id.menu_action_accept: onBackPressed(); return true;
      default: return super.onOptionsItemSelected(item);
    }
  }

}
