package com.ua.inr0001.intime;

import android.content.Intent;
import android.graphics.ImageFormat;
import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.PreviewCallback;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import net.sourceforge.zbar.Config;
import net.sourceforge.zbar.Image;
import net.sourceforge.zbar.ImageScanner;
import net.sourceforge.zbar.Symbol;
import net.sourceforge.zbar.SymbolSet;

import java.util.List;

import intime.llc.ua.R;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CameraActivity extends AppCompatActivity {

  private SurfaceView surfaceCamera;
  private SurfaceHolder surfaceHolder;
  private HolderCallback holderCallback;
  private FrameLayout frFlashLight;
  private FrameLayout frCameraPreview;
  private FrameLayout frScanLine;
  private Handler autoFocusHandler;
  private Camera camera = null;
  private Button tvCloseActivity;
  final int CAMERA_ID = 0;
  final boolean FULL_SCREEN = true;
  private ImageScanner scanner;
  private Image codeImage;
  private byte[] cameraBuffer;
  private boolean previewing = true;
  private boolean flash = false;
  private Camera.Size camSize;
  //private StringBuilder logStringBuilder;

//  private void getDeviceInfo()
//  {
//    logStringBuilder.append("Device :");
//    logStringBuilder.append("\nOS Version: " + System.getProperty("os.version") + "(" + android.os.Build.VERSION.INCREMENTAL + ")");
//    logStringBuilder.append("\nAndroid Version: " + System.getProperty("os.version") + "(" + Build.VERSION.RELEASE + ")");
//    logStringBuilder.append("\n OS API Level: " + android.os.Build.VERSION.SDK_INT);
//    logStringBuilder.append("\n Device: " + android.os.Build.DEVICE);
//    logStringBuilder.append("\n Model (and Product): " + android.os.Build.MODEL + " ("+ android.os.Build.PRODUCT + ")\n");
//  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_camera);
//    logStringBuilder = new StringBuilder();
//    getDeviceInfo();
    tvCloseActivity = (Button) findViewById(R.id.tvAcceptBarcode);
    frCameraPreview = (FrameLayout) findViewById(R.id.frCameraPreview);
    frScanLine = (FrameLayout) findViewById(R.id.frScanLine);
    tvCloseActivity.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        finish();
      }
    });

    autoFocusHandler = new Handler();

    surfaceCamera = (SurfaceView) findViewById(R.id.surfaceView1);
    surfaceHolder = surfaceCamera.getHolder();
    surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

    holderCallback = new HolderCallback();
    surfaceHolder.addCallback(holderCallback);

    scanner = new ImageScanner();
    scanner.setConfig(0, Config.X_DENSITY, 3); //почему именно эти параметры нигде не указано
    scanner.setConfig(0, Config.Y_DENSITY, 3);

    frFlashLight = (FrameLayout) findViewById(R.id.flashLight);
    frFlashLight.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        turnOnFlashLight(flash);
      }
    });

    Log.d("Camera", "Activity create");

    TranslateAnimation mAnimation = new TranslateAnimation(
      TranslateAnimation.ABSOLUTE, 0f,
      TranslateAnimation.ABSOLUTE, 0f,
      TranslateAnimation.RELATIVE_TO_PARENT, 0f,
      TranslateAnimation.RELATIVE_TO_PARENT, 1.0f);
    mAnimation.setDuration(500);
    mAnimation.setRepeatCount(-1);
    mAnimation.setRepeatMode(Animation.REVERSE);
    mAnimation.setInterpolator(new LinearInterpolator());
    frScanLine.setAnimation(mAnimation);
  }

  private void turnOnFlashLight(boolean flash)
  {
    if (camera!=null) {
      previewing = false;
      Camera.Parameters params = camera.getParameters();
      if (flash == true) {
        params.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
        camera.setParameters(params);
      } else {
        params.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
        camera.setParameters(params);
      }
      this.flash = !flash;
      previewing = true;
    }
  }

  @Override
  protected void onResume() {
    super.onResume();
  }

  @Override
  protected void onPause() {
    super.onPause();
  }

  private String getBarcodeFromUrl(final String url)
  {
    if (url.startsWith("http"))
      return url.substring(url.indexOf("!") + 1, url.length());
    else
      return url;
  }

  private void showPreviewFrame(int[] bounds)
  {
    //frCameraPreview.setTop();
    Log.d("Test", "bounds: " + Integer.toString(bounds[0]));
  }

  private PreviewCallback previewCallback = new PreviewCallback() {
    public void onPreviewFrame(byte[] data, Camera camera) {
      //Log.d("Test", "Preview");
      String lastScannedCode = "";
      codeImage.setData(data);
      calcCrop(camSize);
      int result = scanner.scanImage(codeImage);
      if (result != 0) {
        //Log.d("Test", "Result!=0");
        SymbolSet syms = scanner.getResults();
        for (Symbol sym : syms) {
          lastScannedCode = sym.getData();
          showPreviewFrame(sym.getBounds());
          if (lastScannedCode!=null) {
            lastScannedCode = getBarcodeFromUrl(lastScannedCode);
            Log.d("TTN", lastScannedCode);
            if (lastScannedCode.matches("^[0-9]{10}$")) {
              Intent intent = new Intent();
              intent.putExtra("TTN", lastScannedCode);
              setResult(RESULT_OK, intent);
              finish();
              //Log.d("Test", lastScannedCode);
            } else {
              //tvBarcode.setText(lastScannedCode);
              startCamera(); // reinit camera for scan next
            }
          }
        }
      }
      camera.addCallbackBuffer(cameraBuffer);
    }
  };

  private AutoFocusCallback autoFocusCallback = new AutoFocusCallback() {
    public void onAutoFocus(boolean success, Camera camera) {
      if (success)
        autoFocusHandler.postDelayed(doAutoFocus, 1000);
    }
  };

  private Runnable doAutoFocus = new Runnable() {
    public void run() {
      if (previewing)
        camera.autoFocus(autoFocusCallback);
    }
  };

  private boolean isAutoFocusEnabled()
  {
    Camera.Parameters p = camera.getParameters();
    List<String> focusModes = p.getSupportedFocusModes();

    if(focusModes != null &&
      (focusModes.contains(Camera.Parameters.FOCUS_MODE_AUTO) ||
        focusModes.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE) ||
        focusModes.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO) ||
        focusModes.contains(Camera.Parameters.FOCUS_MODE_FIXED) ||
        focusModes.contains(Camera.Parameters.FOCUS_MODE_INFINITY) ||
        focusModes.contains(Camera.Parameters.FOCUS_MODE_MACRO)
        )) {
      //Phone supports autofocus!
//      logStringBuilder.append("Autofocus enabled\n");
      return true;
    }
    else {
      Toast.makeText(this, getResources().getString(R.string.autofocus_disabled), Toast.LENGTH_LONG).show();
      return false;
    }
  }

  private void startAutofocus()
  {
    Log.d("Camera", "startAutoFocus");
//    logStringBuilder.append("Camera start autofocus\n");
    new Handler().postDelayed(new Runnable() {
      @Override
      public void run() {
        if (camera!=null && autoFocusCallback!=null)
          camera.autoFocus(autoFocusCallback);
      }
    }, 1000);
  }

  private void calcCrop(final Camera.Size camSize)
  {
    if (camSize.height>480 && camSize.width>640)
    {
      int top = Math.round(camSize.height/2) - 240;
      int left = Math.round(camSize.width/2) - 320;
      if (codeImage!=null) {
        // left,top,width,height
        codeImage.setCrop(left, top, 640, 480);
        ViewGroup.LayoutParams lp = frCameraPreview.getLayoutParams();
        frCameraPreview.setLeft(left);
        frCameraPreview.setTop(top);
        lp.height = 480;
        lp.width = 640;
        frCameraPreview.setLayoutParams(lp);
      }
    }
  }

  private void setFocusMode(Camera.Parameters params)
  {
    List<String> modeList = params.getSupportedFocusModes();
    if (modeList!=null) {
      if (modeList.contains(
        Camera.Parameters.FOCUS_MODE_AUTO)) {
        params.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
//        logStringBuilder.append("Focus mode: FOCUS_MODE_AUTO\n");
      } else if (modeList.contains(
        Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
        params.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
//        logStringBuilder.append("Focus mode: FOCUS_MODE_CONTINUOUS_PICTURE\n");
      } else if (modeList.contains(
        Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO)) {
        params.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO);
//        logStringBuilder.append("Focus mode: FOCUS_MODE_CONTINUOUS_VIDEO\n");
      } else if (modeList.contains(
        Camera.Parameters.FOCUS_MODE_INFINITY)) {
        params.setFocusMode(Camera.Parameters.FOCUS_MODE_INFINITY);
//        logStringBuilder.append("Focus mode: FOCUS_MODE_INFINITY\n");
      } else if (modeList.contains(
        Camera.Parameters.FOCUS_MODE_MACRO)) {
        params.setFocusMode(Camera.Parameters.FOCUS_MODE_MACRO);
//        logStringBuilder.append("Focus mode: FOCUS_MODE_MACRO\n");
      } else if (modeList.contains(
        Camera.Parameters.FOCUS_MODE_FIXED)) {
        params.setFocusMode(Camera.Parameters.FOCUS_MODE_FIXED);
//        logStringBuilder.append("Focus mode: FOCUS_MODE_FIXED\n");
      }
    }
  }

  private void setSceneMode(Camera.Parameters params)
  {
    List<String> modeList = params.getSupportedSceneModes();
    if (modeList!=null) {
      if (modeList.contains(Camera.Parameters.SCENE_MODE_AUTO)) {
        params.setSceneMode(Camera.Parameters.SCENE_MODE_AUTO);
//        logStringBuilder.append("Scene mode: SCENE_MODE_AUTO");
      } else if (modeList.contains(Camera.Parameters.SCENE_MODE_BARCODE)) {
        params.setSceneMode(Camera.Parameters.SCENE_MODE_BARCODE);
//        logStringBuilder.append("Scene mode: SCENE_MODE_BARCODE");
      }
    }
  }

  private void startCamera()
  {
//    logStringBuilder.append("Start Camera\n");
    previewing = false;
    if (camera!=null) {
//      logStringBuilder.append("Camera not null\n");
      camera.stopPreview();
      try {
          setCameraDisplayOrientation(CAMERA_ID);
          Camera.Parameters params = camera.getParameters();
          if (params!=null)
          {
            setFocusMode(params);
            setSceneMode(params);
          }
          camera.setParameters(params);
          camera.setPreviewCallbackWithBuffer(null);
          camera.setPreviewCallbackWithBuffer(previewCallback);
          Camera.Size size = params.getPreviewSize();
          codeImage = new Image(size.width, size.height, "Y800");
          camSize = size;
//          logStringBuilder.append("Screen size: " + Integer.toString(size.width) + "x" + Integer.toString(size.height) + "\n");
          int imageFormat = params.getPreviewFormat();
          Camera.Size previewSize = params.getPreviewSize();
          int bufferSize = previewSize.width * previewSize.height * ImageFormat.getBitsPerPixel(imageFormat) / 8;
          cameraBuffer = new byte[bufferSize];
          camera.addCallbackBuffer(cameraBuffer);
          //camera.autoFocus(autoFocusCallback);

        camera.setPreviewDisplay(surfaceHolder);
//        logStringBuilder.append("Camera start preview\n");
        camera.startPreview();

        if (isAutoFocusEnabled()) {
          startAutofocus();
        }

        previewing = true;
        Log.d("Camera", "Camera started");
      } catch (Exception e) {
        Log.d("Camera", "Error start camera " + e.getMessage());
//        logStringBuilder.append("Error start Camera: " + e.getMessage() + "\n");
//        logStringBuilder.append(Log.getStackTraceString(e));
      }
    }
//    String logg = logStringBuilder.toString();
//    sendLog(logg);
  }

  class HolderCallback implements SurfaceHolder.Callback {

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
      Log.d("Camera", "surface Created");
      try
      {
        Log.d("Camera", "Get camera");
//        logStringBuilder.append("Get Camera: " + Integer.toString(CAMERA_ID) + "\n");
        camera = Camera.open(CAMERA_ID);
      }
      catch(Exception e)
      {
        Log.d("Camera", "Camera error: " + e.getMessage());
//        logStringBuilder.append("Camera error: " + e.getMessage() + "\n");
//        logStringBuilder.append(Log.getStackTraceString(e));
      }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width,
        int height) {
        Log.d("Camera", "Surface changed");
        startCamera();
      }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
      Log.d("Camera", "Surface desptroyed");
      if (camera != null) {
        previewing = false;
        camera.release();
      }
      camera = null;
    }
  }
  
  public void setCameraDisplayOrientation(int cameraId) {
    int rotation = getWindowManager().getDefaultDisplay().getRotation();
    int degrees = 0;
    switch (rotation) {
    case Surface.ROTATION_0:
      degrees = 0;
      break;
    case Surface.ROTATION_90:
      degrees = 90;
      break;
    case Surface.ROTATION_180:
      degrees = 180;
      break;
    case Surface.ROTATION_270:
      degrees = 270;
      break;
    }
    
    int result = 0;
    
    CameraInfo info = new CameraInfo();
    Camera.getCameraInfo(cameraId, info);

    if (info.facing == CameraInfo.CAMERA_FACING_BACK) {
      result = ((360 - degrees) + info.orientation);
    } else

    if (info.facing == CameraInfo.CAMERA_FACING_FRONT) {
      result = ((360 - degrees) - info.orientation);
      result += 360;
    }
    result = result % 360;
    camera.setDisplayOrientation(result);
  }


//  private void sendLog(String log) {
//
//    Call<ResponseBody> call;
//    Retrofit retrofit = new Retrofit.Builder()
//      .baseUrl("http://newapi.intime.webdev.uislab.com:8081/")
//      .addConverterFactory(GsonConverterFactory.create())
//      //.client(GlobalApplicationData.getUnsafeOkHttpClient())
//      .build();
//
//    IRtfAPI service = retrofit.create(IRtfAPI.class);
//    call = service.sendLog(log);
//    call.enqueue(
//      new Callback<ResponseBody>() {
//        @Override
//        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//
//        }
//
//        @Override
//        public void onFailure(Call<ResponseBody> call, Throwable t) {
//          //frPreCalcSum1.setVisibility(View.GONE);
//          Log.d("Test", "Send Error: " + t.getMessage());
//        }
//
//      }
//    );
//  }


}
