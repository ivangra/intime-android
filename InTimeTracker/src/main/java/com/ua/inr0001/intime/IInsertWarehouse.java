package com.ua.inr0001.intime;

public interface IInsertWarehouse {

  public void insert(String TownKey, String TownKeyEng,
      String Town, String TownUkr,
      String Address, String AddressUkr,
      String Phone, String PhoneUkr,
      String PhoneEx,
      String Mail, String Region,
      String Lat, String Lng,
      String SubId, String Econom,
      String WorkHours, String WorkHoursUkr,
      String DayIn, String DayInUkr,
      String Simple,
      String RepCode, String StoreNumber,
      String CharCode, String MaxWeight,
      String MaxLength);
  
}
