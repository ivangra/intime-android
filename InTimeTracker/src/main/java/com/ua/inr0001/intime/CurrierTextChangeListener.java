package com.ua.inr0001.intime;

import android.text.Editable;
import android.text.TextWatcher;

import intime.calculator.CalculatorModel;

/**
 * Created by technomag on 02.02.18.
 */

public class CurrierTextChangeListener implements TextWatcher {

  private String type;

  public CurrierTextChangeListener(String type)
  {
    this.type = type;
  }

  @Override
  public void beforeTextChanged(CharSequence s, int start, int count, int after) {

  }

  @Override
  public void onTextChanged(CharSequence s, int start, int before, int count) {

  }

  @Override
  public void afterTextChanged(Editable s) {
    if (type.compareTo("width") == 0) {
      if (s.length() == 0)
        MailCallCurrier.getInstance().setWidth("1");
      else
        MailCallCurrier.getInstance().setWidth(s.toString());
    }
    if (type.compareTo("height") == 0) {
      if (s.length() == 0)
        MailCallCurrier.getInstance().setHeight("1");
      else
        MailCallCurrier.getInstance().setHeight(s.toString());
    }
    if (type.compareTo("length") == 0) {
      if (s.length() == 0)
        MailCallCurrier.getInstance().setLength("1");
      else
        MailCallCurrier.getInstance().setLength(s.toString());
    }
    if (type.compareTo("cost") == 0)
      MailCallCurrier.getInstance().setCost(s.toString());
    if (type.compareTo("podSum") == 0)
      MailCallCurrier.getInstance().setPodSum(s.toString());
  }

}
