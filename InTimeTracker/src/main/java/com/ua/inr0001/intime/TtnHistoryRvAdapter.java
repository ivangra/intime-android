package com.ua.inr0001.intime;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import intime.api.model.ttnhistory.Datum;
import intime.api.model.ttnhistory.TtnHistory;
import intime.llc.ua.R;
import intime.utils.MyDateUtils;

/**
 * Created by ILYA on 25.10.2016.
 */

public class TtnHistoryRvAdapter extends RecyclerView.Adapter<TtnHistoryRvAdapter.TtnHolder> {

  public interface OnItemClickListener {
    void onItemClick(View view, int position);
  }

  private Context context;
  private TtnHistory ttnHistory;
  private String humanTTN;
  private String prevStatusId = "0";

  public TtnHistoryRvAdapter(Context context, TtnHistory ttnHistory, String humanTTN)
  {
    this.humanTTN = humanTTN;
    this.context = context;
    this.ttnHistory = ttnHistory;
  }

  public void setTtnHistory(TtnHistory ttnHistory)
  {
    this.ttnHistory = ttnHistory;
  }

  @Override
  public TtnHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(context).inflate(R.layout.ttn_history_list_item, null, false);
    view.setLayoutParams(new RecyclerView.LayoutParams(
      RecyclerView.LayoutParams.MATCH_PARENT,
      RecyclerView.LayoutParams.WRAP_CONTENT
    ));
    return new TtnHolder(view);
  }

  @Override
  public void onBindViewHolder(TtnHolder holder, final int position) {
    Datum data = ttnHistory.getData().get(position);
    holder.tvDate.setText(MyDateUtils.reformatDateTime(data.getDate()));
    holder.tvStatus.setText(data.getStatus());
    holder.tvTTN.setText(humanTTN);
    int pos = (position - 1)<0 ? 0 : position - 1;

    if (prevStatusId.compareTo("0")==0 && position!=0)
    {
      holder.llBackground.setBackgroundColor(context.getResources().getColor(R.color.ttn_details_edecl_end_background));
    }
    if (data.getStatusId().compareTo("0")==0)
      holder.llBackgroundDrawable.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.ttn_edecl_background));

    if (data.getStatusId().compareTo("3")==0) // "Выдан"
      SetTTNStatusView.setCargoGivenStatus(holder.imgStage1, holder.imgStage2, holder.imgStage3, holder.imgLine1, holder.imgLine2, holder.tvCargoGiven);
    else
    if (data.getStatusId().compareTo("2")==0) // "В пути"
      SetTTNStatusView.setOnWayStatus(holder.imgStage1, holder.imgStage2, holder.imgStage3, holder.imgLine1, holder.imgLine2);
    else
    if (data.getStatusId().compareTo("16")==0) // "На складе"
      SetTTNStatusView.setOnWarehouseStatus(holder.imgStage1, holder.imgStage2, holder.imgStage3, holder.imgLine1, holder.imgLine2);
    else
    if (data.getStatusId().compareTo("1")==0) // "Принят"
      SetTTNStatusView.setBeginStatus(holder.imgStage1, holder.imgStage2, holder.imgStage3, holder.imgLine1, holder.imgLine2);
    else
    if (data.getStatusId().compareTo("0")==0) // "Создан документ Веб-заявка"
      SetTTNStatusView.setWebDeclarationStatus(holder.flWebDecLine, holder.llWebDecLine);
    else
      SetTTNStatusView.setWebDeclarationStatus(holder.flWebDecLine, holder.llWebDecLine);
    prevStatusId = data.getStatusId();
  }

  @Override
  public int getItemCount() {
    return ttnHistory.getData().size();
  }

  class TtnHolder extends RecyclerView.ViewHolder {

    ImageView imgStage1;
    ImageView imgStage2;
    ImageView imgStage3;
    ImageView imgLine1;
    ImageView imgLine2;
    FrameLayout flWebDecLine;
    LinearLayout llWebDecLine;
    LinearLayout llBackground;
    LinearLayout llBackgroundDrawable;
    TextView tvDate;
    TextView tvStatus;
    TextView tvTTN;
    TextView tvCargoGiven;
    View container;

    public TtnHolder(View view) {
      super(view);
      container = view;
      imgStage1 = (ImageView) view.findViewById(R.id.imgStage1);
      imgStage2 = (ImageView) view.findViewById(R.id.imgStage2);
      imgStage3 = (ImageView) view.findViewById(R.id.imgStage3);
      imgLine1 = (ImageView) view.findViewById(R.id.imgLine1);
      imgLine2 = (ImageView) view.findViewById(R.id.imgLine2);
      tvDate = (TextView) view.findViewById(R.id.tvTtnHistoryDate);
      tvStatus = (TextView) view.findViewById(R.id.tvTtnHistoryStatus);
      tvTTN = (TextView) view.findViewById(R.id.tvTtnHistoryNumber);
      llBackground = (LinearLayout) view.findViewById(R.id.llTtnHistoryBackground);
      llBackgroundDrawable = (LinearLayout) view.findViewById(R.id.llTtnHistoryBackgroundDrawable);
      flWebDecLine = (FrameLayout) view.findViewById(R.id.flWebDecLine);
      llWebDecLine = (LinearLayout) view.findViewById(R.id.llWebDecLine);
      tvCargoGiven = (TextView) view.findViewById(R.id.tvCargoGiven);
    }
  }


}
