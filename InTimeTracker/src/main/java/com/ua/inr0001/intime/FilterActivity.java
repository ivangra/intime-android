package com.ua.inr0001.intime;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import intime.llc.ua.R;

public class FilterActivity extends AppCompatActivity {

  private TextView tvDeclaration;
  private TextView tvInSenderdepartment;
  private TextView tvInWay;
  private TextView tvWaitForGiven;
  private TextView tvReceived;
  private TextView tvTracked;
  //private TextView tvArchive;

  private ListView lvSort;
  private SimpleAdapter sortAdapter;
  private ViewGroup.LayoutParams backParams = null;
  private int sortSelected = 0;
  private SwitchCompat swDeclaration;
  private SwitchCompat swReceived;
  private SwitchCompat swInSendDepartment;
  private SwitchCompat swInWay;
  private SwitchCompat swWait;
  private SwitchCompat swTracked;

  private void setSortAdapter(Context context)
  {
    ArrayList<Map<String, Object>> cargoTypeList = new ArrayList<Map<String, Object>>();
    String[] listItems = getResources().getStringArray(R.array.sort_ttn);

    String[] from = { "Image", "Text" };
    int[] to = { R.id.imgSortIcon, R.id.tvSortDesc};

    Map<String, Object> m;
    for (int i = 0; i < listItems.length; i++) {
      m = new HashMap<String, Object>();
      m.put("Image", R.drawable.ic_sort);
      m.put("Text", listItems[i]);
      cargoTypeList.add(m);
    }

    sortAdapter = new SimpleAdapter(context, cargoTypeList,
            R.layout.sort_item, from, to)
    {
      @NonNull
      @Override
      public View getView(int position, View convertView, ViewGroup parent) {
        final View view = super.getView(position, convertView, parent);
        ImageView cargoIcon = (ImageView) view.findViewById(R.id.imgSortIcon);
        TextView tvSortDesc = (TextView) view.findViewById(R.id.tvSortDesc);
        LinearLayout llSelectedItem = (LinearLayout) view.findViewById(R.id.llSortItem);
        if (position == sortSelected) {
          cargoIcon.setVisibility(View.VISIBLE);
          llSelectedItem.setBackgroundDrawable(getResources().getDrawable(R.drawable.item_filter_selected));
          tvSortDesc.setTextColor(getResources().getColor(R.color.base_text_color));
        }
        else {
          cargoIcon.setVisibility(View.INVISIBLE);
          //llSelectedItem.setBackgroundColor(getResources().getColor(R.color.app_background));
          llSelectedItem.setBackgroundDrawable(getResources().getDrawable(R.drawable.item_filterunselected1));
          tvSortDesc.setTextColor(getResources().getColor(R.color.text_hint));
        }
        return view;
      }
    };

    lvSort.setAdapter(sortAdapter);
  }

//  private void initSortList()
//  {
//    lvSort = (ListView) findViewById(R.id.lvFilterSort);
//    lvSort.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
//    lvSort.setItemsCanFocus(true);
//    setSortAdapter(this);
//    lvSort.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//      @Override
//      public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//        sortSelected = position;
//        sortAdapter.notifyDataSetChanged();
//      }
//    });
//  }

  private void initFilterWitches()
  {
    tvDeclaration = (TextView) findViewById(R.id.tvDeclaration);
    tvInSenderdepartment = (TextView) findViewById(R.id.tvInSendDepartment);
    tvInWay = (TextView) findViewById(R.id.tvInWay);
    tvWaitForGiven = (TextView) findViewById(R.id.tvWaitForGiven);
    tvReceived = (TextView) findViewById(R.id.tvReceived);
    tvTracked = (TextView) findViewById(R.id.tvTracked);
    //tvArchive = (TextView) findViewById(R.id.tvArchive);
    swDeclaration = (SwitchCompat) findViewById(R.id.swDeclaration);
    swReceived = (SwitchCompat) findViewById(R.id.swReceived);
    swInSendDepartment = (SwitchCompat) findViewById(R.id.swInSendDepartment);
    swInWay = (SwitchCompat) findViewById(R.id.swInWay);
    swWait = (SwitchCompat) findViewById(R.id.swWaitForGiven);
    swTracked = (SwitchCompat) findViewById(R.id.swTracked);
    //SwitchCompat swArchive = (SwitchCompat) findViewById(R.id.swArchive);

    swDeclaration.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
          if (isChecked) {
            tvDeclaration.setTextColor(getResources().getColor(R.color.base_text_color));
            GlobalApplicationData.getInstance().filterList[0] = 0;
          }
          else {
            GlobalApplicationData.getInstance().filterList[0] = -1;
            tvDeclaration.setTextColor(getResources().getColor(R.color.text_hint));
          }
          isLastUncheked();
      }
    });


    swInSendDepartment.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
          tvInSenderdepartment.setTextColor(getResources().getColor(R.color.base_text_color));
          GlobalApplicationData.getInstance().filterList[1] = 1;
        }
        else {
          tvInSenderdepartment.setTextColor(getResources().getColor(R.color.text_hint));
          GlobalApplicationData.getInstance().filterList[1] = -1;
        }
        isLastUncheked();
      }
    });


    swInWay.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
          tvInWay.setTextColor(getResources().getColor(R.color.base_text_color));
          GlobalApplicationData.getInstance().filterList[2] = 2;
        }
        else {
          tvInWay.setTextColor(getResources().getColor(R.color.text_hint));
          GlobalApplicationData.getInstance().filterList[2] = -1;
        }
        isLastUncheked();
      }
    });


    swWait.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
          tvWaitForGiven.setTextColor(getResources().getColor(R.color.base_text_color));
          GlobalApplicationData.getInstance().filterList[3] = 16;
        }
        else {
          tvWaitForGiven.setTextColor(getResources().getColor(R.color.text_hint));
          GlobalApplicationData.getInstance().filterList[3] = -1;
        }
        isLastUncheked();
      }
    });


    swReceived.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
          tvReceived.setTextColor(getResources().getColor(R.color.base_text_color));
          GlobalApplicationData.getInstance().filterList[4] = 3;
        }
        else {
          tvReceived.setTextColor(getResources().getColor(R.color.text_hint));
          GlobalApplicationData.getInstance().filterList[4] = -1;
        }
        isLastUncheked();
      }
    });

    swTracked.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
          tvTracked.setTextColor(getResources().getColor(R.color.base_text_color));
        }
        else {
          tvTracked.setTextColor(getResources().getColor(R.color.text_hint));
        }
        GlobalApplicationData.getInstance().showTracked = isChecked;
        isLastUncheked();
      }
    });

//    swArchive.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//      @Override
//      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//        if (isChecked) {
//          tvArchive.setTextColor(getResources().getColor(R.color.base_text_color));
//        }
//        else {
//          tvArchive.setTextColor(getResources().getColor(R.color.text_hint));
//        }
//        GlobalApplicationData.getInstance().showArchived = isChecked;
//      }
//    });

    swDeclaration.setChecked(GlobalApplicationData.getInstance().filterList[0]!=-1);
    swInSendDepartment.setChecked(GlobalApplicationData.getInstance().filterList[1]!=-1);
    swInWay.setChecked(GlobalApplicationData.getInstance().filterList[2]!=-1);
    swWait.setChecked(GlobalApplicationData.getInstance().filterList[3]!=-1);
    swReceived.setChecked(GlobalApplicationData.getInstance().filterList[4]!=-1);
    swTracked.setChecked(GlobalApplicationData.getInstance().showTracked);
//    swArchive.setChecked(GlobalApplicationData.getInstance().showArchived);
  }

  private void isLastUncheked()
  {
    if (!swDeclaration.isChecked() &&
      !swInSendDepartment.isChecked() &&
      !swInWay.isChecked() &&
      !swWait.isChecked() &&
      !swReceived.isChecked() &&
      !swTracked.isChecked()
      )
    {
      swWait.setChecked(true);
      tvWaitForGiven.setTextColor(getResources().getColor(R.color.base_text_color));
      GlobalApplicationData.getInstance().filterList[3] = 16;
    }
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_filter);
    //initSortList();
    initFilterWitches();
    Toolbar toolbar = (Toolbar) findViewById(R.id.filterToolbar);
    toolbar.setTitle("");
    setSupportActionBar(toolbar);
    toolbar.setNavigationOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        onBackPressed();
      }
    });
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.accept_action_menu, menu);
    return true;
  }

  private boolean isEmptyFilterList()
  {
    for (int fl = 0; fl <= GlobalApplicationData.getInstance().filterList.length - 1; fl++)
    {
      if (GlobalApplicationData.getInstance().filterList[fl]!=-1)
        return false;
    }
    return true;
  }

  private void buildFilter()
  {
    GlobalApplicationData.getInstance().filterQuery = "";
    GlobalApplicationData.getInstance().filterQueryAPI = "";
    StringBuilder sb = new StringBuilder();
    StringBuilder sbView = new StringBuilder();
    boolean emptyFilter = true;
    if (!isEmptyFilterList()) {
      sb.append(HistoryTtnTable.Column.STATUS_ID + " IN (");
      for (int fl = 0; fl <= GlobalApplicationData.getInstance().filterList.length - 1; fl++) {
        if (GlobalApplicationData.getInstance().filterList[fl] != -1) {
          emptyFilter = false;
          sb.append(Integer.toString(GlobalApplicationData.getInstance().filterList[fl]));
          sb.append(",");
        }
      }
    }
    if (emptyFilter==false) {
      sb.deleteCharAt(sb.length() - 1);
      sb.append(")");
    }
    if (sb.length()>0) {
      GlobalApplicationData.getInstance().filterQueryAPI = sb.toString();
      sbView.append(sb.toString());
    }
    if (GlobalApplicationData.getInstance().showTracked==true ||
      GlobalApplicationData.getInstance().showArchived==true
      ) {
      if (sbView.length()>0)
        sbView.append(" OR ");
      sbView.append(HistoryTtnTable.Column.VIEW_STATUS + " IN (");
      if (GlobalApplicationData.getInstance().showTracked==true) {
        sbView.append("3");
        if (GlobalApplicationData.getInstance().showArchived==true)
          sbView.append(", ");
      }
      if (GlobalApplicationData.getInstance().showArchived==true) {
        sbView.append("2");
      }
      sbView.append(")");
    }
    if (sbView.toString().length()>0)
      GlobalApplicationData.getInstance().filterQuery = "(" + sbView.toString() + ") AND "
        + HistoryTtnTable.Column.VIEW_STATUS + "!=1";
    Log.d("FilterQuery", GlobalApplicationData.getInstance().filterQuery);
    GlobalApplicationData.getInstance().saveFilterSettings();
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();
    switch (id)
    {
      case R.id.menu_action_accept: {
        buildFilter();
        onBackPressed();
        return true;
      }
      default: return super.onOptionsItemSelected(item);
    }
  }

}
