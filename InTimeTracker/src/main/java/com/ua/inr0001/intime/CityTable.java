package com.ua.inr0001.intime;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import intime.utils.DbQuickFixUtils;

import static intime.utils.DbQuickFixUtils.*;

public final class CityTable implements ITable, IInsertCity {

	private SQLiteDatabase sqlDB;
	private SQLiteStatement insertStatement;
	private SQLiteStatement insertStatementUA;

	public CityTable(SQLiteDatabase sqldb) {
		sqlDB = sqldb;
		insertStatement = sqlDB.compileStatement(INSERT_CITY);
		insertStatementUA = sqlDB.compileStatement(INSERT_CITY_UA);
	}

	public static final class Index {
		public static final String CODE = "idx_CityCode";
		public static final String CODE_UA = "idx_CityNameUA";
		public static final String NAME = "idx_CityName";
		public static final String NAME_UA = "idx_CityNameUA";
		public static final String HITS = "idx_Hits";
		public static final String HITS_UA = "idx_hitsUA";
	}

	public static final class Column {
		public static final String CITY = "City";
		public static final String ID = "_id";
		public static final String STATE = "State";
		public static final String HAS_STORE = "HasStore";
		public static final String HAS_POSTOMAT = "HasPostomat";
		public static final String CITY_TYPE = "CityType";
		public static final String CODE = "Code";
		public static final String AREA = "Area";
		public static final String HITS = "Hits";
		public static final String WAREHOUSE_ID = "WarehouseId";
		public static final String DUBLICATE_NAME = "DiblicateName";
	}

	public static final String NAME = "tblCities";
	public static final String VIEW_NAME = "vwCities";

	public static final String NAME_UA = "tblCitiesUA";
	public static final String VIEW_NAME_UA = "vwCitiesUA";

	public static final String INSERT_CITY =
		"insert into " + NAME + "(" +
			Column.CITY + ", " +
			Column.CODE + ", " +
			Column.WAREHOUSE_ID + ", " +
      Column.STATE + ", " +
			Column.AREA + ", " +
			Column.HAS_STORE + ", " +
			Column.HAS_POSTOMAT + ", " +
			Column.CITY_TYPE + ", " +
			Column.HITS +
			") VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";

	public static final String INSERT_CITY_UA =
		"insert into " + NAME_UA + "(" +
			Column.CITY + ", " +
			Column.CODE + ", " +
			Column.WAREHOUSE_ID + ", " +
			Column.STATE + ", " +
			Column.AREA + ", " +
			Column.HAS_STORE + ", " +
			Column.HAS_POSTOMAT + ", " +
			Column.CITY_TYPE + ", " +
			Column.HITS +
			") VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";

	public static final String SQL_CREATE_TABLE =
		"create table if not exists " + NAME + " ( " +
			Column.ID + " integer primary key autoincrement, " +
			Column.CITY + " text, " +
			Column.CODE + " text, " +
			Column.WAREHOUSE_ID + " text, " +
			Column.DUBLICATE_NAME + " integer default 0, " +
			Column.CITY_TYPE + " integer, " +
			Column.HAS_POSTOMAT + " integer, " +
			Column.HAS_STORE + " integer, " +
			Column.HITS + " integer, " +
			Column.STATE + " text, " +
			Column.AREA + " text); ";

	public static final String SQL_CREATE_VIEW =
		"CREATE VIEW if not exists " + VIEW_NAME + " AS" +
			" SELECT " + Column.ID + ", " +
			Column.CITY + ", " +
			Column.CODE + ", " +
			Column.WAREHOUSE_ID + ", " +
			Column.DUBLICATE_NAME + ", " +
			Column.CITY_TYPE + ", " +
			Column.HAS_STORE + ", " +
			Column.HAS_POSTOMAT + ", " +
			Column.AREA + ", " +
			Column.HITS + ", " +
			Column.STATE +
			" FROM " + NAME;

	public static final String SQL_CREATE_INDEX_NAME =
		"CREATE INDEX if not exists " + Index.NAME + " ON " + NAME + " (" + Column.CITY + "); ";
	public static final String SQL_CREATE_INDEX_CODE =
			"CREATE INDEX if not exists " + Index.CODE + " ON " + NAME + " (" + Column.CODE + "); ";
	public static final String SQL_CREATE_INDEX_HITS =
		"CREATE INDEX if not exists " + Index.HITS + " ON " + NAME + " (" + Column.HITS + "); ";

	public static final String SQL_CREATE_TABLE_UA =
		"create table if not exists " + NAME_UA + " ( " +
			Column.ID + " integer primary key autoincrement, " +
			Column.CITY + " text, " +
			Column.CODE + " text, " +
			Column.WAREHOUSE_ID + " text, " +
			Column.DUBLICATE_NAME + " integer default 0, " +
			Column.STATE + " text, " +
			Column.CITY_TYPE + " integer, " +
			Column.HAS_POSTOMAT + " integer, " +
			Column.HAS_STORE + " integer, " +
			Column.HITS + " integer, " +
			Column.AREA + " text); ";

	public static final String SQL_CREATE_VIEW_UA =
		"CREATE VIEW if not exists " + VIEW_NAME_UA + " AS" +
			" SELECT " + Column.ID + ", " +
			Column.CITY + ", " +
			Column.CODE + ", " +
			Column.WAREHOUSE_ID + ", " +
			Column.DUBLICATE_NAME + ", " +
			Column.CITY_TYPE + ", " +
			Column.HAS_STORE + ", " +
			Column.HAS_POSTOMAT + ", " +
			Column.AREA + ", " +
			Column.HITS + ", " +
			Column.STATE +
			" FROM " + NAME_UA;

	public static final String SQL_CREATE_INDEX_NAME_UA =
		"CREATE INDEX if not exists " + Index.NAME_UA + " ON " + NAME_UA + " (" + Column.CITY + "); ";
	public static final String SQL_CREATE_INDEX_CODE_UA =
			"CREATE INDEX if not exists " + Index.CODE_UA + " ON " + NAME_UA + " (" + Column.CODE + "); ";
	public static final String SQL_CREATE_INDEX_HITS_UA =
		"CREATE INDEX if not exists " + Index.HITS_UA + " ON " + NAME_UA + " (" + Column.HITS + "); ";

	public static final String SQL_DROP_TABLE =
		"DROP TABLE IF EXISTS " + NAME;

	public static final String SQL_DROP_VIEW =
		"DROP VIEW IF EXISTS " + VIEW_NAME;

	public static final String SQL_DROP_TABLE_UA =
		"DROP TABLE IF EXISTS " + NAME_UA;

	public static final String SQL_DROP_VIEW_UA =
		"DROP VIEW IF EXISTS " + VIEW_NAME_UA;

//	public static final String SQL_DROP_TABLE =
//		"DROP TABLE IF EXISTS " + NAME_UA;
//
//	public static final String SQL_DROP_VIEW =
//		"DROP VIEW IF EXISTS " + VIEW_NAME_UA;

	@Override
	public Cursor selectByID(int Id) {

		return null;
	}

	@Override
	public Cursor selectByName(String Name) {
		Name = checkAndEscape(Name);
		// заменяем первую буквы слова на большую
		String searchString = Name.replaceFirst(String.valueOf(Name.charAt(0)), String.valueOf(Name.charAt(0)).toUpperCase());
		return sqlDB.query(VIEW_NAME, null, Column.CITY + " like \"" + searchString + "%\"", null, null, null, Column.CITY_TYPE + " ASC");
	}

	public Cursor selectByNameUA(String Name) {
	    Name = checkAndEscape(Name);
		// заменяем первую буквы слова на большую
		String searchString = Name.replaceFirst(String.valueOf(Name.charAt(0)), String.valueOf(Name.charAt(0)).toUpperCase());
		return sqlDB.query(VIEW_NAME_UA, null, Column.CITY + " like \"" + searchString + "%\"", null, null, null, Column.CITY_TYPE + " ASC");
	}

	// CSA - City, State, Area
	public Cursor selectByCSA(String city, String state, String area)
	{
		city = checkAndEscape(city);
		state = checkAndEscape(state);
		area = checkAndEscape(area);
		return sqlDB.query(VIEW_NAME, null, Column.CITY + "=\"" + city + "\" AND " +
				Column.STATE + "=\"" + state + "\" AND " + Column.AREA + "=\"" + area + "\""
			, null, null, null, Column.CITY);
	}

	public Cursor selectByCSA_UA(String city, String state, String area)
	{
        city = checkAndEscape(city);
        state = checkAndEscape(state);
        area = checkAndEscape(area);
		return sqlDB.query(VIEW_NAME_UA, null, Column.CITY + "=\"" + city + "\" AND " +
				Column.STATE + "=\"" + state + "\" AND " + Column.AREA + "=\"" + area + "\""
			, null, null, null, Column.CITY);
	}

	public Cursor selectByCS(String city, String state)
	{
        city = checkAndEscape(city);
        state = checkAndEscape(state);
		return sqlDB.query(VIEW_NAME, null, Column.CITY + "=\"" + city + "\" AND " +
				Column.STATE + "=\"" + state + "\""
			, null, null, null, Column.CITY);
	}

	public Cursor selectByCS_UA(String city, String state)
	{
        city = checkAndEscape(city);
        state = checkAndEscape(state);
		return sqlDB.query(VIEW_NAME_UA, null, Column.CITY + "=\"" + city + "\" AND " +
				Column.STATE + "=\"" + state + "\""
			, null, null, null, Column.CITY);
	}

	@Override
	public Cursor selectAll() {
		return sqlDB.query(VIEW_NAME, null, null, null, null, null, Column.CITY_TYPE + " ASC");
	}

	public Cursor selectAllUA() {
		return sqlDB.query(VIEW_NAME_UA, null, null, null, null, null, Column.CITY_TYPE + " ASC");
	}

	@Override
	public boolean exists(String fieldName, String fieldValue) {
		Cursor slCursor = sqlDB.query(VIEW_NAME, null, fieldName + "=?", new String[]{fieldValue}, null, null, null);
		if (slCursor.getCount() > 0) {
			slCursor.close();
			return true;
		} else {
			slCursor.close();
			return false;
		}
	}

	public void clearTable() {
		sqlDB.delete(NAME, null, null);
		sqlDB.execSQL("DROP INDEX IF EXISTS " + Index.NAME);
		sqlDB.execSQL("DROP INDEX IF EXISTS " + Index.CODE);
		sqlDB.execSQL("DROP INDEX IF EXISTS " + Index.HITS);
	}

	public void creatIndex() {
		sqlDB.execSQL(CityTable.SQL_CREATE_INDEX_NAME);
		sqlDB.execSQL(CityTable.SQL_CREATE_INDEX_CODE);
		sqlDB.execSQL(CityTable.SQL_CREATE_INDEX_HITS);
	}

	public void creatIndexUA() {
		sqlDB.execSQL(CityTable.SQL_CREATE_INDEX_NAME_UA);
		sqlDB.execSQL(CityTable.SQL_CREATE_INDEX_CODE_UA);
		sqlDB.execSQL(CityTable.SQL_CREATE_INDEX_HITS_UA);
	}

	public void clearTableUA()
	{
		sqlDB.delete(NAME_UA, null, null);
		sqlDB.execSQL("DROP INDEX IF EXISTS " + Index.NAME_UA);
		sqlDB.execSQL("DROP INDEX IF EXISTS " + Index.CODE_UA);
		sqlDB.execSQL("DROP INDEX IF EXISTS " + Index.HITS_UA);
	}

	public void insert(String City, String Code, String WarehouseId, String State, String Area, int hasStore,
										 int hasPostomat, int cityType, int hits)
    {
    	  insertStatement.clearBindings();
				DictionariesDB.bindString(insertStatement, 1, City);
        DictionariesDB.bindString(insertStatement, 2, Code);
				DictionariesDB.bindString(insertStatement, 3, WarehouseId);
        DictionariesDB.bindString(insertStatement, 4, State);
        DictionariesDB.bindString(insertStatement, 5, Area);
				DictionariesDB.bindLong(insertStatement, 6, hasStore);
				DictionariesDB.bindLong(insertStatement, 7, hasPostomat);
				DictionariesDB.bindLong(insertStatement, 8, cityType);
				DictionariesDB.bindLong(insertStatement, 9, hits);
        insertStatement.executeInsert();
    }

	public void insertUA(String City, String Code, String WarehouseId, String State, String Area, int hasStore,
											 int hasPostomat, int cityType, int hits)
	{
		insertStatementUA.clearBindings();
		DictionariesDB.bindString(insertStatementUA, 1, City);
		DictionariesDB.bindString(insertStatementUA, 2, Code);
		DictionariesDB.bindString(insertStatementUA, 3, WarehouseId);
		DictionariesDB.bindString(insertStatementUA, 4, State);
		DictionariesDB.bindString(insertStatementUA, 5, Area);
		DictionariesDB.bindLong(insertStatementUA, 6, hasStore);
		DictionariesDB.bindLong(insertStatementUA, 7, hasPostomat);
		DictionariesDB.bindLong(insertStatementUA, 8, cityType);
		DictionariesDB.bindLong(insertStatementUA, 9, hits);
		insertStatementUA.executeInsert();
	}

	public void markDuplicates(String tableName)
	{
		sqlDB.execSQL("UPDATE " + tableName +
			" set " + Column.DUBLICATE_NAME + "=1 " +
			"where _id IN ( " +
			"select ct._id from tblCities ct " +
			"INNER JOIN " +
			" (SELECT ct2._id, ct2.city FROM tblCities ct2 GROUP BY ct2.city HAVING ( COUNT(*) > 1 )) ct1 ON ct.city = ct1.city);"
			);
	}

}
