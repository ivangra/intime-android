package com.ua.inr0001.intime;

import android.content.Context;
import android.database.Cursor;
import android.location.Location;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import intime.llc.ua.R;
import intime.utils.MyDateUtils;

import static com.ua.inr0001.intime.DictionariesDB.getColumnValue;
import static java.lang.Math.round;

/**
 * Created by ILYA on 25.10.2016.
 */

public class DepartmentsRvCursorAdapter extends RecyclerView.Adapter<DepartmentsRvCursorAdapter.DepartmentHolder> {

  public interface OnItemClickListener {
    void onItemClick(View view, Cursor cursor, int position);
  }

  private Cursor cursor;
  private Context context;
  private Location myLocation;
  private OnItemClickListener mOnItemClickListener;
  private Location location;

  public DepartmentsRvCursorAdapter(Context context, Cursor cursor, Location myLocation)
  {
    this.context = context;
    this.myLocation = myLocation;
    this.cursor = cursor;
  }

  public void setOnItemClickListener(OnItemClickListener onItemClickListener)
  {
    mOnItemClickListener = onItemClickListener;
  }

  public void setMyLocation(Location location)
  {
    this.location = location;
  }

  @Override
  public DepartmentHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(context).inflate(R.layout.departments_city_item, null, false);
    view.setLayoutParams(new RecyclerView.LayoutParams(
      RecyclerView.LayoutParams.MATCH_PARENT,
      RecyclerView.LayoutParams.WRAP_CONTENT
    ));
    return new DepartmentHolder(view);
  }

  private void setWeightLength(TextView tvCity)
  {
    StringBuilder sbWLengthWeight = new StringBuilder();
    String weight = DictionariesDB.getColumnValue(cursor, WarehouseDetailsTable.Column.CAPACITY);
    if (weight != null && weight.length() > 0)
      sbWLengthWeight.append("до " + weight + " кг");
    String length = DictionariesDB.getColumnValue(cursor, WarehouseDetailsTable.Column.MAX_LENGTH);
    if (length != null && length.length() > 0)
      sbWLengthWeight.append(" / до " + length + " м");
    if (sbWLengthWeight.length() > 0)
      tvCity.setText(sbWLengthWeight.toString() + " - " + DictionariesDB.getColumnValue(cursor, WarehouseDetailsTable.Column.CITY));
    else
      tvCity.setText(DictionariesDB.getColumnValue(cursor, WarehouseDetailsTable.Column.CITY));
  }

  @Override
  public void onBindViewHolder(DepartmentHolder holder, final int position) {
    if (cursor!=null) {
      cursor.moveToPosition(position);
      holder.container.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          if (mOnItemClickListener != null)
            mOnItemClickListener.onItemClick(v, cursor, position);
        }
      });

      holder.img.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_dep_arrow));


      if (DictionariesDB.getColumnValue(cursor, WarehouseDetailsTable.Column.TYPE).equals("store"))
        holder.tvDepNumber.setText(context.getResources().getString(R.string.text_department) + " №" + DictionariesDB.getColumnValue(cursor, WarehouseDetailsTable.Column.NUMBER));
      else
        holder.tvDepNumber.setText(context.getResources().getString(R.string.text_postomat) + " №" + DictionariesDB.getColumnValue(cursor, WarehouseDetailsTable.Column.NUMBER));

      holder.tvAddress.setText(DictionariesDB.getColumnValue(cursor, WarehouseDetailsTable.Column.ADDRESS));

      setWeightLength(holder.tvCity);

      DepartmentsRvAdapter.setworkStatusText(context, holder.tvWorkStatus,
        MyDateUtils.getWorhHoursStart(cursor),
        MyDateUtils.getWorhHoursEnd(cursor),
        MyDateUtils.getNextWorhHours(cursor));

      double humanDist = 0;

      if (myLocation != null) {
        double dist = DistanceCalculator.distance(Double.parseDouble(getColumnValue(cursor, WarehouseDetailsTable.Column.LAT)),
          Double.parseDouble(getColumnValue(cursor, WarehouseDetailsTable.Column.LNG)),
          myLocation.getLatitude(), myLocation.getLongitude());
        humanDist = dist / 1000;
      }

      if (location != null && humanDist != 0) {
        holder.tvDistance.setText("~" + Long.toString(round(humanDist)) + " км");
        holder.tvDistance.setVisibility(View.VISIBLE);
      }
    }
  }

  public void updateRequest(Cursor cursor)
  {
    if (!this.cursor.isClosed())
      this.cursor.close();
    this.cursor = cursor;
    notifyDataSetChanged();
  }

  @Override
  public int getItemCount() {
    if (cursor != null)
      return cursor.getCount();
    else
      return 0;
  }

  class DepartmentHolder extends RecyclerView.ViewHolder {

    ImageView img;
    TextView tvDepNumber;
    TextView tvWorkStatus;
    TextView tvCity; // wight + city
    TextView tvAddress;
    TextView tvDistance;
    View container;

    public DepartmentHolder(View view) {
      super(view);
      container = view;
      img = (ImageView) view.findViewById(R.id.imgDepArrow);
      tvDepNumber = (TextView) view.findViewById(R.id.tvDepNumber);
      tvWorkStatus = (TextView) view.findViewById(R.id.tvWhStatus);
      tvCity = (TextView) view.findViewById(R.id.tvWhCity);
      tvAddress = (TextView) view.findViewById(R.id.tvWhAddress);
      tvDistance = (TextView) view.findViewById(R.id.tvDistance);
    }
  }


}
