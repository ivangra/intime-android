package com.ua.inr0001.intime;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import intime.llc.ua.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class TtnInfoFragment extends Fragment {


  public TtnInfoFragment() {
    // Required empty public constructor
  }


  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    return inflater.inflate(R.layout.fragment_ttn_info, container, false);
  }

}
