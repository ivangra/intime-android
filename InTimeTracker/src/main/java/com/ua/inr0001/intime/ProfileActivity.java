package com.ua.inr0001.intime;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import intime.llc.ua.R;

public class ProfileActivity extends AppCompatActivity {

  private Button btnEditProfile;
  private Button btnChangePassword;
  private Context context;
  private TextView tvProfName;
  private TextView tvProfPhone;
  private TextView tvProfMail;
  private TextView tvProfAddress;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_profile);
    context = this;

    tvProfName = (TextView) findViewById(R.id.tvProfName);
    tvProfPhone = (TextView) findViewById(R.id.tvProfPhome);
    tvProfMail = (TextView) findViewById(R.id.tvProfMail);
    tvProfAddress = (TextView) findViewById(R.id.tvProfAddr);

    tvProfName.setText(GlobalApplicationData.getInstance().getUserInfo().getSurname() + " " +
      GlobalApplicationData.getInstance().getUserInfo().getName() + " " +
      GlobalApplicationData.getInstance().getUserInfo().getPatronymic()
    );
    tvProfPhone.setText(GlobalApplicationData.getInstance().getUserInfo().getPhoneNumbers());
    tvProfMail.setText(GlobalApplicationData.getInstance().getUserInfo().getEmails());

    btnEditProfile = (Button) findViewById(R.id.btnEditProfile);
    btnEditProfile.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        startActivity(new Intent(context, EditProfileActivity.class));

        /*Fragment fragment = new EditProfileFragment();
        FragmentTransaction fTrans = getActivity().getSupportFragmentManager().beginTransaction();
        fTrans.replace(R.id.frActivities, fragment);
        fTrans.addToBackStack("EditProfile");
        fTrans.commit();*/
      }
    });

    btnChangePassword = (Button) findViewById(R.id.btnChangePassword);
    btnChangePassword.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        startActivity(new Intent(context, ChangePasswordActivity.class));
        /*Fragment fragment = new ChangePasswordFragment();
        FragmentTransaction fTrans = getActivity().getSupportFragmentManager().beginTransaction();
        fTrans.replace(R.id.frActivities, fragment);
        fTrans.addToBackStack("ChangePassword");
        fTrans.commit();*/
      }
    });

    //((TextView) findViewById(R.id.tvProfName)).setText(GlobalApplicationData.getInstance().getContragentNAME());

    Toolbar toolbar = (Toolbar) findViewById(R.id.profileToolbar);
    setSupportActionBar(toolbar);

  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.accept_action_menu, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();
    switch (id)
    {
      case R.id.menu_action_accept: onBackPressed(); return true;
      default: return super.onOptionsItemSelected(item);
    }
  }

}
