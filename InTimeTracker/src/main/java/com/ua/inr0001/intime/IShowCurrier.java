package com.ua.inr0001.intime;

import intime.currier.DataFromCalc;

/**
 * Created by ILYA on 17.02.2017.
 */

public interface IShowCurrier {
  void showCurrierFragment(DataFromCalc dataFromCalc);
}
