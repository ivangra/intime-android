package com.ua.inr0001.intime;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import intime.api.model.billingcalc.RowDataWrapper;
import intime.api.model.billingcalc.Serv;
import intime.calculator.CalculatorModel;
import intime.calculator.DeclarationWrapper;
import intime.calculator.constants.CargoType;
import intime.currier.DataFromCalc;
import intime.googleapiclientutils.AutocompleteHelper;
import intime.gps.location.helpers.ISendLocation;
import intime.gps.location.helpers.LocationListenerServices;
import intime.intime.calcscrollview.CalcScrollView;
import intime.intime.mylistview.MyListView;
import intime.intime.weightseekbar.WeightSeekBar;
import intime.llc.ua.R;
import intime.utils.MaterialDatePickerUtils;
import intime.utils.MyDateFormatSymbols;
import intime.utils.MyDateUtils;
import intime.utils.MyStringUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.app.Activity.RESULT_OK;
import static android.view.animation.AnimationUtils.loadAnimation;
import static com.ua.inr0001.intime.DictionariesDB.getColumnValue;
import static java.lang.Integer.parseInt;

public class CalculationOfShippingCosts extends Fragment implements ICalculate, WeightSeekBar.IWeightSeekBar, IShowHideKeyboard,
  ISendLocation, GoogleApiClient.OnConnectionFailedListener, DatePickerDialog.OnDateSetListener {

  private static CalculationOfShippingCosts instance;
  private DatePickerDialog.OnDateSetListener dateSelectListener;

  private CalculationOfShippingCosts mActivity;

  public static CalculationOfShippingCosts getInstance()
  {
    return instance==null ? instance = new CalculationOfShippingCosts(): instance;
  }
  public Toolbar toolBar;
  private Context mContext;
  private Call<RowDataWrapper> callBilleng = null;
  private static final String TAG = "Calculator";

  public void setShowCurrier(IShowCurrier showCurrier) {
    this.showCurrier = showCurrier;
  }

  private IShowCurrier showCurrier;

  private GoogleApiClient mGoogleApiClient;

  private boolean scrollBottom = false;
  private CalculatorModel calculatorModel;
  private Context activityContext;
  private Animation animationHide;
  private Animation animationShow;

  private CalcScrollView scrollView1;

  // hint Frame
  private TextView tvWarningDesc;
  private TextView btnOnHintOk;
  private FrameLayout frLayoutHint;
  private LinearLayout frPreCalcSum;
  private LinearLayout frPreCalcSum1;
  private FrameLayout frPreCalcSumDisc;
  private FrameLayout frPreCalcError;
  private TextView tvCalcErrorMessage;
  private TextView tvCalcSum;
  private FrameLayout frRootShippingCost;

  // wheels controls
  private LinearLayout llWheels;
  private SeekBar sbRadius;
  private TextView tvSelectedRadius;
  private RadioButton rbWheelsLight;
  private TextView tvWheelBeginArray;
  private TextView tvWheelEndArray;
  private String wheelRadius[];

  // pallete types controls
  private RadioGroup rgPaleteType;
  private RadioButton rbPalete1;
  private RadioButton rbPalete2;
  private RadioButton rbPalete3;
  private CompoundButton.OnCheckedChangeListener listener;

  private LinearLayout llVolumePack;
  private RelativeLayout rlPackageCount;
  private Button btnPackageInc;
  private Button btnPackageDec;
  private TextView tvPackagesNumber;

  private TextView tvWeightCaption;
  private TextView tvSizeCaption;

  private Button btnCallCurrier;

  private TextView tvResultDateDelivery;
  private TextView tvResultCostDelivery;
  //private TextView tvPackgeSelect;

  private TextView edDateOfSend;
  private TextView edCitySender;
  private TextView edCityReciever;

  // Volume controls
  private FrameLayout frEditWidth;
  private FrameLayout frEditHeight;
  private FrameLayout frEditLength;
  private EditText edVolumeWidth;
  private EditText edVolumeLength;
  private EditText edVolumeHeight;
  private TextView tvVolumeWidth;
  private TextView tvVolumeLength;
  private TextView tvVolumeHeight;


  private EditText edCosts;

  private ImageView btnWarningCost;
  private String warningCostText;
  //private ImageView btnVolumeWarning;

  private MyListView lvCargoType;
  private MyListView lvSendTo;
  private ArrayList<Map<String, Object>> sendToList = new ArrayList<Map<String, Object>>();
  private MyListView lvSendFrom;
  private ArrayList<Map<String, Object>> sendFromList = new ArrayList<Map<String, Object>>();
  private MyListView lvPackages;
  private ArrayList<Map<String, Object>> packageList = new ArrayList<Map<String, Object>>();

  private LinearLayout layoutCost;
  //private LinearLayout layoutVolume;
  private LinearLayout layoutResults;


  private SwitchCompat swFeddBack;
  private SwitchCompat swSumPay;
  private ImageView btnWarningPodSum;
  private String warningPodSumText;
  private LinearLayout llPodSum;
  private TextView tvSumPay;
  private EditText editSumPay;
  private List<PackageSpinnerItem> packages;

  private FrameLayout weightFrame;
  private LinearLayout prevWeightLayout = null;

  // weight controls
  private LinearLayout llWeights;
  private LinearLayout llSeekBarWeights;
  private ImageView imgHumanWeight;
  private TextView tvWeightDescritption;
  private WeightSeekBar sbWeight;
  private EditText tvWeight;
  private FrameLayout frBoxWeight;
  private LinearLayout firstWeightItem;

  // layout Cost
  private LinearLayout llDiscount;
  private TextView tvResultDiscountDelivery;
  private TextView tvPreCalcDate;
  private TextView tvDiscountSum;

  private CalcTextChangeListener costTextChangeListener;
  private CalcTextChangeListener podSumTextChangeListener;

  private int selectedWeight = 0;

  private int[] cargoIcons = new int[]
    { R.drawable.ic_cargo_normal,
      R.drawable.ic_doc_normal,
      R.drawable.ic_paleta_normal,
      R.drawable.ic_wheel_normal
      };
  private int[] cargoIconsSelected = new int[]
    { R.drawable.ic_cargo_active,
      R.drawable.ic_doc_active,
      R.drawable.ic_paleta_active,
      R.drawable.ic_wheel_active
      };

  private int cargoTypeSelected = 0;
  private int sendToSelected = 0;
  private int sendFromSelected = 0;
  private int packageSelected = 0;
  private int samplePackages = 1;
  private String[] weights;

  private SimpleAdapter cargoTypeAdapter;
  private SimpleAdapter sendToAdapter;
  private SimpleAdapter sendFromAdapter;
  private SimpleAdapter packageAdapter;

  int mDay = 15;
  int mMonth = 7;
  int mYear = 2012;

//  private Handler mDateHandler = new Handler()
//  {
//    @Override
//    public void handleMessage(Message m)
//    {
//      Bundle b = m.getData();
//      mDay = b.getInt("set_day");
//      mMonth = b.getInt("set_month");
//      mYear = b.getInt("set_year");
//      edDateOfSend.setText(b.getString("set_date"));
//      calculatorModel.setDate(b.getString("set_date_request"));
//    }
//  };

//  private Handler mLocationHandler = new Handler()
//  {
//    @Override
//    public void handleMessage(Message m)
//    {
//      Bundle b = m.getData();
//      String mVityName = b.getString("CityName");
//    }
//  };

  private String getCurrentDate()
  {
    Calendar c = Calendar.getInstance();
    mMonth = c.get(Calendar.MONTH) + 1;
    mDay = c.get(Calendar.DATE);
    mYear = c.get(Calendar.YEAR);

    String months[] = GlobalApplicationData.getInstance().getContext().getResources().getStringArray(R.array.months);

    SimpleDateFormat sdf = new SimpleDateFormat(
      GlobalApplicationData.getInstance().getContext().getResources().getString(R.string.text_to_day) +
      ", d MMMM", new MyDateFormatSymbols(getContext()));

    SimpleDateFormat sdf1 = new SimpleDateFormat("dd.MM.yyyy",
      new MyDateFormatSymbols(getContext()));

    calculatorModel.setDate(sdf1.format(c.getTime()));

    return sdf.format(c.getTime());
  }

  private boolean checkPodSum(boolean result)
  {
    if (swSumPay.isChecked()) {
      try {
        if (!edCosts.isFocused())
        {
          if (parseInt(calculatorModel.getCost()) < parseInt(calculatorModel.getPodSum())) {
            edCosts.removeTextChangedListener(costTextChangeListener);
            calculatorModel.setCostWithoutEvent(calculatorModel.getPodSum());
//            Log.d(TAG, "Pod sun: " + calculatorModel.getPodSum());
            edCosts.setText(calculatorModel.getPodSum());
            edCosts.addTextChangedListener(costTextChangeListener);
            warningPodSumText = GlobalApplicationData.getInstance().getContext().getResources().getString(R.string.hint_pod_sum_error);
            btnWarningPodSum.setVisibility(View.VISIBLE);
            result = false;
          }
        }
      } catch (Exception e) {
        warningPodSumText = GlobalApplicationData.getInstance().getContext().getResources().getString(R.string.hint_pod_sum_error);
        btnWarningPodSum.setVisibility(View.VISIBLE);
        result = false;
      }
    }
    return result;
  }

  private boolean checkCost(boolean result)
  {
    if (swSumPay.isChecked()) {
      try {
        if (!editSumPay.isFocused())
        {
          if (parseInt(calculatorModel.getCost()) < parseInt(calculatorModel.getPodSum())) {
            editSumPay.removeTextChangedListener(podSumTextChangeListener);
            calculatorModel.setPodSumWithoutEvent(calculatorModel.getCost());
//            Log.d(TAG, "Cost: " + calculatorModel.getCost());
            editSumPay.setText(calculatorModel.getCost());
            editSumPay.addTextChangedListener(podSumTextChangeListener);
            warningCostText = GlobalApplicationData.getInstance().getContext().getResources().getString(R.string.hint_cost_sum_error);
            btnWarningCost.setVisibility(View.VISIBLE);
            //btnWarningPodSum.setVisibility(View.VISIBLE);
            result = false;
          }
        }
      } catch (Exception e) {
        //btnWarningPodSum.setVisibility(View.VISIBLE);
        result = false;
      }
    }
    return result;
  }

  // проверка правильности заполнения формы
  private boolean validateForm() {

    boolean result = true;

    if (calculatorModel.getReceiverCityName().length()==0) {
      result = false;
    }

    if (calculatorModel.getSenderCityName().length()==0) {
      result = false;
    }

    result = checkPodSum(result);

    result = checkCost(result);

    // стоимость
    try
    {
      int cost = parseInt(edCosts.getText().toString());
      if (cost>150000)
      {
        warningCostText = GlobalApplicationData.getInstance().getContext().getResources().getString(R.string.dialog_message_max_cost);
        edCosts.setText("150000");
        btnWarningCost.setVisibility(View.VISIBLE);
        result = false;
      }
    }
    catch(Exception e)
    {
    }

    if (calculatorModel.getCargoType().compareTo("cargo")==0) {
      // Объем
      if (calculatorModel.getLength().length() == 0) {
        edVolumeLength.setHintTextColor(GlobalApplicationData.getInstance().getContext().getResources().getColor(R.color.error_massage));
        result = false;
      }

      if (calculatorModel.getWidth().length() == 0) {
        edVolumeWidth.setHintTextColor(GlobalApplicationData.getInstance().getContext().getResources().getColor(R.color.error_massage));
        result = false;
      }

      if (calculatorModel.getHeight().length() == 0) {
        edVolumeHeight.setHintTextColor(GlobalApplicationData.getInstance().getContext().getResources().getColor(R.color.error_massage));
        result = false;
      }
    }

    // наложенный платеж

    int podSum = Integer.parseInt(calculatorModel.getPodSum());

    if (calculatorModel.getTypeTo().compareTo("address") == 0)
    {
      if (podSum > 25000)
      {
        btnWarningPodSum.setVisibility(View.VISIBLE);
        warningPodSumText = GlobalApplicationData.getInstance().getContext().getResources().getString(R.string.dialog_message_max_pod_sum_address);
        result = false;
      }
      if (calculatorModel.isHasStore() == false && podSum > 3000)
      {
        btnWarningPodSum.setVisibility(View.VISIBLE);
        warningPodSumText = GlobalApplicationData.getInstance().getContext().getResources().getString(R.string.dialog_message_max_pod_sum_address_no_deps);
        result = false;
      }
    }
    else
    if (podSum > 150000)
    {
      editSumPay.setText("150000");
      btnWarningPodSum.setVisibility(View.VISIBLE);
      warningPodSumText = GlobalApplicationData.getInstance().getContext().getResources().getString(R.string.dialog_message_max_pod_sum);
      result = false;
    }

    return result;
  }

  @Override
  public void onPause() {
    super.onPause();
    if (callBilleng != null && callBilleng.isExecuted())
      callBilleng.cancel();
    frPreCalcSum1.setVisibility(View.GONE);
    frPreCalcSumDisc.setVisibility(View.GONE);
  }

  private void setCargoTypeAdapter(String selectedType)
  {

    ArrayList<Map<String, Object>> cargoTypeList = new ArrayList<Map<String, Object>>();
    String[] cargoText = GlobalApplicationData.getInstance().getContext().getResources().getStringArray(R.array.cargo_types);

    Map<String, Object> m;

    m = new HashMap<String, Object>();
    m.put("Image", Integer.toString(cargoIcons[0]));
    m.put("Text", cargoText[0]);
    cargoTypeList.add(m);

    m = new HashMap<String, Object>();
    m.put("Image", Integer.toString(cargoIcons[1]));
    m.put("Text", cargoText[1]);
    cargoTypeList.add(m);

    if (selectedType.compareTo("postmat") != 0)
    {
      m = new HashMap<String, Object>();
      m.put("Image", Integer.toString(cargoIcons[2]));
      m.put("Text", cargoText[2]);
      cargoTypeList.add(m);

      m = new HashMap<String, Object>();
      m.put("Image", Integer.toString(cargoIcons[3]));
      m.put("Text", cargoText[3]);
      cargoTypeList.add(m);

    }

    String[] from = { "Image", "Text" };
    int[] to = { R.id.imageCargoIcon, R.id.tvcCargoType};

    cargoTypeAdapter = new SimpleAdapter(getContext(), cargoTypeList,
      R.layout.single_choise_item_def, from, to)
    {
      @NonNull
      @Override
      public View getView(int position, View convertView, ViewGroup parent) {
        final View view = super.getView(position, convertView, parent);
        ImageView cargoIcon = (ImageView) view.findViewById(R.id.imageCargoIcon);
        LinearLayout llSelectedItem = (LinearLayout) view.findViewById(R.id.llSingleChoiseItem);
        TextView tvDescription = (TextView) view.findViewById(R.id.tvcCargoType);
        if (position == cargoTypeSelected) {
          llSelectedItem.setBackgroundColor(GlobalApplicationData.getInstance().getContext().getResources().getColor(R.color.background_white));
          cargoIcon.setImageResource(cargoIconsSelected[position]);
          tvDescription.setTextColor(GlobalApplicationData.getInstance().getContext().getResources().getColor(R.color.base_text_color));
        }
        else {
          llSelectedItem.setBackgroundColor(GlobalApplicationData.getInstance().getContext().getResources().getColor(R.color.app_background));
          cargoIcon.setImageResource(cargoIcons[position]);
          tvDescription.setTextColor(GlobalApplicationData.getInstance().getContext().getResources().getColor(R.color.paper_caption));
        }
        return view;
      }
    };

    lvCargoType.setAdapter(cargoTypeAdapter);
  }

  private void setSendToAdapter(boolean hasStore, boolean hasPostomat)
  {

    sendToList.clear();
    String[] sendToText = GlobalApplicationData.getInstance().getContext().getResources().getStringArray(R.array.send_to);

    Map<String, Object> m;

    if (hasStore==true) {
      m = new HashMap<String, Object>();
      m.put("Text", sendToText[0]);
      sendToList.add(m);
    }

    m = new HashMap<String, Object>();
    m.put("Text", sendToText[1]);
    sendToList.add(m);

    if (hasPostomat==true) {
      m = new HashMap<String, Object>();
      m.put("Text", sendToText[2]);
      sendToList.add(m);
    }

    String[] from = { "Text" };
    int[] to = { R.id.tvcCargoType};

    sendToAdapter = new SimpleAdapter(getContext(), sendToList,
      R.layout.single_choise_item_def, from, to)
    {
      @NonNull
      @Override
      public View getView(int position, View convertView, ViewGroup parent) {
        final View view = super.getView(position, convertView, parent);
        ImageView cargoIcon = (ImageView) view.findViewById(R.id.imageCargoIcon);
        TextView tvDescription = (TextView) view.findViewById(R.id.tvcCargoType);
        LinearLayout llSelectedItem = (LinearLayout) view.findViewById(R.id.llSingleChoiseItem);
        if (position == sendToSelected) {
          llSelectedItem.setBackgroundColor(GlobalApplicationData.getInstance().getContext().getResources().getColor(R.color.background_white));
          cargoIcon.setImageResource(R.drawable.ic_list_check);
          tvDescription.setTextColor(GlobalApplicationData.getInstance().getContext().getResources().getColor(R.color.base_text_color));
        }
        else {
          llSelectedItem.setBackgroundColor(GlobalApplicationData.getInstance().getContext().getResources().getColor(R.color.app_background));
          cargoIcon.setImageResource(0);
          tvDescription.setTextColor(GlobalApplicationData.getInstance().getContext().getResources().getColor(R.color.paper_caption));
        }
        return view;
      }
    };

    lvSendTo.setAdapter(sendToAdapter);
  }

  private void setSendFromAdapter(boolean hasStore)
  {

    sendFromList.clear();
    String[] sendFromText = GlobalApplicationData.getInstance().getContext().getResources().getStringArray(R.array.send_from);

    Map<String, Object> m;

    if (hasStore==true) {
      m = new HashMap<String, Object>();
      m.put("Text", sendFromText[0]);
      sendFromList.add(m);
    }

    m = new HashMap<String, Object>();
    m.put("Text", sendFromText[1]);
    sendFromList.add(m);

    String[] from = { "Text" };
    int[] to = { R.id.tvcCargoType};

    sendFromAdapter = new SimpleAdapter(getContext(), sendFromList,
      R.layout.single_choise_item_def, from, to)
    {
      @NonNull
      @Override
      public View getView(int position, View convertView, ViewGroup parent) {
        final View view = super.getView(position, convertView, parent);
        ImageView cargoIcon = (ImageView) view.findViewById(R.id.imageCargoIcon);
        LinearLayout llSelectedItem = (LinearLayout) view.findViewById(R.id.llSingleChoiseItem);
        TextView tvDescription = (TextView) view.findViewById(R.id.tvcCargoType);
        if (position == sendFromSelected) {
          llSelectedItem.setBackgroundColor(GlobalApplicationData.getInstance().getContext().getResources().getColor(R.color.background_white));
          cargoIcon.setImageResource(R.drawable.ic_list_check);
          tvDescription.setTextColor(GlobalApplicationData.getInstance().getContext().getResources().getColor(R.color.base_text_color));
        }
        else {
          llSelectedItem.setBackgroundColor(GlobalApplicationData.getInstance().getContext().getResources().getColor(R.color.app_background));
          cargoIcon.setImageResource(0);
          tvDescription.setTextColor(GlobalApplicationData.getInstance().getContext().getResources().getColor(R.color.paper_caption));
        }
        return view;
      }
    };

    lvSendFrom.setAdapter(sendFromAdapter);
  }

  private void resetPrevLayout(LinearLayout v)
  {
    if (v!=null) {
      TextView tvWeightValue = (TextView) v.findViewById(R.id.tvWeightValue);
      tvWeightValue.setTypeface(null, Typeface.NORMAL);
      tvWeightValue.setTextColor(GlobalApplicationData.getInstance().getContext().getResources().getColor(R.color.text_hint));
      tvWeightValue = (TextView) v.findViewById(R.id.tvTopWeight);
      tvWeightValue.setVisibility(View.INVISIBLE);
      tvWeightValue = (TextView) v.findViewById(R.id.tvWeightUnit);
      tvWeightValue.setVisibility(View.INVISIBLE);
    }
  }

  private String[] getPaletteWeights(String[] weights, int lastCount)
  {
    String[] dest = new String[lastCount];
    System.arraycopy(weights, 3-lastCount, dest, 0, lastCount);
    return dest;
  }

  private LinearLayout createWightsList(Context context, LinearLayout parent, int cargo, int palletteLastCount)
  {
    parent.removeAllViewsInLayout();
    LinearLayout firstItem = null;
    LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    switch (cargo) {
      case CargoType.CARGO: weights = GlobalApplicationData.getInstance().getContext().getResources().getStringArray(R.array.wight_values); break;
      case CargoType.DOC: weights = GlobalApplicationData.getInstance().getContext().getResources().getStringArray(R.array.doc_weights); break;
      case CargoType.PALLET: {
        weights = GlobalApplicationData.getInstance().getContext().getResources().getStringArray(R.array.palete_weights);
        weights = getPaletteWeights(weights, palletteLastCount);
        break;
      }
    }


    for (int i = 0; i<weights.length; i++) {
      LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.list_weight_item, null);
      if (i==0) {
        prevWeightLayout = layout;
        firstItem = layout;
        selectedWeight = i;

        TextView tvWeightValue = (TextView) layout.findViewById(R.id.tvWeightValue);
        tvWeightValue.setTypeface(null, Typeface.BOLD);
        tvWeightValue.setTextColor(GlobalApplicationData.getInstance().getContext().getResources().getColor(R.color.base_text_color));
        tvWeightValue = (TextView) layout.findViewById(R.id.tvTopWeight);
        tvWeightValue.setVisibility(View.VISIBLE);
        tvWeightValue = (TextView) layout.findViewById(R.id.tvWeightUnit);
        tvWeightValue.setVisibility(View.VISIBLE);

      }
      layout.setTag(i);
      layout.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          resetPrevLayout(prevWeightLayout);
          prevWeightLayout = (LinearLayout)v;
          ViewGroup.LayoutParams params1 = weightFrame.getLayoutParams();

          params1.width = v.getWidth();
          weightFrame.setLayoutParams(params1);
          weightFrame.animate().x(v.getX()).setDuration(500).start();

          selectedWeight = (int) v.getTag();
          calculatorModel.setWeight(weights[selectedWeight]);
          TextView tvWeightValue = (TextView) v.findViewById(R.id.tvWeightValue);
          tvWeightValue.setTypeface(null, Typeface.BOLD);
          tvWeightValue.setTextColor(GlobalApplicationData.getInstance().getContext().getResources().getColor(R.color.base_text_color));
          tvWeightValue = (TextView) v.findViewById(R.id.tvTopWeight);
          tvWeightValue.setVisibility(View.VISIBLE);
          tvWeightValue = (TextView) v.findViewById(R.id.tvWeightUnit);
          tvWeightValue.setVisibility(View.VISIBLE);
        }
      });
      TextView tvValue = (TextView) layout.findViewById(R.id.tvWeightValue);
      tvValue.setText(weights[i]);
      LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
        LinearLayout.LayoutParams.WRAP_CONTENT,
        LinearLayout.LayoutParams.MATCH_PARENT
      );
      params.weight = 1;

      parent.addView(layout, params);

    }
    return firstItem;
  }

  private void setWeightFramePos(final LinearLayout firstItem)
  {
    if (firstItem!=null) {
      ViewGroup.LayoutParams params1 = weightFrame.getLayoutParams();
      params1.width = firstItem.getWidth();
      weightFrame.setLayoutParams(params1);
      weightFrame.setVisibility(View.VISIBLE);
      weightFrame.animate().x(firstItem.getX()).setDuration(500).start();
    }
  }

  private void setTypeTo(int position)
  {
    ArrayList<Object> list1 = new ArrayList<Object>(sendToList.get(position).values());
    String strPosition = (String) list1.get(0);
    String[] sendToText = GlobalApplicationData.getInstance().getContext().getResources().getStringArray(R.array.send_to);
    if (strPosition.compareTo(sendToText[0])==0)
    {
      calculatorModel.setTypeTo("store");
      setCargoTypeAdapter("");
      calculatorModel.setCargoType("cargo");
      selectCargoType(CargoType.CARGO);
    }
    if (strPosition.compareTo(sendToText[1])==0)
    {
      calculatorModel.setTypeTo("address");
      setCargoTypeAdapter("");
      calculatorModel.setCargoType("cargo");
      selectCargoType(CargoType.CARGO);
    }
    if (strPosition.compareTo(sendToText[2])==0)
    {
      calculatorModel.setTypeTo("postmat");
      setCargoTypeAdapter("postmat");
      calculatorModel.setCargoType("cargo");
      selectCargoType(CargoType.CARGO);
    }
  }

  private void initSendTo(final View view)
  {
    lvSendTo = (MyListView) view.findViewById(R.id.lvSendTo);
    lvSendTo.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
    lvSendTo.setItemsCanFocus(true);
    setSendToAdapter(true, true);
    lvSendTo.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        sendToSelected = position;
        setTypeTo(sendToSelected);
        sendToAdapter.notifyDataSetChanged();
      }
    });
  }

  private void setTypeFrom(int position)
  {
    ArrayList<Object> list1 = new ArrayList<Object>(sendFromList.get(position).values());
    String strPosition = (String) list1.get(0);
    String[] sendFromText = GlobalApplicationData.getInstance().getContext().getResources().getStringArray(R.array.send_from);
    if (strPosition.compareTo(sendFromText[0])==0)
    {
      calculatorModel.setTypeFrom("store");
    }
    if (strPosition.compareTo(sendFromText[1])==0)
    {
      calculatorModel.setTypeFrom("address");
    }
  }

  private void initSendFrom(final View view)
  {
    lvSendFrom = (MyListView) view.findViewById(R.id.lvSendFrom);
    lvSendFrom.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
    lvSendFrom.setItemsCanFocus(true);
    setSendFromAdapter(true);
    lvSendFrom.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        sendFromSelected = position;
        setTypeFrom(sendFromSelected);
        sendFromAdapter.notifyDataSetChanged();
      }
    });
  }

  private void selectCargoType(int position)
  {
    cargoTypeSelected = position;
    switch (cargoTypeSelected)
    {
      case CargoType.CARGO: calculatorModel.setCargoType("cargo"); break;
      case CargoType.DOC: calculatorModel.setCargoType("doc"); break;
      case CargoType.PALLET: calculatorModel.setCargoType("pallet"); break;
      default: calculatorModel.setCargoType("cargo");
    }
    cargoTypeAdapter.notifyDataSetChanged();
    //initPackageList(calculatorModel.getCargoType());
    setSubtypeViews(cargoTypeSelected);
    initPackageList();
  }

  private void initCargoType(final View view)
  {
    lvCargoType = (MyListView) view.findViewById(R.id.lvCargoType);
    lvCargoType.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
    lvCargoType.setItemsCanFocus(true);
    calculatorModel.setCargoType("cargo");
    setCargoTypeAdapter("");
    lvCargoType.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        selectCargoType(position);
      }
    });
    initPackageList();
  }

  private void showHint(float buttom, String warningText)
  {
    tvWarningDesc.setText(warningText);
    frLayoutHint.setTranslationY(buttom);
    frLayoutHint.setVisibility(View.VISIBLE);
  }

  private void initHintFrame(final View view)
  {
    frLayoutHint = (FrameLayout) view.findViewById(R.id.frWarningHint);
    frLayoutHint.setVisibility(View.GONE);
    tvWarningDesc = (TextView) view.findViewById(R.id.tvWarningDesc);

    btnOnHintOk = (TextView) view.findViewById(R.id.btnOnHint);
    btnOnHintOk.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        frLayoutHint.setVisibility(View.GONE);
      }
    });
  }

  private boolean isKeyboardShown(View rootView) {
    /* 128dp = 32dp * 4, minimum button height 32dp and generic 4 rows soft keyboard */
    final int SOFT_KEYBOARD_HEIGHT_DP_THRESHOLD = 20;

    Rect r = new Rect();
    rootView.getWindowVisibleDisplayFrame(r);
    DisplayMetrics dm = Resources.getSystem().getDisplayMetrics(); //rootView.getResources().getDisplayMetrics();
    /* heightDiff = rootView height - status bar height (r.top) - visible frame height (r.bottom - r.top) */
    int heightDiff = rootView.getBottom() - r.bottom;
    /* Threshold size: dp to pixels, multiply with display density */
    boolean isKeyboardShown = heightDiff > SOFT_KEYBOARD_HEIGHT_DP_THRESHOLD * dm.density;

    //Log.d("Test", "isKeyboardShown ? " + isKeyboardShown + ", heightDiff:" + heightDiff + ", density:" + dm.density
    //  + "root view height:" + rootView.getHeight() + ", rect:" + r);
    return isKeyboardShown;
  }

  private void initGooglePlaces()
  {
    mGoogleApiClient = new GoogleApiClient
      .Builder(activityContext)
      .addApi(Places.GEO_DATA_API)
      .addApi(Places.PLACE_DETECTION_API)
      .enableAutoManage(getActivity(), this)
      .build();
  }

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    dateSelectListener = this;
    activityContext = context;
    Log.d(TAG, "Attach");
  }

  @Override
  public void onActivityCreated(@Nullable Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
    ((MainActivity)getActivity()).setToolbar(toolBar);
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);

    if (data == null) return;

    if (resultCode == RESULT_OK) {

      try {
        String city = data.getStringExtra("City");
        if (city.compareTo("Sender") == 0) {
          calculatorModel.setSenderCityName(data.getStringExtra("Name"));
          calculatorModel.setSenderCityCode(data.getStringExtra("Code"),
            data.getStringExtra("WarehouseId"));
          sendFromSelected = 0;
          setSendFromAdapter(data.getIntExtra("HasStore", 0)==1);
          setTypeFrom(sendFromSelected);
          lvSendFrom.setSelection(0);
          sendFromAdapter.notifyDataSetChanged();
          edCitySender.setText(calculatorModel.getSenderCityName()); // ----
        } else {
          calculatorModel.setReceiverCityName(data.getStringExtra("Name"));
          calculatorModel.setReceiverCityCode(data.getStringExtra("Code"),
            data.getStringExtra("WarehouseId"));
          sendToSelected = 0;
          calculatorModel.setHasStore(data.getIntExtra("HasStore", 0)==1);
          setSendToAdapter(data.getIntExtra("HasStore", 0)==1, data.getIntExtra("HasPostomat", 0)==1);
          setTypeTo(sendToSelected);
          lvSendTo.setSelection(0);
          sendToAdapter.notifyDataSetChanged();
          edCityReciever.setText(calculatorModel.getReceiverCityName()); // ---
        }
      }
      catch(Exception e)
      {
        Log.d(TAG, "Error select city: " + e.getMessage());
      }

    }
  }

  private void initSenderReceiverInput(final View view)
  {
    edCityReciever = (TextView) view.findViewById(R.id.editCityReciever);
    edCitySender = (TextView) view.findViewById(R.id.editCitySender);
    edCityReciever.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent intent = new Intent(activityContext, SelectCityActivity.class);
        intent.putExtra("City", "Receiver");
        startActivityForResult(intent, 2000);
      }
    });

    edCitySender.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent intent = new Intent(activityContext, SelectCityActivity.class);
        intent.putExtra("City", "Sender");
        startActivityForResult(intent, 2001);
      }
    });
  }

  private void initCost(final View view)
  {
    layoutCost = (LinearLayout) view.findViewById(R.id.llCost);
    btnWarningCost = (ImageView) view.findViewById(R.id.imgWarningCost);
    btnWarningCost.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        showHint(layoutCost.getBottom(), warningCostText);
      }
    });

    edCosts = (EditText) view.findViewById(R.id.editccCargoCost);
    //edCosts.setText("200");
    edCosts.addTextChangedListener(new TextChangeListener(getActivity(), null, null, btnWarningCost));
    costTextChangeListener = new CalcTextChangeListener("cost");
    edCosts.addTextChangedListener(costTextChangeListener);
  }

  private void setFocusToVolumeEdit(final EditText edit)
  {
    edit.requestFocus();
    edit.setSelection(edit.length());
    InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
    imm.showSoftInput(edit, InputMethodManager.SHOW_IMPLICIT);
  }

  private void initVolume(final View view)
  {
    edVolumeWidth = (EditText) view.findViewById(R.id.editVolumeWidth);
    edVolumeHeight = (EditText) view.findViewById(R.id.editVolumeHeight);
    edVolumeLength = (EditText) view.findViewById(R.id.editVolumeLength);
    tvVolumeWidth = (TextView) view.findViewById(R.id.tvVolumeWidth);
    tvVolumeLength = (TextView) view.findViewById(R.id.tvVolumeLength);
    tvVolumeHeight = (TextView) view.findViewById(R.id.tvVolumeHeight);
    frEditHeight = (FrameLayout) view.findViewById(R.id.frEditHeight);
    frEditWidth = (FrameLayout) view.findViewById(R.id.frEditWeight);
    frEditLength = (FrameLayout) view.findViewById(R.id.frEditLength);

    frEditHeight.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        setFocusToVolumeEdit(edVolumeHeight);
      }
    });

    frEditWidth.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        setFocusToVolumeEdit(edVolumeWidth);
      }
    });

    frEditLength.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        setFocusToVolumeEdit(edVolumeLength);
      }
    });
    edVolumeLength.addTextChangedListener(new VolumeTextChangeListener(getActivity(), edVolumeLength, tvVolumeLength));
    edVolumeLength.addTextChangedListener(new CalcTextChangeListener("length"));
    edVolumeHeight.addTextChangedListener(new VolumeTextChangeListener(getActivity(), edVolumeHeight, tvVolumeHeight));
    edVolumeHeight.addTextChangedListener(new CalcTextChangeListener("height"));
    edVolumeWidth.addTextChangedListener(new VolumeTextChangeListener(getActivity(), edVolumeWidth, tvVolumeWidth));
    edVolumeWidth.addTextChangedListener(new CalcTextChangeListener("width"));
  }

  private void initPackageNumber(final View view)
  {
    btnPackageInc = (Button) view.findViewById(R.id.btnPackageInc);
    btnPackageInc.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        String packageCount = Integer.toString(++samplePackages);
        tvPackagesNumber.setText(packageCount);
        calculatorModel.setCount(packageCount);
        btnPackageDec.setBackgroundResource(R.drawable.button_normal);
        btnPackageDec.setTextColor(GlobalApplicationData.getInstance().getContext().getResources().getColor(R.color.additional_action));
      }
    });
    btnPackageDec = (Button) view.findViewById(R.id.btnPackageDec);
    btnPackageDec.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (--samplePackages<=1) {
          samplePackages = 1;
          btnPackageDec.setBackgroundResource(R.drawable.button_dec);
          btnPackageDec.setTextColor(GlobalApplicationData.getInstance().getContext().getResources().getColor(R.color.single_select_list_border));
        }
        String packageCount = Integer.toString(samplePackages);
        tvPackagesNumber.setText(packageCount);
        calculatorModel.setCount(packageCount);
      }
    });
    tvPackagesNumber = (TextView) view.findViewById(R.id.tvPackagesNumber);
    tvPackagesNumber.setText(Integer.toString(samplePackages));
  }

  private void initResultLayout(final View view)
  {
    layoutResults = (LinearLayout) view.findViewById(R.id.layoutResult);
    tvResultDateDelivery = (TextView) view.findViewById(R.id.tvResultDateDelivery);
    tvResultCostDelivery = (TextView) view.findViewById(R.id.tvResultCostDelivery);
    llDiscount = (LinearLayout) view.findViewById(R.id.llDiscount);
    tvResultDiscountDelivery = (TextView) view.findViewById(R.id.tvResultDiscountDelivery);
  }

  private void setPreCalcSumMargin(int buttomMargin)
  {
    FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) frPreCalcSum.getLayoutParams();
    params.setMargins(0, 0, 0, buttomMargin);
    frPreCalcSum.setLayoutParams(params);
    /*FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,
      FrameLayout.LayoutParams.WRAP_CONTENT);
    params.setMargins(0, 0, 0, buttomMargin);
    frPreCalcSum.setLayoutParams(params);*/
  }

  private void setSubtypeViews(int cargoType)
  {
    switch (cargoType)
    {
      case CargoType.CARGO: // Cargo
        rgPaleteType.setVisibility(View.GONE);
        rlPackageCount.setVisibility(View.GONE);
        llSeekBarWeights.setVisibility(View.VISIBLE);
        llWeights.setVisibility(View.GONE);
        llVolumePack.setVisibility(View.VISIBLE);
        tvSizeCaption.setVisibility(View.VISIBLE);
        tvWeightCaption.setVisibility(View.VISIBLE);
        llWheels.setVisibility(View.GONE);
        calculatorModel.setWeight("0.1");
        break;
      case CargoType.DOC:
        rgPaleteType.setVisibility(View.GONE);
        llSeekBarWeights.setVisibility(View.GONE);
        llWeights.setVisibility(View.VISIBLE);
        rlPackageCount.setVisibility(View.GONE);
        llVolumePack.setVisibility(View.GONE);
        tvSizeCaption.setVisibility(View.GONE);
        tvWeightCaption.setVisibility(View.VISIBLE);
        llWheels.setVisibility(View.GONE);
        firstWeightItem = createWightsList(getContext(), llWeights, CargoType.DOC, 0);
        calculatorModel.setWeight("0.1");
        break;
      case CargoType.PALLET:
        rgPaleteType.setVisibility(View.VISIBLE);
        llWeights.setVisibility(View.GONE);
        llSeekBarWeights.setVisibility(View.GONE);
        rlPackageCount.setVisibility(View.VISIBLE);
        llVolumePack.setVisibility(View.GONE);
        tvSizeCaption.setVisibility(View.GONE);
        tvWeightCaption.setVisibility(View.GONE);
        llWheels.setVisibility(View.GONE);
        //firstWeightItem = createWightsList(getContext(), llWeights, 2);
        rbPalete2.setChecked(true);
        calculatorModel.setTypePallets(GlobalApplicationData.getInstance().getContext().getResources().getString(R.string.pallete2));
        calculatorModel.setWeight("500");
        break;
      case CargoType.WHEEL:
        rgPaleteType.setVisibility(View.GONE);
        rlPackageCount.setVisibility(View.VISIBLE);
        llVolumePack.setVisibility(View.GONE);
        llWeights.setVisibility(View.GONE);
        llSeekBarWeights.setVisibility(View.GONE);
        tvSizeCaption.setVisibility(View.GONE);
        tvWeightCaption.setVisibility(View.GONE);
        llWheels.setVisibility(View.VISIBLE);
        rbWheelsLight.setChecked(true);
        calculatorModel.setWeight("0.1");
        initWheelsRadius(true);
        break;
      default:
        rgPaleteType.setVisibility(View.GONE);
        llWheels.setVisibility(View.GONE);
        break;
    }
  }

  private void initPackageList() {
    packageSelected = 0;
    packageList.clear();
    packages = DictionariesDB.getInstance(activityContext).tablePackages.getPackages(calculatorModel.getCargoType(),
      Double.valueOf(calculatorModel.getWidth()),
      Double.valueOf(calculatorModel.getHeight()),
      Double.valueOf(calculatorModel.getLength()),
      Double.valueOf(calculatorModel.getWeight())
    );

    Map<String, Object> m;
    for (int i = 0; i < packages.size(); i++) {
      m = new HashMap<String, Object>();
      m.put("Text", packages.get(i).toString());
      m.put("Price", packages.get(i).getPrice());
      packageList.add(m);
    }

    String[] from = { "Text" , "Price"};
    int[] to = { R.id.tvcCargoType, R.id.tvcPrice};

    packageAdapter = new SimpleAdapter(getContext(), packageList,
      R.layout.package_single_choise_item_def, from, to)
    {
      @NonNull
      @Override
      public View getView(int position, View convertView, ViewGroup parent) {
        final View view = super.getView(position, convertView, parent);
        ImageView cargoIcon = (ImageView) view.findViewById(R.id.imageCargoIcon);
        TextView tvDescription = (TextView) view.findViewById(R.id.tvcCargoType);
        TextView tvPrice = (TextView) view.findViewById(R.id.tvcPrice);
        LinearLayout llSelectedItem = (LinearLayout) view.findViewById(R.id.llSingleChoiseItem);
        if (position == packageSelected) {
          llSelectedItem.setBackgroundColor(GlobalApplicationData.getInstance().getContext().getResources().getColor(R.color.background_white));
          cargoIcon.setImageResource(R.drawable.ic_list_check);
          tvDescription.setTextColor(GlobalApplicationData.getInstance().getContext().getResources().getColor(R.color.base_text_color));
          tvPrice.setTextColor(GlobalApplicationData.getInstance().getContext().getResources().getColor(R.color.base_text_color));
        }
        else {
          llSelectedItem.setBackgroundColor(GlobalApplicationData.getInstance().getContext().getResources().getColor(R.color.app_background));
          cargoIcon.setImageResource(0);
          tvDescription.setTextColor(GlobalApplicationData.getInstance().getContext().getResources().getColor(R.color.paper_caption));
          tvPrice.setTextColor(GlobalApplicationData.getInstance().getContext().getResources().getColor(R.color.paper_caption));
        }
        return view;
      }
    };

    lvPackages.setAdapter(packageAdapter);
    calculatorModel.setPackage("0");
  }

  private void initAdditionalServices(final View view)
  {
    llPodSum = (LinearLayout) view.findViewById(R.id.llPodSum);
    swFeddBack = (SwitchCompat) view.findViewById(R.id.swFeedBack);
    swFeddBack.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        calculatorModel.setReturnDocuents(isChecked);
      }
    });
    swSumPay = (SwitchCompat) view.findViewById(R.id.swSumPay);
    swSumPay.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        swapPodPay(isChecked);
      }
    });
    tvSumPay = (TextView) view.findViewById(R.id.tvPodPaySum);
    btnWarningPodSum = (ImageView) view.findViewById(R.id.imgWarningPosSum);
    btnWarningPodSum.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        showHint(llPodSum.getBottom(), warningPodSumText);
      }
    });
    editSumPay = (EditText) view.findViewById(R.id.editPodPaySum);
    editSumPay.addTextChangedListener(new TextChangeListener(getActivity(), null, null, btnWarningPodSum));
    podSumTextChangeListener = new CalcTextChangeListener("podSum");
    editSumPay.addTextChangedListener(podSumTextChangeListener);
  }

  private void swapPodPay(boolean checked)
  {
    if (checked)
    {
      tvSumPay.setVisibility(View.GONE);
      editSumPay.setVisibility(View.VISIBLE);
      editSumPay.requestFocus();
    }
    else {
      tvSumPay.setVisibility(View.VISIBLE);
      editSumPay.setVisibility(View.GONE);
      calculatorModel.setPodSum("0");
    }
  }

  private void initWheelsRadius(boolean checked)
  {
    if (checked) {
      calculatorModel.setCargoType("wheels_car");
      wheelRadius = GlobalApplicationData.getInstance().getContext().getResources().getStringArray(R.array.wheels_light);
    }
    else {
      calculatorModel.setCargoType("wheels_truck");
      wheelRadius = GlobalApplicationData.getInstance().getContext().getResources().getStringArray(R.array.wheels_cargo);
    }
    tvWheelBeginArray.setText(wheelRadius[0]);
    tvWheelEndArray.setText(wheelRadius[wheelRadius.length-1]);
    sbRadius.setMax(wheelRadius.length-1);
    sbRadius.setProgress(0);
    calculatorModel.setRadius(wheelRadius[0]);
  }

  private void initWheels(View view)
  {
    llWheels = (LinearLayout) view.findViewById(R.id.llWheels);
    sbRadius = (SeekBar) view.findViewById(R.id.sbRadius);
    tvSelectedRadius = (TextView) view.findViewById(R.id.tvSelectedRadius);
    tvWheelBeginArray = (TextView) view.findViewById(R.id.tvWheelsBeginArray);
    tvWheelEndArray = (TextView) view.findViewById(R.id.tvWheelsEndArray);
    rbWheelsLight = (RadioButton) view.findViewById(R.id.rbWheelsLight);
    sbRadius.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
      @Override
      public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        tvSelectedRadius.setText(wheelRadius[progress]);
        calculatorModel.setRadius(wheelRadius[progress]);
      }

      @Override
      public void onStartTrackingTouch(SeekBar seekBar) {

      }

      @Override
      public void onStopTrackingTouch(SeekBar seekBar) {
      }
    });
    rbWheelsLight.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        initWheelsRadius(isChecked);
      }
    });
  }

  private void initPalleteTypes(View view)
  {
    rgPaleteType = (RadioGroup) view.findViewById(R.id.rgPaleteType);
    rbPalete1 = (RadioButton) view.findViewById(R.id.rbPalete1);
    rbPalete2 = (RadioButton) view.findViewById(R.id.rbPalete2);
    rbPalete3 = (RadioButton) view.findViewById(R.id.rbPalete3);
    listener = new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
          calculatorModel.setTypePallets(buttonView.getText().toString());
          if (buttonView.getText().toString().compareTo("1200x800")==0)
          {
            firstWeightItem = createWightsList(getContext(), llWeights, CargoType.PALLET, 3);
            calculatorModel.setWeight("500");
          }
          if (buttonView.getText().toString().compareTo("1200x1000")==0)
          {
            firstWeightItem = createWightsList(getContext(), llWeights, CargoType.PALLET, 2);
            calculatorModel.setWeight("750");
          }
          if (buttonView.getText().toString().compareTo("1200x1200")==0)
          {
            firstWeightItem = createWightsList(getContext(), llWeights, CargoType.PALLET, 1);
            calculatorModel.setWeight("1000");
          }
          llWeights.setVisibility(View.VISIBLE);
        }
      }
    };
    rbPalete1.setOnCheckedChangeListener(listener);
    rbPalete2.setOnCheckedChangeListener(listener);
    rbPalete3.setOnCheckedChangeListener(listener);
  }

  @Override
  public void weightChanged(Double weight) {
    calculatorModel.setWeight(String.valueOf(weight));
    initPackageList();
  }

  private void initMainScrollView(final View view)
  {
      scrollView1 = (CalcScrollView) view.findViewById(R.id.scrollView1);
      scrollView1.setOnBottomReachedListener(new CalcScrollView.OnBottomReachedListener() {
        @Override
        public void onBottomReached() {
          scrollBottom = true;
          if (layoutResults.getVisibility()==View.VISIBLE && frPreCalcSum1.getVisibility()==View.VISIBLE) {
            frPreCalcSum1.startAnimation(animationHide);
          }
        }
      });
      scrollView1.setOnTopReachedListener(new CalcScrollView.OnTopReachedListener() {
        @Override
        public void onTopReached() {
          scrollBottom = false;
          if (layoutResults.getVisibility()==View.VISIBLE && frPreCalcSum1.getVisibility()==View.INVISIBLE) {
            frPreCalcSum1.startAnimation(animationShow);
          }
        }
      });
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    mActivity = this;
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.activity_shipping_costs, container, false);
    getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
    animationHide = loadAnimation(GlobalApplicationData.getInstance().getContext(), R.anim.price_fade_out);
    animationShow = loadAnimation(GlobalApplicationData.getInstance().getContext(), R.anim.price_fade_in);
    animationShow.setAnimationListener(new Animation.AnimationListener() {
      @Override
      public void onAnimationStart(Animation animation) {
      }

      @Override
      public void onAnimationEnd(Animation animation) {
        frPreCalcSum1.setVisibility(View.VISIBLE);
      }

      @Override
      public void onAnimationRepeat(Animation animation) {

      }
    });
    animationHide.setAnimationListener(new Animation.AnimationListener() {
      @Override
      public void onAnimationStart(Animation animation) {

      }

      @Override
      public void onAnimationEnd(Animation animation) {
        frPreCalcSum1.setVisibility(View.INVISIBLE);
      }

      @Override
      public void onAnimationRepeat(Animation animation) {

      }
    });
    //getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
    //getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE|WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
    toolBar = (Toolbar) view.findViewById(R.id.toolBarShippingCost);
    mContext = getContext();
    Log.d(TAG, "onCreateView()");
    calculatorModel = new CalculatorModel(this);

    weightFrame = (FrameLayout) view.findViewById(R.id.frameRectangleWieght);
    llWeights = (LinearLayout) view.findViewById(R.id.llWeights);
    sbWeight = (WeightSeekBar) view.findViewById(R.id.sbWeight);
    tvWeight = (EditText) view.findViewById(R.id.tvWeight);
    tvWeightDescritption = (TextView) view.findViewById(R.id.tvWeightDescription);
    frBoxWeight = (FrameLayout) view.findViewById(R.id.frBoxWeight);
    imgHumanWeight = (ImageView) view.findViewById(R.id.imgHumanWeight);
    sbWeight.setParams(tvWeight, imgHumanWeight, tvWeightDescritption, frBoxWeight, this);
    sbWeight.setMax(1000);
    llSeekBarWeights = (LinearLayout) view.findViewById(R.id.llSeekBarWeights);
    tvWeightCaption = (TextView) view.findViewById(R.id.tvWeightCaption);
    tvSizeCaption = (TextView) view.findViewById(R.id.tvSizeCaption);
    llVolumePack = (LinearLayout) view.findViewById(R.id.llVolumePack);
    //lvResults = (MyListView) view.findViewById(R.id.lvResults);
    lvPackages = (MyListView) view.findViewById(R.id.lvPackages);
    lvPackages.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
    lvPackages.setItemsCanFocus(true);
    lvPackages.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        packageSelected = position;
        calculatorModel.setPackage(packages.get(position).getCode());
        packageAdapter.notifyDataSetChanged();
      }
    });

    //firstWeightItem = createWightsList(getContext(), llWeights);
    //spinnerPackages = (AppCompatSpinner) view.findViewById(spinnerPackages);
    btnCallCurrier = (Button) view.findViewById(R.id.btnCallCurrierCalc);
    if (MyDateUtils.nowIsHoliday())
      btnCallCurrier.setVisibility(View.GONE);
    btnCallCurrier.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        showCurrier.showCurrierFragment(setDataToCurrier());
        //scrollToCurrier.scrollCallCurrier();
        //sendDataToCurrier();
      }
    });

    initCargoType(view);
    initPalleteTypes(view);
    initWheels(view);
    initSendTo(view);
    initSendFrom(view);

    frPreCalcSum = (LinearLayout) view.findViewById(R.id.frPreCalcSum);
    frPreCalcSum1 = (LinearLayout) view.findViewById(R.id.frPreCalcSum1);
    frPreCalcSumDisc = (FrameLayout) view.findViewById(R.id.frPreCalcSumDisc);
    frPreCalcError = (FrameLayout) view.findViewById(R.id.frPreCalcError);
    tvCalcErrorMessage = (TextView) view.findViewById(R.id.tvCalcErrorMessage);
    tvCalcSum = (TextView) view.findViewById(R.id.tvCalcSum);
    tvPreCalcDate = (TextView) view.findViewById(R.id.tvPreCalcDate);
    tvDiscountSum = (TextView) view.findViewById(R.id.tvDiscountSum);
    frRootShippingCost = (FrameLayout) view.findViewById(R.id.frRootShippingCost);


    edDateOfSend = (TextView) view.findViewById(R.id.editDateOfSend);
    edDateOfSend.setText(getCurrentDate());
    edDateOfSend.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          Calendar now = Calendar.getInstance();
          DatePickerDialog dpd = DatePickerDialog.newInstance(
            dateSelectListener,
            now.get(Calendar.YEAR),
            now.get(Calendar.MONTH),
            now.get(Calendar.DAY_OF_MONTH)
          );
          MaterialDatePickerUtils.setSundays(dpd);
          MaterialDatePickerUtils.setHolidays(dpd);
          dpd.setMinDate(now);
          dpd.setAccentColor(getResources().getColor(R.color.action_bar_color));
          dpd.show(getActivity().getFragmentManager(), "CurrierDatepickerdialog");
//          DateFragmentDialog datePicker = new DateFragmentDialog();
//                                        datePicker.setHandler(mDateHandler);
//                                        datePicker.setHumanStringFormat("d MMMM, EEEE");
//                                        datePicker.setSystemStringFormat("dd.MM.yyyy");
//                                        FragmentManager fm = getFragmentManager();
//                                        FragmentTransaction ft = fm.beginTransaction();
//                                        ft.add(datePicker, "date_picker");
//                                        ft.commit();
          }
        }
    );

    rlPackageCount = (RelativeLayout) view.findViewById(R.id.rlPackageCount);
    initCost(view);
    initVolume(view);
    initHintFrame(view);
    initSenderReceiverInput(view);
    initPackageNumber(view);
    initResultLayout(view);

    setPreCalcSumMargin(0);

    view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
      @Override
      public void onGlobalLayout() {
        setWeightFramePos(firstWeightItem);
        firstWeightItem = null;
      }
    });

    //spinnerCargoSubType = (AppCompatSpinner) view.findViewById(spinnerCargoSubType);
    //spinnerCargoWeight = (AppCompatSpinner) view.findViewById(spinnerCargoWeight);

    //initSubTypeSpinner(view);
    initAdditionalServices(view);
    setSubtypeViews(cargoTypeSelected);

    initMainScrollView(view);
    initGooglePlaces();
    startLocation();
    return view;
  } // onCreateView

  @Override
  public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

    final String humanStringFormat = "d MMMM, EEEE";
    final String systemStringFormat = "dd.MM.yyyy";
    mDay = dayOfMonth;
    mMonth = monthOfYear;
    mYear = year;

    Calendar cal1 = Calendar.getInstance(Locale.getDefault());
    cal1.set(Calendar.DATE, mDay);
    cal1.set(Calendar.MONTH, mMonth);
    cal1.set(Calendar.YEAR, mYear);
    SimpleDateFormat sdf = new SimpleDateFormat(humanStringFormat,
      new MyDateFormatSymbols(getContext()));
    edDateOfSend.setText(sdf.format(cal1.getTime()));

    SimpleDateFormat sdf1 = new SimpleDateFormat(systemStringFormat,
      new MyDateFormatSymbols(getContext()));
    calculatorModel.setDate(sdf1.format(cal1.getTime()));
  }

  @Override
  public void onStart()
  {
    super.onStart();
    mGoogleApiClient.connect();
  }

  @Override
  public void onStop()
  {
    super.onStart();
    mGoogleApiClient.disconnect();
  }

  @Override
  public void onDestroyView()
  {
    super.onDestroyView();
    ((MainActivity)getActivity()).setToolbar(null);
    calculatorModel.setReceiverCityName("");
    calculatorModel.setReceiverCityCode("", "");
    calculatorModel.setSenderCityCode("", "");
    calculatorModel.setSenderCityName("");
    if (callBilleng != null && callBilleng.isExecuted())
      callBilleng.cancel();
  }

  @Override
  public void onDetach() {
    super.onDetach();
    if (mGoogleApiClient != null) {
      mGoogleApiClient.stopAutoManage(getActivity());
      mGoogleApiClient.disconnect();
    }
  }

//  private Converter.Factory createGsonConverter() {
//    GsonBuilder gsonBuilder = new GsonBuilder();
//    gsonBuilder.registerTypeAdapter(Calculator.class, new CalculatorDeserializer());
//    Gson gson = gsonBuilder.create();
//    return GsonConverterFactory.create(gson);
//  }

  private DataFromCalc setDataToCurrier()
  {
    DataFromCalc dataFromCalc = new DataFromCalc();
    dataFromCalc.setSystemDate(calculatorModel.getDate());
    dataFromCalc.setHumanDate(edDateOfSend.getText().toString());
    dataFromCalc.setCityCode(calculatorModel.getSenderCityCode());
    dataFromCalc.setCityName(calculatorModel.getSenderCityName());
    dataFromCalc.setWidth(calculatorModel.getWidth());
    dataFromCalc.setHeight(calculatorModel.getHeight());
    dataFromCalc.setLength(calculatorModel.getLength());
    dataFromCalc.setWeight(calculatorModel.getWeight());
    dataFromCalc.setSelectedWeight(selectedWeight);
    dataFromCalc.setWeightText(tvWeightDescritption.getText().toString());
    dataFromCalc.setCost(calculatorModel.getCost());
    dataFromCalc.setCount(calculatorModel.getCount());
    dataFromCalc.setPodSum(calculatorModel.getPodSum());
    dataFromCalc.setRadius(calculatorModel.getRadius());
    dataFromCalc.setCargoType(calculatorModel.getCargoType());
    dataFromCalc.setCargoSubType(calculatorModel.getTypePallets());
    dataFromCalc.setRadiusPosition(sbRadius.getProgress());
    return dataFromCalc;
  }

//  private String roundTotalSum(final String totalsum)
//  {
//    double dTotalSun = parseDouble(totalsum);
//    DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.ENGLISH);
//    DecimalFormat df = new DecimalFormat("#.##", otherSymbols);
//    return df.format(dTotalSun);
//  }

//  private double initResults(Map<String, String> parameters)
//  {
//    ArrayList<Map<String, String>> resultsList = new ArrayList<Map<String, String>>();
//
//    Map<String, String> m;
//    double podSum = 0;
//    double docReturn = 0;
//    double sum = 0;
//    for (Map.Entry<String, String> entry : parameters.entrySet()) {
//      String key = entry.getKey();
//      if (key.compareToIgnoreCase("Обратная доставка документов")==0)
//      {
//        String value = entry.getValue();
//        docReturn = parseDouble(value);
//        m = new HashMap<String, String>();
//        m.put("Key", key);
//        m.put("Value", value + " грн.");
//        resultsList.add(m);
//      }
//      if (key.compareToIgnoreCase("Всего с учетом всех скидок и промокодов")==0)
//        sum = parseDouble(entry.getValue());
//
//      if (key.contains("Наложенный платеж"))
//      {
//        podSum += parseDouble(entry.getValue());
//      }
//    }
//
//    if (podSum>0)
//    {
//      m = new HashMap<String, String>();
//      m.put("Key", "Наложенный платеж");
//      m.put("Value", Double.toString(podSum) + " грн.");
//      resultsList.add(m);
//    }
//
//    String[] from = { "Key", "Value" };
//    int[] to = { R.id.tvResultKey, R.id.tvResultValue};
//
//    SimpleAdapter sumsAdapter = new SimpleAdapter(getContext(), resultsList,
//      R.layout.calc_results_item, from, to);
//
//    lvResults.setAdapter(sumsAdapter);
//    lvResults.setVisibility(View.VISIBLE);
//    return docReturn>0 ? docReturn + sum : sum;
//  }

//  private void callRequest()
//  {
//    if (call != null && call.isExecuted())
//      call.cancel();
//    Retrofit retrofit = new Retrofit.Builder()
//      .baseUrl("https://newapi.intime.ua/")
//      .addConverterFactory(createGsonConverter())
//      .client(GlobalApplicationData.getUnsafeOkHttpClient())
//      .build();
//
//    Log.d(TAG, "From: " + calculatorModel.getTypeFrom());
//    Log.d(TAG, "To: " + calculatorModel.getTypeTo());
//    Log.d(TAG, "City from: " + calculatorModel.getSenderCityCode());
//    Log.d(TAG, "City to: " + calculatorModel.getReceiverCityCode());
//    Log.d(TAG, "Cargo type: " + calculatorModel.getCargoType());
//    Log.d(TAG, "Weight: " + calculatorModel.getWeight());
//    Log.d(TAG, "Width: " + calculatorModel.getWidth());
//    Log.d(TAG, "Height: " + calculatorModel.getHeight());
//    Log.d(TAG, "Length: " + calculatorModel.getLength());
//    Log.d(TAG, "Radius: " + calculatorModel.getRadius());
//    Log.d(TAG, "Count package: " + calculatorModel.getCount());
//    Log.d(TAG, "Pallets: " + calculatorModel.getTypePallets());
//    Log.d(TAG, "Cost: " + calculatorModel.getCost());
//    Log.d(TAG, "POD amount: " + calculatorModel.getPodSum());
//    Log.d(TAG, "Package: " + calculatorModel.getPackage());
//    Log.d(TAG, "Return documents: " + calculatorModel.getReturnDocuents());
//    Log.d(TAG, "Date: " + calculatorModel.getDate());
//
//    IRtfAPI service = retrofit.create(IRtfAPI.class);
//    call = service.calculate(GlobalApplicationData.getCurrentLanguage(),
//      calculatorModel.getTypeFrom(),
//      calculatorModel.getTypeTo(),
//      calculatorModel.getSenderCityCode(),
//      calculatorModel.getReceiverCityCode(),
//      calculatorModel.getCargoType(),
//      calculatorModel.getWeight(),
//      calculatorModel.getWidth(),
//      calculatorModel.getHeight(),
//      calculatorModel.getLength(),
//      calculatorModel.getRadius(),
//      calculatorModel.getCount(),
//      calculatorModel.getTypePallets(),
//      calculatorModel.getCost(),
//      calculatorModel.getPodSum(),
//      calculatorModel.getPackage(),
//      calculatorModel.getDate(),
//      calculatorModel.getReturnDocuents()
//    );
//
//    //Log.d("TrackTTN", call.request().url().encodedQuery());
//    call.enqueue(
//      new Callback<Calculator>() {
//        @Override
//        public void onResponse(Call<Calculator> call, Response<Calculator> response) {
//          Calculator calculator = response.body();
//          if (!calculator.isError()) {
//            Log.d(TAG, "Price: " + calculator.getParameters().get("Всего с учетом всех скидок и промокодов") + " grn. ");
//
//            String totalSum = "0";
//
//            if (swSumPay.isChecked() || swFeddBack.isChecked())
//              totalSum = Double.toString(initResults(calculator.getParameters()));
//            else {
//              totalSum = calculator.getParameters().get("Всего с учетом всех скидок и промокодов");
//              lvResults.setVisibility(View.GONE);
//            }
//
//            totalSum = roundTotalSum(totalSum);
//            tvCalcSum.setText(totalSum + " грн.");
//            frPreCalcSum1.setVisibility(View.VISIBLE);
//            frPreCalcError.setVisibility(View.GONE);
//            layoutResults.setVisibility(View.VISIBLE);
//            tvResultCostDelivery.setText(totalSum + " грн.");
//
//          } else {
//            Log.d(TAG, "Price: " + calculator.getErrorMessage());
//            //frPreCalcSum1.setVisibility(View.GONE);
//            //layoutResults.setVisibility(View.GONE);
//          }
//        }
//
//        @Override
//        public void onFailure(Call<Calculator> call, Throwable t) {
//          //frPreCalcSum1.setVisibility(View.GONE);
//          Log.d(TAG, "Calc error: " + t.getMessage());
//        }
//
//      }
//    );
//  }

  private void callRequestBilling()
  {
    if (callBilleng != null && callBilleng.isExecuted())
      callBilleng.cancel();
    Retrofit retrofit = new Retrofit.Builder()
      .baseUrl("http://195.13.178.5:5001/")
      .addConverterFactory(GsonConverterFactory.create())
      .build();

//    Log.d(TAG, "From: " + calculatorModel.getTypeFrom());
//    Log.d(TAG, "To: " + calculatorModel.getTypeTo());
//    Log.d(TAG, "City from: " + calculatorModel.getSenderCityCode());
//    Log.d(TAG, "City to: " + calculatorModel.getReceiverCityCode());
//    Log.d(TAG, "Cargo type: " + calculatorModel.getCargoType());
//    Log.d(TAG, "Weight: " + calculatorModel.getDeclaration().seats.get(0).weight_m);
//    Log.d(TAG, "Width: " + calculatorModel.getDeclaration().seats.get(0).width_m);
//    Log.d(TAG, "Height: " + calculatorModel.getDeclaration().seats.get(0).height_m);
//    Log.d(TAG, "Length: " + calculatorModel.getDeclaration().seats.get(0).length_m);
//    Log.d(TAG, "Radius: " + calculatorModel.getRadius());
//    Log.d(TAG, "Count package: " + calculatorModel.getCount());
//    Log.d(TAG, "Pallets: " + calculatorModel.getTypePallets());
//    Log.d(TAG, "Cost: " + calculatorModel.getCost());
//    Log.d(TAG, "POD amount: " + calculatorModel.getPodSum());
//    Log.d(TAG, "Package: " + calculatorModel.getPackage());
//    Log.d(TAG, "Return documents: " + calculatorModel.getReturnDocuents());
//    Log.d(TAG, "Date: " + calculatorModel.getDate());

    IRtfAPI service = retrofit.create(IRtfAPI.class);
    callBilleng = service.calcBilling("application/json",new DeclarationWrapper(calculatorModel.getDeclaration()));

    //Log.d("TrackTTN", call.request().url().encodedQuery());
    callBilleng.enqueue(
      new Callback<RowDataWrapper>() {
        @Override
        public void onResponse(Call<RowDataWrapper> call, Response<RowDataWrapper> response) {
          RowDataWrapper calculator = response.body();
//          try {
//            Log.d(TAG, calculator.string());
//          } catch (IOException e) {
//
//          }
//          try {
//            Log.d(TAG, response.errorBody().string());
//          } catch (IOException e) {
//            //e.printStackTrace();
//          }

          if (calculator!=null) {

            Double dTotalSum = 0d;
            Double dDiscount = 0d;
            Double dWithDiscount = 0d;
            try {
              for (Serv serv : calculator.rowData.calculationData.servs.serv) {
                dTotalSum += serv.cost;
                if (serv.discount != 0d)
                  dDiscount = serv.discount;
              }
              dWithDiscount = dTotalSum - dDiscount;
            }
            catch(Exception e){

            }
            String strTotalSum = MyStringUtils.roundTotalSum(dTotalSum);
            tvCalcSum.setText(strTotalSum + " грн");
            tvCalcSum.setVisibility(View.VISIBLE);
            tvResultCostDelivery.setText(strTotalSum + " грн");
            String dateDelivery = MyDateUtils.addDay(calculatorModel.getDate(), calculator.rowData.calculationData.Calcparams.Daytodelivery);
            tvResultDateDelivery.setText(dateDelivery);
            tvPreCalcDate.setText(getResources().getString(R.string.date_delivery) + " " + dateDelivery);
            if (dDiscount != 0d)
            {
              llDiscount.setVisibility(View.VISIBLE);
              strTotalSum = MyStringUtils.roundTotalSum(dWithDiscount);
              tvResultDiscountDelivery.setText(strTotalSum + " грн");
              tvDiscountSum.setText(strTotalSum + " грн");
              tvResultCostDelivery.setPaintFlags(tvResultCostDelivery.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
              tvResultCostDelivery.setTextColor(mContext.getResources().getColor(R.color.text_hint));
              tvCalcSum.setPaintFlags(tvCalcSum.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
              tvCalcSum.setTextColor(mContext.getResources().getColor(R.color.text_hint));
              frPreCalcSumDisc.setVisibility(View.VISIBLE);
            }
            else {
              llDiscount.setVisibility(View.GONE);
              frPreCalcSumDisc.setVisibility(View.GONE);
              tvResultCostDelivery.setPaintFlags(Paint.ANTI_ALIAS_FLAG);
              tvResultCostDelivery.setTextColor(mContext.getResources().getColor(R.color.base_text_color));
              tvCalcSum.setPaintFlags(Paint.ANTI_ALIAS_FLAG);
              tvCalcSum.setTextColor(mContext.getResources().getColor(R.color.base_text_color));
            }
//            Log.d(TAG, "Total sum: " + strTotalSum);
            if (!scrollBottom)
            frPreCalcSum1.setVisibility(View.VISIBLE);
            frPreCalcError.setVisibility(View.GONE);
            layoutResults.setVisibility(View.VISIBLE);
            layoutResults.postDelayed(new Runnable() {
              @Override
              public void run() {
                scrollView1.setHeightBottomLayout(layoutResults.getHeight());
              }
            }, 100);
            //scrollView1.setHeightBottomLayout(layoutResults.getHeight());

          } else {
            //Log.d(TAG, "Price: " + calculator.getErrorMessage());
            llDiscount.setVisibility(View.GONE);
            frPreCalcSum1.setVisibility(View.GONE);
            frPreCalcSumDisc.setVisibility(View.GONE);
            layoutResults.setVisibility(View.GONE);
          }
        }

        @Override
        public void onFailure(Call<RowDataWrapper> call, Throwable t) {
          //frPreCalcSum1.setVisibility(View.GONE);
          Log.d(TAG, "Calc error: " + t.getMessage());
        }

      }
    );
  }

  private void setViewAndCalc()
  {
    
      btnWarningPodSum.setVisibility(View.GONE);
      btnWarningCost.setVisibility(View.GONE);
      frPreCalcError.setVisibility(View.GONE);
      if (!scrollBottom)
        frPreCalcSum1.setVisibility(View.VISIBLE);
      frPreCalcSumDisc.setVisibility(View.GONE);
      tvPreCalcDate.setText(GlobalApplicationData.getInstance().getContext().getResources().getString(R.string.text_do_calc));
      tvCalcSum.setVisibility(View.GONE);
//      Log.d(TAG, "call callRequestMethod()");
      //callRequest();
      callRequestBilling();
    
  }

  public void doCalcRequest()
  {
    try {
//      Log.d(TAG, "call validateForm()");
      if (GlobalApplicationData.getInstance().IsOnline()) {
        if (validateForm()) {
          setViewAndCalc();
        } else {
          if (calculatorModel.getReceiverCityName().length() == 0 ||
            calculatorModel.getSenderCityName().length() == 0)
            tvCalcErrorMessage.setText(getResources().getString(R.string.set_delivery_route));
          else
            tvCalcErrorMessage.setText(getResources().getString(R.string.text_fields_wrong_short));
          frPreCalcError.setVisibility(View.VISIBLE);
          frPreCalcSum1.setVisibility(View.GONE);
          frPreCalcSumDisc.setVisibility(View.GONE);
        }
      }
    }
    catch(Exception e)
    {
      Log.d(TAG, "doCalcRequest error: " + e.getMessage());
    }
  }

  @Override
  public void keyboardSHow(boolean visible) {
    if (frPreCalcSum!=null) {
      if (true)
        frPreCalcSum.setVisibility(View.VISIBLE);
      else
        frPreCalcSum.setVisibility(View.GONE);
    }
  }

  @Override
  public void sendLocation(Location location) {
    Intent intent = new Intent(mContext, FetchAddressIntentService.class);
    intent.putExtra(Constants.RECEIVER, new AddressResultReceiver(new Handler(), location));
    intent.putExtra(Constants.LOCATION_DATA_EXTRA, location);
    mContext.startService(intent);
  }

  private void startLocation() {
    new LocationListenerServices(mContext, this);
  }

  @Override
  public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

  }

  class AddressResultReceiver extends ResultReceiver {

    private Location myLocation;

    public AddressResultReceiver(Handler handler, Location location)
    {
      super(handler);
      this.myLocation = location;
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
      if (resultCode == Constants.SUCCESS_RESULT) {
        Log.d(TAG, "Address found!");
        SearchCityByGeo sGeo = new SearchCityByGeo(resultData.getString("City"), mGoogleApiClient, myLocation);
        sGeo.linkMain(mActivity);
        sGeo.execute();
      }
    }
  }

  private class SearchCityByGeo extends AsyncTask<Void, Void, Void> {

    private CalculationOfShippingCosts shippingCostActivity;
    private String city;
    private GoogleApiClient mGoogleApiClient;
    private Location myLocation;
    private String findedCityName;

    public SearchCityByGeo(String city, GoogleApiClient mGoogleApiClient, Location location)
    {
      this.city = city;
      this.mGoogleApiClient = mGoogleApiClient;
      this.myLocation = location;
    }

    public void linkMain(CalculationOfShippingCosts activity)
    {
      shippingCostActivity = activity;
    }

    public void unlink()
    {
      shippingCostActivity = null;
    }

    private void searchSuitableCityByPlace(String cleanRequst)
    {
      ArrayList<CityInfoForCalc> cityInfo = new ArrayList<CityInfoForCalc>();
      double humanDist;
      ArrayList<AutocompletePrediction> filterData = new ArrayList<>();
      if (mGoogleApiClient != null && mGoogleApiClient.isConnected())
        filterData = new AutocompleteHelper(mGoogleApiClient).getAutocomplete("Украина " + cleanRequst);
      if (filterData != null && filterData.size()>0)
      {
        for (AutocompletePrediction aup : filterData)
        {
          PlaceBuffer places = null;
          if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            places = Places.GeoDataApi.getPlaceById(mGoogleApiClient, aup.getPlaceId()).await();
          }
          if (places != null && places.getStatus().isSuccess()){
            LatLng queriedLocation = places.get(0).getLatLng();
            if (myLocation != null) {
              double dist = DistanceCalculator.distance(queriedLocation.latitude,
                queriedLocation.longitude,
                myLocation.getLatitude(), myLocation.getLongitude());
              humanDist = dist / 1000;
              try {
                CityInfoForCalc ci = new CityInfoForCalc();
                ci.setCityName(aup.getPrimaryText(null).toString());
                String state = aup.getSecondaryText(null).toString();
                state = state.substring(0, state.indexOf(','));
                ci.setState(state);
                ci.setDisatnce(humanDist);
                cityInfo.add(ci);
              }
              catch(Exception e)
              {

              }
            }
          }
        } // for (AutocompletePrediction aup : filterData)

        if (cityInfo.size() > 0) {
          Cursor cursor;
          sortByDistance(cityInfo);
          if (GlobalApplicationData.getCurrentLanguage().compareTo("ua") == 0) {
            cursor = DictionariesDB.getInstance(mContext).tableCity.selectByCS_UA(cityInfo.get(0).getCityName(),
              cityInfo.get(0).getState());
          } else {
            cursor = DictionariesDB.getInstance(mContext).tableCity.selectByCS(cityInfo.get(0).getCityName(),
              cityInfo.get(0).getState());
          }
          cursor.moveToFirst();
          if (cursor.getCount() > 0) {
            findedCityName = getColumnValue(cursor, CityTable.Column.CITY);
            String cityCode = getColumnValue(cursor, CityTable.Column.CODE);
            String warehouseId = getColumnValue(cursor, CityTable.Column.WAREHOUSE_ID);
            calculatorModel.setSenderCityName(findedCityName);
            calculatorModel.setSenderCityCode(cityCode, warehouseId);
          }
          cursor.close();
        }
      }
    }

    private void sortByDistance(ArrayList<CityInfoForCalc> list)
    {
      Collections.sort(list, new Comparator<CityInfoForCalc>() {
        public int compare(CityInfoForCalc o1, CityInfoForCalc o2) {
          double depNum1 = o1.getDisatnce();
          double depNum2 = o2.getDisatnce();
          if (depNum1 < depNum2)
            return -1;
          else
            return 1;
        }
      });
    }

    private boolean searchSuitableCityInWarehouseTable(/*final String city*/) {
      boolean result = false;
      double humanDist = 0;
      ArrayList<CityInfoForCalc> cityInfo = new ArrayList<CityInfoForCalc>();
      Cursor cursor;
      if (GlobalApplicationData.getCurrentLanguage().compareTo("ua") == 0)
        cursor = DictionariesDB.getInstance(mContext).tableWarehouseDetails.
          selectByDiffLocationUA(Double.toString(myLocation.getLongitude()), Double.toString(myLocation.getLatitude()));//selectByCityNameUA(city);
      else
        cursor = DictionariesDB.getInstance(mContext).tableWarehouseDetails.
          selectByDiffLocation(Double.toString(myLocation.getLongitude()), Double.toString(myLocation.getLatitude()));//selectByCityName(city);
      cursor.moveToFirst();
      while (!cursor.isAfterLast()) {
        if (myLocation != null) {
          double dist = DistanceCalculator.distance(Double.parseDouble(getColumnValue(cursor, WarehouseDetailsTable.Column.LAT)),
            Double.parseDouble(getColumnValue(cursor, WarehouseDetailsTable.Column.LNG)),
            myLocation.getLatitude(), myLocation.getLongitude());
          humanDist = dist / 1000;
          CityInfoForCalc ci = new CityInfoForCalc();
          ci.setArea(getColumnValue(cursor, WarehouseDetailsTable.Column.AREA));
          ci.setCityName(getColumnValue(cursor, WarehouseDetailsTable.Column.CITY));
          ci.setState(getColumnValue(cursor, WarehouseDetailsTable.Column.STATE));
          ci.setDisatnce(humanDist);
          cityInfo.add(ci);
        }
        cursor.moveToNext();
      }
      cursor.close();
      if (cityInfo.size() > 0) {
        sortByDistance(cityInfo);
        if (GlobalApplicationData.getCurrentLanguage().compareTo("ua") == 0) {
          cursor = DictionariesDB.getInstance(mContext).tableCity.selectByCSA_UA(cityInfo.get(0).getCityName(),
            cityInfo.get(0).getState(), cityInfo.get(0).getArea());
        } else {
          cursor = DictionariesDB.getInstance(mContext).tableCity.selectByCSA(cityInfo.get(0).getCityName(),
            cityInfo.get(0).getState(), cityInfo.get(0).getArea());
        }
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
          findedCityName = getColumnValue(cursor, CityTable.Column.CITY);
          String cityCode = getColumnValue(cursor, CityTable.Column.CODE);
          String warehouseId = getColumnValue(cursor, CityTable.Column.WAREHOUSE_ID);
          calculatorModel.setSenderCityName(findedCityName);
          calculatorModel.setSenderCityCode(cityCode, warehouseId);
          result = true;
        }
        cursor.close();
      }
      return result;
    }

    @Override
    protected Void doInBackground(Void... voids) {
      if (searchSuitableCityInWarehouseTable(/*city*/) == false)
        searchSuitableCityByPlace(city);
      return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
      super.onPostExecute(aVoid);
      if (shippingCostActivity != null && findedCityName != null && findedCityName.length() > 0)
        shippingCostActivity.edCitySender.setText(findedCityName);
    }
  }

}
