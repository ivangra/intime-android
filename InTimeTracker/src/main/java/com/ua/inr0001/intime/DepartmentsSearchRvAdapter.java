package com.ua.inr0001.intime;

import android.content.Context;
import android.location.Location;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import intime.llc.ua.R;

import static java.lang.Math.round;

/**
 * Created by ILYA on 25.10.2016.
 */

public class DepartmentsSearchRvAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

  private static final int ITEM_DEP = 1300;
  private static final int ITEM_PLACE = 1402;

  public interface OnItemClickListener {
    void onItemClick(View view, int position);
  }

  private Context context;
  private List<DepartmentMapItem> depList;
  private OnItemClickListener mOnItemClickListener;
  private Location location;

  public DepartmentsSearchRvAdapter(Context context, List<DepartmentMapItem> depList)
  {
    this.context = context;
    this.depList = depList;
  }

  public void setmOnItemClickListener(OnItemClickListener onItemClickListener)
  {
    mOnItemClickListener = onItemClickListener;
  }

  @Override
  public int getItemViewType(int position) {
    if (this.depList.get(position).depType.compareTo("place")==0)
      return ITEM_PLACE;
    else
      return ITEM_DEP;
    //return super.getItemViewType(position);
  }

  public void setDataList(List<DepartmentMapItem> depList)
  {
    this.depList = new ArrayList<DepartmentMapItem>(depList);
  }

  public void setMyLocation(Location location)
  {
    this.location = location;
  }

  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    if (viewType == ITEM_DEP) {
      View view = LayoutInflater.from(context).inflate(R.layout.departments_city_item, null, false);
      view.setLayoutParams(new RecyclerView.LayoutParams(
        RecyclerView.LayoutParams.MATCH_PARENT,
        RecyclerView.LayoutParams.WRAP_CONTENT
      ));
      return new DepartmentHolder(view);
    }
    else
    {
      View view = LayoutInflater.from(context).inflate(R.layout.places_list_item_1, null, false);
      view.setLayoutParams(new RecyclerView.LayoutParams(
        RecyclerView.LayoutParams.MATCH_PARENT,
        RecyclerView.LayoutParams.WRAP_CONTENT
      ));
      return new PlacesHolder(view);
    }
  }

  private DepartmentMapItem getDepItem(int position)
  {
    DepartmentMapItem depItem = null;
    try {
      depItem = depList.get(position);
      return depItem;
    }
    catch(Exception e)
    {
      return depItem;
    }
  }

  private void setWeightLength(TextView tvCity, DepartmentMapItem depItem)
  {
    if (tvCity != null) {
      StringBuilder sbWLengthWeight = new StringBuilder();

      if (depItem.weightLimit > 0)
        sbWLengthWeight.append("до " + Integer.toString(depItem.weightLimit) + " кг");

      if (depItem.lengthLimit != null && depItem.lengthLimit.length() > 0)
        sbWLengthWeight.append(" / до " + depItem.lengthLimit + " м");
      if (sbWLengthWeight.length() > 0) {
        tvCity.setText(sbWLengthWeight.toString());
        tvCity.setVisibility(View.VISIBLE);
      } else
        tvCity.setVisibility(View.GONE);
    }
  }

  private void setCity(TextView tvCity, DepartmentMapItem depItem)
  {
    if (depItem.dublicate == 1)
    {
      tvCity.setText(depItem.cityName + ", " + depItem.state);
    }
    else
      tvCity.setText(depItem.cityName);
  }

  private void bindDepartment(DepartmentHolder holder, final int position)
  {
    DepartmentMapItem depItem = getDepItem(position);
    if (depItem!=null) {
      holder.container.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          if (mOnItemClickListener != null)
            mOnItemClickListener.onItemClick(v, position);
        }
      });

      holder.img.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_dep_arrow));

      if (depItem.depType.equals("store"))
        holder.tvDepNumber.setText(context.getResources().getString(R.string.text_department) + " №" + depItem.depNumber);
      else
        holder.tvDepNumber.setText(context.getResources().getString(R.string.text_postomat) + " №" + depItem.depNumber);

      holder.tvAddress.setText(depItem.depAddress);

      setWeightLength(holder.tvDepParams, depItem);
      setCity(holder.tvCity, depItem);

      DepartmentsRvAdapter.setworkStatusText(context, holder.tvWorkStatus, depItem.workHoursStart, depItem.workHoursEnd, depItem.workHoursData);

      if (location != null && depItem.distance != 0) {
        holder.tvDistance.setText("~" + Long.toString(round(depItem.distance)) + " км");
        holder.tvDistance.setVisibility(View.VISIBLE);
      }
    }
  }

  private void bindPlace(PlacesHolder holder, final int position)
  {
    DepartmentMapItem depItem = getDepItem(position);
    if (depItem != null) {
      holder.container.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          if (mOnItemClickListener != null)
            mOnItemClickListener.onItemClick(v, position);
        }
      });
      holder.tvPlacesStreet.setText(depItem.cityName);
      holder.tvPlacesRegion.setText(depItem.depAddress);
    }
  }

  @Override
  public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
    if (this.depList.get(position).depType.compareTo("place") == 0)
      bindPlace((PlacesHolder) holder, position);
    else
      bindDepartment((DepartmentHolder) holder, position);
  }

  @Override
  public int getItemCount() {
    if (depList.size()>120)
      return 120;
    else
      return depList.size();
  }

  class DepartmentHolder extends RecyclerView.ViewHolder {

    ImageView img;
    TextView tvDepNumber;
    TextView tvWorkStatus;
    TextView tvCity; // wight + city
    TextView tvAddress;
    TextView tvDistance;
    TextView tvDepParams;
    View container;

    public DepartmentHolder(View view) {
      super(view);
      container = view;
      img = (ImageView) view.findViewById(R.id.imgDepArrow);
      tvDepNumber = (TextView) view.findViewById(R.id.tvDepNumber);
      tvWorkStatus = (TextView) view.findViewById(R.id.tvWhStatus);
      tvCity = (TextView) view.findViewById(R.id.tvWhCity);
      tvAddress = (TextView) view.findViewById(R.id.tvWhAddress);
      tvDistance = (TextView) view.findViewById(R.id.tvDistance);
      tvDepParams = (TextView) view.findViewById(R.id.tvDepParams);
    }
  }

  class PlacesHolder extends RecyclerView.ViewHolder {

    TextView tvPlacesStreet;
    TextView tvPlacesRegion;
    View container;

    public PlacesHolder(View view) {
      super(view);
      container = view;
      tvPlacesStreet = (TextView) view.findViewById(R.id.tvPlaceStreet);
      tvPlacesRegion = (TextView) view.findViewById(R.id.tvPlacesRegion);
    }
  }

}
