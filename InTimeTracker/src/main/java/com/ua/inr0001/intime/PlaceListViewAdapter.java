package com.ua.inr0001.intime;

import android.content.Context;
import android.graphics.Typeface;
import android.text.style.CharacterStyle;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.view.inputmethod.InputMethodSubtype;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataBufferUtils;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.AutocompletePredictionBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLngBounds;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import intime.api.model.yatranslate.YaResult;
import intime.googleapiclientutils.AutocompleteHelper;
import intime.llc.ua.R;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by technomag on 21.02.17.
 */

public class PlaceListViewAdapter
  extends BaseAdapter implements Filterable {

  private Call<YaResult> yaCall;
  private Context context;

  //private static final CharacterStyle STYLE_BOLD = new StyleSpan(Typeface.BOLD);
  private static final CharacterStyle STYLE_NORMAL = new StyleSpan(Typeface.NORMAL);
  /**
   * Current results returned by this adapter.
   */
  private ArrayList<AutocompletePrediction> mResultList;

  /**
   * Handles autocomplete requests.
   */
  private GoogleApiClient mGoogleApiClient;

  private AutocompleteHelper autocompleteHelper;

  private String cityName = "";

  public void setCity(String city)
  {
    cityName = city;
  }

  /**
   * Initializes with a resource for text rows and autocomplete query bounds.
   *
   * @see android.widget.ArrayAdapter#ArrayAdapter(android.content.Context, int)
   */
  public PlaceListViewAdapter(Context context, GoogleApiClient googleApiClient, String cityName) {
    mGoogleApiClient = googleApiClient;
    this.autocompleteHelper = new AutocompleteHelper(googleApiClient);
    this.context = context;
    this.cityName = cityName;
  }

  /**
   * Returns the number of results received in the last autocomplete query.
   */
  @Override
  public int getCount() {
    if (mResultList!=null)
      return mResultList.size();
    else
      return 0;
  }

  /**
   * Returns an item from the last autocomplete query.
   */
  @Override
  public AutocompletePrediction getItem(int position) {
    return mResultList.get(position);
  }

  @Override
  public long getItemId(int position) {
    return 0;
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {

    if (convertView == null) {
      convertView = LayoutInflater.from(context)
        .inflate(R.layout.places_list_item, null);
    }



    AutocompletePrediction item = getItem(position);

    TextView textView1 = (TextView) convertView.findViewById(R.id.tvPlaceStreet);
    TextView textView2 = (TextView) convertView.findViewById(R.id.tvPlacesRegion);
    textView1.setText(item.getPrimaryText(STYLE_NORMAL));
    textView2.setText(item.getSecondaryText(STYLE_NORMAL));

    return convertView;
  }

  private String getTranslation(final String textToTranslate)
  {

    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://translate.yandex.net/api/v1.5/tr.json/")
            .addConverterFactory(GsonConverterFactory.create())
            //.addConverterFactory(createGsonConverter())
            .client(GlobalApplicationData.getUnsafeOkHttpClient())
            .build();

    IRtfAPI service = retrofit.create(IRtfAPI.class);
    yaCall = service.translate("trnsl.1.1.20170421T062340Z.f4850849e54089b6.3ff815daa296ef93f84694bb9ca1333b33c230a0",
            textToTranslate, "ru-uk", "plain");
    //Log.d("TrackTTN", call.request().url().encodedQuery());
    try {
      Response<YaResult> translated = yaCall.execute();
      Log.d("Currier", translated.body().getText().get(0));
      return translated.body().getText().get(0);
    } catch (Exception e) {
        //Log.d("Currier", "translate Error: " + e.getMessage().toString());
      return textToTranslate;
    }
  }

  private boolean isUaString(final String testString)
  {
    InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
    InputMethodSubtype ims = imm.getCurrentInputMethodSubtype();
    String locale = ims.getLocale();
    if (locale.compareTo("uk")==0)
      return true;
    else
      return false;
  }

  /**
   * Returns the filter for the current set of autocomplete results.
   */
  @Override
  public Filter getFilter() {
    return new Filter() {
      @Override
      protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results = new FilterResults();

        // We need a separate list to store the results, since
        // this is run asynchronously.
        ArrayList<AutocompletePrediction> filterData = new ArrayList<>();

        // Skip the autocomplete query if no constraints are given.
        if (constraint != null) {
          // Query the autocomplete API for the (constraint) search string.
          //if (isUaString(constraint.toString()))
            filterData = autocompleteHelper.getAutocomplete("Украина " + cityName + " " + constraint.toString());
          //else
          //{
          //  String translated = getTranslation("Украина " + cityName + " " + constraint.toString());
          //  filterData = getAutocomplete(translated);
          //}
        }

        results.values = filterData;
        if (filterData != null) {
          results.count = filterData.size();
        } else {
          results.count = 0;
        }

        return results;
      }

      @Override
      protected void publishResults(CharSequence constraint, FilterResults results) {

        if (results != null && results.count > 0) {
          // The API returned at least one result, update the data.
          mResultList = (ArrayList<AutocompletePrediction>) results.values;
          notifyDataSetChanged();
        } else {
          // The API did not return any results, invalidate the data set.
          notifyDataSetInvalidated();
        }
      }

      @Override
      public CharSequence convertResultToString(Object resultValue) {
        // Override this method to display a readable result in the AutocompleteTextView
        // when clicked.
        if (resultValue instanceof AutocompletePrediction) {
          return ((AutocompletePrediction) resultValue).getFullText(null);
        } else {
          return super.convertResultToString(resultValue);
        }
      }
    };
  }

}

