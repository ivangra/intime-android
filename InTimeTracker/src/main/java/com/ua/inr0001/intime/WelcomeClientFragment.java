package com.ua.inr0001.intime;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import intime.llc.ua.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class WelcomeClientFragment extends Fragment {

  private String welcomeText;

  public WelcomeClientFragment() {
    // Required empty public constructor
  }

  public void setText(String text)
  {
    welcomeText = text;
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_welcome_client, container, false);
    TextView tvWelcomeClient = (TextView) view.findViewById(R.id.tvWelcomeClient);
    tvWelcomeClient.setText(welcomeText);
    return view;
  }

}
