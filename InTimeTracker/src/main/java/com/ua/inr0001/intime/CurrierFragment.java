package com.ua.inr0001.intime;


import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import intime.currier.DataFromCalc;
import intime.googleapiclientutils.AutocompleteHelper;
import intime.gps.location.helpers.ISendLocation;
import intime.gps.location.helpers.LocationListenerServices;
import intime.intime.mylistview.MyListView;
import intime.intime.weightseekbar.WeightSeekBar;
import intime.llc.ua.R;
import intime.maskedtextedit.MaskedInputFilter;
import intime.utils.MaterialDatePickerUtils;
import intime.utils.MyDateFormatSymbols;
import intime.utils.MyDateUtils;
import intime.utils.MyStringUtils;

import static android.app.Activity.RESULT_OK;
import static com.ua.inr0001.intime.DictionariesDB.getColumnValue;
import static java.lang.Integer.parseInt;


/**
 * A simple {@link Fragment} subclass.
 */
public class CurrierFragment extends Fragment implements ICalcCurrierView, DatePickerDialog.OnDateSetListener,
  ISendLocation, GoogleApiClient.OnConnectionFailedListener {

  private CurrierFragment mActivity;
  private Context context;
  private Toolbar toolbar;
  private Button btnCallCurrier;
  private MaskedInputFilter maskedInputFilter;
  private EditText editPhone;
  private EditText editFullName;
  private TextView editCity;
  private TextView tvClientPhoneHint;
  private TextView tvClientFullNameHint;

  private TextView edDateOfSend;
  private DatePickerDialog.OnDateSetListener dateSelectListener;

  private TextView editStreet;
  private EditText editHouse;
  private EditText editFloor;
  private EditText editRoom;

  private TextView tvStreet;
  private TextView tvHouse;
  private TextView tvFloor;
  private TextView tvRoom;
  private TextView tvCity;

  private SwitchCompat swLiftFromStage;
  private EditText editComment;

  private DataFromCalc dataFromCalc;

  private int selectedWeight = 0;

  int mDay = 15;
  int mMonth = 7;
  int mYear = 2012;

  private String[] weights;

  private String timeFormatted;

//  private Handler mDateHandler = new Handler()
//  {
//    @Override
//    public void handleMessage(Message m)
//    {
//      Bundle b = m.getData();
//      mDay = b.getInt("set_day");
//      mMonth = b.getInt("set_month");
//      mYear = b.getInt("set_year");
//      edDateOfSend.setText(b.getString("set_date"));
//      timeFormatted = b.getString("set_date_request");
//      edDateOfSend.clearFocus();
//    }
//  };

  //private String cityCode;
  private MailCallCurrier mailCallCurrier;

  private TextView tvWarningDesc;
  private TextView btnOnHintOk;
  private FrameLayout frLayoutHint;

  // Cost controls

  private EditText edCosts;
  private CurrierTextChangeListener costTextChangeListener;
  private CalcTextChangeListener podSumTextChangeListener;
  private ImageView btnWarningCost;
  private LinearLayout layoutCost;

  // Cargo Type structures

  private MyListView lvCargoType;

  private int[] cargoIcons = new int[]
    { R.drawable.ic_cargo_normal,
      R.drawable.ic_doc_normal,
      R.drawable.ic_paleta_normal,
      R.drawable.ic_wheel_normal
    };
  private int[] cargoIconsSelected = new int[]
    { R.drawable.ic_cargo_active,
      R.drawable.ic_doc_active,
      R.drawable.ic_paleta_active,
      R.drawable.ic_wheel_active
    };

  private int cargoTypeSelected = 0;

  private SimpleAdapter cargoTypeAdapter;

  // pallete types controls

  private RadioGroup rgPaleteType;
  private RadioButton rbPalete1;
  private RadioButton rbPalete2;
  private RadioButton rbPalete3;
  private CompoundButton.OnCheckedChangeListener listener;

  // Volume controls and atructures

  private LinearLayout llVolumePack;
  private FrameLayout frEditWidth;
  private FrameLayout frEditHeight;
  private FrameLayout frEditLength;
  private EditText edVolumeWidth;
  private EditText edVolumeLength;
  private EditText edVolumeHeight;
  private TextView tvVolumeWidth;
  private TextView tvVolumeLength;
  private TextView tvVolumeHeight;

  // weight controls

  private LinearLayout llWeights;
  private LinearLayout llSeekBarWeights;
  private ImageView imgHumanWeight;
  private TextView tvWeightDescritption;
  private WeightSeekBar sbWeight;
  private EditText tvWeight;
  private FrameLayout frBoxWeight;
  private LinearLayout firstWeightItem;
  private FrameLayout weightFrame;
  private LinearLayout prevWeightLayout = null;

  // WeightCaptions
  private TextView tvWeightCaption;
  private TextView tvSizeCaption;

  // PackageCount Controls

  private RelativeLayout rlPackageCount;
  private Button btnPackageInc;
  private Button btnPackageDec;
  private TextView tvPackagesNumber;
  private int samplePackages = 1;

  // wheels controls
  private LinearLayout llWheels;
  private SeekBar sbRadius;
  private TextView tvSelectedRadius;
  private RadioButton rbWheelsLight;
  private RadioButton rbWheelsTruck;
  private TextView tvWheelBeginArray;
  private TextView tvWheelEndArray;
  private String wheelRadius[];

  // Pod Sum

  private SwitchCompat swSumPay;
  private ImageView btnWarningPodSum;
  private LinearLayout llPodSum;
  private TextView tvSumPay;
  private EditText editSumPay;

  private GoogleApiClient mGoogleApiClient;

  private void showMessage(String caption, String message)
  {
    MessageDialog dlgWarning = new MessageDialog();
    dlgWarning.setParams(caption, message, false, null);
    dlgWarning.show(getFragmentManager(), "Warning");
  }

  private boolean checkPodSum(boolean result)
  {
    if (swSumPay.isChecked()) {
      try {
        if (!edCosts.isFocused())
        {
          if (parseInt(mailCallCurrier.getCost()) < parseInt(mailCallCurrier.getPodSum())) {
            edCosts.removeTextChangedListener(costTextChangeListener);
            mailCallCurrier.setCostWithoutEvent(mailCallCurrier.getPodSum());
            edCosts.setText(mailCallCurrier.getPodSum());
            edCosts.addTextChangedListener(costTextChangeListener);
            btnWarningPodSum.setVisibility(View.VISIBLE);
            result = false;
          }
        }
      } catch (Exception e) {
        btnWarningPodSum.setVisibility(View.VISIBLE);
        result = false;
      }
    }
    return result;
  }

  private boolean checkCost(boolean result)
  {
    if (swSumPay.isChecked()) {
      try {
        if (!editSumPay.isFocused())
        {
          if (parseInt(mailCallCurrier.getCost()) < parseInt(mailCallCurrier.getPodSum())) {
            editSumPay.removeTextChangedListener(podSumTextChangeListener);
            mailCallCurrier.setPodSumWithoutEvent(mailCallCurrier.getCost());
            editSumPay.setText(mailCallCurrier.getCost());
            editSumPay.addTextChangedListener(costTextChangeListener);
            //btnWarningPodSum.setVisibility(View.VISIBLE);
            result = false;
          }
        }
      } catch (Exception e) {
        //btnWarningPodSum.setVisibility(View.VISIBLE);
        result = false;
      }
    }
    return result;
  }

  private boolean validateForm() {

    boolean result = true;

    if (editPhone.getText().toString().length()<17)
    {
      tvClientPhoneHint.setTextColor(getResources().getColor(R.color.error_massage));
      result = false;
    }

//    if (mailCallCurrier.getCargoType().compareTo("cargo")==0) {
//      // Объем
//      if (mailCallCurrier.getLength().length() == 0) {
//        edVolumeLength.setHintTextColor(GlobalApplicationData.getInstance().getContext().getResources().getColor(R.color.error_massage));
//        result = false;
//      }
//
//      if (mailCallCurrier.getWidth().length() == 0) {
//        edVolumeWidth.setHintTextColor(GlobalApplicationData.getInstance().getContext().getResources().getColor(R.color.error_massage));
//        result = false;
//      }
//
//      if (mailCallCurrier.getHeight().length() == 0) {
//        edVolumeHeight.setHintTextColor(GlobalApplicationData.getInstance().getContext().getResources().getColor(R.color.error_massage));
//        result = false;
//      }
//    }

//    result = checkPodSum(result);
//    result = checkCost(result);

//    if (editFullName.getText().toString().length()<3)
//    {
//      tvClientFullNameHint.setTextColor(getResources().getColor(R.color.error_massage));
//      result = false;
//    }

//    if (editCity.getText().toString().length()==0) {
//      editCity.setHintTextColor(getResources().getColor(R.color.error_massage));
//      result = false;
//    }
//
//    if (editStreet.getText().toString().length()==0) {
//      editStreet.setHintTextColor(getResources().getColor(R.color.error_massage));
//      result = false;
//    }
//
//    if (editHouse.getText().toString().length()==0) {
//      editHouse.setHintTextColor(getResources().getColor(R.color.error_massage));
//      result = false;
//    }
//
//    if (swLiftFromStage.isChecked()) {
//      if (editFloor.getText().toString().length() == 0) {
//        editFloor.setHintTextColor(getResources().getColor(R.color.error_massage));
//        result = false;
//      }
//    }

//    if (editRoom.getText().toString().length()==0) {
//      editRoom.setHintTextColor(getResources().getColor(R.color.error_massage));
//      result = false;
//    }
    return result;
  }

  private void setPhoneText(final String phone)
  {
    editPhone.setFilters(new InputFilter[]{});
    editPhone.setText(phone);
    editPhone.setFilters(new InputFilter[]{maskedInputFilter});
  }

  private void clearForm()
  {
    editCity.setText("");
    editFullName.setText("");
    editStreet.setText("");
    editHouse.setText("");
    editFloor.setText("");
    editRoom.setText("");
    edCosts.setText("");
    edVolumeWidth.setText("");
    edVolumeHeight.setText("");
    edVolumeLength.setText("");
    setPhoneText("+38 0");
    editComment.setText("");
    mailCallCurrier.setSended(false);
  }

  private String getCurrentDate(final Calendar c)
  {
    mMonth = c.get(Calendar.MONTH) + 1;
    mDay = c.get(Calendar.DATE);
    mYear = c.get(Calendar.YEAR);

    String months[] = getResources().getStringArray(R.array.months);

    SimpleDateFormat sdf = new SimpleDateFormat(
      getResources().getString(R.string.text_to_day) +
        ", d MMMM", new MyDateFormatSymbols(getContext()));

    SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
    timeFormatted = sdf1.format(c.getTime());
    return sdf.format(c.getTime());
  }

  private void initDateOfsend()
  {
    if (mailCallCurrier.getDateSend().length()==0)
      edDateOfSend.setText(getCurrentDate(Calendar.getInstance()));
    edDateOfSend.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
          dateSelectListener,
          now.get(Calendar.YEAR),
          now.get(Calendar.MONTH),
          now.get(Calendar.DAY_OF_MONTH)
        );
        MaterialDatePickerUtils.setSundays(dpd);
        MaterialDatePickerUtils.setHolidays(dpd);
        dpd.setMinDate(now);
        dpd.setAccentColor(getResources().getColor(R.color.action_bar_color));
        dpd.show(getActivity().getFragmentManager(), "CurrierDatepickerdialog");
      }
    });
  }

  @Override
  public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

    final String humanStringFormat = "d MMMM, EEEE";
    final String systemStringFormat = "yyyy-MM-dd";
    mDay = dayOfMonth;
    mMonth = monthOfYear;
    mYear = year;

    Calendar cal1 = Calendar.getInstance(Locale.getDefault());
    cal1.set(Calendar.DATE, mDay);
    cal1.set(Calendar.MONTH, mMonth);
    cal1.set(Calendar.YEAR, mYear);
    SimpleDateFormat sdf = new SimpleDateFormat(humanStringFormat,
      new MyDateFormatSymbols(getContext()));
    edDateOfSend.setText(sdf.format(cal1.getTime()));
    SimpleDateFormat sdf1 = new SimpleDateFormat(systemStringFormat,
      new MyDateFormatSymbols(getContext()));
    timeFormatted = sdf1.format(cal1.getTime());
    edDateOfSend.clearFocus();
  }

  private void initAddressCity()
  {
    editCity.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent intent = new Intent(context, SelectCityActivity.class);
        intent.putExtra("City", "Sender");
        intent.putExtra("Currier", true);
        startActivityForResult(intent, 2000);
      }
    });
    editStreet.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (editCity.getText().toString().length()>0) {
          Intent intent = new Intent(context, SelectStreetActivity.class);
          intent.putExtra("CityName", editCity.getText().toString());
          startActivityForResult(intent, 2001);
        }
        else
          editCity.setHintTextColor(getResources().getColor(R.color.error_massage));
      }
    });
    editHouse.addTextChangedListener(new TextChangeListener(context, editHouse, tvHouse, null));
    editFloor.addTextChangedListener(new TextChangeListener(context, editFloor, tvFloor, null));
    editRoom.addTextChangedListener(new TextChangeListener(context, editRoom, tvRoom, null));
  }

  public void callCurrierDataUpdated()
  {
    if (validateForm())
      btnCallCurrier.setVisibility(View.VISIBLE);
    else
      btnCallCurrier.setVisibility(View.GONE);
  }

  private void initPhoneAndName()
  {
    if (GlobalApplicationData.getInstance().getUserInfo().getName().length()>0)
    {
      String ph1 = MyStringUtils.getFormattedPhone(GlobalApplicationData.getInstance().getUserInfo().getPhoneNumbers());
      editPhone.setText(ph1);
      editFullName.setText(GlobalApplicationData.getInstance().getUserInfo().getSurname() + " " +
        GlobalApplicationData.getInstance().getUserInfo().getName() + " " +
        GlobalApplicationData.getInstance().getUserInfo().getPatronymic()
      );
      btnCallCurrier.setVisibility(View.VISIBLE);
    }
    else
      editPhone.setText("+38 0");
    editPhone.addTextChangedListener(new TextChangeListener(getActivity(), null, tvClientPhoneHint, null));
    editPhone.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {

      }

      @Override
      public void afterTextChanged(Editable s) {
        //myAfterTextChanged(this, s.toString());
        mailCallCurrier.setPhone(editPhone.getText().toString());
      }
    });
    maskedInputFilter = new MaskedInputFilter("+38 0## ### ## ##", editPhone, '#');
    editPhone.setFilters(new InputFilter[]{maskedInputFilter});
    editFullName.addTextChangedListener(new TextChangeListener(getActivity(), null, tvClientFullNameHint, null));
    editFullName.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {

      }

      @Override
      public void afterTextChanged(Editable s) {
        mailCallCurrier.setFullName(editFullName.getText().toString());
      }
    });
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);

    if (data==null) return;
    if (resultCode == RESULT_OK) {
      if (requestCode==2000) {
        String city = data.getStringExtra("City");
        if (city.compareTo("Sender") == 0) {
          editCity.setText(data.getStringExtra("Name"));
          mailCallCurrier.setCurrierFromCity(data.getStringExtra("Name"));
          mailCallCurrier.setCityCode(data.getStringExtra("Code"));
          editStreet.setText(data.getStringExtra("Street"));
          editHouse.setText(data.getStringExtra("StreetNumber"));
          editCity.setHintTextColor(getResources().getColor(R.color.paper_caption));
          if (editCity.getText().toString().length()>0)
            tvCity.setVisibility(View.VISIBLE);
          else
            tvCity.setVisibility(View.GONE);
        }
      }
      if (requestCode==2001) {
          mailCallCurrier.setCurrierFromStreet(data.getStringExtra("StreetName"));
          editStreet.setText(mailCallCurrier.getCurrierFromStreet());
          editStreet.setHintTextColor(getResources().getColor(R.color.paper_caption));
          if (editStreet.getText().toString().length()>0)
            tvStreet.setVisibility(View.VISIBLE);
          else
            tvStreet.setVisibility(View.GONE);
      }
    }
  }

  private void convertDate(final String strDate)
  {
    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
    try {
      Date date = format.parse(strDate);
      Calendar calendar = Calendar.getInstance();
      calendar.setTime(date);
      edDateOfSend.setText(getCurrentDate(calendar));
    } catch (ParseException e) {

    }
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_currier, container, false);
    getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
    toolbar = (Toolbar) view.findViewById(R.id.toolbarCurrier);
//    toolbar.setTitle("");
    initGooglePlaces();
    return view;
  }

  public void setDataFromCalc(DataFromCalc datafromCalc) {
      this.dataFromCalc = datafromCalc;
  }

  private void setCargoTypeAdapter()
  {

    ArrayList<Map<String, Object>> cargoTypeList = new ArrayList<Map<String, Object>>();
    String[] cargoText = GlobalApplicationData.getInstance().getContext().getResources().getStringArray(R.array.cargo_types);

    Map<String, Object> m;
    for (int i = 0; i < cargoText.length; i++) {
      m = new HashMap<String, Object>();
      m.put("Image", Integer.toString(cargoIcons[i]));
      m.put("Text", cargoText[i]);
      cargoTypeList.add(m);
    }

    String[] from = { "Image", "Text" };
    int[] to = { R.id.imageCargoIcon, R.id.tvcCargoType};

    cargoTypeAdapter = new SimpleAdapter(getContext(), cargoTypeList,
      R.layout.single_choise_item_def, from, to)
    {
      @NonNull
      @Override
      public View getView(int position, View convertView, ViewGroup parent) {
        final View view = super.getView(position, convertView, parent);
        ImageView cargoIcon = (ImageView) view.findViewById(R.id.imageCargoIcon);
        LinearLayout llSelectedItem = (LinearLayout) view.findViewById(R.id.llSingleChoiseItem);
        TextView tvDescription = (TextView) view.findViewById(R.id.tvcCargoType);
        if (position == cargoTypeSelected) {
          llSelectedItem.setBackgroundColor(GlobalApplicationData.getInstance().getContext().getResources().getColor(R.color.background_white));
          cargoIcon.setImageResource(cargoIconsSelected[position]);
          tvDescription.setTextColor(GlobalApplicationData.getInstance().getContext().getResources().getColor(R.color.base_text_color));
        }
        else {
          llSelectedItem.setBackgroundColor(GlobalApplicationData.getInstance().getContext().getResources().getColor(R.color.app_background));
          cargoIcon.setImageResource(cargoIcons[position]);
          tvDescription.setTextColor(GlobalApplicationData.getInstance().getContext().getResources().getColor(R.color.paper_caption));
        }
        return view;
      }
    };

    lvCargoType.setAdapter(cargoTypeAdapter);
  }

  private void resetPrevLayout(LinearLayout v)
  {
    if (v!=null) {
      TextView tvWeightValue = (TextView) v.findViewById(R.id.tvWeightValue);
      tvWeightValue.setTypeface(null, Typeface.NORMAL);
      tvWeightValue.setTextColor(GlobalApplicationData.getInstance().getContext().getResources().getColor(R.color.text_hint));
      tvWeightValue = (TextView) v.findViewById(R.id.tvTopWeight);
      tvWeightValue.setVisibility(View.INVISIBLE);
      tvWeightValue = (TextView) v.findViewById(R.id.tvWeightUnit);
      tvWeightValue.setVisibility(View.INVISIBLE);
    }
  }

  private String[] getPaletteWeights(String[] weights, int lastCount)
  {
    String[] dest = new String[lastCount];
    System.arraycopy(weights, 3-lastCount, dest, 0, lastCount);
    return dest;
  }

  private View findChildByTag(LinearLayout parent, int selectedWeight)
  {
    for (int i=0; i<=parent.getChildCount()-1; i++)
    {
      if ((int)parent.getChildAt(i).getTag() == selectedWeight)
        return parent.getChildAt(i);
    }
    return null;
  }

  private void selectWeightView(View v)
  {
    resetPrevLayout(prevWeightLayout);
    prevWeightLayout = (LinearLayout)v;
    ViewGroup.LayoutParams params1 = weightFrame.getLayoutParams();

    params1.width = v.getWidth();
    weightFrame.setLayoutParams(params1);
    weightFrame.animate().x(v.getX()).setDuration(500).start();

    selectedWeight = (int) v.getTag();
    mailCallCurrier.setWeight(weights[selectedWeight]);
    TextView tvWeightValue = (TextView) v.findViewById(R.id.tvWeightValue);
    tvWeightValue.setTypeface(null, Typeface.BOLD);
    tvWeightValue.setTextColor(GlobalApplicationData.getInstance().getContext().getResources().getColor(R.color.base_text_color));
    tvWeightValue = (TextView) v.findViewById(R.id.tvTopWeight);
    tvWeightValue.setVisibility(View.VISIBLE);
    tvWeightValue = (TextView) v.findViewById(R.id.tvWeightUnit);
    tvWeightValue.setVisibility(View.VISIBLE);
  }

  private LinearLayout createWightsList(Context context, LinearLayout parent, int cargo, int palletteLastCount)
  {
    parent.removeAllViewsInLayout();
    LinearLayout firstItem = null;
    LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    switch (cargo) {
      case 0: weights = GlobalApplicationData.getInstance().getContext().getResources().getStringArray(R.array.wight_values); break;
      case 1: weights = GlobalApplicationData.getInstance().getContext().getResources().getStringArray(R.array.doc_weights); break;
      case 2: {
        weights = GlobalApplicationData.getInstance().getContext().getResources().getStringArray(R.array.palete_weights);
        weights = getPaletteWeights(weights, palletteLastCount);
        break;
      }
    }


    for (int i = 0; i<weights.length; i++) {
      LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.list_weight_item, null);
      if (i==0) {
        prevWeightLayout = layout;
        firstItem = layout;
        selectedWeight = i;

        TextView tvWeightValue = (TextView) layout.findViewById(R.id.tvWeightValue);
        tvWeightValue.setTypeface(null, Typeface.BOLD);
        tvWeightValue.setTextColor(GlobalApplicationData.getInstance().getContext().getResources().getColor(R.color.base_text_color));
        tvWeightValue = (TextView) layout.findViewById(R.id.tvTopWeight);
        tvWeightValue.setVisibility(View.VISIBLE);
        tvWeightValue = (TextView) layout.findViewById(R.id.tvWeightUnit);
        tvWeightValue.setVisibility(View.VISIBLE);

      }
      layout.setTag(i);
      layout.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          selectWeightView(v);
        }
      });
      TextView tvValue = (TextView) layout.findViewById(R.id.tvWeightValue);
      tvValue.setText(weights[i]);
      LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
        LinearLayout.LayoutParams.WRAP_CONTENT,
        LinearLayout.LayoutParams.MATCH_PARENT
      );
      params.weight = 1;

      parent.addView(layout, params);

    }
    return firstItem;
  }

  private void initWheelsRadius(boolean checked)
  {
    if (checked) {
      mailCallCurrier.setCargoSubType("wheels_car");
      wheelRadius = GlobalApplicationData.getInstance().getContext().getResources().getStringArray(R.array.wheels_light);
    }
    else {
      mailCallCurrier.setCargoSubType("wheels_truck");
      wheelRadius = GlobalApplicationData.getInstance().getContext().getResources().getStringArray(R.array.wheels_cargo);
    }
    tvWheelBeginArray.setText(wheelRadius[0]);
    tvWheelEndArray.setText(wheelRadius[wheelRadius.length-1]);
    sbRadius.setMax(wheelRadius.length-1);
    sbRadius.setProgress(0);
    mailCallCurrier.setRadius(wheelRadius[0]);
  }

  private void initPalleteTypes()
  {
    listener = new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
          mailCallCurrier.setTypePallets(buttonView.getText().toString());
          if (buttonView.getText().toString().compareTo("1200x800")==0)
          {
            firstWeightItem = createWightsList(getContext(), llWeights, 2, 3);
            mailCallCurrier.setWeight("500");
          }
          if (buttonView.getText().toString().compareTo("1200x1000")==0)
          {
            firstWeightItem = createWightsList(getContext(), llWeights, 2, 2);
            mailCallCurrier.setWeight("750");
          }
          if (buttonView.getText().toString().compareTo("1200x1200")==0)
          {
            firstWeightItem = createWightsList(getContext(), llWeights, 2, 1);
            mailCallCurrier.setWeight("1000");
          }
          llWeights.setVisibility(View.VISIBLE);
        }
      }
    };
    rbPalete1.setOnCheckedChangeListener(listener);
    rbPalete2.setOnCheckedChangeListener(listener);
    rbPalete3.setOnCheckedChangeListener(listener);
  }

  private void setSubtypeViews(int cargoType)
  {
    switch (cargoType)
    {
      case 0: // Cargo
        rgPaleteType.setVisibility(View.GONE);
        rlPackageCount.setVisibility(View.GONE);
        llSeekBarWeights.setVisibility(View.VISIBLE);
        llWeights.setVisibility(View.GONE);
        llVolumePack.setVisibility(View.VISIBLE);
        tvSizeCaption.setVisibility(View.VISIBLE);
        tvWeightCaption.setVisibility(View.VISIBLE);
        llWheels.setVisibility(View.GONE);
        mailCallCurrier.setWeight("0.1");
        break;
      case 1:
        rgPaleteType.setVisibility(View.GONE);
        llSeekBarWeights.setVisibility(View.GONE);
        llWeights.setVisibility(View.VISIBLE);
        rlPackageCount.setVisibility(View.GONE);
        llVolumePack.setVisibility(View.GONE);
        tvSizeCaption.setVisibility(View.GONE);
        tvWeightCaption.setVisibility(View.VISIBLE);
        llWheels.setVisibility(View.GONE);
        firstWeightItem = createWightsList(getContext(), llWeights, 1, 0);
        mailCallCurrier.setWeight("0.1");
        break;
      case 2:
        rgPaleteType.setVisibility(View.VISIBLE);
        llWeights.setVisibility(View.GONE);
        llSeekBarWeights.setVisibility(View.GONE);
        rlPackageCount.setVisibility(View.VISIBLE);
        llVolumePack.setVisibility(View.GONE);
        tvSizeCaption.setVisibility(View.GONE);
        tvWeightCaption.setVisibility(View.GONE);
        llWheels.setVisibility(View.GONE);
        rbPalete2.setChecked(true);
        mailCallCurrier.setCargoSubType(GlobalApplicationData.getInstance().getContext().getResources().getString(R.string.pallete2));
        mailCallCurrier.setWeight("500");
        break;
      case 3:
        rgPaleteType.setVisibility(View.GONE);
        rlPackageCount.setVisibility(View.VISIBLE);
        llVolumePack.setVisibility(View.GONE);
        llWeights.setVisibility(View.GONE);
        llSeekBarWeights.setVisibility(View.GONE);
        tvSizeCaption.setVisibility(View.GONE);
        tvWeightCaption.setVisibility(View.GONE);
        llWheels.setVisibility(View.VISIBLE);
        rbWheelsLight.setChecked(true);
        mailCallCurrier.setWeight("0.1");
        initWheelsRadius(true);
        break;
      default:
        rgPaleteType.setVisibility(View.GONE);
        llWheels.setVisibility(View.GONE);
        break;
    }
  }

  private void selectCargoType(int position)
  {
    cargoTypeSelected = position;
    switch (cargoTypeSelected)
    {
      case 0: mailCallCurrier.setCargoType("cargo"); break;
      case 1: mailCallCurrier.setCargoType("doc"); break;
      case 2: mailCallCurrier.setCargoType("pallet"); break;
      default: mailCallCurrier.setCargoType("cargo");
    }
    cargoTypeAdapter.notifyDataSetChanged();
    setSubtypeViews(cargoTypeSelected);
    //initPackageList();
  }

  private void initCargoType()
  {
    lvCargoType.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
    lvCargoType.setItemsCanFocus(true);
    mailCallCurrier.setCargoType("cargo");
    setCargoTypeAdapter();
    lvCargoType.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        selectCargoType(position);
      }
    });
    //initPackageList();
  }

  private void initWheels()
  {
    sbRadius.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
      @Override
      public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        tvSelectedRadius.setText(wheelRadius[progress]);
        mailCallCurrier.setRadius(wheelRadius[progress]);
      }

      @Override
      public void onStartTrackingTouch(SeekBar seekBar) {

      }

      @Override
      public void onStopTrackingTouch(SeekBar seekBar) {
      }
    });
    rbWheelsLight.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        initWheelsRadius(isChecked);
      }
    });
  }

  private void initCost()
  {
    btnWarningCost.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        showHint(GlobalApplicationData.getInstance().getContext().getResources().getString(R.string.dialog_message_min_costs), layoutCost.getBottom());
      }
    });

    edCosts.addTextChangedListener(new TextChangeListener(getActivity(), null, null, btnWarningCost));
    costTextChangeListener = new CurrierTextChangeListener("cost");
    edCosts.addTextChangedListener(costTextChangeListener);
    edCosts.setOnFocusChangeListener(new View.OnFocusChangeListener() {

      @Override
      public void onFocusChange(View v, boolean hasFocus) {
        if(!hasFocus){
          checkPodSum(false);
        }
      }
    });
  }

  private void setFocusToVolumeEdit(final EditText edit)
  {
    edit.requestFocus();
    edit.setSelection(edit.length());
    InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
    imm.showSoftInput(edit, InputMethodManager.SHOW_IMPLICIT);
  }

  private void initVolume()
  {
    frEditHeight.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        setFocusToVolumeEdit(edVolumeHeight);
      }
    });

    frEditWidth.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        setFocusToVolumeEdit(edVolumeWidth);
      }
    });

    frEditLength.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        setFocusToVolumeEdit(edVolumeLength);
      }
    });

    edVolumeLength.addTextChangedListener(new VolumeTextChangeListener(getActivity(), edVolumeLength, tvVolumeLength));
    edVolumeLength.addTextChangedListener(new CurrierTextChangeListener("length"));
    edVolumeHeight.addTextChangedListener(new VolumeTextChangeListener(getActivity(), edVolumeHeight, tvVolumeHeight));
    edVolumeHeight.addTextChangedListener(new CurrierTextChangeListener("height"));
    edVolumeWidth.addTextChangedListener(new VolumeTextChangeListener(getActivity(), edVolumeWidth, tvVolumeWidth));
    edVolumeWidth.addTextChangedListener(new CurrierTextChangeListener("width"));
  }

  private void setWeightFramePos(final LinearLayout firstItem)
  {
    if (firstItem!=null) {
      ViewGroup.LayoutParams params1 = weightFrame.getLayoutParams();
      params1.width = firstItem.getWidth();
      weightFrame.setLayoutParams(params1);
      weightFrame.setVisibility(View.VISIBLE);
      weightFrame.animate().x(firstItem.getX()).setDuration(500).start();
    }
  }

  private void showHint(String message, float buttom)
  {
    tvWarningDesc.setText(message);
    frLayoutHint.setTranslationY(buttom);
    frLayoutHint.setVisibility(View.VISIBLE);
  }

  private void initHintFrame()
  {
    frLayoutHint.setVisibility(View.GONE);
    btnOnHintOk.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        frLayoutHint.setVisibility(View.GONE);
      }
    });
  }

  private void swapPodPay(boolean checked)
  {
    if (checked)
    {
      tvSumPay.setVisibility(View.GONE);
      editSumPay.setVisibility(View.VISIBLE);
      editSumPay.requestFocus();
    }
    else {
      tvSumPay.setVisibility(View.VISIBLE);
      editSumPay.setVisibility(View.GONE);
      mailCallCurrier.setPodSum("0");
    }
  }

  private void initAdditionalServices()
  {
    swSumPay.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        swapPodPay(isChecked);
      }
    });
    btnWarningPodSum.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        showHint(GlobalApplicationData.getInstance().getContext().getResources().getString(R.string.hint_pod_sum_error), llPodSum.getBottom());
      }
    });
    editSumPay.addTextChangedListener(new TextChangeListener(getActivity(), null, null, btnWarningPodSum));
    podSumTextChangeListener = new CalcTextChangeListener("podSum");
    editSumPay.addTextChangedListener(podSumTextChangeListener);
  }

  private void initPackageNumber()
  {
    btnPackageInc.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        String packageCount = Integer.toString(++samplePackages);
        tvPackagesNumber.setText(packageCount);
        mailCallCurrier.setCount(packageCount);
        btnPackageDec.setBackgroundResource(R.drawable.button_normal);
        btnPackageDec.setTextColor(GlobalApplicationData.getInstance().getContext().getResources().getColor(R.color.additional_action));
      }
    });
    btnPackageDec.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (--samplePackages<=1) {
          samplePackages = 1;
          btnPackageDec.setBackgroundResource(R.drawable.button_dec);
          btnPackageDec.setTextColor(GlobalApplicationData.getInstance().getContext().getResources().getColor(R.color.single_select_list_border));
        }
        String packageCount = Integer.toString(samplePackages);
        tvPackagesNumber.setText(packageCount);
        mailCallCurrier.setCount(packageCount);
      }
    });
    tvPackagesNumber.setText(Integer.toString(samplePackages));
  }

  @Override
  public void onActivityCreated(@Nullable Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
    ((MainActivity)getActivity()).setToolbar(toolbar);
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    mActivity = this;
    //((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
    sbWeight = (WeightSeekBar) view.findViewById(R.id.sbWeight);
    tvWeight = (EditText) view.findViewById(R.id.tvWeight);
    tvWeightDescritption = (TextView) view.findViewById(R.id.tvWeightDescription);
    frBoxWeight = (FrameLayout) view.findViewById(R.id.frBoxWeight);
    imgHumanWeight = (ImageView) view.findViewById(R.id.imgHumanWeight);
    sbWeight.setParams(tvWeight, imgHumanWeight, tvWeightDescritption, frBoxWeight, null);
    sbWeight.setMax(1000);

    weightFrame = (FrameLayout) view.findViewById(R.id.frameRectangleWieght);
    llWeights = (LinearLayout) view.findViewById(R.id.llWeights);
    llSeekBarWeights = (LinearLayout) view.findViewById(R.id.llSeekBarWeights);
    tvWeightCaption = (TextView) view.findViewById(R.id.tvWeightCaption);
    tvSizeCaption = (TextView) view.findViewById(R.id.tvSizeCaption);
    llVolumePack = (LinearLayout) view.findViewById(R.id.llVolumePack);

    mailCallCurrier = MailCallCurrier.getInstance();
    mailCallCurrier.setDateSend("");

    btnCallCurrier = (Button) view.findViewById(R.id.btnCallCurrier);
    btnCallCurrier.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (validateForm()) {
          if (GlobalApplicationData.getInstance().confidentialityReaded==true) {
            fillData();
            startActivity(new Intent(getContext(), CallCurrierActivity.class));
          }
          else
            startActivity(new Intent(getContext(), ConfidentialityActivity.class));
        }
        else
        {
          showMessage(getResources().getString(R.string.dialog_warning_caption),
            getResources().getString(R.string.dialog_message_fill_all_fields));
        }
      }
    });


    // ----
    editCity = (TextView) view.findViewById(R.id.editAddressCity);
    editStreet = (TextView) view.findViewById(R.id.editAddressStreet);
    editHouse = (EditText) view.findViewById(R.id.editAddressHouse);
    editFloor = (EditText) view.findViewById(R.id.editAddressFloor);
    editRoom = (EditText) view.findViewById(R.id.editAddressRoom);

    tvStreet = (TextView) view.findViewById(R.id.tvAddressStreet);
    tvHouse = (TextView) view.findViewById(R.id.tvAddressHouse);
    tvFloor = (TextView) view.findViewById(R.id.tvAddressFloor);
    tvRoom = (TextView) view.findViewById(R.id.tvAddressRoom);
    tvCity = (TextView) view.findViewById(R.id.tvAddressCity);

    editPhone = (EditText) view.findViewById(R.id.editClientPhone);
    editFullName = (EditText) view.findViewById(R.id.editClientFullName);

    tvClientFullNameHint = (TextView) view.findViewById(R.id.tvClientFullNameHint);
    tvClientPhoneHint = (TextView) view.findViewById(R.id.tvClientPhoneHint);

    edDateOfSend = (TextView) view.findViewById(R.id.editCurrierDateOfSend);

    lvCargoType = (MyListView) view.findViewById(R.id.lvCargoType);

    rgPaleteType = (RadioGroup) view.findViewById(R.id.rgPaleteType);
    rbPalete1 = (RadioButton) view.findViewById(R.id.rbPalete1);
    rbPalete2 = (RadioButton) view.findViewById(R.id.rbPalete2);
    rbPalete3 = (RadioButton) view.findViewById(R.id.rbPalete3);

    llWheels = (LinearLayout) view.findViewById(R.id.llWheels);
    sbRadius = (SeekBar) view.findViewById(R.id.sbRadius);

    rbWheelsLight = (RadioButton) view.findViewById(R.id.rbWheelsLight);
    rbWheelsTruck = (RadioButton) view.findViewById(R.id.rbWheelsTruck);
    tvSelectedRadius = (TextView) view.findViewById(R.id.tvSelectedRadius);
    tvWheelBeginArray = (TextView) view.findViewById(R.id.tvWheelsBeginArray);
    tvWheelEndArray = (TextView) view.findViewById(R.id.tvWheelsEndArray);

    rlPackageCount = (RelativeLayout) view.findViewById(R.id.rlPackageCount);

    layoutCost = (LinearLayout) view.findViewById(R.id.llCost);
    btnWarningCost = (ImageView) view.findViewById(R.id.imgWarningCost);
    edCosts = (EditText) view.findViewById(R.id.editccCargoCost);

    edVolumeWidth = (EditText) view.findViewById(R.id.editVolumeWidth);
    edVolumeHeight = (EditText) view.findViewById(R.id.editVolumeHeight);
    edVolumeLength = (EditText) view.findViewById(R.id.editVolumeLength);
    tvVolumeWidth = (TextView) view.findViewById(R.id.tvVolumeWidth);
    tvVolumeLength = (TextView) view.findViewById(R.id.tvVolumeLength);
    tvVolumeHeight = (TextView) view.findViewById(R.id.tvVolumeHeight);
    frEditHeight = (FrameLayout) view.findViewById(R.id.frEditHeight);
    frEditWidth = (FrameLayout) view.findViewById(R.id.frEditWeight);
    frEditLength = (FrameLayout) view.findViewById(R.id.frEditLength);

    frLayoutHint = (FrameLayout) view.findViewById(R.id.frWarningHint);
    tvWarningDesc = (TextView) view.findViewById(R.id.tvWarningDesc);

    btnOnHintOk = (TextView) view.findViewById(R.id.btnOnHint);

    btnPackageInc = (Button) view.findViewById(R.id.btnPackageInc);
    btnPackageDec = (Button) view.findViewById(R.id.btnPackageDec);
    tvPackagesNumber = (TextView) view.findViewById(R.id.tvPackagesNumber);

    llPodSum = (LinearLayout) view.findViewById(R.id.llPodSum);
    swSumPay = (SwitchCompat) view.findViewById(R.id.swSumPay);

    tvSumPay = (TextView) view.findViewById(R.id.tvPodPaySum);
    btnWarningPodSum = (ImageView) view.findViewById(R.id.imgWarningPosSum);

    editSumPay = (EditText) view.findViewById(R.id.editPodPaySum);

    view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
      @Override
      public void onGlobalLayout() {
        setWeightFramePos(firstWeightItem);
        firstWeightItem = null;
      }
    });

    swLiftFromStage = (SwitchCompat) view.findViewById(R.id.swLiftFromStage);
    editComment = (EditText) view.findViewById(R.id.editComment);
    startLocation();

    initAddressCity();
    initPhoneAndName();
    initDateOfsend();
    initCargoType();
    initPalleteTypes();
    initWheels();

    initCost();
    initVolume();
    initHintFrame();
    initPackageNumber();

    initAdditionalServices();
    setSubtypeViews(cargoTypeSelected);

    swLiftFromStage.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (!isChecked)
          editFloor.setHintTextColor(getResources().getColor(R.color.paper_caption));
      }
    });

    mailCallCurrier.setView(this);
    if (dataFromCalc!=null)
    {
      mailCallCurrier.setCityCode(dataFromCalc.getCityCode());
      mailCallCurrier.setCurrierFromCity(dataFromCalc.getCityName());
      editCity.setText(mailCallCurrier.getCurrierFromCity());
      convertDate(dataFromCalc.getSystemDate());
      edDateOfSend.setText(dataFromCalc.getHumanDate());
      setupCargoType();
    }
  }

  private void selectWeightFrame(int weightTag)
  {
    try {
      final View v = findChildByTag(llWeights, weightTag);
      if (v != null) {
        llWeights.postDelayed(new Runnable() {
          @Override
          public void run() {
            selectWeightView(v);
          }
        }, 1000);
      }
    }
    catch(Exception e)
    {}
  }

  private void setupCargoType()
  {
    if (dataFromCalc.getCargoType().compareTo("cargo") == 0)
    {
      selectCargoType(0);
      if (dataFromCalc.getWidth().compareTo("1") != 0)
        edVolumeWidth.setText(dataFromCalc.getWidth());
      if (dataFromCalc.getHeight().compareTo("1") != 0)
        edVolumeHeight.setText(dataFromCalc.getHeight());
      if (dataFromCalc.getLength().compareTo("1") != 0)
        edVolumeLength.setText(dataFromCalc.getLength());
      sbWeight.setProgress(dataFromCalc.getSelectedWeight());
      tvWeightDescritption.setText(dataFromCalc.getWeightText());
      tvWeight.setText(dataFromCalc.getWeight());
    }
    if (dataFromCalc.getCargoType().compareTo("doc") == 0)
    {
      selectCargoType(1);
      selectWeightFrame(dataFromCalc.getSelectedWeight());
      mailCallCurrier.setWeight(dataFromCalc.getWeight());
    }
    if (dataFromCalc.getCargoType().compareTo("pallet") == 0)
    {
      selectCargoType(2);
      tvPackagesNumber.setText(Integer.toString(dataFromCalc.getCount()));
      mailCallCurrier.setCount(Integer.toString(dataFromCalc.getCount()));
      mailCallCurrier.setCargoSubType(dataFromCalc.getCargoSubType());
      mailCallCurrier.setWeight(dataFromCalc.getWeight());
      if (dataFromCalc.getCargoSubType().compareTo("1200*800") == 0)
        rbPalete1.setChecked(true);
      if (dataFromCalc.getCargoSubType().compareTo("1200*1000") == 0)
        rbPalete2.setChecked(true);
      if (dataFromCalc.getCargoSubType().compareTo("1200*1200") == 0)
        rbPalete3.setChecked(true);
      selectWeightFrame(dataFromCalc.getSelectedWeight());
    }
    if (dataFromCalc.getCargoType().startsWith("wheels_"))
    {
      selectCargoType(3);
      if (dataFromCalc.getCargoType().compareTo("wheels_car") == 0)
        rbWheelsLight.setChecked(true);
      else
        rbWheelsTruck.setChecked(true);
      sbRadius.setProgress(dataFromCalc.getRadiusPosition());
    }
    if (dataFromCalc.getPodSum() != null && dataFromCalc.getPodSum().length() > 0 && dataFromCalc.getPodSum().compareTo("0") != 0) {
      swSumPay.setChecked(true);
      editSumPay.setText(dataFromCalc.getPodSum());
    }
    if (dataFromCalc.getCost() != null && dataFromCalc.getCost().length() > 0 && dataFromCalc.getCost().compareTo("0") != 0)
    {
      edCosts.setText(dataFromCalc.getCost());
    }
  }

  @Override
  public void onDetach() {
    super.onDetach();
    if (mGoogleApiClient != null) {
      mGoogleApiClient.stopAutoManage(getActivity());
      mGoogleApiClient.disconnect();
    }
  }

  private void initGooglePlaces()
  {
    mGoogleApiClient = new GoogleApiClient
      .Builder(context)
      .addApi(Places.GEO_DATA_API)
      .addApi(Places.PLACE_DETECTION_API)
      .enableAutoManage(getActivity(), this)
      .build();
  }

  @Override
  public void onStart() {
    super.onStart();
    mGoogleApiClient.connect();
  }

  @Override
  public void onStop() {
    super.onStop();
    mGoogleApiClient.disconnect();
  }

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    this.context = context;
    this.dateSelectListener = this;
  }

  @Override
  public void onDestroyView() {
    super.onDestroyView();
    ((MainActivity)getActivity()).setToolbar(null);
  }

  @Override
  public void onResume() {
    super.onResume();
    if (MyDateUtils.nowIsHoliday())
      btnCallCurrier.setVisibility(View.GONE);
    if (mailCallCurrier.isSended()) {
      clearForm();
    }
  }

  private void fillData()
  {
    mailCallCurrier.setCurrierFromStreet(editStreet.getText().toString());
    //mailCallCurrier.setCurrierFromCity(editCity.getText().toString());
    mailCallCurrier.setCurrierFromHouse(editHouse.getText().toString());
    mailCallCurrier.setCurrierFromFloor(editFloor.getText().toString());
    mailCallCurrier.setCurrierFromRoom(editRoom.getText().toString());
    //mailCallCurrier.setCityCode(cityCode);
    mailCallCurrier.setListToStage(swLiftFromStage.isChecked() ? "1" : "0");
    mailCallCurrier.setFullName(editFullName.getText().toString());
    mailCallCurrier.setDateSend(timeFormatted);
    mailCallCurrier.setHumanDate(edDateOfSend.getText().toString());
    mailCallCurrier.setComment(editComment.getText().toString());
    mailCallCurrier.setSended(false);
    mailCallCurrier.setPhone(editPhone.getText().toString());
  }

  @Override
  public void sendLocation(Location location) {
    Intent intent = new Intent(context, FetchAddressIntentService.class);
    intent.putExtra(Constants.RECEIVER, new AddressResultReceiver(new Handler(), location));
    intent.putExtra(Constants.LOCATION_DATA_EXTRA, location);
    context.startService(intent);
  }

  private void startLocation() {
    new LocationListenerServices(context, this);
  }

  @Override
  public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

  }

  class AddressResultReceiver extends ResultReceiver {

    private Location myLocation;

    public AddressResultReceiver(Handler handler, Location location)
    {
      super(handler);
      this.myLocation = location;
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
      if (resultCode == Constants.SUCCESS_RESULT) {
        //Log.d(TAG, "Address found!");
        SearchCityByGeo sGeo = new SearchCityByGeo(resultData.getString("City"), mGoogleApiClient, myLocation);
        sGeo.linkMain(mActivity);
        sGeo.execute();
      }
    }
  }

  private class SearchCityByGeo extends AsyncTask<Void, Void, Void> {

    private CurrierFragment currierFragment;
    private String city;
    private GoogleApiClient mGoogleApiClient;
    private Location myLocation;
    private String findedCityName;

    public SearchCityByGeo(String city, GoogleApiClient mGoogleApiClient, Location location)
    {
      this.city = city;
      this.mGoogleApiClient = mGoogleApiClient;
      this.myLocation = location;
    }

    public void linkMain(CurrierFragment activity)
    {
      currierFragment = activity;
    }

    public void unlink()
    {
      currierFragment = null;
    }

    private void searchSuitableCityByPlace(String cleanRequst)
    {
      ArrayList<CityInfoForCalc> cityInfo = new ArrayList<CityInfoForCalc>();
      double humanDist;
      ArrayList<AutocompletePrediction> filterData = new ArrayList<>();
      if (mGoogleApiClient != null && mGoogleApiClient.isConnected())
        filterData = new AutocompleteHelper(mGoogleApiClient).getAutocomplete("Украина " + cleanRequst);
      if (filterData != null && filterData.size()>0)
      {
        for (AutocompletePrediction aup : filterData)
        {
          PlaceBuffer places = null;
          if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            places = Places.GeoDataApi.getPlaceById(mGoogleApiClient, aup.getPlaceId()).await();
          }
          if (places != null && places.getStatus().isSuccess()){
            LatLng queriedLocation = places.get(0).getLatLng();
            if (myLocation != null) {
              double dist = DistanceCalculator.distance(queriedLocation.latitude,
                queriedLocation.longitude,
                myLocation.getLatitude(), myLocation.getLongitude());
              humanDist = dist / 1000;
              try {
                CityInfoForCalc ci = new CityInfoForCalc();
                ci.setCityName(aup.getPrimaryText(null).toString());
                String state = aup.getSecondaryText(null).toString();
                state = state.substring(0, state.indexOf(','));
                ci.setState(state);
                ci.setDisatnce(humanDist);
                cityInfo.add(ci);
              }
              catch(Exception e)
              {

              }
            }
          }
        } // for (AutocompletePrediction aup : filterData)

        if (cityInfo.size() > 0) {
          Cursor cursor;
          sortByDistance(cityInfo);
          if (GlobalApplicationData.getCurrentLanguage().compareTo("ua") == 0) {
            cursor = DictionariesDB.getInstance(context).tableCity.selectByCS_UA(cityInfo.get(0).getCityName(),
              cityInfo.get(0).getState());
          } else {
            cursor = DictionariesDB.getInstance(context).tableCity.selectByCS(cityInfo.get(0).getCityName(),
              cityInfo.get(0).getState());
          }
          cursor.moveToFirst();
          if (cursor.getCount() > 0) {
            findedCityName = getColumnValue(cursor, CityTable.Column.CITY);
            String cityCode = getColumnValue(cursor, CityTable.Column.CODE);
            mailCallCurrier.setCurrierFromCity(findedCityName);
            mailCallCurrier.setCityCode(cityCode);
          }
          cursor.close();
        }
      }
    }

    private void sortByDistance(ArrayList<CityInfoForCalc> list)
    {
      Collections.sort(list, new Comparator<CityInfoForCalc>() {
        public int compare(CityInfoForCalc o1, CityInfoForCalc o2) {
          double depNum1 = o1.getDisatnce();
          double depNum2 = o2.getDisatnce();
          if (depNum1 < depNum2)
            return -1;
          else
            return 1;
        }
      });
    }

    private boolean searchSuitableCityInWarehouseTable(/*final String city*/) {
      boolean result = false;
      double humanDist = 0;
      ArrayList<CityInfoForCalc> cityInfo = new ArrayList<CityInfoForCalc>();
      Cursor cursor;
      if (GlobalApplicationData.getCurrentLanguage().compareTo("ua") == 0)
        cursor = DictionariesDB.getInstance(context).tableWarehouseDetails.
          selectByDiffLocationUA(Double.toString(myLocation.getLongitude()), Double.toString(myLocation.getLatitude()));//selectByCityNameUA(city);
      else
        cursor = DictionariesDB.getInstance(context).tableWarehouseDetails.
          selectByDiffLocation(Double.toString(myLocation.getLongitude()), Double.toString(myLocation.getLatitude()));//selectByCityName(city);
      cursor.moveToFirst();
      while (!cursor.isAfterLast()) {
        if (myLocation != null) {
          double dist = DistanceCalculator.distance(Double.parseDouble(getColumnValue(cursor, WarehouseDetailsTable.Column.LAT)),
            Double.parseDouble(getColumnValue(cursor, WarehouseDetailsTable.Column.LNG)),
            myLocation.getLatitude(), myLocation.getLongitude());
          humanDist = dist / 1000;
          CityInfoForCalc ci = new CityInfoForCalc();
          ci.setArea(getColumnValue(cursor, WarehouseDetailsTable.Column.AREA));
          ci.setCityName(getColumnValue(cursor, WarehouseDetailsTable.Column.CITY));
          ci.setState(getColumnValue(cursor, WarehouseDetailsTable.Column.STATE));
          ci.setDisatnce(humanDist);
          cityInfo.add(ci);
        }
        cursor.moveToNext();
      }
      cursor.close();
      if (cityInfo.size() > 0) {
        sortByDistance(cityInfo);
        if (GlobalApplicationData.getCurrentLanguage().compareTo("ua") == 0) {
          cursor = DictionariesDB.getInstance(context).tableCity.selectByCSA_UA(cityInfo.get(0).getCityName(),
            cityInfo.get(0).getState(), cityInfo.get(0).getArea());
        } else {
          cursor = DictionariesDB.getInstance(context).tableCity.selectByCSA(cityInfo.get(0).getCityName(),
            cityInfo.get(0).getState(), cityInfo.get(0).getArea());
        }
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
          findedCityName = getColumnValue(cursor, CityTable.Column.CITY);
          String cityCode = getColumnValue(cursor, CityTable.Column.CODE);
          mailCallCurrier.setCurrierFromCity(findedCityName);
          mailCallCurrier.setCityCode(cityCode);
          result = true;
        }
        cursor.close();
      }
      return result;
    }

    @Override
    protected Void doInBackground(Void... voids) {
      if (searchSuitableCityInWarehouseTable(/*city*/) == false)
        searchSuitableCityByPlace(city);
      return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
      super.onPostExecute(aVoid);
      if (currierFragment != null && findedCityName != null && findedCityName.length() > 0)
        currierFragment.editCity.setText(findedCityName);
    }
  }

}
