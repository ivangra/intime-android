package com.ua.inr0001.intime;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.webkit.WebView;

import intime.llc.ua.R;

public class AboutActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_about);
    Toolbar toolbar = (Toolbar) findViewById(R.id.aboutToolbar);
    toolbar.setTitle("");
    setSupportActionBar(toolbar);

    WebView webView = (WebView) findViewById(R.id.aboutWeb);
    webView.loadDataWithBaseURL(null, getResources().getString(R.string.about_text), "text/html", "utf-8", null);
    //webView.loadData(getResources().getString(R.string.about_text), "text/html", "utf-8");
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.accept_action_menu, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();
    switch (id)
    {
      case R.id.menu_action_accept: onBackPressed(); return true;
      default: return super.onOptionsItemSelected(item);
    }
  }

}
