package com.ua.inr0001.intime;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.TouchDelegate;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.technomag.mpageindicator.MPageIndicator;

import java.util.ArrayList;
import java.util.List;

import intime.api.model.trackttn.TrackTtn;
import intime.api.model.trackttn.TrackTtnDeserializer;
import intime.api.model.ttnlist.EntryGetTtnByApiKey;
import intime.api.model.ttnlist.TTNList;
import intime.getttnlist.GetTTnList;
import intime.getttnlist.PostGetTtnByApiKeyRest;
import intime.llc.ua.R;
import intime.maskedtextedit.MaskedInputFilter;
import intime.ttnlistcommand.CommandHistory;
import intime.utils.DisplayUnits;
import intime.utils.MyStringUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.view.animation.AnimationUtils.loadAnimation;

public class DeliveryActivity extends Fragment implements ITTNManage {

  private static int BTN_SCAN_BARCODE = 2001;
  private static int IMG_SCAN_BARCODE = 2002;
  private static int RESULT_FROM_TTN_DETAILS = 3001;

  private static int ACTION_DELIVERY = 4001;
  private static int ACTION_TRACK_TTN = 4002;

  private int actionView = ACTION_DELIVERY;

  private ProgressDialog dialog;

  private boolean activityPaused = false;

  private LinearLayout llDeleteTtnHint;
  private Button btnUndoDeleteTtn;
  private TextView tvDelete;
  private Animation animationShow1;
  private Animation animationHide1;
  private TextView tvToolbarTitle;

  private boolean toDelete = false;
  private CommandHistory history = new CommandHistory();

  @Override
  public void showDeleteHint(int deleteType) {
    if (llDeleteTtnHint != null && llDeleteTtnHint.getVisibility() != View.INVISIBLE) {
      if (deleteType == TtnHistoryRvCursorAdapter.DELETE)
        tvDelete.setText(mContext.getResources().getString(R.string.ttn_removed));
      else
        tvDelete.setText(mContext.getResources().getString(R.string.move_to_archive));
      llDeleteTtnHint.setVisibility(View.INVISIBLE);
      llDeleteTtnHint.startAnimation(animationShow1);
      toDelete = true;
      llDeleteTtnHint.postDelayed(new Runnable() {
        @Override
        public void run() {
          if (toDelete==true)
            llDeleteTtnHint.startAnimation(animationHide1);
        }
      }, 7000);
    }
  }

  private boolean filterState = false;
  private static final String TAG = "TrackTTN";
  public static final String LOGIN_STATUS_CHANGED = "login_status_changed";

  //private MenuItem miDepartments;
  private MenuItem miClose;
  private MenuItem miFilter;

  private CoordinatorLayout coordinatorLayout;
  private LinearLayout bottomSheet;
  private RecyclerView rcTtnHistoryList;
  private TtnHistoryRvCursorAdapter ttnHistoryAdapter;
//  private NestedScrollView nsTtnHistory;
  private FrameLayout frPaginationProgress;
  private boolean loadList = false;

  private BroadcastReceiver brUpdateLogin;

  private GlobalApplicationData appData;
  private Animation animationHide;
  private Animation animationShow;
  private Toolbar toolbar;

  private LinearLayout layoutTTN;
  private LinearLayout layoutTTNParentScan;
  private TextView tvTrackTtnButton;


  // Track TTN views
  private MaskedInputFilter maskedInputFilter;
  private EditText edTTN;
  private Button btnGetInfo;
  private TextView tvHintCaption;
  private TextView tvHintText;
  private Button btnScanBarcode;
  private ImageView btnTTNClear;

  // Track
  private Call<TrackTtn> call;


  private FrameLayout ttnFrame;
  private TextView btnOkLoginHint;
  private LinearLayout lvWelcomeLoginHint;
  private LinearLayout lvCarouselLayout;
  private Context mContext;
  private TextView tvConf;
  private ImageView imgScanBarcode;
  private FloatingActionButton fabCreateDeclaration;
  private BottomSheetBehavior bh;
  private boolean showTrackInfo = false;
  private int bottomSheetState;
  private int ttnFrameState;

  private int iTTNListHeight = 0;
  private Handler hTTNListHeigth = new Handler(){
    @Override
    public void handleMessage(Message m){
      if (iTTNListHeight != 0)
        bh.setPeekHeight(iTTNListHeight);
      else
        bh.setPeekHeight((int) DisplayUnits.pxToDp(mContext, 160));
    }
  };

	private void setTTNList()
  {
//    Log.d("ttn_bind", "setTTNList");
    if (GlobalApplicationData.getInstance().getUserInfo().getName().length()==0 && !isTTNListExists()) {
      //fabCreateDeclaration.setVisibility(View.GONE);
      LoginOnTTNFragment frActivity = new LoginOnTTNFragment();
      frActivity.setWelcomeFrameLayout(lvWelcomeLoginHint);
      frActivity.setTtnFrame(ttnFrame);
      FragmentTransaction fTrans = getChildFragmentManager().beginTransaction();
      fTrans.replace(R.id.ttn_frame, frActivity);
      fTrans.commit();
      filterState = false;
      //miFilter.setVisible(filterState);
      bottomSheet.setVisibility(View.GONE);
      ttnFrame.setVisibility(View.VISIBLE);
    }
    else
    {
      //fabCreateDeclaration.setVisibility(View.VISIBLE);
      if (GlobalApplicationData.getInstance().IsOnline())
        getTtnList(GlobalApplicationData.getInstance().getAPIkey());
      else {
        ttnFrame.postDelayed(new Runnable() {
          @Override
          public void run() {
            initTTNHistoryRecyclerView();
          }
        }, 100);

      }
    }
    fabCreateDeclaration.setVisibility(View.GONE);
  }

  private void welcomeScreen(String welcomeText)
  {
    WelcomeClientFragment frActivity = new WelcomeClientFragment();
    frActivity.setText(welcomeText);
    FragmentTransaction fTrans = getChildFragmentManager().beginTransaction();
    fTrans.replace(R.id.ttn_frame, frActivity);
    fTrans.commit();
  }

  private void showMessage(String caption, String message)
  {
    if (activityPaused==false) {
      MessageDialog dlgWarning = new MessageDialog();
      dlgWarning.setParams(caption, message, false, null);
      dlgWarning.show(getFragmentManager(), "Warning");
    }
  }

  public void hideSoftKeyboard() {
    try {
      InputMethodManager inputMethodManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
      inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
    } catch (Exception e) { }
  }

  private void myAfterTextChanged(TextWatcher textWatcher, final String originalString, final int before, final int after)
  {
    if (edTTN.getText().toString().length()>0)
      btnTTNClear.setVisibility(View.VISIBLE);
    else
      btnTTNClear.setVisibility(View.INVISIBLE);

    if (originalString.replaceAll(" ", "").length() >= 10)
    {
      layoutTTN.setBackgroundResource(R.drawable.alert_background_filled/*edit_track_ttn_def*/);
      btnGetInfo.setBackgroundResource(R.drawable.button_track_ttn_active);
      btnGetInfo.setTextColor(getResources().getColor(R.color.additional_action));
      btnGetInfo.setEnabled(true);
    }
    else
    {
      layoutTTN.setBackgroundResource(R.drawable.alert_background /*edit_track_ttn_active*/);
      btnGetInfo.setBackgroundResource(R.drawable.button_track_ttn_not_active);
      btnGetInfo.setTextColor(getResources().getColor(R.color.button_background_pressed));
      btnGetInfo.setEnabled(false);
    }

//    int caret = edTTN.getSelectionStart();
//    int caretIterations = 0;
//    edTTN.removeTextChangedListener(textWatcher);
//
//    try {
//
//      String formattedString = originalString.replaceAll(" ", "");
//
//      try {
//        if (formattedString.charAt(6)!=' '){
//          formattedString = insertChar(formattedString, 6);
//          caret++;
//          caretIterations++;
//        }
//      }
//      catch(Exception e){}
//
//      try {
//        if (formattedString.charAt(3)!=' ') {
//          formattedString = insertChar(formattedString, 3);
//          caret++;
//          caretIterations++;
//        }
//      }
//      catch(Exception e){}
//
//      //setting text after format to EditText
//      edTTN.setText(formattedString);
//    } catch (NumberFormatException nfe) {
//    }
//
//    if (edTTN.getText().toString().length()<caret)
//      edTTN.setSelection(edTTN.getText().toString().length());
//    else {
//      int currentCaret = originalString.length();
//      if (caret<originalString.length())
//        currentCaret = caret - caretIterations;
//      if (currentCaret<0)
//        currentCaret = 0;
//      edTTN.setSelection(currentCaret);
//    }
//
//    //if (before>after)
//    //  edTTN.setSelection(caret-1);
    //edTTN.addTextChangedListener(textWatcher);
  }

  private void myBeforeTextChanged(){
    ClipboardManager clipboard = (ClipboardManager) appData.getContext().getSystemService(Context.CLIPBOARD_SERVICE);

    String pasteData = "";
    // If the clipboard doesn't contain data, disable the paste menu item.
    // If it does contain data, decide if you can handle the data.
    if (clipboard.hasPrimaryClip()) {
      // Examines the item on the clipboard. If getText() does not return null, the clip item contains the
      // text. Assumes that this application can only handle one item at a time.
      ClipData.Item item = clipboard.getPrimaryClip().getItemAt(0);

      if (item!=null)
      {
        // Gets the clipboard as text.
        pasteData = item.coerceToText(appData.getContext()).toString();

        // If the string contains data, then the paste operation is done
        if (pasteData != null) {
          try {
            pasteData = pasteData.replaceAll("[^0-9]", "").trim();
            pasteData = MyStringUtils.getFormattedTTN(pasteData);
            edTTN.setText(pasteData);
            ClipData clip = ClipData.newPlainText("simple text", null);
            clipboard.setPrimaryClip(clip);
          }
          catch(Exception e)
          {
            Log.d(TAG, "Error: " + e.getMessage().toString());
          }
        }
      }
    }
  }

//  private void slideIn(final View view, final int targetHeight)
//  {
//    view.setVisibility(View.VISIBLE);
//
//    ResizeAnimation resizeAnimation = new ResizeAnimation(view, (int) DisplayUnits.pxToDp(getContext(), targetHeight), 0);
//    resizeAnimation.setDuration(500);
//    view.startAnimation(resizeAnimation);
//  }
//
//  private int slideOut(final View view, final int startHeight)
//  {
//    ResizeAnimation resizeAnimation = new ResizeAnimation(view, 0, (int) DisplayUnits.pxToDp(getContext(), startHeight));
//    resizeAnimation.setDuration(500);
//    view.startAnimation(resizeAnimation);
//    return startHeight;
//  }

//  private void animateMenuItem(final MenuItem mi, final int idIcon, final boolean show, EndAnimation endAnimation)
//  {
//    ImageView image = new ImageView(GlobalApplicationData.getInstance().getContext());
//    if (idIcon!=-1)
//      image.setImageResource(idIcon);
//    int padding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 18, getResources().getDisplayMetrics());
//    image.setPadding(padding, padding, padding, padding);
//    mi.setActionView(image);
//
//    if (!show) {
//      MyHideAnimationListener animationListener = new MyHideAnimationListener(mi, show, endAnimation);
//      Animation animationHide = AnimationUtils.loadAnimation(GlobalApplicationData.getInstance().getContext(), R.anim.fade_out);
//      animationHide.setAnimationListener(animationListener);
//      mi.getActionView().startAnimation(animationHide);
//    }
//    else {
//      MyHideAnimationListener animationListener = new MyHideAnimationListener(mi, show, endAnimation);
//      Animation animationShow = AnimationUtils.loadAnimation(GlobalApplicationData.getInstance().getContext(), R.anim.fade_in);
//      animationShow.setAnimationListener(animationListener);
//      mi.getActionView().startAnimation(animationShow);
//    }
//  }

  private void setupActionBarTrack()
  {
    if (miFilter.isVisible()) {
//      animateMenuItem(miFilter, R.drawable.ic_filter, false,
//        new EndAnimation() {
//          @Override
//          public void animatioEnd() {
//            animateMenuItem(miClose, R.drawable.ic_clear, true, null);
//            miClose.setVisible(true);
//          }
//        }
//      );
      miClose.setVisible(true);
      miFilter.setVisible(false);
    }
    else{
      //animateMenuItem(miClose, R.drawable.ic_clear, true, null);
      miClose.setVisible(true);
    }

    miClose.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
      @Override
      public boolean onMenuItemClick(MenuItem item) {
        backFromTrackTTN();
        hideSoftKeyboard();
        return false;
      }
    });
  }

  private void goTrackTTN() {
	  actionView = ACTION_TRACK_TTN;
    showTrackInfo = true;
    tvToolbarTitle.setText(mContext.getResources().getString(R.string.text_check_delivery));
    ttnFrameState = ttnFrame.getVisibility();
    bottomSheetState = bottomSheet.getVisibility();
    bottomSheet.setVisibility(View.GONE);
    ttnFrame.setVisibility(View.GONE);
    //fabCreateDeclaration.setVisibility(View.GONE);
    tvTrackTtnButton.startAnimation(animationHide);
    tvTrackTtnButton.setVisibility(View.GONE);
    edTTN.startAnimation(animationShow);
    edTTN.setVisibility(View.VISIBLE);
    btnGetInfo.startAnimation(animationShow);
    btnGetInfo.setVisibility(View.VISIBLE);
    tvHintCaption.startAnimation(animationShow);
    tvHintCaption.setVisibility(View.VISIBLE);
    tvHintText.startAnimation(animationShow);
    tvHintText.setVisibility(View.VISIBLE);
    lvCarouselLayout.startAnimation(animationHide);
    lvCarouselLayout.setVisibility(View.GONE);
    btnScanBarcode.startAnimation(animationShow);
    btnScanBarcode.setVisibility(View.VISIBLE);
    imgScanBarcode.startAnimation(animationHide);
    imgScanBarcode.setVisibility(View.GONE);
    if (edTTN.getText().length()>0) {
      btnTTNClear.startAnimation(animationShow);
      btnTTNClear.setVisibility(View.VISIBLE);
    }
    setupActionBarTrack();
    layoutTTNParentScan.setTouchDelegate(null);
    extendScanBarcodeArea();
    myBeforeTextChanged();
  }

  private void setupActionBarMain()
  {
    tvTrackTtnButton.setEnabled(true);
    miClose.setVisible(false);
    if (filterState)
      miFilter.setVisible(filterState);
//    tvTrackTtnButton.setEnabled(false);
//    animateMenuItem(miClose, R.drawable.ic_clear, false,
//      new EndAnimation() {
//        @Override
//        public void animatioEnd() {
//          miClose.setVisible(false);
//          tvTrackTtnButton.setEnabled(true);
//          if (filterState) {
//            miFilter.setVisible(filterState);
//            animateMenuItem(miFilter, R.drawable.ic_filter, true, new EndAnimation() {
//              @Override
//              public void animatioEnd() {
//                tvTrackTtnButton.setEnabled(true);
//              }
//            });
//          }
//        }
//      });
    layoutTTN.setBackgroundResource(R.drawable.alert_background);
  }

  private void backFromTrackTTN()
  {
    showTrackInfo = false;
    actionView = ACTION_DELIVERY;
    tvToolbarTitle.setText(mContext.getResources().getString(R.string.tab_delivery));
//    if (GlobalApplicationData.getInstance().getUserInfo().getApiKey().length()!=0)
//      fabCreateDeclaration.setVisibility(View.VISIBLE);
    bottomSheet.setVisibility(bottomSheetState);
    ttnFrame.setVisibility(ttnFrameState);
    tvTrackTtnButton.startAnimation(animationShow);
    tvTrackTtnButton.setVisibility(View.VISIBLE);
    edTTN.startAnimation(animationHide);
    edTTN.setVisibility(View.GONE);
    btnGetInfo.startAnimation(animationHide);
    btnGetInfo.setVisibility(View.GONE);
    tvHintCaption.startAnimation(animationHide);
    tvHintCaption.setVisibility(View.GONE);
    tvHintText.startAnimation(animationHide);
    tvHintText.setVisibility(View.GONE);
    lvCarouselLayout.startAnimation(animationShow);
    lvCarouselLayout.setVisibility(View.VISIBLE);
    btnScanBarcode.startAnimation(animationHide);
    btnScanBarcode.setVisibility(View.GONE);
    imgScanBarcode.startAnimation(animationShow);
    imgScanBarcode.setVisibility(View.VISIBLE);
    btnTTNClear.startAnimation(animationHide);
    btnTTNClear.setVisibility(View.GONE);
    setupActionBarMain();
    layoutTTNParentScan.setTouchDelegate(null);
    extendImgScanBarcodeArea();
    setTTNList();
  }

  private void extendImgScanBarcodeArea()
  {
    //final View parent = (View) btnScanBarcode.getParent();

    layoutTTNParentScan.post(new Runnable() {
      // Post in the parent's message queue to make sure the parent
      // lays out its children before you call getHitRect()
      @Override
      public void run() {
        // The bounds for the delegate view (an ImageButton
        // in this example)
        Rect delegateArea = new Rect();

        // The hit rectangle for the ImageButton
        imgScanBarcode.getHitRect(delegateArea);

        // Extend the touch area of the ImageButton beyond its bounds
        // on the right and bottom.
        delegateArea.right += DisplayUnits.pxToDp(mContext, 20);
        delegateArea.bottom += DisplayUnits.pxToDp(mContext, 20);
        delegateArea.left -= DisplayUnits.pxToDp(mContext, 20);
        delegateArea.top -= DisplayUnits.pxToDp(mContext, 20);

        // Instantiate a TouchDelegate.
        // "delegateArea" is the bounds in local coordinates of
        // the containing view to be mapped to the delegate view.
        // "myButton" is the child view that should receive motion
        // events.
        TouchDelegate touchDelegate = new TouchDelegate(delegateArea,
          imgScanBarcode);

        // Sets the TouchDelegate on the parent view, such that touches
        // within the touch delegate bounds are routed to the child.
        //if (View.class.isInstance(btnScanBarcode.getParent())) {
        layoutTTNParentScan.setTouchDelegate(touchDelegate);
        //}
      }
    });

  }

  private void extendScanBarcodeArea()
  {
    //final View parent = (View) btnScanBarcode.getParent();

    layoutTTNParentScan.post(new Runnable() {
      // Post in the parent's message queue to make sure the parent
      // lays out its children before you call getHitRect()
      @Override
      public void run() {
        // The bounds for the delegate view (an ImageButton
        // in this example)
        Rect delegateArea = new Rect();

        // The hit rectangle for the ImageButton
        btnScanBarcode.getHitRect(delegateArea);

        // Extend the touch area of the ImageButton beyond its bounds
        // on the right and bottom.
        delegateArea.right += DisplayUnits.pxToDp(mContext, 20);
        delegateArea.bottom += DisplayUnits.pxToDp(mContext, 20);
        delegateArea.left -= DisplayUnits.pxToDp(mContext, 20);
        delegateArea.top -= DisplayUnits.pxToDp(mContext, 20);

        // Instantiate a TouchDelegate.
        // "delegateArea" is the bounds in local coordinates of
        // the containing view to be mapped to the delegate view.
        // "myButton" is the child view that should receive motion
        // events.
        TouchDelegate touchDelegate = new TouchDelegate(delegateArea,
          btnScanBarcode);

        // Sets the TouchDelegate on the parent view, such that touches
        // within the touch delegate bounds are routed to the child.
        //if (View.class.isInstance(btnScanBarcode.getParent())) {
        layoutTTNParentScan.setTouchDelegate(touchDelegate);
        //}
      }
    });

  }

  private void setTTNText(final String ttn)
  {
    edTTN.setFilters(new InputFilter[]{});
    edTTN.setText(ttn);
    edTTN.setFilters(new InputFilter[]{maskedInputFilter});
  }

  private void initTrackViews(final View view)
  {
    edTTN = (EditText) view.findViewById(R.id.edTTN);
    btnGetInfo = (Button) view.findViewById(R.id.btnGetInfo);
    btnScanBarcode = (Button) view.findViewById(R.id.btnScanBarcode);
    btnTTNClear = (ImageView) view.findViewById(R.id.btnTTNClear);
    imgScanBarcode = (ImageView) view.findViewById(R.id.imgScanBarcode);
    imgScanBarcode.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        startActivityForResult(new Intent(mContext, CameraActivity.class), IMG_SCAN_BARCODE);
      }
    });

    edTTN.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        btnGetInfo.setVisibility(View.VISIBLE);
      }
    });

    edTTN.addTextChangedListener(new TextWatcher() {

      private int before = 0;
      private int after = 0;

      public void afterTextChanged(Editable s) {
        myAfterTextChanged(this, s.toString(), before, after);
      }

      public void beforeTextChanged(CharSequence s, int start,
                                    int count, int after) {
        before = s.toString().length();
      }

      public void onTextChanged(CharSequence s, int start, int before, int count) {
        after = s.toString().length();
      }

    });

    maskedInputFilter = new MaskedInputFilter("### ### ####", edTTN, '#');
    edTTN.setFilters(new InputFilter[]{maskedInputFilter});

    btnTTNClear.setOnClickListener(new View.OnClickListener() {
      public void onClick(View v) {
        setTTNText("");
      }
    });

    btnGetInfo.setOnClickListener(new View.OnClickListener() {
      public void onClick(View v) {
        hideSoftKeyboard();
        callGetInfo(edTTN.getText().toString());
      }
    });

    btnScanBarcode.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        startActivityForResult(new Intent(mContext, CameraActivity.class), BTN_SCAN_BARCODE);
      }
    });
    extendImgScanBarcodeArea();
  }

  private void callGetInfo(String inTTN)
  {
    if (inTTN.replaceAll(" ", "").length() >= 10)
    {
      if (!appData.IsOnline())
      {
        // есть ли доступ к интернет на устройстве
        showMessage(getResources().getString(R.string.dialog_warning_caption),
          getResources().getString(R.string.dialog_message_no_network_access));
      }
      else
        doRequest(inTTN);
      // если нужно отслеживать состояние посылки
						/*if (chSimple.isChecked())
						  new AsyncCallWS().execute(edTTN.getText().toString(), "register");
						else
						  new AsyncCallWS().execute(edTTN.getText().toString(), "notregister");*/

      // сохраняем введенный TTN
      //addTTNToList(edTTN.getText().toString());


    } else
    {
      // если поле TTN пустое
      showMessage(getResources().getString(R.string.dialog_warning_caption),
        getResources().getString(R.string.dialog_warning_message));
    }
  }

  public void initButtomSheetTtnHistory(final View view)
  {
    rcTtnHistoryList = (RecyclerView) view.findViewById(R.id.rcTtnHistoryList);
    coordinatorLayout = (CoordinatorLayout) view.findViewById(R.id.deliveryCoordinatorLayut);
    bottomSheet = (LinearLayout) coordinatorLayout.findViewById(R.id.ttnHistoryNestedScroll);
    bh = BottomSheetBehavior.from(bottomSheet);
//    frPaginationProgress = (FrameLayout) view.findViewById(R.id.frPaginationProgress);
//    nsTtnHistory = (NestedScrollView) view.findViewById(R.id.ttnHistoryScroll);
//    nsTtnHistory.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
//      @Override
//      public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
//        //Log.d("TTNList", "Diff: " + Integer.toString(diff));
//        if (GlobalApplicationData.getInstance().getUserInfo().getName().length()!=0) {
//          View view = (View) v.getChildAt(v.getChildCount()-1);
//          int diff = (view.getBottom() - (v.getHeight() + scrollY));
//          if (diff <= 100 && loadList == false) {
//            frPaginationProgress.setVisibility(View.VISIBLE);
//            loadPageTtnList(GlobalApplicationData.getInstance().getAPIkey(), ttnHistoryAdapter.getStartPosition());
//          }
//        }
//      }
//    });
  }

  @Override
  public void initTTNHistoryRecyclerView()
  {
    Cursor cursor;
    if (GlobalApplicationData.getInstance().filterQuery.length()>0)
      cursor = InTimeDB.getInstance(mContext).tableTTNHistory.selectFiltered();
    else
      cursor = InTimeDB.getInstance(mContext).tableTTNHistory.selectAll();
    if (cursor!=null && cursor.getCount()>0) {
      if (bh!=null) {
        ttnFrame.postDelayed(new Runnable() {
          @Override
          public void run() {
            iTTNListHeight = ttnFrame.getMeasuredHeight();
            if (hTTNListHeigth != null)
              hTTNListHeigth.sendEmptyMessage(0);
          }
        }, 500);
      }
      ttnFrame.setVisibility(View.INVISIBLE);
      LinearLayoutManager lm = new LinearLayoutManager(mContext);
      lm.setAutoMeasureEnabled(true);
      rcTtnHistoryList.setLayoutManager(lm);
      ttnHistoryAdapter = new TtnHistoryRvCursorAdapter(mContext, cursor, this, history);
      ttnHistoryAdapter.setOnItemClickListener(new TtnHistoryRvCursorAdapter.OnItemClickListener() {
        @Override
        public void onItemClick(View view, String id) {
          if (id!=null) {
            if (GlobalApplicationData.getInstance().IsOnline())
              doRequest(id);
            else
            {
              openTtnOffline(id);
            }
          }

        }
      });
      rcTtnHistoryList.setAdapter(ttnHistoryAdapter);
      rcTtnHistoryList.setHasFixedSize(false);
      rcTtnHistoryList.setNestedScrollingEnabled(false);

//      -- work swipe
//      rcTtnHistoryList.setItemAnimator(new DefaultItemAnimator());
//      ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, null);
//      new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(rcTtnHistoryList);
//      -- work swipe


//      rcTtnHistoryList.addOnItemTouchListener(new MySwipeListener(mContext,
//        rcTtnHistoryList,
//        new MySwipeListener.OnTouchActionListener() {
//          @Override
//          public void onLeftSwipe(View view, int position) {
//
//            Log.d("Swipe", "Swipe left");
//            //code as per your need
//          }
//
//          @Override
//          public void onRightSwipe(View view, int position) {
//            //code as per your need
//          }
//
//          @Override
//          public void onClick(View view, int position) {
//            //code as per your need
//          }
//        }));
      bottomSheet.setVisibility(View.VISIBLE);
      filterState = true;
      //miFilter.setVisible(filterState);
    }
    else {
      if (isTTNListExists())
      {
        filterState = true;
        if (GlobalApplicationData.getInstance().filterQuery.length()>0) {
          bottomSheet.setVisibility(View.GONE);
          welcomeScreen(getResources().getString(R.string.filter_is_empty));
        }
      }
      else {
        filterState = false;
        bottomSheet.setVisibility(View.GONE);
        String formattedString = String.format(getResources().getString(R.string.text_welcome_client),
          GlobalApplicationData.getInstance().getUserInfo().getName());
        welcomeScreen(formattedString);
      }
      ttnFrame.setVisibility(View.VISIBLE);
      //miFilter.setVisible(filterState);
    }
  }

  private void openTtnOffline(String id) {

	  Cursor cursor = InTimeDB.getInstance(mContext).tableTTNHistory.selectByID(id);
	  cursor.moveToFirst();
    Intent intent = new Intent(appData.getContext(), TtnDetailsActivity.class);
    intent.putExtra("ttn", DictionariesDB.getColumnValue(cursor, HistoryTtnTable.Column.TTN)/*track.getData().getNumber()*/);
    intent.putExtra("fromCity", DictionariesDB.getColumnValue(cursor, HistoryTtnTable.Column.LOCALITY_SENDER)/*track.getData().getFrom().getCity()*/);
    intent.putExtra("toCity", DictionariesDB.getColumnValue(cursor, HistoryTtnTable.Column.LOCALITY_RECEIVER)/*track.getData().getTo().getCity()*/);
    intent.putExtra("toStoreCode", DictionariesDB.getColumnValue(cursor, HistoryTtnTable.Column.STORE_CODE)/*track.getData().getTo().getStoreCode()*/);
    intent.putExtra("statusId", DictionariesDB.getColumnValue(cursor, HistoryTtnTable.Column.STATUS_ID)/*track.getData().getStatusId()*/);
    intent.putExtra("type", DictionariesDB.getColumnValue(cursor, HistoryTtnTable.Column.TYPE)/*track.getData().getType()*/);
    intent.putExtra("status", DictionariesDB.getColumnValue(cursor, HistoryTtnTable.Column.STATUS_NAME_RU)/*track.getData().getStatus()*/);
    intent.putExtra("description", DictionariesDB.getColumnValue(cursor, HistoryTtnTable.Column.DESCRIPTION)/*track.getData().getDescription()*/);
    intent.putExtra("dateCreation", DictionariesDB.getColumnValue(cursor, HistoryTtnTable.Column.DATE_CREATION)/*track.getData().getDateCreation().getDate()*/);
    intent.putExtra("dateDelivery", DictionariesDB.getColumnValue(cursor, HistoryTtnTable.Column.DATE_DELIVERY)/*track.getData().getDateDelivery().getDate()*/);
    intent.putExtra("seats", DictionariesDB.getColumnValue(cursor, HistoryTtnTable.Column.SEATS)/*track.getData().getSeats()*/);
    intent.putExtra("weight", DictionariesDB.getColumnValue(cursor, HistoryTtnTable.Column.WEIGHT)/*track.getData().getWeight()*/);
    intent.putExtra("volume", DictionariesDB.getColumnValue(cursor, HistoryTtnTable.Column.VOLUME)/*track.getData().getVolume()*/);
    intent.putExtra("cost", DictionariesDB.getColumnValue(cursor, HistoryTtnTable.Column.COST)/*track.getData().getCost()*/);
    intent.putExtra("cost_return", DictionariesDB.getColumnValue(cursor, HistoryTtnTable.Column.COST_RETURN)/*track.getData().getCostReturn()*/);
    intent.putExtra("cache_on_delivery", DictionariesDB.getColumnValue(cursor, HistoryTtnTable.Column.COD)/*track.getData().getCashOnDelivery()*/);
    intent.putExtra("toPay", DictionariesDB.getColumnValue(cursor, HistoryTtnTable.Column.TO_PAY)/*track.getData().getToPay()*/);
    intent.putExtra("payer", DictionariesDB.getColumnValue(cursor, HistoryTtnTable.Column.PAYER)/*track.getData().getPayer()*/);
    intent.putExtra("address", DictionariesDB.getColumnValue(cursor, HistoryTtnTable.Column.ADDRESS)/*track.getData().getTo().getStore().getAddress()*/);
    intent.putExtra("store_type", DictionariesDB.getColumnValue(cursor, HistoryTtnTable.Column.STORE_TYPE)/*track.getData().getTo().getStore().getType()*/);
    intent.putExtra("cargo_type", DictionariesDB.getColumnValue(cursor, HistoryTtnTable.Column.CARGO_TYPE)/*track.getData().getCargoType()*/);
    if (cursor != null)
      cursor.close();
    startActivityForResult(intent, RESULT_FROM_TTN_DETAILS);
  }


  private boolean isTTNListExists()
  {
    boolean result = false;
    Cursor cr = InTimeDB.getInstance(mContext).tableTTNHistory.getCount();
    if (cr != null && cr.moveToFirst()) {
      if (cr.getInt(0)==0)
        result = false;
      else
        result = true;
    }
    cr.close();
    return result;
  }

  @Override
  public void onStart() {
    super.onStart();
    IntentFilter intFilt = new IntentFilter(LOGIN_STATUS_CHANGED);
    mContext.registerReceiver(brUpdateLogin, intFilt);
  }

  @Override
  public void onStop() {
    super.onStop();
    if (brUpdateLogin!=null)
      mContext.unregisterReceiver(brUpdateLogin);
  }

  private void initDeleteTtnHint(final View view)
  {
    llDeleteTtnHint = (LinearLayout) view.findViewById(R.id.llDeleteTtnHint);
    btnUndoDeleteTtn = (Button) view.findViewById(R.id.btnUndoDeleteTtn);
    tvDelete = (TextView) view.findViewById(R.id.tvDeleteTtn);
    btnUndoDeleteTtn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        toDelete = false;
        llDeleteTtnHint.setVisibility(View.GONE);
        ttnHistoryAdapter.undoremoveItem();
      }
    });
    animationShow1 = AnimationUtils.loadAnimation(GlobalApplicationData.getInstance().getContext(), R.anim.fade_in);
    animationShow1.setAnimationListener(new Animation.AnimationListener() {
      @Override
      public void onAnimationStart(Animation animation) {

      }

      @Override
      public void onAnimationEnd(Animation animation) {
        llDeleteTtnHint.setVisibility(View.VISIBLE);
      }

      @Override
      public void onAnimationRepeat(Animation animation) {

      }
    });
    animationHide1 = AnimationUtils.loadAnimation(GlobalApplicationData.getInstance().getContext(), R.anim.fade_out);
    animationHide1.setAnimationListener(new Animation.AnimationListener() {
      @Override
      public void onAnimationStart(Animation animation) {

      }

      @Override
      public void onAnimationEnd(Animation animation) {
        llDeleteTtnHint.setVisibility(View.INVISIBLE);
//        if (toDelete==true)
//          ttnHistoryAdapter.fullDelete();
      }

      @Override
      public void onAnimationRepeat(Animation animation) {

      }
    });
  }

  @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	      Bundle savedInstanceState) {

    setHasOptionsMenu(true);
    animationHide = loadAnimation(GlobalApplicationData.getInstance().getContext(), R.anim.fade_out);
    animationShow = loadAnimation(GlobalApplicationData.getInstance().getContext(), R.anim.fade_in);
	  View view = inflater.inflate(R.layout.activity_delivery, null);
    getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
    mContext = view.getContext();
	  appData = GlobalApplicationData.getInstance();
    tvToolbarTitle = (TextView) view.findViewById(R.id.tvToolbarTitleDelivery);
    layoutTTN = (LinearLayout) view.findViewById(R.id.layoutTTN);
    layoutTTNParentScan = (LinearLayout) view.findViewById(R.id.layoutTTNParentScan);
    //layoutTTN.setOnClickListener(new View.OnClickListener() {
    //  @Override
    //  public void onClick(View v) {
    //    startActivity(new Intent(getContext(), TrackActivity.class));
    //  }
    //});

    tvTrackTtnButton = (TextView) view.findViewById(R.id.tvTrackTtnButton);
    tvTrackTtnButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        goTrackTTN();
        //startActivity(new Intent(getContext(), TrackActivity.class));
      }
    });

		ttnFrame = (FrameLayout) view.findViewById(R.id.ttn_frame);
    //lvCarousel = (LinearLayout) view.findViewById(R.id.lvCarouselLayout);

    lvWelcomeLoginHint = (LinearLayout) view.findViewById(R.id.lvWelcomeLoginHint);
    btnOkLoginHint = (TextView) view.findViewById(R.id.btnOnLoginHint);
    btnOkLoginHint.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        GlobalApplicationData.getInstance().welcomeScreenShow = true;
        GlobalApplicationData.getInstance().saveAppSettings();
        lvWelcomeLoginHint.setVisibility(View.GONE);
      }
    });

    tvConf = (TextView) view.findViewById(R.id.tvConf);
    tvConf.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        startActivity(new Intent(mContext, ConfidentialityActivity.class));
      }
    });

    fabCreateDeclaration = (FloatingActionButton) view.findViewById(R.id.fabCreateDeclaration);
    fabCreateDeclaration.setVisibility(View.GONE);
    fabCreateDeclaration.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        startActivity(new Intent(mContext, CreateDeclarationActivity.class));
      }
    });

    tvHintCaption = (TextView) view.findViewById(R.id.tvHintCaption);
    tvHintText = (TextView) view.findViewById(R.id.tvHintText);
    lvCarouselLayout = (LinearLayout) view.findViewById(R.id.lvCarouselLayout);

    initTrackViews(view);
    initViews(view);
    initButtomSheetTtnHistory(view);
    initDeleteTtnHint(view);

    brUpdateLogin = new BroadcastReceiver() {
      @Override
      public void onReceive(Context context, Intent intent) {
        try {
          setTTNList();
        }catch(Exception e)
        {
          Log.d(TAG, "Error receive message: " + e.getMessage());
        }
      }
    };
	  return view;
	} // onCreateView

  @Override
  public void onActivityCreated(@Nullable Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
    ((MainActivity)getActivity()).setToolbar(toolbar);
  }

  @Override
  public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    inflater.inflate(R.menu.main_activity_menu, menu);
    //miDepartments = (MenuItem) menu.findItem(R.id.menu_departments);
    miClose = (MenuItem) menu.findItem(R.id.menu_close);
    miFilter = (MenuItem) menu.findItem(R.id.menu_filters);
    miFilter.setVisible(isTTNListExists());
    super.onCreateOptionsMenu(menu,inflater);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();
    switch (id)
    {
      case R.id.menu_filters: startActivity(new Intent(mContext, FilterActivity.class)); return true;
      default: return super.onOptionsItemSelected(item);
    }
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    toolbar = (Toolbar) view.findViewById(R.id.deliveryToolBar);
    toolbar.setTitle("");
    ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
  }

  private void setupPager(final ViewPager pager)
  {
    if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE) {
      pager.setPadding(155,0,155,0);
    }
    else if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_NORMAL) {
      pager.setPadding(55,0,55,0);
    }
    else if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_SMALL) {
      pager.setPadding(55,0,55,0);
    }
    else {
      pager.setPadding(55,0,55,0);
    }
  }


  @SuppressWarnings("ConstantConditions")
  private void initViews(View view) {
    CarouselSimplePagerAdapter adapter = new CarouselSimplePagerAdapter(mContext, createPageList());

    ViewPager pager = (ViewPager) view.findViewById(R.id.vpPager);
    setupPager(pager);
    pager.setClipToPadding(false);
    pager.setPageTransformer(false, new CarouselEffectTransformer()); // Set transformer
    //pager.setPadding(55,0,55,0);
    //pager.seinittPadding(155,0,155,0);
    //pager.setPageMargin(60);
    MPageIndicator pageIndicator = (MPageIndicator) view.findViewById(R.id.piPageIndicator);
    pageIndicator.setCount(adapter.getCount());
    pager.setAdapter(adapter);
  }

  private List<CarouselPagerItem> createPageList() {
    List<CarouselPagerItem> pageList = new ArrayList<CarouselPagerItem>();
    String[] title = getResources().getStringArray(R.array.carousel_titles);
    String[] text = getResources().getStringArray(R.array.carousel_text);
    for (int i=0; i<title.length; i++) {
      CarouselPagerItem pi = new CarouselPagerItem();
      pi.text = text[i];
      pi.title = title[i];
      pageList.add(pi);
    }
    return pageList;
  }

  @Override
	public void onResume() {
		super.onResume();
    activityPaused = false;
    //myBeforeTextChanged();
    if (showTrackInfo==false)
      setTTNList();
    if (miFilter!=null && actionView == ACTION_DELIVERY) {
      ttnFrame.postDelayed(new Runnable() {
        @Override
        public void run() {
          miFilter.setVisible(isTTNListExists());
        }
      }, 1000);
    }
	}

  @Override
  public void onPause() {
    super.onPause();
    activityPaused = true;
    hideSoftKeyboard();
    if (call!=null && call.isExecuted())
      call.cancel();
  }

  @Override
  public void onDestroyView()
  {
    super.onDestroyView();
    ((MainActivity)getActivity()).setToolbar(null);
  }

  private Converter.Factory createGsonConverter() {
    GsonBuilder gsonBuilder = new GsonBuilder();
    gsonBuilder.registerTypeAdapter(TrackTtn.class, new TrackTtnDeserializer());
    Gson gson = gsonBuilder.create();
    return GsonConverterFactory.create(gson);
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
      if (data!=null && requestCode==BTN_SCAN_BARCODE) {
        String ttn = data.getStringExtra("TTN");
        setTTNText(MyStringUtils.getFormattedTTN(ttn));
        btnGetInfo.callOnClick();
      }
      if (data!=null && requestCode==IMG_SCAN_BARCODE)
      {
        String ttn = data.getStringExtra("TTN");
        setTTNText(MyStringUtils.getFormattedTTN(ttn));
        callGetInfo(ttn);
      }
      if (requestCode==RESULT_FROM_TTN_DETAILS)
      {
        tvTrackTtnButton.setEnabled(true);
      }
      if (actionView == ACTION_TRACK_TTN)
        miFilter.setVisible(false);
      //startActivity(new Intent(appData.getContext(), TtnDetailsActivity.class));
  }

  private void doRequestOtheLang(final String ttn,
                                 final String sender, final String senderUA,
                                 final String receiver, final String receiverUA,
                                 final String status, final String statusUA,
                                 final String type, final String typeUA,
                                 final String payer, final String payerUA,
                                 final String address, final String addressUA,
                                 final String cargoType, final String cargoTypeUA)
  {

    Retrofit retrofit = new Retrofit.Builder()
      .baseUrl("https://newapi.intime.ua/")
      //.addConverterFactory(GsonConverterFactory.create())
      .addConverterFactory(createGsonConverter())
      .client(GlobalApplicationData.getUnsafeOkHttpClient())
      .build();

    IRtfAPI service = retrofit.create(IRtfAPI.class);
    Call<TrackTtn> call;
    if (sender.length()==0)
      call = service.trackTtn(ttn, "ru");
    else
      call = service.trackTtn(ttn, "ua");
    call.enqueue(
      new Callback<TrackTtn>() {
        @Override
        public void onResponse(Call<TrackTtn> call, Response<TrackTtn> response) {

          TrackTtn track = response.body();
          if (track!=null && !track.isError()) {
            //Log.d(TAG, "TTN: " + track.getData().getFrom().getCity());
//            DictionariesDB.getInstance(mContext).tableTTNHistory.insert(ttn, ttn, track.getData().getDateDelivery().getDate(),
//              track.getData().getDateCreation().getDate(), track.getData().getFrom().getCity(), track.getData().getFrom().getCity(),
//              track.getData().getTo().getCity(), track.getData().getTo().getCity(), track.getData().getStatus(), track.getData().getStatus(), track.getData().getCost(),
//              track.getData().getStatusId(), 1);
            String mSender = "", mSenderUA = "";
            String mReceiver = "", mReceiverUA = "";
            String mStatus = "", mStatusUA = "";
            String mType = "", mTypeUA = "";
            String mPayer = "", mPayerUA = "";
            String mAddress = "", mAddressUA = "";
            String mCargoType = "", mCargoTypeUA = "";
            if (sender.length()==0)
              mSender = track.getData().getFrom().getCity();
            else
              mSender = sender;

            if (senderUA.length()==0)
              mSenderUA = track.getData().getFrom().getCity();
            else
              mSenderUA = senderUA;

            if (receiver.length()==0)
              mReceiver = track.getData().getTo().getCity();
            else
              mReceiver = receiver;

            if (receiverUA.length()==0)
              mReceiverUA = track.getData().getTo().getCity();
            else
              mReceiverUA = receiverUA;

            if (status.length()==0)
              mStatus = track.getData().getStatus();
            else
              mStatus = status;

            if (statusUA.length()==0)
              mStatusUA = track.getData().getStatus();
            else
              mStatusUA = statusUA;

            if (type.length()==0)
              mType = track.getData().getType();
            else
              mType = type;

            if (typeUA.length()==0)
              mTypeUA = track.getData().getType();
            else
              mTypeUA = typeUA;

            if (payer.length()==0)
              mPayer = track.getData().getPayer();
            else
              mPayer = payer;

            if (payerUA.length()==0)
              mPayerUA = track.getData().getPayer();
            else
              mPayerUA = payerUA;

            if (address.length()==0)
              mAddress = track.getData().getTo().getStore().getAddress();
            else
              mAddress = address;

            if (addressUA.length()==0)
              mAddressUA = track.getData().getTo().getStore().getAddress();
            else
              mAddressUA = addressUA;

            if (cargoType.length()==0)
              mCargoType = track.getData().getCargoType();
            else
              mCargoType = cargoType;

            if (cargoTypeUA.length()==0)
              mCargoTypeUA = track.getData().getCargoType();
            else
              mCargoTypeUA = cargoTypeUA;

              if (!InTimeDB.getInstance(mContext).tableTTNHistory.existDB(ttn)) {
                if (track.getData().getStatusId().compareTo("95") != 0) {
                  InTimeDB.getInstance(mContext).tableTTNHistory.insert(ttn, ttn, track.getData().getDateDelivery().getDate(),
                    track.getData().getDateCreation().getDate(),
                    mSender, mSenderUA,
                    mReceiver, mReceiverUA, mStatus, mStatusUA, track.getData().getCost(),
                    track.getData().getToPay(),
                    track.getData().getCashOnDelivery(),
                    track.getData().getCostReturn(),
                    track.getData().getStatusId(),
                    track.getData().getTo().getStoreCode(),
                    track.getData().getTo().getStore().getType(),
                    mType,
                    mTypeUA,
                    track.getData().getDescription(),
                    track.getData().getSeats(),
                    track.getData().getWeight(),
                    mPayer,
                    mPayerUA,
                    mAddress,
                    mAddressUA,
                    mCargoType,
                    mCargoTypeUA,
                    track.getData().getVolume(),
                    HistoryTtnTable.StatusView.TRACKED);
                }
              }
              else
              {
                InTimeDB.getInstance(mContext).tableTTNHistory.update(ttn, ttn, track.getData().getDateDelivery().getDate(),
                  track.getData().getDateCreation().getDate(),
                  mSender, mSenderUA,
                  mReceiver, mReceiverUA, mStatus, mStatusUA, track.getData().getCost(),
                  track.getData().getToPay(),
                  track.getData().getCashOnDelivery(),
                  track.getData().getCostReturn(),
                  track.getData().getStatusId(),
                  track.getData().getTo().getStoreCode(),
                  track.getData().getTo().getStore().getType(),
                  mType,
                  mTypeUA,
                  track.getData().getDescription(),
                  track.getData().getSeats(),
                  track.getData().getWeight(),
                  mPayer,
                  mPayerUA,
                  mAddress,
                  mAddressUA,
                  mCargoType,
                  mCargoTypeUA,
                  track.getData().getVolume(),
                  HistoryTtnTable.StatusView.TRACKED);
              }
//              if (ttnHistoryAdapter!=null) {
//                ttnHistoryAdapter.dataUpdated();
//              }
          }

          if (dialog!=null && dialog.isShowing())
          {
            dialog.dismiss();
            dialog = null;
          }

        }

        @Override
        public void onFailure(Call<TrackTtn> call, Throwable t) {
          Log.d(TAG, "Error: " + t.getMessage());
          if (dialog!=null && dialog.isShowing())
          {
            dialog.dismiss();
            dialog = null;
          }
        }

      }
    );

  }

  private void doRequest(String ttn)
  {
    dialog = new ProgressDialog(mContext);
    dialog.setCancelable(false);
    dialog.setIndeterminate(true);
    dialog.setMessage(mContext.getResources().getString(R.string.search_ttn));
    dialog.show();

    btnGetInfo.setEnabled(false);
    Retrofit retrofit = new Retrofit.Builder()
      .baseUrl("https://newapi.intime.ua/")
      //.addConverterFactory(GsonConverterFactory.create())
      .addConverterFactory(createGsonConverter())
      .client(GlobalApplicationData.getUnsafeOkHttpClient())
      .build();

    IRtfAPI service = retrofit.create(IRtfAPI.class);
    //Log.d(TAG, "TTN " + edTTN.getText().toString() + " (" + edTTN.getText().toString().replaceAll(" ", "").toString() + ")");
    call = service.trackTtn(/*edTTN.getText().toString().replaceAll(" ", "")*/ttn.replaceAll(" ", ""), GlobalApplicationData.getCurrentLanguage());
    //Log.d("TrackTTN", call.request().url().encodedQuery());
    call.enqueue(
      new Callback<TrackTtn>() {
        @Override
        public void onResponse(Call<TrackTtn> call, Response<TrackTtn> response) {

          btnGetInfo.setEnabled(true);

          TrackTtn track = response.body();
          if (track!=null && !track.isError()) {
            //Log.d(TAG, "TTN: " + track.getData().getFrom().getCity());
            Intent intent = new Intent(appData.getContext(), TtnDetailsActivity.class);
            intent.putExtra("ttn", track.getData().getNumber());
            intent.putExtra("fromCity", track.getData().getFrom().getCity());
            intent.putExtra("toCity", track.getData().getTo().getCity());
            intent.putExtra("toStoreCode", track.getData().getTo().getStoreCode());
            intent.putExtra("statusId", track.getData().getStatusId());
            intent.putExtra("type", track.getData().getType());
            intent.putExtra("status", track.getData().getStatus());
            intent.putExtra("description", track.getData().getDescription());
            intent.putExtra("dateCreation", track.getData().getDateCreation().getDate());
            intent.putExtra("dateDelivery", track.getData().getDateDelivery().getDate());
            intent.putExtra("seats", track.getData().getSeats());
            intent.putExtra("weight", track.getData().getWeight());
            intent.putExtra("volume", track.getData().getVolume());
            intent.putExtra("cost", track.getData().getCost());
            intent.putExtra("cost_return", track.getData().getCostReturn());
            intent.putExtra("cache_on_delivery", track.getData().getCashOnDelivery());
            intent.putExtra("toPay", track.getData().getToPay());
            intent.putExtra("payer", track.getData().getPayer());
            intent.putExtra("address", track.getData().getTo().getStore().getAddress());
            intent.putExtra("store_type", track.getData().getTo().getStore().getType());
            intent.putExtra("cargo_type", track.getData().getCargoType());

            if (track.getData().getStatusId().compareTo("95") != 0) {
              if (GlobalApplicationData.getCurrentLanguage().compareTo("ru") == 0)
                doRequestOtheLang(track.getData().getNumber(),
                  track.getData().getFrom().getCity(), "",
                  track.getData().getTo().getCity(), "",
                  track.getData().getStatus(), "",
                  track.getData().getType(), "",
                  track.getData().getPayer(), "",
                  track.getData().getTo().getStore().getAddress(), "",
                  track.getData().getCargoType(), ""
                );
              else
                doRequestOtheLang(track.getData().getNumber(),
                  "", track.getData().getFrom().getCity(),
                  "", track.getData().getTo().getCity(),
                  "", track.getData().getStatus(),
                  "", track.getData().getType(),
                  "", track.getData().getPayer(),
                  "", track.getData().getTo().getStore().getAddress(),
                  "", track.getData().getCargoType()
                );

              if (dialog!=null && dialog.isShowing())
              {
                dialog.dismiss();
                dialog = null;
              }
              tvTrackTtnButton.setEnabled(true);
              backFromTrackTTN();
              actionView = RESULT_FROM_TTN_DETAILS;
              startActivityForResult(intent, RESULT_FROM_TTN_DETAILS);

            }
            else
            {
              if (dialog!=null && dialog.isShowing())
              {
                dialog.dismiss();
                dialog = null;
              }
              tvTrackTtnButton.setEnabled(true);
              backFromTrackTTN();
              showMessage(getResources().getString(R.string.dialog_title),
                getResources().getString(R.string.declaration_removed));
            }

          }
          else {
            //Log.d(TAG, "TTN error: " + track.getErrorMessage());
            if (track!=null) {
              if (dialog!=null && dialog.isShowing())
              {
                dialog.dismiss();
                dialog = null;
              }
              if (track.getErrorMessage().compareToIgnoreCase("Service Unavailable") == 0)
                showMessage(getResources().getString(R.string.dialog_title),
                  getResources().getString(R.string.try_later));
              else
                showMessage(getResources().getString(R.string.dialog_title),
                  getResources().getString(R.string.ttn_not_found));
            }
            else
            {
              if (dialog!=null && dialog.isShowing())
              {
                dialog.dismiss();
                dialog = null;
              }
              showMessage(getResources().getString(R.string.dialog_title),
                getResources().getString(R.string.try_later));
            }
          }
                  /*try {
                    Log.d("TrackTTN", response.body().string());
                  } catch (IOException e) {
                    e.printStackTrace();
                  }*/
        }

        @Override
        public void onFailure(Call<TrackTtn> call, Throwable t) {
          //call.request().
          //Log.d(TAG, "Error: " + t.getMessage());
          if (dialog!=null && dialog.isShowing())
          {
            dialog.dismiss();
            dialog = null;
          }
          if (activityPaused==false) {
            if (call != null && !call.isCanceled())
              showMessage(getResources().getString(R.string.dialog_title),
                getResources().getString(R.string.ttn_not_found));
            btnGetInfo.setEnabled(true);
          }
        }

      }
    );

  }


  private void getTtnList(String apiKey) {

    Call<TTNList> call;
    Retrofit retrofit = new Retrofit.Builder()
      .baseUrl("http://esb.intime.ua:8080/services/intime_auth_rest/")
      .addConverterFactory(GsonConverterFactory.create())
      //.client(GlobalApplicationData.getUnsafeOkHttpClient())
      .build();

    IRtfAPI service = retrofit.create(IRtfAPI.class);
    PostGetTtnByApiKeyRest getTTN = new PostGetTtnByApiKeyRest();
    getTTN.apiKey = apiKey;
    getTTN.where = GlobalApplicationData.getInstance().filterQueryAPI;
    getTTN.order = "DATE_CREATION DESC";
    getTTN.rowStart = "0";
    getTTN.rowEnd = "20";
    call = service.ttnHistory("application/json",new GetTTnList(getTTN));
    call.enqueue(
      new Callback<TTNList>() {
        @Override
        public void onResponse(Call<TTNList> call, Response<TTNList> response) {
          TTNList ttnList = response.body();
          if (ttnList!=null && ttnList.entriesGetTtnByApiKey.entryGetTtnByApiKey!=null)
          {
            //Log.d(TAG, "Insert Begin");
            try {
              for (EntryGetTtnByApiKey ttnInfo  : ttnList.entriesGetTtnByApiKey.entryGetTtnByApiKey)
              {
                if (InTimeDB.getInstance(mContext).tableTTNHistory.existDB(ttnInfo.ID)==false) {
                  if (ttnInfo.StatusId.compareTo("95") != 0) {
                    InTimeDB.getInstance(mContext).tableTTNHistory.insert(
                      ttnInfo.ID,
                      ttnInfo.DeclNum,
                      ttnInfo.DeteDelivery,
                      ttnInfo.DateCreation,
                      ttnInfo.LocalitySenderRu,
                      ttnInfo.LocalitySender,
                      ttnInfo.LocalityReceiverRu,
                      ttnInfo.LocalityReceiver,
                      ttnInfo.StatusNameRu,
                      ttnInfo.SiteNameUkr,
                      ttnInfo.Cost,
                      ttnInfo.PaidAmount,
                      ttnInfo.Cod,
                      ttnInfo.CostReturn,
                      ttnInfo.StatusId,
                      ttnInfo.SenderWarehouseId,
                      ttnInfo.FormId,
                      ttnInfo.fORMNAMERU,
                      ttnInfo.fORMNAMERU,
                      "", // description,
                      ttnInfo.SeatsNum,
                      ttnInfo.Weight,
                      ttnInfo.PayerType,
                      ttnInfo.PayerType,
                      ttnInfo.ReceiverAddress,
                      ttnInfo.ReceiverAddress,
                      ttnInfo.GoodsTypeId,
                      ttnInfo.GoodsTypeId,
                      ttnInfo.gSize,
                      HistoryTtnTable.StatusView.DEFAULT
                    );
                  }
                }
                else {
                    InTimeDB.getInstance(mContext).tableTTNHistory.update(
                      ttnInfo.ID,
                      ttnInfo.DeclNum,
                      ttnInfo.DeteDelivery,
                      ttnInfo.DateCreation,
                      ttnInfo.LocalitySenderRu,
                      ttnInfo.LocalitySender,
                      ttnInfo.LocalityReceiverRu,
                      ttnInfo.LocalityReceiver,
                      ttnInfo.StatusNameRu,
                      ttnInfo.SiteNameUkr,
                      ttnInfo.Cost,
                      ttnInfo.PaidAmount,
                      ttnInfo.Cod,
                      ttnInfo.CostReturn,
                      ttnInfo.StatusId,
                      ttnInfo.SenderWarehouseId,
                      ttnInfo.FormId,
                      ttnInfo.DeclType,
                      ttnInfo.DeclType,
                      "", // description,
                      ttnInfo.SeatsNum,
                      ttnInfo.Weight,
                      ttnInfo.PayerType,
                      ttnInfo.PayerType,
                      ttnInfo.ReceiverAddress,
                      ttnInfo.ReceiverAddress,
                      ttnInfo.GoodsTypeId,
                      ttnInfo.GoodsTypeId,
                      ttnInfo.gSize,
                      HistoryTtnTable.StatusView.DEFAULT
                    );
                }
              }

            }
            finally {
              //Log.d(TAG, "Insert end");
            }
          }
          if (!activityPaused)
            initTTNHistoryRecyclerView();
        }

        @Override
        public void onFailure(Call<TTNList> call, Throwable t) {
          //frPreCalcSum1.setVisibility(View.GONE);
          Log.d(TAG, "TTN History Error: " + t.getMessage());
        }

      }
    );
  }

  private void loadPageTtnList(String apiKey, int position) {

    loadList = true;
    Call<TTNList> call;
    Retrofit retrofit = new Retrofit.Builder()
      .baseUrl("http://esb.intime.ua:8080/services/intime_auth_rest/")
      .addConverterFactory(GsonConverterFactory.create())
      //.client(GlobalApplicationData.getUnsafeOkHttpClient())
      .build();

    IRtfAPI service = retrofit.create(IRtfAPI.class);
    PostGetTtnByApiKeyRest getTTN = new PostGetTtnByApiKeyRest();
    getTTN.apiKey = apiKey;
    getTTN.where = GlobalApplicationData.getInstance().filterQueryAPI;
    getTTN.order = "DATE_CREATION DESC";
    getTTN.rowStart = Integer.toString(position);
    getTTN.rowEnd = Integer.toString(position + TtnHistoryRvCursorAdapter.PAGE_SIZE);
    Log.d("TTNList", "Start position:" + getTTN.rowStart + " End position: " + getTTN.rowEnd);
    call = service.ttnHistory("application/json",new GetTTnList(getTTN));
    call.enqueue(
      new Callback<TTNList>() {
        @Override
        public void onResponse(Call<TTNList> call, Response<TTNList> response) {
          TTNList ttnList = response.body();
          if (ttnList!=null && ttnList.entriesGetTtnByApiKey.entryGetTtnByApiKey!=null)
          {
            FillTableTask fillTask = new FillTableTask(ttnList);
            fillTask.execute();
            //Log.d(TAG, "Insert Begin");
//            try {
//              for (EntryGetTtnByApiKey ttnInfo  : ttnList.entriesGetTtnByApiKey.entryGetTtnByApiKey)
//              {
//                if (/*DictionariesDB.getInstance(mContext).tableTTNHistory.existDB(ttnInfo.ID)==false &&*/
//                  DictionariesDB.getInstance(mContext).tableDeletedTtn.existDB(ttnInfo.ID)==false
//                  ) {
//                  DictionariesDB.getInstance(mContext).tableTTNHistory.insert(
//                    ttnInfo.ID,
//                    ttnInfo.DeclNum,
//                    ttnInfo.DeteDelivery,
//                    ttnInfo.DateCreation,
//                    ttnInfo.LocalitySenderRu,
//                    ttnInfo.LocalitySender,
//                    ttnInfo.LocalityReceiverRu,
//                    ttnInfo.LocalityReceiver,
//                    ttnInfo.StatusNameRu,
//                    ttnInfo.SiteNameUkr,
//                    ttnInfo.Cost,
//                    ttnInfo.StatusId,
//                    0
//                  );
//                }
//              }
//
//            }
//            finally {
//              //Log.d(TAG, "Insert end");
//            }
          }
//          if (!activityPaused) {
//            frPaginationProgress.setVisibility(View.GONE);
//            ttnHistoryAdapter.updatePage(ttnList.entriesGetTtnByApiKey.entryGetTtnByApiKey.size());
//            //initTTNHistoryRecyclerView();
//          }
        }

        @Override
        public void onFailure(Call<TTNList> call, Throwable t) {
          //frPreCalcSum1.setVisibility(View.GONE);
          frPaginationProgress.setVisibility(View.GONE);
          Log.d(TAG, "TTN History Error: " + t.getMessage());
          loadList = false;
        }

      }
    );
  }

  class FillTableTask extends AsyncTask<Void, Void, Void>
  {

    TTNList ttnList;

    public FillTableTask(TTNList ttnList)
    {
      this.ttnList = ttnList;
    }

    @Override
    protected Void doInBackground(Void... params) {
      try {
        for (EntryGetTtnByApiKey ttnInfo  : ttnList.entriesGetTtnByApiKey.entryGetTtnByApiKey)
        {
          if (InTimeDB.getInstance(mContext).tableTTNHistory.existDB(ttnInfo.ID)==false) {
            if (ttnInfo.StatusId.compareTo("95") != 0) {
              InTimeDB.getInstance(mContext).tableTTNHistory.insert(
                ttnInfo.ID,
                ttnInfo.DeclNum,
                ttnInfo.DeteDelivery,
                ttnInfo.DateCreation,
                ttnInfo.LocalitySenderRu,
                ttnInfo.LocalitySender,
                ttnInfo.LocalityReceiverRu,
                ttnInfo.LocalityReceiver,
                ttnInfo.StatusNameRu,
                ttnInfo.SiteNameUkr,
                ttnInfo.Cost,
                ttnInfo.PaidAmount,
                ttnInfo.Cod,
                ttnInfo.CostReturn,
                ttnInfo.StatusId,
                ttnInfo.SenderWarehouseId,
                ttnInfo.FormId,
                ttnInfo.DeclType,
                ttnInfo.DeclType,
                "", // description,
                ttnInfo.SeatsNum,
                ttnInfo.Weight,
                ttnInfo.PayerType,
                ttnInfo.PayerType,
                ttnInfo.ReceiverAddress,
                ttnInfo.ReceiverAddress,
                ttnInfo.GoodsTypeId,
                ttnInfo.GoodsTypeId,
                ttnInfo.gSize,
                HistoryTtnTable.StatusView.DEFAULT);
            }
          } else {
            InTimeDB.getInstance(mContext).tableTTNHistory.update(
                ttnInfo.ID,
                ttnInfo.DeclNum,
                ttnInfo.DeteDelivery,
                ttnInfo.DateCreation,
                ttnInfo.LocalitySenderRu,
                ttnInfo.LocalitySender,
                ttnInfo.LocalityReceiverRu,
                ttnInfo.LocalityReceiver,
                ttnInfo.StatusNameRu,
                ttnInfo.SiteNameUkr,
                ttnInfo.Cost,
                ttnInfo.PaidAmount,
                ttnInfo.Cod,
                ttnInfo.CostReturn,
                ttnInfo.StatusId,
                ttnInfo.SenderWarehouseId,
                ttnInfo.FormId,
                ttnInfo.DeclType,
                ttnInfo.DeclType,
                "", // description,
                ttnInfo.SeatsNum,
                ttnInfo.Weight,
                ttnInfo.PayerType,
                ttnInfo.PayerType,
                ttnInfo.ReceiverAddress,
                ttnInfo.ReceiverAddress,
                ttnInfo.GoodsTypeId,
                ttnInfo.GoodsTypeId,
                ttnInfo.gSize,
                HistoryTtnTable.StatusView.DEFAULT);
          }
        }
      }
      finally {
        //Log.d(TAG, "Insert end");
      }

      return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
      super.onPostExecute(aVoid);
      if (!activityPaused) {
        ttnHistoryAdapter.updatePage(ttnList.entriesGetTtnByApiKey.entryGetTtnByApiKey.size());
        frPaginationProgress.setVisibility(View.GONE);
        //initTTNHistoryRecyclerView();
      }
      loadList = false;
    }
  }


}
