package com.ua.inr0001.intime;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import intime.intime.mylistview.MyListView;
import intime.llc.ua.R;
import intime.map.builds.route.IBuildRoute;
import intime.utils.DisplayUnits;
import intime.utils.MyDateUtils;

import static com.ua.inr0001.intime.DictionariesDB.getColumnValue;
import static java.lang.Math.round;

/**
 * Created by technomag on 30.05.17.
 */

public class DepartmentDetailsView {

  private Context context;

  private TextView tvDepNumber;
  private TextView tvDepCity;
  private TextView tvDepAddress;
  private TextView tvDepDistance;
  private TextView tvDepWorkStatus;
  private TextView tvDepParams;
  private LinearLayout llTopInfo;
  private LinearLayout llDepCallCenter;
  private LinearLayout lldepNestedScroll;
  private MyListView lvWorkHours;

  private LinearLayout llWhIcon;

  private TextView tvDepDayIn;
  private MyListView lvPhones;
  private LinearLayout frPhones;
  private String [] phone;
  private String depNumber;
  private String Lat = "47.860460";
  private String Lng = "35.101626";
  private double meLat;
  private double meLng;
  //private String code;
  //private String depType;
  //private boolean over30;
  private View view;
  private IBuildRoute route;
  private int topBarHeight;

  public int getTopBarHeight()
  {
    return (int) DisplayUnits.pxToDp(context, (int ) DisplayUnits.dpToPx(context, llTopInfo.getMeasuredHeight()));
  }

  public DepartmentDetailsView(Context context, /*AppCompatActivity activity,*/ IBuildRoute buildRoute, View view)
  {
    this.context = context;
//    this.activity = activity;
    this.view = view;
    route = buildRoute;
//    if (this.activity!=null)
//      initDepetailsActivity();
//    else
      initDepetailsView();
  }

  public void setMyLocation(double longitude, double latitude)
  {
    meLat = latitude;
    meLng = longitude;
  }

  private void initDepetailsView()
  {
    if (!GlobalApplicationData.getInstance().IsOnline()) {
      lldepNestedScroll = (LinearLayout) view.findViewById(R.id.depNestedScroll);
      ViewGroup.LayoutParams lp = lldepNestedScroll.getLayoutParams();
      lp.height = -1;
      lldepNestedScroll.setLayoutParams(lp);
    }
    llWhIcon = (LinearLayout) view.findViewById(R.id.llWhIcon);
    llWhIcon.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (route != null)
        route.getRoute();
      }
    });

    tvDepNumber = (TextView) view.findViewById(R.id.tvDepNumber);
    tvDepAddress = (TextView) view.findViewById(R.id.tvDepAddress);
    tvDepCity = (TextView) view.findViewById(R.id.tvDepCity);
    tvDepDistance = (TextView) view.findViewById(R.id.tvDepDistance);
    tvDepParams = (TextView) view.findViewById(R.id.tvDepParamsDetails);

    tvDepWorkStatus = (TextView) view.findViewById(R.id.tvDepStatus);
    tvDepDayIn = (TextView) view.findViewById(R.id.tvDayIn);

    frPhones = (LinearLayout) view.findViewById(R.id.frPhones);
    lvPhones = (MyListView) view.findViewById(R.id.lvPhones);
    lvWorkHours = (MyListView) view.findViewById(R.id.lvWorkHours);
    llTopInfo = (LinearLayout) view.findViewById(R.id.depDetailsTopLayerInfo);
    llDepCallCenter = (LinearLayout) view.findViewById(R.id.llDepCallCenter);
    llDepCallCenter.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + context.getResources().getString(R.string.contacts_call_center)));
        context.startActivity(intent);
      }
    });
  }

//  private void initDepetailsActivity()
//  {
//    llWhIcon = (LinearLayout) activity.findViewById(R.id.llWhIcon);
//    llWhIcon.setOnClickListener(new View.OnClickListener() {
//      @Override
//      public void onClick(View v) {
//        route.getRoute();
//      }
//    });
//
//    tvDepNumber = (TextView) activity.findViewById(R.id.tvDepNumber);
//    tvDepAddress = (TextView) activity.findViewById(R.id.tvDepAddress);
//    tvDepCity = (TextView) activity.findViewById(R.id.tvDepCity);
//    tvDepDistance = (TextView) activity.findViewById(R.id.tvDepDistance);
//
//    tvDepWorkStatus = (TextView) activity.findViewById(R.id.tvDepStatus);
//    tvDepDayIn = (TextView) activity.findViewById(R.id.tvDayIn);
//
//    frPhones = (LinearLayout) activity.findViewById(R.id.frPhones);
//    lvPhones = (MyListView) activity.findViewById(R.id.lvPhones);
//    lvWorkHours = (MyListView) activity.findViewById(R.id.lvWorkHours);
//    llTopInfo = (LinearLayout) activity.findViewById(R.id.depDetailsTopLayerInfo);
//    llDepCallCenter = (LinearLayout) activity.findViewById(R.id.llDepCallCenter);
//    llDepCallCenter.setOnClickListener(new View.OnClickListener() {
//      @Override
//      public void onClick(View view) {
//        Intent intent = new Intent(Intent.ACTION_DIAL);
//        intent.setData(Uri.parse("tel:" + context.getResources().getString(R.string.contacts_call_center)));
//        context.startActivity(intent);
//      }
//    });
//  }

  public void selectDepDetails(String code)
  {
    //llTopInfo.animate().translationX(0).translationX(-llTopInfo.getWidth()).setDuration(2000).start();
    if (GlobalApplicationData.getCurrentLanguage().compareTo("ru") == 0)
      setWarehouseData(DictionariesDB.getInstance(context).tableWarehouseDetails.selectWarehouseByCode(code));
    else
      setWarehouseData(DictionariesDB.getInstance(context).tableWarehouseDetails.selectWarehouseByCodeUA(code));
  }

  private String formatPhone(final String phone)
  {
    StringBuilder fPhone = new StringBuilder();
    fPhone.append("+");
    fPhone.append(phone);
    fPhone.insert(3, " ");
    fPhone.insert(7, " ");
    fPhone.insert(11, " ");
    fPhone.insert(14, " ");
    return fPhone.toString();
  }

  private void setPhones(String phones)
  {
    try{
      phone = phones.split(";");

      String[] from = { "phone" };
      // массив ID View-компонентов, в которые будут вставлять данные
      int[] to = { R.id.tvPhone1 };
      ArrayList<Map<String, String>> data = new ArrayList<Map<String, String>>(
        phone.length);
      Map<String, String> m;
      for (int i = 0; i < phone.length; i++) {
        if (phone[i].length()>0) {
          m = new HashMap<String, String>();
          m.put("phone", formatPhone(phone[i]));
          data.add(m);
        }
      }

      if (data.size()>0) {
        SimpleAdapter sAdapter = new SimpleAdapter(context, data, R.layout.phones_list_item,
          from, to);

        lvPhones.setAdapter(sAdapter);
        lvPhones.setOnItemClickListener(new AdapterView.OnItemClickListener() {
          @Override
          public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            try {
              Intent intent = new Intent(Intent.ACTION_DIAL);
              intent.setData(Uri.parse("tel:+" + phone[position]));
              view.getContext().startActivity(intent);
            } catch (Exception e)
            {
              Log.d("Phone1", e.getMessage());
            }
          }
        });

        frPhones.setVisibility(View.VISIBLE);
      }
    }catch(Exception e)
    {
      Log.d("Test", e.toString());
    }
  }

  private void calcDistance(double latitude1, double longitude1, double latitude2, double longitude2)
  {
    double dist = DistanceCalculator.distance(latitude1, longitude1,
      latitude2, longitude2);
    tvDepDistance.setText("~" + Long.toString(round(dist/1000)) + " км");
    tvDepDistance.setVisibility(View.VISIBLE);
  }

  private int getWeekDay()
  {
    Calendar calendar = Calendar.getInstance();

    int now = calendar.get(Calendar.DAY_OF_WEEK);
    //int day = calendar.get(Calendar.DAY_OF_WEEK) + 1;
    // vs = 1
    // pn = 2
    // vt = 3
    // sr = 4
    // cht = 5
    // pt = 6
    // sb = 7
    int []weekd = {0, 7, 1 , 2 , 3 , 4, 5, 6};
    //if (day>weekd.length-1)
    //  day = 1;
    return weekd[now];
  }

  private void setWeightLength(Cursor cursor, TextView tvCity)
  {
    if (tvCity != null) {
      StringBuilder sbWLengthWeight = new StringBuilder();
      String weight = DictionariesDB.getColumnValue(cursor, WarehouseDetailsTable.Column.CAPACITY);
      if (weight != null && weight.length() > 0)
        sbWLengthWeight.append("до " + weight + " кг");
      String length = DictionariesDB.getColumnValue(cursor, WarehouseDetailsTable.Column.MAX_LENGTH);
      if (length != null && length.length() > 0)
        sbWLengthWeight.append(" / до " + length + " м");

      if (sbWLengthWeight.length() > 0) {
        tvCity.setText(sbWLengthWeight.toString());
        tvCity.setVisibility(View.VISIBLE);
      } else
        tvCity.setVisibility(View.GONE);
    }
  }

  private void setCity(Cursor cursor ,TextView tvCity)
  {
    String cityName = DictionariesDB.getColumnValue(cursor, WarehouseDetailsTable.Column.CITY);
    String state = DictionariesDB.getColumnValue(cursor, WarehouseDetailsTable.Column.STATE);
    int dublicate = Integer.parseInt(DictionariesDB.getColumnValue(cursor, WarehouseDetailsTable.Column.DUBLICATE_CITY));
    if (tvCity != null) {
      if (dublicate == 1) {
        tvCity.setText(cityName + ", " + state);
      } else
        tvCity.setText(cityName);
    }
  }

  private void setWarehouseData(Cursor cursor)
  {
    String tempString;
    cursor.moveToFirst();
    depNumber = DictionariesDB.getColumnValue(cursor, WarehouseDetailsTable.Column.NUMBER);
    if (DictionariesDB.getColumnValue(cursor, WarehouseDetailsTable.Column.TYPE).equalsIgnoreCase("postmat")) {
      frPhones.setVisibility(View.GONE);
      tvDepNumber.setText(
        context.getResources().getString(R.string.text_postomat) + " №" + depNumber );
    }
    else {
      setPhones(getColumnValue(cursor, WarehouseDetailsTable.Column.PHONE));
      tvDepNumber.setText(
        context.getResources().getString(R.string.text_department) + " №" + depNumber );
    }
//    if (activity!=null)
//      activity.setTitle(tvDepNumber.getText().toString());

      setWeightLength(cursor, tvDepParams);
      setCity(cursor, tvDepCity);

    tvDepAddress.setText(getColumnValue(cursor, WarehouseDetailsTable.Column.ADDRESS));

    tempString = getColumnValue(cursor, "DayIn" + Integer.toString(getWeekDay()));
    if (tempString!=null &&
      tempString.length()>0) {
      tvDepDayIn.setText(context.getResources().getString(R.string.day_in) + " " + tempString);
      tvDepDayIn.setVisibility(View.VISIBLE);
    }

    DepartmentsRvAdapter.setworkStatusText(context, tvDepWorkStatus, MyDateUtils.getWorhHoursStart(cursor),
      MyDateUtils.getWorhHoursEnd(cursor),
      MyDateUtils.getNextWorhHours(cursor));

    setWorkHoursEveryDay(cursor);
    Lat = getColumnValue(cursor, WarehouseDetailsTable.Column.LAT);
    Lng = getColumnValue(cursor, WarehouseDetailsTable.Column.LNG);
    try{
      if (meLat!=0 && meLng!=0)
        calcDistance(Double.parseDouble(Lat),
          Double.parseDouble(Lng),
          meLat, meLng);
    }
    catch (Exception e)
    {
      Log.d("Test", e.getMessage());
    }
    cursor.close();
    llTopInfo.postDelayed(new Runnable() {
      @Override
      public void run() {
        topBarHeight = (int) DisplayUnits.pxToDp(context, (int ) DisplayUnits.dpToPx(context, llTopInfo.getMeasuredHeight()));
      }
    }, 100);
  }

//  private String getWorkDatRange(int startPos, int endpos)
//  {
//    String[] sWorkDays = context.getResources().getStringArray(R.array.work_days);
//    if (startPos!=endpos)
//      return sWorkDays[startPos] + " - " + sWorkDays[endpos];
//    else
//      return sWorkDays[startPos];
//  }

  private void setWorkHoursEveryDay(Cursor cursor)
  {
    ArrayList<String> workHours = new ArrayList<String>();
    String[] workDays = context.getResources().getStringArray(R.array.work_days);
    String curStartTime = "";
    String curEndTime = "";
    for (int i=1; i<=7; i++)
    {
      curStartTime = getColumnValue(cursor, "Open" + Integer.toString(i));
      curEndTime = getColumnValue(cursor, "Close" + Integer.toString(i));
      if (curStartTime==null)
        curStartTime = "null";
      if (curEndTime==null)
        curEndTime = "null";
      workHours.add(curStartTime + " - " + curEndTime);
    }
    try{
      String[] from = { "workday", "workhour" };
      int[] to = { R.id.tvWorkDay, R.id.tvWorkHour };
      ArrayList<Map<String, String>> data = new ArrayList<Map<String, String>>(workHours.size());
      int i=0;
      for (String work : workHours) {
        Map<String, String> m = new HashMap<String, String>();
        m.put("workday", workDays[i]);
        if (work.contains("null"))
          m.put("workhour", context.getResources().getString(R.string.weekend));
        else
          m.put("workhour", work);
        data.add(m);
        i++;
      }
      SimpleAdapter sAdapter = new SimpleAdapter(context, data, R.layout.work_hours_list_item,
        from, to);
      lvWorkHours.setAdapter(sAdapter);
      lvWorkHours.setVisibility(View.VISIBLE);
    }catch(Exception e)
    {
      Log.d("Test", e.toString());
    }
  }

//  private void setWorkHours(Cursor cursor)
//  {
//    ArrayList<String> workHours = new ArrayList<String>();
//    ArrayList<Integer> workDays = new ArrayList<Integer>();
//    String curStartTime = "";
//    String curEndTime = "";
//    String prevStartTime = "";
//    String prevEndTime = "";
//    for (int i=1; i<=7; i++)
//    {
//      curStartTime = getColumnValue(cursor, "Open" + Integer.toString(i));
//      curEndTime = getColumnValue(cursor, "Close" + Integer.toString(i));
//      if (curStartTime==null)
//        curStartTime = "null";
//      if (curEndTime==null)
//        curEndTime = "null";
//      if (curStartTime.compareToIgnoreCase(prevStartTime)!=0 ||
//        curEndTime.compareToIgnoreCase(prevEndTime)!=0){
//        workHours.add(curStartTime + " - " + curEndTime);
//        workDays.add(i-1);
//      }
//      prevStartTime = curStartTime;
//      prevEndTime = curEndTime;
//    }
//    try{
//      String[] from = { "workday", "workhour" };
//      int[] to = { R.id.tvWorkDay, R.id.tvWorkHour };
//      ArrayList<Map<String, String>> data = new ArrayList<Map<String, String>>(
//        workHours.size());
//      Map<String, String> m;
//      int i=0;
//      for (String work : workHours) {
//        if (work!=null && work.length()>0) {
//          int start = workDays.get(i);
//          int end =0;
//          try {
//            end = workDays.get(i + 1);
//          }
//          catch (Exception e)
//          {
//            end = 7;
//          }
//          m = new HashMap<String, String>();
//          m.put("workday", getWorkDatRange(start, end-1));
//          if (work.contains("null"))
//            m.put("workhour", context.getResources().getString(R.string.weekend));
//          else
//            m.put("workhour", work);
//          data.add(m);
//          i++;
//        }
//      }
//
//      SimpleAdapter sAdapter = new SimpleAdapter(context, data, R.layout.work_hours_list_item,
//        from, to);
//
//      lvWorkHours.setAdapter(sAdapter);
//      lvWorkHours.setVisibility(View.VISIBLE);
//    }catch(Exception e)
//    {
//      Log.d("Test", e.toString());
//    }
//
//  }

}
