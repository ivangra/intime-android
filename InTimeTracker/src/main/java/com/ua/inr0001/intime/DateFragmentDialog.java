package com.ua.inr0001.intime;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import intime.llc.ua.R;
import intime.utils.MyDateFormatSymbols;

public class DateFragmentDialog extends DialogFragment
{
    Handler mHandler;
	    int mDay;
	    int mMonth;
	    int mYear;

			private String humanStringFormat = "d MMMM, EEEE";
			private String systemStringFormat = "yyyy-MM-dd";

			public void setHumanStringFormat(String format)
			{
				humanStringFormat = format;
			}

			public void setSystemStringFormat(String format)
			{
				systemStringFormat = format;
			}

			public DateFragmentDialog() {

			}

			@Override
			public void setArguments(Bundle args) {
				super.setArguments(args);
			}

			public void setHandler(Handler h)
			{
				mHandler = h;
			}

			/*public DateFragmentDialog(Handler h){
					super();
	        mHandler = h;
	    }*/
      
      @Override
      public void onStart() {
          super.onStart();
          final Resources res = getResources();
          final int yellow = res.getColor(android.R.color.holo_blue_light);

          // Title
          final int titleId = res.getIdentifier("alertTitle", "id", "android");
          final View title = getDialog().findViewById(titleId);
          if (title != null) {
              ((TextView) title).setTextColor(yellow);
          }

          // Title divider
          final int titleDividerId = res.getIdentifier("titleDivider", "id", "android");
          final View titleDivider = getDialog().findViewById(titleDividerId);
          if (titleDivider != null) {
              titleDivider.setBackgroundColor(yellow);
          }
          
          // selection divider
          final int selDividerId = res.getIdentifier("selectionDivider", "id", "android");
          final View selDivider = getDialog().findViewById(selDividerId);
          if (selDivider != null) {
              selDivider.setBackgroundColor(yellow);
          }
      }
      
	    @Override
	    public Dialog onCreateDialog(Bundle savedInstanceState){
	 
	        Calendar cal = Calendar.getInstance(Locale.getDefault());
	        
	        mDay = cal.get(Calendar.DAY_OF_MONTH);
	        mMonth = cal.get(Calendar.MONTH);
	        mYear = cal.get(Calendar.YEAR);
	 
	        DatePickerDialog.OnDateSetListener listener  = new DatePickerDialog.OnDateSetListener() {
	            @Override
	            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
	                mDay = dayOfMonth;
	                mMonth = monthOfYear;
	                mYear = year;
	                Bundle b = new Bundle();
	                b.putInt("set_day", mDay);
	                b.putInt("set_month", mMonth+1);
	                b.putInt("set_year", mYear);
	                
	                Calendar cal1 = Calendar.getInstance(Locale.getDefault());
	                cal1.set(Calendar.DATE, mDay);
	                cal1.set(Calendar.MONTH, mMonth);
	                cal1.set(Calendar.YEAR, mYear);
									SimpleDateFormat sdf = new SimpleDateFormat(humanStringFormat,
                    new MyDateFormatSymbols(getContext()));
	                b.putString("set_date", sdf.format(cal1.getTime()));

									SimpleDateFormat sdf1 = new SimpleDateFormat(systemStringFormat,
										new MyDateFormatSymbols(getContext()));
									b.putString("set_date_request", sdf1.format(cal1.getTime()));

	                Message m = new Message();
	                m.setData(b);
	                mHandler.sendMessage(m);
	            }
	        };
					DatePickerDialog dpd = new DatePickerDialog(getActivity(), R.style.TimePicker, listener, mYear, mMonth, mDay);
					dpd.getDatePicker().setMinDate(cal.getTimeInMillis());
	        return dpd;
	    }
}
