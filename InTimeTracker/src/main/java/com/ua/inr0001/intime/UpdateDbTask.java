package com.ua.inr0001.intime;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;

import intime.api.model.citylist.CityList;
import intime.api.model.citylist.Datum;
import intime.api.model.departments.Departments;
import intime.api.model.packages.Packages;
import intime.api.model.packages.Size;
import intime.llc.ua.R;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class UpdateDbTask extends AsyncTask<String, String, Boolean> {

  private static UpdateDbTask instance;

  private GlobalApplicationData appData;
  private DoneTaskCallBack activity;
  private boolean onlyUpdate = false;

  private DictionariesDB dictionariesDB;
  private IProgressUpdate progressUpdate;
  private String updateStage[];

  private void UpdatePackages() throws Exception
  {

    dictionariesDB.tablePackages.clearTable();

    Log.d("DBUpdate", "Begin load Packages");

    Retrofit retrofit = new Retrofit.Builder()
      .baseUrl("https://newapi.intime.ua/")
      .addConverterFactory(GsonConverterFactory.create())
      //.addConverterFactory(createGsonConverter())
      .client(GlobalApplicationData.getUnsafeOkHttpClient())
      .build();

    IRtfAPI service = retrofit.create(IRtfAPI.class);

    Call<Packages> call = service.packages("ru");

    Response<Packages> respCityList = call.execute();
    Packages cityList = respCityList.body();
    Log.d("DBUpdate", "Packages: " + respCityList.message());

    for (intime.api.model.packages.Datum data : cityList.getData())
    {
      for (Size size : data.getSizes()) {

        try {
          //Log.d("DBUpdate", "Package" + data.getName());

          StringBuilder sb = new StringBuilder();
          for (String ct : data.getCargoTypes())
          {
            sb.append(ct + ";");
          }

          dictionariesDB.tablePackages.insert(
            Integer.toString(data.getId()),
            data.getName(),
            size.getCode(),
            size.getWidth(),
            size.getDepth(),
            size.getHeight(),
            size.getPrice(),
            Integer.toString(data.getOrder()),
            sb.toString(),
            size.getWeight_max(),
            size.getWeight_min()
          );
        } catch (Exception e) {
          Log.d("DBUpdate", "Insert package: " + e.getMessage());
        }
      }
    }


  }

  private void UpdatePackagesUA() throws Exception
  {

    Log.d("DBUpdate", "Begin load Packages UA");

    Retrofit retrofit = new Retrofit.Builder()
      .baseUrl("https://newapi.intime.ua/")
      .addConverterFactory(GsonConverterFactory.create())
      //.addConverterFactory(createGsonConverter())
      .client(GlobalApplicationData.getUnsafeOkHttpClient())
      .build();

    IRtfAPI service = retrofit.create(IRtfAPI.class);

    Call<Packages> call = service.packages("ua");

    Response<Packages> respCityList = call.execute();
    Packages cityList = respCityList.body();
    Log.d("DBUpdate", "Packages UA: " + respCityList.message());

    for (intime.api.model.packages.Datum data : cityList.getData())
    {
      for (Size size : data.getSizes()) {

        try {
          //Log.d("DBUpdate", "Package" + data.getName());

          dictionariesDB.tablePackages.update(
            data.getName(),
            size.getCode()
          );
        } catch (Exception e) {
          Log.d("DBUpdate", "Update package UA: " + e.getMessage());
        }
      }
    }
  }

  private void UpdateCities() throws Exception
  {
    dictionariesDB.tableCity.clearTable();

    Log.d("DBUpdate", "Begin load CityList");

    Retrofit retrofit = new Retrofit.Builder()
      .baseUrl("https://newapi.intime.ua/")
      .addConverterFactory(GsonConverterFactory.create())
      //.addConverterFactory(createGsonConverter())
      .client(GlobalApplicationData.getUnsafeOkHttpClient())
      .build();

    IRtfAPI service = retrofit.create(IRtfAPI.class);

    Call<CityList> call = service.cityList("ru");
    Response<CityList> respCityList = call.execute();
    CityList cityList = respCityList.body();
    Log.d("DBUpdate", "CityList: " + respCityList.message());

    for (Datum data : cityList.getData())
    {
      try {
        dictionariesDB.tableCity.insert(
          data.getName(),
          data.getCode(),
          data.getWarehouse_id(),
          data.getState(),
          data.getArea(),
          data.getHas_store(),
          data.getHas_postmat(),
          data.getCity_type(),
          data.getHits()
        );
      }
      catch(Exception e)
      {
        Log.d("DBUpdate", "insert city " +  e.getMessage());
      }
    }
    Log.d("DBUpdate", "CityList insert finish");
  }

  private void UpdateCitiesUA() throws Exception
  {
    dictionariesDB.tableCity.clearTableUA();

    Log.d("DBUpdate", "Begin load CityList UA");
    Retrofit retrofit = new Retrofit.Builder()
      .baseUrl("https://newapi.intime.ua/")
      .addConverterFactory(GsonConverterFactory.create())
      //.addConverterFactory(createGsonConverter())
      .client(GlobalApplicationData.getUnsafeOkHttpClient())
      .build();

    IRtfAPI service = retrofit.create(IRtfAPI.class);

    Call<CityList> call = service.cityList("ua");
    Response<CityList> respCityList = call.execute();
    CityList cityList = respCityList.body();
    Log.d("DBUpdate", "CityList UA: " + respCityList.message());

    for (Datum data : cityList.getData())
    {
      try {
        dictionariesDB.tableCity.insertUA(
          data.getName(),
          data.getCode(),
          data.getWarehouse_id(),
          data.getState(),
          data.getArea(),
          data.getHas_store(),
          data.getHas_postmat(),
          data.getCity_type(),
          data.getHits()
        );
      }
      catch(Exception e)
      {
        Log.d("DBUpdate", "insert city " +  e.getMessage());
      }
    }
    Log.d("DBUpdate", "CityList UA insert finish");
  }
  
  private void UpdateWarehouses() throws IOException {
    dictionariesDB.tableWarehouseDetails.clearTable();

    Log.d("DBUpdate", "Begin load Warehouses");

    Retrofit retrofit = new Retrofit.Builder()
      .baseUrl("https://newapi.intime.ua/")
      .addConverterFactory(GsonConverterFactory.create())
      //.addConverterFactory(createGsonConverter())
      .client(GlobalApplicationData.getUnsafeOkHttpClient())
      .build();

    IRtfAPI service = retrofit.create(IRtfAPI.class);

    Call<Departments> call = service.stores("ru");

    Response<Departments> respCityList = call.execute();
    Departments cityList = respCityList.body();
    Log.d("DBUpdate", "Warehouses: " + respCityList.message());

    for (intime.api.model.departments.Datum data : cityList.getData())
    {
      try {
        //Log.d("DBUpdate", "Warehouse " + data.getAddress() + " " + data.getCity());
        //Log.d("DBUpdate", "Package" + data.getName());
        dictionariesDB.tableWarehouseDetails.insert(
          Integer.toString(data.getCode()),
          data.getLat(),
          data.getLon(),
          data.getType(),
          Integer.toString(data.getType_id()),
          Integer.toString(data.getNumber()),
          data.getCapacity(),
          data.getCity(),
          Integer.toString(data.getCityType()),
          data.getState(),
          data.getDistrict(),
          data.getAddress(),
          data.getPhonesString(),
          data.getMax_length(),
          data.getDayInDay().get1(),
          data.getDayInDay().get2(),
          data.getDayInDay().get3(),
          data.getDayInDay().get4(),
          data.getDayInDay().get5(),
          data.getDayInDay().get6(),
          data.getDayInDay().get7(),
          data.getScheduleD1O(),
          data.getScheduleD1C(),
          data.getScheduleD2O(),
          data.getScheduleD2C(),
          data.getScheduleD3O(),
          data.getScheduleD3C(),
          data.getScheduleD4O(),
          data.getScheduleD4C(),
          data.getScheduleD5O(),
          data.getScheduleD5C(),
          data.getScheduleD6O(),
          data.getScheduleD6C(),
          data.getScheduleD7O(),
          data.getScheduleD7C()
        );
      }
      catch(Exception e)
      {
        Log.d("DBUpdate", "insert Warehouse " +  e.getMessage());
      }
    }

  }

  private void UpdateWarehousesUA() throws IOException {
    dictionariesDB.tableWarehouseDetails.clearTableUA();

    Log.d("DBUpdate", "Begin load Warehouses");

    Retrofit retrofit = new Retrofit.Builder()
      .baseUrl("https://newapi.intime.ua/")
      .addConverterFactory(GsonConverterFactory.create())
      //.addConverterFactory(createGsonConverter())
      .client(GlobalApplicationData.getUnsafeOkHttpClient())
      .build();

    IRtfAPI service = retrofit.create(IRtfAPI.class);

    Call<Departments> call = service.stores("ua");

    Response<Departments> respCityList = call.execute();
    Departments cityList = respCityList.body();
    Log.d("DBUpdate", "WarehousesUA: " + respCityList.message());

    for (intime.api.model.departments.Datum data1 : cityList.getData())
    {
      try {
        //Log.d("DBUpdate", "Warehouse " + data.getAddress() + " " + data.getCity());
        //Log.d("DBUpdate", "Package" + data.getName());
        dictionariesDB.tableWarehouseDetails.insertUA(
          Integer.toString(data1.getCode()),
          data1.getCity(),
          data1.getState(),
          data1.getDistrict(),
          data1.getAddress(),
          data1.getType(),
          Integer.toString(data1.getNumber())
        );
      }
      catch(Exception e)
      {
        Log.d("DBUpdate", "insert Warehouse UA" +  e.getMessage());
      }
    }

  }

  private UpdateDbTask(Context context, IProgressUpdate progressUpdate, DoneTaskCallBack activity, boolean onlyUpdate)
  {
    this.activity = activity;
    this.progressUpdate = progressUpdate;
    this.onlyUpdate = onlyUpdate;
    appData = GlobalApplicationData.getInstance();
    updateStage = context.getResources().getStringArray(R.array.update_stages);
  }

  public static UpdateDbTask getInstance(Context context, IProgressUpdate progressUpdate, DoneTaskCallBack activity, boolean onlyUpdate)
  {
    return instance==null ? instance = new UpdateDbTask(context, progressUpdate, activity, onlyUpdate): instance;
  }

  private void updateCities()
  {
    Log.d("DBUpdate", "updateCities");
    try {
      dictionariesDB.tableCity.markDuplicates(CityTable.NAME);
      dictionariesDB.tableCity.markDuplicates(CityTable.NAME_UA);
    } catch (Exception e)
    {
      Log.d("DBUpdate", e.getMessage());
    }
  }

  private void updateWarehouses()
  {
    Log.d("DBUpdate", "updateWarehouses");
    try {
      dictionariesDB.tableWarehouseDetails.markDublicates();
    } catch (Exception e)
    {
      Log.d("DBUpdate", e.getMessage());
    }
  }

  @Override
  protected void onCancelled() {
    super.onCancelled();
    appData.writeDateUpdateDB();
    activity.done();
  }

  @Override
  protected Boolean doInBackground(String... params) {
    try
    {
      //Thread.sleep(1000);

      dictionariesDB = DictionariesDB.getInstance(appData.getContext());
      
      dictionariesDB.beginTransaction();
      try
      {
        if (isCancelled()) {
          dictionariesDB.endTransaction();
          return false;
        }

        if (onlyUpdate == false) {

          publishProgress(updateStage[0], "1");
          UpdatePackages();
          if (isCancelled()) {
            dictionariesDB.endTransaction();
            return false;
          }

          publishProgress(updateStage[1], "2");
          UpdateWarehouses();
          if (isCancelled()) {
            dictionariesDB.endTransaction();
            return false;
          }

          UpdateWarehousesUA();
          if (isCancelled()) {
            dictionariesDB.endTransaction();
            return false;
          }

          publishProgress(updateStage[2], "3");
          UpdateCities();
          if (isCancelled()) {
            dictionariesDB.endTransaction();
            return false;
          }

          UpdateCitiesUA();
          if (isCancelled()) {
            dictionariesDB.endTransaction();
            return false;
          }

          dictionariesDB.tableCity.creatIndex();
          dictionariesDB.tableCity.creatIndexUA();
          dictionariesDB.tableWarehouseDetails.creatIndex();
          dictionariesDB.tableWarehouseDetails.creatIndexUA();
        }

        Log.d("DBUpdate", "Commit db");
        dictionariesDB.commitTransaction();

        if (onlyUpdate == false) {
          UpdatePackagesUA();
          updateCities();
          updateWarehouses();
        }
      }
      catch (Exception e)
      {
        Log.d("DBUpdate", e.getMessage());
        dictionariesDB.endTransaction();
      }
      finally
      {
        dictionariesDB.endTransaction();
      }
      return true;
    }
    catch (Exception e)
    {
      Log.d("XML", e.getMessage().toString());
      return false;
    }
  }

  @Override
  protected void onPostExecute(Boolean result) {
    if (result)
    {
//      Toast.makeText(appData.getContext(), R.string.refresh_db_succerfull, Toast.LENGTH_SHORT).show();
      appData.writeDateUpdateDB(); // write last update date
      instance = null;
      if (activity!=null)
        activity.done();
//      Intent intent = new Intent(CalculationOfShippingCosts.DB_UPDATED);
//      intent.putExtra("DB", true);
//      appData.getContext().sendBroadcast(intent);
    }
    else
    {
      instance = null;
      if (activity!=null)
        activity.done();
    }
  }

  @Override
  protected void onPreExecute() {
    progressUpdate.setMaxProgress(updateStage.length+1);
  }

  @Override
  protected void onProgressUpdate(String... values) {
    if (progressUpdate!=null)
      progressUpdate.update(values);
  }
 
}
