package com.ua.inr0001.intime;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import intime.intime.mylistview.MyListView;
import intime.llc.ua.R;

public class LanguageActivity extends AppCompatActivity {

  private MyListView lvLanguage;
  private int lvSelected = 0;
  private ArrayList<Map<String, Object>> langList = new ArrayList<Map<String, Object>>();
  private SimpleAdapter langListAdapter;
  private Context mContext;

  private void setSendFromAdapter()
  {

    langList.clear();
    String[] sendFromText = GlobalApplicationData.getInstance().getContext().getResources().getStringArray(R.array.languages);

    Map<String, Object> m;
    for (int i = 0; i < sendFromText.length; i++) {
      m = new HashMap<String, Object>();
      m.put("Text", sendFromText[i]);
      langList.add(m);
    }

    String[] from = { "Text" };
    int[] to = { R.id.tvcCargoType};

    langListAdapter = new SimpleAdapter(mContext, langList,
      R.layout.single_choise_item_def, from, to)
    {
      @NonNull
      @Override
      public View getView(int position, View convertView, ViewGroup parent) {
        final View view = super.getView(position, convertView, parent);
        ImageView cargoIcon = (ImageView) view.findViewById(R.id.imageCargoIcon);
        LinearLayout llSelectedItem = (LinearLayout) view.findViewById(R.id.llSingleChoiseItem);
        TextView tvDescription = (TextView) view.findViewById(R.id.tvcCargoType);
        if (position == lvSelected) {
          llSelectedItem.setBackgroundColor(GlobalApplicationData.getInstance().getContext().getResources().getColor(R.color.background_white));
          cargoIcon.setImageResource(R.drawable.ic_list_check);
          tvDescription.setTextColor(GlobalApplicationData.getInstance().getContext().getResources().getColor(R.color.base_text_color));
        }
        else {
          llSelectedItem.setBackgroundColor(GlobalApplicationData.getInstance().getContext().getResources().getColor(R.color.app_background));
          cargoIcon.setImageResource(0);
          tvDescription.setTextColor(GlobalApplicationData.getInstance().getContext().getResources().getColor(R.color.paper_caption));
        }
        return view;
      }
    };

    lvLanguage.setAdapter(langListAdapter);
  }

  private void initSendFrom(final View view)
  {
    //lvLanguage = (MyListView) view.findViewById(R.id.lvLanguage);
    lvLanguage = (MyListView) findViewById(R.id.lvLanguage);
    lvLanguage.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
    lvLanguage.setItemsCanFocus(true);
    setSendFromAdapter();
    lvLanguage.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        lvSelected = position;
        langListAdapter.notifyDataSetChanged();
      }
    });
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_language);
    Toolbar toolbar = (Toolbar) findViewById(R.id.languageToolbar);
    setSupportActionBar(toolbar);
    toolbar.setNavigationOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        onBackPressed();
      }
    });
    mContext = this;
    initSendFrom(null);
  }
}
