package com.ua.inr0001.intime;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import intime.intime.mylistview.MyListView;
import intime.llc.ua.R;

public class CurrierTimeActivity extends AppCompatActivity {

  private MyListView lvTimeToSend;
  private int timeToSendSelected = 0;
  private SimpleAdapter timeToSendAdapter;
  private String[] sendTimeText;
  private Context context;

  private void setTimeToSendAdapter()
  {
    ArrayList<Map<String, Object>> sendToList = new ArrayList<Map<String, Object>>();
    sendTimeText = getResources().getStringArray(R.array.time_to_delivery);

    Map<String, Object> m;
    for (int i = 0; i < sendTimeText.length; i++) {
      m = new HashMap<String, Object>();
      m.put("Text", sendTimeText[i]);
      sendToList.add(m);
    }

    String[] from = { "Text" };
    int[] to = { R.id.tvcCargoType};

    timeToSendAdapter = new SimpleAdapter(context, sendToList,
      R.layout.single_choise_item_def, from, to)
    {
      @NonNull
      @Override
      public View getView(int position, View convertView, ViewGroup parent) {
        final View view = super.getView(position, convertView, parent);
        ImageView cargoIcon = (ImageView) view.findViewById(R.id.imageCargoIcon);
        LinearLayout llSelectedItem = (LinearLayout) view.findViewById(R.id.llSingleChoiseItem);
        TextView tvDescription = (TextView) view.findViewById(R.id.tvcCargoType);
        if (position == timeToSendSelected) {
          llSelectedItem.setBackgroundColor(getResources().getColor(R.color.background_white));
          cargoIcon.setImageResource(R.drawable.ic_list_check);
          tvDescription.setTextColor(getResources().getColor(R.color.base_text_color));
        }
        else {
          llSelectedItem.setBackgroundColor(getResources().getColor(R.color.app_background));
          cargoIcon.setImageResource(0);
          tvDescription.setTextColor(getResources().getColor(R.color.paper_caption));
        }
        return view;
      }
    };

    lvTimeToSend.setAdapter(timeToSendAdapter);
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_currier_time);
    Toolbar toolbar = (Toolbar) findViewById(R.id.currierTimeToolbar);
    setSupportActionBar(toolbar);
    toolbar.setNavigationOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        onBackPressed();
      }
    });

    context = this;

    Intent intent = getIntent();
    timeToSendSelected = intent.getIntExtra("SendTimePosition", 0);

    lvTimeToSend = (MyListView) findViewById(R.id.lvTimeToSend);
    lvTimeToSend.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
    lvTimeToSend.setItemsCanFocus(true);
    setTimeToSendAdapter();
    lvTimeToSend.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        timeToSendSelected = position;
        timeToSendAdapter.notifyDataSetChanged();
      }
    });
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.accept_action_menu, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();
    switch (id)
    {
      case R.id.menu_action_accept:
        Intent intent = new Intent();
        intent.putExtra("SendTime", sendTimeText[timeToSendSelected]);
        intent.putExtra("SendTimePosition", timeToSendSelected);
        setResult(RESULT_OK, intent);
        finish();
        return true;
      default: return super.onOptionsItemSelected(item);
    }
  }

}
