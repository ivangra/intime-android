package com.ua.inr0001.intime;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class PackageTable implements ITable {

	private SQLiteDatabase sqlDB;

	public PackageTable(SQLiteDatabase sqldb)
	{
		sqlDB = sqldb;
	}
	
	@Override
	protected void finalize() throws Throwable {
		super.finalize();
	}
	
	public static final class Column
	{
			public static final String PACKAGE = "PackgeDescription";
			public static final String PACKAGE_UA = "PackgeDescriptionUA";
	    public static final String ID = "_id";
	    public static final String M_ID = "mId";
	    public static final String CODE = "Code";
			public static final String WIDTH = "Width";
			public static final String LENGTH = "Length";
      public static final String HEIGHT = "Height";
      public static final String PRICE = "Price";
      public static final String PORDER = "pOrder";
			public static final String CARGO_TYPE = "CaroType";
			public static final String WEIGHT_MAX = "WeightMax";
			public static final String WEIGHT_MIN = "WeightMin";
	}
	
	public static final String NAME = "tblPackage";
	public static final String VIEW_NAME = "vwPackage";
	
	public static final String SQL_CREATE_TABLE =
            "create table if not exists " + NAME + " ( " +
                    Column.ID + " integer primary key autoincrement, " +
										Column.M_ID + " integer, " +
                    Column.PACKAGE + " text, " +
										Column.PACKAGE_UA + " text, " +
                    Column.WIDTH + " text, " +
                    Column.LENGTH + " text, " +
                    Column.HEIGHT + " text, " +
                    Column.PRICE + " text, " +
                    Column.PORDER + " text, " +
										Column.CARGO_TYPE + " text, " +
										Column.WEIGHT_MAX + " text, " +
										Column.WEIGHT_MIN + " text, " +
                    Column.CODE + " text );";
	
	public static final String SQL_CREATE_VIEW =
    		"CREATE VIEW if not exists " + VIEW_NAME + " AS" + 
    	    " SELECT " + Column.ID + ", " +
								Column.M_ID + ", " +
    				    Column.PACKAGE + ", " +
								Column.PACKAGE_UA + ", " +
                Column.WIDTH + " , " +
                Column.LENGTH + " , " +
                Column.HEIGHT + " , " +
                Column.PRICE + " , " +
                Column.PORDER + " , " +
								Column.CARGO_TYPE + " , " +
								Column.WEIGHT_MAX + " , " +
								Column.WEIGHT_MIN + " , " +
    				    Column.CODE +
    	    " FROM " + NAME;
	
	public static final String SQL_CREATE_INDEX =
    		"CREATE INDEX if not exists idx_PackageDescr ON " + NAME + " (" + Column.PACKAGE +");";
	
	public static final String SQL_DROP_TABLE = "DROP TABLE IF EXISTS " + NAME;
	
	public static final String SQL_DROP_VIEW = "DROP VIEW IF EXISTS " + VIEW_NAME;
	
	public void insert(String m_id, String spackage, String code, String width, String length,
										 String height, String price, String pOrder, String cargoType,
										 String weightMax, String weightMin)
	{
   	ContentValues cv = new ContentValues();
		cv.put(Column.M_ID, m_id);
   	cv.put(Column.PACKAGE, spackage);
   	cv.put(Column.CODE, code);
    cv.put(Column.WIDTH, width);
    cv.put(Column.LENGTH, length);
    cv.put(Column.HEIGHT, height);
    cv.put(Column.PRICE, price);
    cv.put(Column.PORDER, pOrder);
		cv.put(Column.WEIGHT_MAX, weightMax);
		cv.put(Column.WEIGHT_MIN, weightMin);
		cv.put(Column.CARGO_TYPE, cargoType);
   	sqlDB.insert(NAME, null, cv);
	}

	public void update(String spackage, String code)
	{
		sqlDB.execSQL("UPDATE " + NAME + " SET " + Column.PACKAGE_UA + "=\"" + spackage +
			"\" WHERE " + Column.CODE + "=\"" + code + "\";");
	}
	
	public void clearTable()
  {
    sqlDB.delete(NAME, null, null);
  }
	
	@Override
	public Cursor selectByID(int Id) {
		return null;
	}

	@Override
	public Cursor selectByName(String Name) {
		return null;
	}

	@Override
	public Cursor selectAll() {
		return sqlDB.query(VIEW_NAME, null, null, null, null, null, Column.PACKAGE);
	}
	
	public Cursor selectCorrespondence() {
	  return sqlDB.rawQuery("select * from " + VIEW_NAME + " where Code in (\"00067\", \"00084\") order by " + Column.PACKAGE, null);
	}

	private boolean isSuitablePakage(PackageSpinnerItem packageItem, List<PackageSpinnerItem> packageList,
																	 Double width, Double height, Double length, Double weight)
	{
    if (packageItem.getCode().compareTo("0")==0)
      return true;
		if (width>1 && height>1 && length>1) {
			if (packageItem.getSimplePackageName().compareTo("Гофрокороб")!=0 &&
					!findContain(packageList, packageItem))
			{
				if (packageItem.getHeight() > 0) {
					//if (packageItem.getWidth() >= width && packageItem.getHeight() >= height && packageItem.getLength() >= length)
					if (packageItem.getWidth() >= width && packageItem.getHeight() >= height && packageItem.getLength() >= length)
						return true;
				} else {
					if (Math.abs(packageItem.getWidth()) <= width && Math.abs(packageItem.getHeight()) <= height && Math.abs(packageItem.getLength()) <= length)
						return true;
				}
			}
		}
		else
		if (weight>=packageItem.getWeightMin() && (weight<packageItem.getWeightMax() || packageItem.getWeightMax()==0))
			return true;
		return false;
	}

	private boolean findContain(List<PackageSpinnerItem> packageList, PackageSpinnerItem packageItem)
	{
			for (PackageSpinnerItem pi : packageList)
			{
					if (pi.getSimplePackageName().compareTo(packageItem.getSimplePackageName())==0)
						return true;
			}
			return false;
	}

	private PackageSpinnerItem createPackgeItem(Cursor cursor)
	{
		String mheight = DictionariesDB.getColumnValue(cursor, Column.HEIGHT);
		if (mheight==null || mheight.length()==0)
			mheight = "0";

		String mwidth = DictionariesDB.getColumnValue(cursor, Column.WIDTH);
		if (mwidth==null || mwidth.length()==0)
			mwidth = "0";

		String mlength = DictionariesDB.getColumnValue(cursor, Column.LENGTH);
		if (mlength==null || mlength.length()==0)
			mlength = "0";

		String weight_max = DictionariesDB.getColumnValue(cursor, Column.WEIGHT_MAX);
		if (weight_max==null || weight_max.length()==0)
			weight_max = "0";

		String weight_min = DictionariesDB.getColumnValue(cursor, Column.WEIGHT_MIN);
		if (weight_min==null || weight_min.length()==0)
			weight_min = "0";

		String packageName = "";
		if (GlobalApplicationData.getCurrentLanguage().compareTo("ru") == 0)
			packageName = DictionariesDB.getColumnValue(cursor, Column.PACKAGE);
		else
			packageName = DictionariesDB.getColumnValue(cursor, Column.PACKAGE_UA);

		return new PackageSpinnerItem(packageName,
			DictionariesDB.getColumnValue(cursor, Column.CODE),
			DictionariesDB.getColumnValue(cursor, Column.PRICE),
			Double.valueOf(mwidth),
			Double.valueOf(mheight),
			Double.valueOf(mlength),
			Double.valueOf(weight_max),
			Double.valueOf(weight_min)
		);
	}

	private PackageSpinnerItem getGroupPackage(Double width, Double height, Double length)
	{
		Cursor cursor = sqlDB.rawQuery("Select * FROM " + VIEW_NAME +
			" WHERE " + Column.PACKAGE + "='Гофрокороб' ORDER BY Width, 'Length', Height", null);
		cursor.moveToFirst();
		do {
			PackageSpinnerItem packageItem =  createPackgeItem(cursor);
			if (packageItem.getWidth() >= width && packageItem.getHeight() >= height && packageItem.getLength() >= length) {
				cursor.close();
				return packageItem;
			}
		} while (cursor.moveToNext());
		cursor.close();
		return null;
	}

	public List<PackageSpinnerItem> getPackages(String cargoType,
																							Double width, Double height, Double length, Double weight)
	{
		List<PackageSpinnerItem> packageList = new ArrayList<PackageSpinnerItem>();
		Cursor cursor = sqlDB.rawQuery("Select * FROM " + VIEW_NAME + " WHERE " + Column.CARGO_TYPE + " like '%" + cargoType + "%' ORDER BY " + Column.PORDER, null);

		if (cursor.moveToFirst()) {
			do {
				PackageSpinnerItem packageItam = createPackgeItem(cursor);
				if (cargoType.compareTo("cargo")==0)
				{
					if (isSuitablePakage(packageItam, packageList, width, height, length, weight))
						packageList.add(packageItam);
				}
				else
					packageList.add(packageItam);
			} while (cursor.moveToNext());
		}
		if (width>1 && height>1 && length>1)
		{
			PackageSpinnerItem groupPi = getGroupPackage(width, height, length);
			if (groupPi!=null)
				packageList.add(groupPi);
		}
		cursor.close();
		return packageList;
	}

	@Override
	public boolean exists(String fieldName, String fieldValue) {
	  Cursor slCursor = sqlDB.query(VIEW_NAME, null, fieldName + "=?", new String[]{fieldValue}, null, null, null);
    if (slCursor.getCount()>0)
    {
      slCursor.close();
      return true;
    }
    else
    {
      slCursor.close();
      return false;
    }
	}

}
