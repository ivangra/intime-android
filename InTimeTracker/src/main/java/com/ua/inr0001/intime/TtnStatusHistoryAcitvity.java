package com.ua.inr0001.intime;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

import intime.api.model.ttnhistory.Datum;
import intime.api.model.ttnhistory.TtnHistory;
import intime.llc.ua.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class TtnStatusHistoryAcitvity extends AppCompatActivity {

  private RecyclerView rvTtnStatuses;
  private LinearLayoutManager mLayoutManager;
  private TtnHistoryRvAdapter rvAdapter;
  private Call<TtnHistory> call;
  private String humanTTN;

  private void setupList(TtnHistory ttnHistory)
  {
    mLayoutManager = new LinearLayoutManager(this);
    rvTtnStatuses.setLayoutManager(mLayoutManager);
    rvAdapter = new TtnHistoryRvAdapter(this, ttnHistory, humanTTN);
    rvTtnStatuses.setNestedScrollingEnabled(false);
    rvTtnStatuses.setHasFixedSize(false);
    rvTtnStatuses.setAdapter(rvAdapter);
    if (ttnHistory.getData().size()>0 && ttnHistory.getData().get(ttnHistory.getData().size()-1).getStatusId().compareTo("0")==0)
      rvTtnStatuses.setBackgroundColor(getResources().getColor(R.color.ttn_details_edecl_end_background));
  }

  private TtnHistory deleteDublicate(final TtnHistory ttnHistory)
  {
    TtnHistory ttnHistory1 = new TtnHistory();
    List<Datum> listHistory = new ArrayList<Datum>();
    String oldStatusId = "-1";

    for (Datum data : ttnHistory.getData())
    {
      if (data.getStatusId().compareTo(oldStatusId)!=0) {
        listHistory.add(data);
        oldStatusId = data.getStatusId();
      }
    }
    ttnHistory1.setData(listHistory);
    return ttnHistory1;
  }

  private void doRequest(final String searchString, final String lang)
  {
    Retrofit retrofit = new Retrofit.Builder()
      .baseUrl("https://newapi.intime.ua/")
      .addConverterFactory(GsonConverterFactory.create())
      //.addConverterFactory(createGsonConverter())
      .client(GlobalApplicationData.getUnsafeOkHttpClient())
      .build();

    IRtfAPI service = retrofit.create(IRtfAPI.class);

    call = service.ttnhistory(searchString, lang);
    call.enqueue(
      new Callback<TtnHistory>() {
        @Override
        public void onResponse(Call<TtnHistory> call, Response<TtnHistory> response) {
          TtnHistory ttnHistory = response.body();
          if (ttnHistory!=null)
            setupList(deleteDublicate(ttnHistory));
        }

        @Override
        public void onFailure(Call<TtnHistory> call, Throwable t) {
          Log.d("TtnHistory", "TtnHistory error: " + t.getMessage());
        }
      }
    );
  }

  @Override
  protected void onPause() {
    super.onPause();
    if (call!=null && call.isExecuted())
      call.cancel();
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_ttn_status_history_acitvity);
    Toolbar mActionBarToolbar = (Toolbar) findViewById(R.id.authToolbar1);
    mActionBarToolbar.setTitle("");
    setSupportActionBar(mActionBarToolbar);
    rvTtnStatuses = (RecyclerView) findViewById(R.id.rvTtnStatuses);
    Intent intent = getIntent();
    humanTTN = intent.getStringExtra("ttn");
    doRequest(humanTTN.replaceAll(" ", ""), GlobalApplicationData.getCurrentLanguage());
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();
    switch (id)
    {
      case R.id.action_close: onBackPressed(); return true;
      default: return super.onOptionsItemSelected(item);
    }
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.close_action_menu, menu);
    return true;
  }

}
