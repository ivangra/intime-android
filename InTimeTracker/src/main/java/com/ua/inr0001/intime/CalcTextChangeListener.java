package com.ua.inr0001.intime;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;

import intime.calculator.CalculatorModel;

/**
 * Created by ILYA on 16.02.2017.
 */

public class CalcTextChangeListener implements TextWatcher {

  private String type;

  public CalcTextChangeListener(String type)
  {
    this.type = type;
  }

  @Override
  public void beforeTextChanged(CharSequence s, int start, int count, int after) {

  }

  @Override
  public void onTextChanged(CharSequence s, int start, int before, int count) {

  }

  @Override
  public void afterTextChanged(Editable s) {
    if (type.compareTo("width")==0) {
      if (s.length() == 0)
        CalculatorModel.getInstance(null).setWidth("1");
      else
        CalculatorModel.getInstance(null).setWidth(s.toString());
    }
    if (type.compareTo("height")==0) {
      if (s.length() == 0)
        CalculatorModel.getInstance(null).setHeight("1");
      else
        CalculatorModel.getInstance(null).setHeight(s.toString());
    }
    if (type.compareTo("length")==0) {
      if (s.length() == 0)
        CalculatorModel.getInstance(null).setLength("1");
      else
        CalculatorModel.getInstance(null).setLength(s.toString());
    }
    if (type.compareTo("cost")==0)
      CalculatorModel.getInstance(null).setCost(s.toString());
    if (type.compareTo("podSum")==0)
      CalculatorModel.getInstance(null).setPodSum(s.toString());
  }
}
