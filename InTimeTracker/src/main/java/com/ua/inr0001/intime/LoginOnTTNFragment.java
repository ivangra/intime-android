package com.ua.inr0001.intime;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import intime.llc.ua.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class LoginOnTTNFragment extends Fragment {

  private TextView btnOkLoginHint;
  private LinearLayout lvWelcomeLoginHint;
  private FrameLayout ttnFrame;
  private Button btnLogin;
  private Context mContext;
  private Animation fadeInAnimation;
  private View view;
  private int top1;
  private boolean showed = false;

  public LoginOnTTNFragment() {
    // Required empty public constructor
  }

  private void showHint(float buttom)
  {
    if (!showed) {
      lvWelcomeLoginHint.setTranslationY(buttom);
      fadeInAnimation = AnimationUtils.loadAnimation(mContext, R.anim.fade_in);
      fadeInAnimation.setAnimationListener(new Animation.AnimationListener() {
        @Override
        public void onAnimationStart(Animation animation) {

        }

        @Override
        public void onAnimationEnd(Animation animation) {
          showed = true;
          lvWelcomeLoginHint.setVisibility(View.VISIBLE);
          lvWelcomeLoginHint.clearAnimation();
          fadeInAnimation.cancel();
          fadeInAnimation.reset();
        }

        @Override
        public void onAnimationRepeat(Animation animation) {

        }
      });
      fadeInAnimation.setFillAfter(true);
      fadeInAnimation.setFillEnabled(true);
      lvWelcomeLoginHint.startAnimation(fadeInAnimation);
      showed = true;
    }
  }

  public void setWelcomeFrameLayout(LinearLayout welcomeLayout)
  {
    lvWelcomeLoginHint = welcomeLayout;
  }

  public void setTtnFrame(FrameLayout ttnFrame)
  {
    this.ttnFrame = ttnFrame;
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    view = inflater.inflate(R.layout.fragment_login_on_ttn, container, false);
    mContext = getActivity();
    //lvWelcomeLoginHint = (FrameLayout) view.findViewById(R.id.lvWelcomeLoginHint);
    //btnOkLoginHint = (TextView) view.findViewById(R.id.btnOnLoginHint);
    btnLogin = (Button) view.findViewById(R.id.btnLogin1);
    btnLogin.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (GlobalApplicationData.getInstance().confidentialityReaded==false)
          startActivity(new Intent(getContext(), ConfidentialityActivity.class));
        else
          startActivity(new Intent(getContext(), AutorizationActivity.class));
      }
    });
//    btnOkLoginHint.setOnClickListener(new View.OnClickListener() {
//      @Override
//      public void onClick(View v) {
//        lvWelcomeLoginHint.setVisibility(View.GONE);
//      }
//    });


    return view;
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
  }

  @Override
  public void onResume() {
    super.onResume();
    if (/*GlobalApplicationData.getInstance().getAPIkey().length()==0 &&*/
        GlobalApplicationData.getInstance().welcomeScreenShow==false) {
      btnLogin.postDelayed(new Runnable() {
        @Override
        public void run() {
          showHint(ttnFrame.getTop() + btnLogin.getTop() - lvWelcomeLoginHint.getHeight() - 8);
        }
      }, 1000);
    }
  }
}
