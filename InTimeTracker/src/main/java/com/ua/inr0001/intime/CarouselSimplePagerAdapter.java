package com.ua.inr0001.intime;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import intime.llc.ua.R;

/**
 * Created by technomag on 25.05.17.
 */

public class CarouselSimplePagerAdapter extends PagerAdapter {

  private Context mContext;

  @Override
  public void destroyItem(ViewGroup container, int position, Object object) {
    ((ViewPager) container).removeView((View) object);
  }

  @Override
  public void destroyItem(View container, int position, Object object) {
    ((ViewPager) container).removeView((View) object);
  }

  private List<CarouselPagerItem> pagerItems;

  public CarouselSimplePagerAdapter(Context context, List<CarouselPagerItem> pagerItemList)
  {
    mContext = context;
    pagerItems = pagerItemList;
  }

  @Override
  public Object instantiateItem(ViewGroup container, int position) {

    LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    View view = inflater.inflate(R.layout.corousel_fragment, null);
    TextView tvLabel = (TextView) view.findViewById(R.id.tvCarouselItemCaption);
    tvLabel.setText(pagerItems.get(position).title);
    TextView tvMessage = (TextView) view.findViewById(R.id.tvCarouselItemText);
    tvMessage.setText(pagerItems.get(position).text);

    container.addView(view);
    return view;
  }


  @Override
  public int getCount() {
    return pagerItems.size();
  }

  @Override
  public boolean isViewFromObject(View view, Object object) {
    return (view == object);
  }
}
