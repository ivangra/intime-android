package com.ua.inr0001.intime;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.TextView;

public class MessageDialog extends DialogFragment implements OnClickListener {
    
	private String dialogCaption;
	private String dialogMessage;
	private String cancelButton;
	private String okButton;
	
	private boolean mCancelButton = false;
	
	private Handler mResultHandler = null;

	private void sendResult(String result)
	{
	  Bundle b = new Bundle();
    b.putString("result", result);
    Message m = new Message();
    m.setData(b);
    mResultHandler.sendMessage(m);
	}

	public MessageDialog()
	{

	}

	@Override
	public void setArguments(Bundle args) {
		super.setArguments(args);
	}

	public void setParams(String caption, String message, boolean cancelButton, Handler resultHandler)
	{
	  mCancelButton = cancelButton;
	  mResultHandler = resultHandler;
		dialogCaption = caption;
		dialogMessage = message;
		Context mContext = GlobalApplicationData.getInstance().getContext();
		this.cancelButton = mContext.getResources().getString(android.R.string.cancel);
		this.okButton = mContext.getResources().getString(android.R.string.ok);
	}
	
	public void setParams(String caption, String message, String strCancelButton, String strOkButton, boolean cancelButton, Handler resultHandler)
  {
    mCancelButton = cancelButton;
    mResultHandler = resultHandler;
    dialogCaption = caption;
    dialogMessage = message;
    this.cancelButton = strCancelButton;
    this.okButton = strOkButton;
  }
	
	@Override
  public void onStart() {
    super.onStart();
    
    final Resources res = getResources();
    final int yellow = res.getColor(android.R.color.holo_blue_light);

    // Title
    final int titleId = res.getIdentifier("alertTitle", "id", "android");
    final View title = getDialog().findViewById(titleId);
    if (title != null) {
        ((TextView) title).setTextColor(yellow);
    }
  }

  // диалоговое окно
	public Dialog onCreateDialog(Bundle savedInstanceState) {
	  AlertDialog.Builder adb = new AlertDialog.Builder(getActivity())
		    .setTitle(dialogCaption)
		    .setPositiveButton(okButton, this)
		    .setMessage(dialogMessage);
		if (mCancelButton==true)
		  adb.setNegativeButton(cancelButton, this);
		return adb.create();
	}
	
	@Override
	public void onClick(DialogInterface arg0, int arg1) {
	  switch (arg1) {
    case Dialog.BUTTON_POSITIVE:
       if (mResultHandler!=null)
         sendResult(okButton);
      break;
    case Dialog.BUTTON_NEGATIVE:
       if (mResultHandler!=null)
        sendResult(cancelButton);
      break;
    }
	}

}
