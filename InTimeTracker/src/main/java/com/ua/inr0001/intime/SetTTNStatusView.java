package com.ua.inr0001.intime;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import intime.llc.ua.R;

/**
 * Created by ILYA on 13.10.2016.
 */

public class SetTTNStatusView{

  public static void setBeginStatus(ImageView imgStage1,
                              ImageView imgStage2, ImageView imgStage3,
                              ImageView imgLine1, ImageView imgLine2){
    imgStage1.setBackgroundResource(R.drawable.ic_cargo_active);
    imgLine1.setBackgroundResource(R.drawable.dotted_line_orange);
    imgStage2.setBackgroundResource(R.drawable.circle_orange);
    imgLine2.setBackgroundResource(R.drawable.dotted_line_gray);
    imgStage3.setBackgroundResource(R.drawable.circle_gray);
  }

  public static void setOnWayStatus(ImageView imgStage1,
                                    ImageView imgStage2, ImageView imgStage3,
                                    ImageView imgLine1, ImageView imgLine2){
    imgStage1.setBackgroundResource(R.drawable.circle_full_orange);
    imgLine1.setBackgroundResource(R.drawable.line_orange);
    imgStage2.setBackgroundResource(R.drawable.ic_cargo_active);
    imgLine2.setBackgroundResource(R.drawable.dotted_line_orange);
    imgStage3.setBackgroundResource(R.drawable.circle_orange);
  }

  public static void setOnWarehouseStatus(ImageView imgStage1,
                                    ImageView imgStage2, ImageView imgStage3,
                                    ImageView imgLine1, ImageView imgLine2){
    imgStage1.setBackgroundResource(R.drawable.circle_full_green);
    imgLine1.setBackgroundResource(R.drawable.line_green);
    imgStage2.setBackgroundResource(R.drawable.circle_full_green);
    imgLine2.setBackgroundResource(R.drawable.line_green);
    imgStage3.setBackgroundResource(R.drawable.ic_cagro_delivered);
  }

  public static void setWebDeclarationStatus(FrameLayout frWebDecLine, LinearLayout llWebDecLine){
    frWebDecLine.setVisibility(View.VISIBLE);
    llWebDecLine.setVisibility(View.GONE);
  }

  public static void setOnClientStatus(ImageView imgStage1,
                                          ImageView imgStage2, ImageView imgStage3,
                                          ImageView imgLine1, ImageView imgLine2){
    imgStage1.setBackgroundResource(R.drawable.circle_full_green);
    imgLine1.setBackgroundResource(R.drawable.line_green);
    imgStage2.setBackgroundResource(R.drawable.circle_full_green);
    imgLine2.setBackgroundResource(R.drawable.line_green);
    imgStage3.setBackgroundResource(R.drawable.ic_delivered);
  }

  public static void setCargoGivenStatus(ImageView imgStage1,
                                       ImageView imgStage2, ImageView imgStage3,
                                       ImageView imgLine1, ImageView imgLine2, TextView tvCargoGiven){
    tvCargoGiven.setVisibility(View.VISIBLE);
    imgStage1.setVisibility(View.GONE);
    imgLine1.setVisibility(View.GONE);
    imgStage2.setVisibility(View.GONE);
    imgLine2.setBackgroundResource(R.drawable.line_green);
    imgStage3.setBackgroundResource(R.drawable.ic_delivered);
  }

}
