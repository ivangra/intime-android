package com.ua.inr0001.intime;

import android.content.Context;
import android.text.Editable;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by ILYA on 11.01.2017.
 */

public class VolumeTextChangeListener extends TextChangeListener {

  private TextView tvVolumeName;

  public VolumeTextChangeListener(Context context, EditText editText, TextView textView)
  {
    super(context, editText, null, null);
    tvVolumeName = textView;
    if (textView!=null)
      textView.setVisibility(View.GONE);
  }

  public VolumeTextChangeListener(Context context, EditText editText, TextView textView, ImageView imgWarinig) {
    super(context, editText, textView, imgWarinig);
  }

  @Override
  public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    super.beforeTextChanged(s, start, count, after);
  }

  @Override
  public void onTextChanged(CharSequence s, int start, int before, int count) {
    super.onTextChanged(s, start, before, count);
    if (editText!=null && tvVolumeName!=null)
    {
      if (editText.getText().toString().length() > 0)
        tvVolumeName.setVisibility(View.VISIBLE);
      else
        tvVolumeName.setVisibility(View.GONE);
    }
  }

  @Override
  public void afterTextChanged(Editable s) {
    super.afterTextChanged(s);
  }
}
