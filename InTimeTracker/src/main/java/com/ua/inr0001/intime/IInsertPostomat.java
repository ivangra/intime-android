package com.ua.inr0001.intime;

/**
 * Created by ILYA on 05.10.2016.
 */

public interface IInsertPostomat {

  void insert(String name, String code, String numberInCity, String address, String addressUkr, String region,
              String regionUkr, String department,
              String longitude, String latitude);

}
