package com.ua.inr0001.intime;

/**
 * Created by ILYA on 11.11.2016.
 */

public class MailCallCurrier {

  private ICalcCurrierView calcCurrierView;
  private static MailCallCurrier instance;

  public static MailCallCurrier getInstance()
  {
    return instance==null ? instance = new MailCallCurrier(): instance;
  }

  public void setView(ICalcCurrierView calcCurrierView)
  {
    this.calcCurrierView = calcCurrierView;
  }

  private String fullName = "";
  private String phone = "";
  private String dateSend = "";
  private String humanDate = "";
  private String cityCode = "";
  private String currierFromCity = "";
  private String currierFromStreet = "";
  private String currierFromHouse = "";
  private String currierFromFloor = "";
  private String currierFromRoom = "";
  private String width = "";
  private String height = "";
  private String length = "";
  private String comment = "";
  private String listToStage = "0";
  private String comfortableTime = "";
  private String weight = "0.1";
  private String numberOrder = "";
  private String cargoType = "";
  private String cargoSubType = "";
  private String radius = "";
  private String cost = "0";
  private String count = "1";
  private String podSum = "0";
  private boolean isSended = false;

  public void setTypePallets(String typePallets) {
    this.cargoSubType = typePallets.replaceAll("x", "*");
  }

  public String getFullName() {
    return fullName;
  }

  public void setFullName(String fullName) {
    this.fullName = fullName;
    if (calcCurrierView != null)
      calcCurrierView.callCurrierDataUpdated();
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
    if (calcCurrierView != null)
      calcCurrierView.callCurrierDataUpdated();
  }

  public String getDateSend() {
    return dateSend;
  }

  public void setDateSend(String dateSend) {
    this.dateSend = dateSend;
  }

  public String getHumanDate() {
    return humanDate;
  }

  public void setHumanDate(String humanDate) {
    this.humanDate = humanDate;
  }

  public String getCityCode() {
    return cityCode;
  }

  public void setCityCode(String cityCode) {
    this.cityCode = cityCode;
  }

  public String getCurrierFromCity() {
    return currierFromCity;
  }

  public void setCurrierFromCity(String currierFromCity) {
    this.currierFromCity = currierFromCity;
  }

  public String getCurrierFromStreet() {
    return currierFromStreet;
  }

  public void setCurrierFromStreet(String currierFromStreet) {
    this.currierFromStreet = currierFromStreet;
  }

  public String getCurrierFromHouse() {
    return currierFromHouse;
  }

  public void setCurrierFromHouse(String currierFromHouse) {
    this.currierFromHouse = currierFromHouse;
  }

  public String getCurrierFromFloor() {
    return currierFromFloor;
  }

  public void setCurrierFromFloor(String currierFromFloor) {
    this.currierFromFloor = currierFromFloor;
  }

  public String getCurrierFromRoom() {
    return currierFromRoom;
  }

  public void setCurrierFromRoom(String currierFromRoom) {
    this.currierFromRoom = currierFromRoom;
  }

  public String getWidth() {
    return width;
  }

  public void setWidth(String width) {
    this.width = width;
    if (calcCurrierView != null)
      calcCurrierView.callCurrierDataUpdated();
  }

  public String getHeight() {
    return height;
  }

  public void setHeight(String height) {
    this.height = height;
    if (calcCurrierView != null)
      calcCurrierView.callCurrierDataUpdated();
  }

  public String getLength() {
    return length;
  }

  public void setLength(String length) {
    this.length = length;
    if (calcCurrierView != null)
      calcCurrierView.callCurrierDataUpdated();
  }

  public String getComment() {
    return comment;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  public String getListToStage() {
    return listToStage;
  }

  public void setListToStage(String listToStage) {
    this.listToStage = listToStage;
  }

  public String getComfortableTime() {
    return comfortableTime;
  }

  public void setComfortableTime(String comfortableTime) {
    this.comfortableTime = comfortableTime;
  }

  public String getWeight() {
    return weight;
  }

  public void setWeight(String weight) {
    this.weight = weight;
  }

  public String getNumberOrder() {
    return numberOrder;
  }

  public void setNumberOrder(String numberOrder) {
    this.numberOrder = numberOrder;
  }

  public String getCargoType() {
    return cargoType;
  }

  public void setCargoType(String cargoType) {
    this.cargoType = cargoType;
  }

  public String getCargoSubType() {
    return cargoSubType;
  }

  public void setCargoSubType(String cargoSubType) {
    this.cargoSubType = cargoSubType;
  }

  public String getRadius() {
    return radius;
  }

  public void setRadius(String radius) {
    this.radius = radius;
  }

  public String getCost() {
    return cost;
  }

  public void setCost(String cost) {
    this.cost = cost;
    if (calcCurrierView != null)
      calcCurrierView.callCurrierDataUpdated();
  }

  public void setCostWithoutEvent(String cost) {
    this.cost = cost;

  }

  public String getCount() {
    return count;
  }

  public void setCount(String count) {
    this.count = count;
  }

  public String getPodSum() {
    return podSum;
  }

  public void setPodSum(String podSum) {
    this.podSum = podSum;
    if (calcCurrierView != null)
      calcCurrierView.callCurrierDataUpdated();
  }

  public void setPodSumWithoutEvent(String podSum)
  {
    this.podSum = podSum;
  }

  public boolean isSended() {
    return isSended;
  }

  public void setSended(boolean sended) {
    isSended = sended;
  }
}
