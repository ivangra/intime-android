package com.ua.inr0001.intime;

import android.content.Context;
import android.location.Location;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import intime.llc.ua.R;

import static java.lang.Math.round;

/**
 * Created by ILYA on 25.10.2016.
 */

public class DepartmentsRvAdapter extends RecyclerView.Adapter<DepartmentsRvAdapter.DepartmentHolder> {

  public interface OnItemClickListener {
    void onItemClick(View view, int position);
  }

  private Context context;
  private List<DepartmentMapItem> depList;
  private OnItemClickListener mOnItemClickListener;
  private Location location;

  public DepartmentsRvAdapter(Context context, List<DepartmentMapItem> depList)
  {
    this.context = context;
    this.depList = depList;
  }

  public void setmOnItemClickListener(OnItemClickListener onItemClickListener)
  {
    mOnItemClickListener = onItemClickListener;
  }

  public void setDataList(List<DepartmentMapItem> depList)
  {
    this.depList = new ArrayList<DepartmentMapItem>(depList);
  }

  public void setMyLocation(Location location)
  {
    this.location = location;
  }

  @Override
  public DepartmentHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(context).inflate(R.layout.departments_city_item, null, false);
    view.setLayoutParams(new RecyclerView.LayoutParams(
      RecyclerView.LayoutParams.MATCH_PARENT,
      RecyclerView.LayoutParams.WRAP_CONTENT
    ));
    return new DepartmentHolder(view);
  }

  private DepartmentMapItem getDepItem(int position)
  {
    DepartmentMapItem depItem = null;
    try {
      depItem = depList.get(position);
      return depItem;
    }
    catch(Exception e)
    {
      return depItem;
    }
  }

  private void setWeightLength(TextView tvCity, DepartmentMapItem depItem)
  {
    if (tvCity != null) {
      StringBuilder sbWLengthWeight = new StringBuilder();

      if (depItem.weightLimit > 0)
        sbWLengthWeight.append("до " + Integer.toString(depItem.weightLimit) + " кг");

      if (depItem.lengthLimit != null && depItem.lengthLimit.length() > 0)
        sbWLengthWeight.append(" / до " + depItem.lengthLimit + " м");
      if (sbWLengthWeight.length() > 0) {
        tvCity.setText(sbWLengthWeight.toString());
        tvCity.setVisibility(View.VISIBLE);
      } else
        tvCity.setVisibility(View.GONE);
    }
  }

  private void setCity(TextView tvCity, DepartmentMapItem depItem)
  {
    if (depItem.dublicate == 1)
    {
      tvCity.setText(depItem.cityName + ", " + depItem.state);
    }
    else
      tvCity.setText(depItem.cityName);
  }

  @Override
  public void onBindViewHolder(DepartmentHolder holder, final int position) {

    if (position < depList.size()) {
      DepartmentMapItem depItem = getDepItem(position);
      if (depItem != null) {
        holder.container.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
            if (mOnItemClickListener != null)
              mOnItemClickListener.onItemClick(v, position);
          }
        });

        holder.img.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_dep_arrow));

        if (depItem.depType.equals("store"))
          holder.tvDepNumber.setText(context.getResources().getString(R.string.text_department) + " №" + depItem.depNumber);
        else
          holder.tvDepNumber.setText(context.getResources().getString(R.string.text_postomat) + " №" + depItem.depNumber);

        holder.tvAddress.setText(depItem.depAddress);
        setWeightLength(holder.tvDepParams, depItem);
        setCity(holder.tvCity, depItem);

        DepartmentsRvAdapter.setworkStatusText(context, holder.tvWorkStatus, depItem.workHoursStart, depItem.workHoursEnd, depItem.workHoursData);

        if (location != null && depItem.distance != 0) {
          holder.tvDistance.setText("~" + Long.toString(round(depItem.distance)) + " км");
          holder.tvDistance.setVisibility(View.VISIBLE);
        }
      }
    }
  }

  public static void setworkStatusText(Context context, TextView tvWorkStatus, String startTime, String endTime, WorkHoursData work)
  {
    if (startTime!=null && (startTime.compareTo("weekend")==0 || startTime.compareTo("holiday")==0))
    {
      tvWorkStatus.setTextColor(context.getResources().getColor(R.color.error_massage));
      String fo = context.getResources().getString(R.string.weekend_open_at);
      tvWorkStatus.setText(String.format(fo, work.workhoursnestDay,  work.workHoursNextTime));
    }
    else
    {
      SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");

        try {
          Date now = new Date();
          now = sdf.parse(now.getHours() + ":" + now.getMinutes());

          Date date1 = sdf.parse(startTime);
          Date date2 = sdf.parse(endTime);
          long difference = (now.getTime() - date2.getTime()) / 1000 / 60;

          if(now.before(date1)) {
            tvWorkStatus.setTextColor(context.getResources().getColor(R.color.error_massage));
            tvWorkStatus.setText(context.getResources().getString(R.string.text_work_status_open_at) + " " + startTime);
          }
          else
          if (now.after(date2)){
            tvWorkStatus.setTextColor(context.getResources().getColor(R.color.error_massage));
            tvWorkStatus.setText(context.getResources().getString(R.string.text_work_status_closed) + " " + work.workhoursnestDay +  " в " + work.workHoursNextTime);
          }
          else
          if (difference >= -60 && difference < 0 )
          {
            tvWorkStatus.setTextColor(context.getResources().getColor(R.color.edit_stroke));
            tvWorkStatus.setText(context.getResources().getString(R.string.text_work_status_opened) + " " + endTime);
          }
          else
          {
            tvWorkStatus.setTextColor(context.getResources().getColor(R.color.base_text_color));
            tvWorkStatus.setText(context.getResources().getString(R.string.text_work_status_opened) + " " + endTime);
          }
        } catch (Exception e){
          tvWorkStatus.setVisibility(View.GONE);
        }
    }
  }

  @Override
  public int getItemCount() {
    if (depList.size() >= 100)
      return 100;
    else
      return depList.size();
  }


  class DepartmentHolder extends RecyclerView.ViewHolder {

    ImageView img;
    TextView tvDepNumber;
    TextView tvWorkStatus;
    TextView tvCity; // wight + city
    TextView tvAddress;
    TextView tvDistance;
    TextView tvDepParams;
    View container;

    public DepartmentHolder(View view) {
      super(view);
      container = view;
      img = (ImageView) view.findViewById(R.id.imgDepArrow);
      tvDepNumber = (TextView) view.findViewById(R.id.tvDepNumber);
      tvWorkStatus = (TextView) view.findViewById(R.id.tvWhStatus);
      tvCity = (TextView) view.findViewById(R.id.tvWhCity);
      tvAddress = (TextView) view.findViewById(R.id.tvWhAddress);
      tvDistance = (TextView) view.findViewById(R.id.tvDistance);
      tvDepParams = (TextView) view.findViewById(R.id.tvDepParams);
    }
  }


}
