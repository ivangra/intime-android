package com.ua.inr0001.intime;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import intime.llc.ua.R;

public class PushHistoryCursorAdapter extends SimpleCursorAdapter{

  private GlobalApplicationData appData;
  private List<ItemInfo> itemsInfo;
  private Cursor mCursor;
  private int selectedPosition = 0;
  //private Button btnSelectAll;
  //private Button btnDelete;
  
  private class ItemInfo
  {
    boolean chackBoxState;
    int id;
  }

  public void setSelectedPosition(final int position)
  {
    selectedPosition = position;
  }

  /*public void setButtons(Button selectAll, Button delete)
  {
    btnDelete = delete;
    btnSelectAll = selectAll;
    enableButtons(false);
    enableSelectAllButton();
  }*/
  
  /*private void enableSelectAllButton()
  {
    if (mCursor!=null && mCursor.getCount()>0)
    {
      btnSelectAll.setEnabled(true);
      btnSelectAll.setTextColor(appData.getContext().getResources().getColor(R.color.base_text_color));
    }
    else
    {
      btnSelectAll.setEnabled(false);
      btnDelete.setTextColor(appData.getContext().getResources().getColor(R.color.button_background_pressed));
    }
  }
  
  private void enableButtons(boolean enabled)
  {
    btnDelete.setEnabled(enabled);
    if (enabled==true)
      btnDelete.setTextColor(appData.getContext().getResources().getColor(R.color.base_text_color));
    else
      btnDelete.setTextColor(appData.getContext().getResources().getColor(R.color.button_background_pressed));
  }*/
  
  /*private int getCountChecked()
  {
    int count = 0;
    for (ItemInfo iInfo : itemsInfo)
      if (iInfo.chackBoxState==true)
        count++;
    return count;
  }*/
  
  /*public void checkAll(boolean checked)
  {
    for (ItemInfo iInfo : itemsInfo)
      iInfo.chackBoxState = checked;
    notifyDataSetChanged();
  }*/
  
  public PushHistoryCursorAdapter(Context context, int layout, Cursor c,
      String[] from, int[] to) {
    super(context, layout, c, from, to);
    
    appData = GlobalApplicationData.getInstance();
    
    itemsInfo = new ArrayList<ItemInfo>();
    mCursor = c;
    
    // добавляем данные о состоянии элементов в сипсик
    mCursor.moveToFirst();
    for (int i=0; i<mCursor.getCount(); i++)
    {
      //String push = mCursor.getString(mCursor.getColumnIndex(PushHistoryTable.Column.NAME));
    
      ItemInfo iInfo = new ItemInfo(); 
      iInfo.id = mCursor.getInt(mCursor.getColumnIndex(PushHistoryTable.Column.ID));
      iInfo.chackBoxState = false;
    
      itemsInfo.add(iInfo);
      mCursor.moveToNext();
    }
    mCursor.moveToFirst();
  }
  
  /*public void deleteSelected()
  {
    List<ItemInfo> itemsForDelete = new ArrayList<ItemInfo>();
    DictionariesDB inTimeDB = DictionariesDB.getInstance(appData.getContext());
    for (ItemInfo iInfo : itemsInfo)
    {
      if (iInfo.chackBoxState==true)
      {
        inTimeDB.tablePushHistory.delete(Integer.toString(iInfo.id));
        itemsForDelete.add(iInfo);
      }
    }
    
    // снимаем галочки
    for (ItemInfo iInfo : itemsForDelete)
    {
      itemsInfo.remove(iInfo);
    }
    itemsForDelete.clear();
    
    enableSelectAllButton();
  }*/

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    return super.getView(position, convertView, parent);
  }

  @Override
  public void bindView(View view, Context _context, Cursor _cursor) {

   TextView titleTextView = (TextView) view.findViewById(R.id.tvPushTitle);
   String pushTitle = _cursor.getString(_cursor.getColumnIndex(PushHistoryTable.Column.NAME));
   titleTextView.setText(pushTitle);
   
   titleTextView = (TextView) view.findViewById(R.id.tvDateUpdate);
   titleTextView.setText(_cursor.getString(_cursor.getColumnIndex(PushHistoryTable.Column.DATE)));
   
   titleTextView = (TextView) view.findViewById(R.id.tvPushDetails);
   titleTextView.setText(_cursor.getString(_cursor.getColumnIndex(PushHistoryTable.Column.DESCRIPTION)));

   titleTextView = (TextView) view.findViewById(R.id.tvMarkPushItem);

   Button showOnMap = (Button) view.findViewById(R.id.btnPushShowOnMap);
    if (_cursor.getPosition()==selectedPosition) {
      view.setBackgroundColor(_context.getResources().getColor(R.color.background_white));
      //showOnMap.setVisibility(View.VISIBLE);
      titleTextView.setVisibility(View.VISIBLE);
    }
      else
    {
      view.setBackgroundColor(_context.getResources().getColor(R.color.app_background));
      //showOnMap.setVisibility(View.GONE);
      titleTextView.setVisibility(View.INVISIBLE);
    }

   /*CheckBox cbBox=(CheckBox)view.findViewById(R.id.chHistoryPush);
   cbBox.setTag(_cursor.getPosition());
   cbBox.setChecked(itemsInfo.get(_cursor.getPosition()).chackBoxState);
   cbBox.setOnCheckedChangeListener( chOnClick );*/
  }
  
  //обработчик для чекбоксов
  /*OnCheckedChangeListener chOnClick = new OnCheckedChangeListener() {
    public void onCheckedChanged(CompoundButton buttonView,
        boolean isChecked) {
      itemsInfo.get((Integer) buttonView.getTag()).chackBoxState = isChecked;
      if (getCountChecked()>0)
        enableButtons(true);
      else
        enableButtons(false);
    }
  };*/
  
}
