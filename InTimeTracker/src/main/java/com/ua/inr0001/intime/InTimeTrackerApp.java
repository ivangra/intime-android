package com.ua.inr0001.intime;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

public class InTimeTrackerApp extends Application {

    private static Context context;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public void onCreate(){
        super.onCreate();
        InTimeTrackerApp.context = getApplicationContext();
        DictionariesDB.getInstance(context);
        //TypefaceUtil.overrideFont(InTimeTrackerApp.context, "SERIF", "fonts/linotypen.otf");
    }

    public static Context getAppContext() {
        return InTimeTrackerApp.context;
    }
}
