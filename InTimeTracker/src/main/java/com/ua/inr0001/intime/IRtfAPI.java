package com.ua.inr0001.intime;

import intime.api.model.auth.Auth;
import intime.api.model.billingcalc.RowDataWrapper;
import intime.api.model.calculator.Calculator;
import intime.api.model.citylist.CityList;
import intime.api.model.createuser.NewUser;
import intime.api.model.currier.Currier;
import intime.api.model.departments.Departments;
import intime.api.model.packages.Packages;
import intime.api.model.recovery.Recovery;
import intime.api.model.trackttn.TrackTtn;
import intime.api.model.ttnhistory.TtnHistory;
import intime.api.model.ttnlist.TTNList;
import intime.api.model.userinfo.UserInfo;
import intime.api.model.yatranslate.YaResult;
import intime.calculator.DeclarationWrapper;
import intime.getttnlist.GetTTnList;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by ILYA on 10.02.2017.
 */

public interface IRtfAPI {

  @POST("track")
  @FormUrlEncoded
  Call<TrackTtn> trackTtn(@Field("ttn") String ttn, @Field("lang") String lang);

  @POST("cities")
  @FormUrlEncoded
  Call<CityList> cityList(@Field("lang") String lang);

  @POST("cities")
  @FormUrlEncoded
  Call<CityList> cityListSearch(@Field("lang") String lang, @Field("search") String city);

  @POST("packages")
  @FormUrlEncoded
  Call<Packages> packages(@Field("lang") String lang);

  @POST("stores")
  @FormUrlEncoded
  Call<Departments> stores(@Field("lang") String lang);

  @POST("courier-order")
  @FormUrlEncoded
  Call<Currier> currier(@Field("lang") String lang,
                        @Field("fullname") String name,
                        @Field("phone") String phone,
                        @Field("date") String date,
                        @Field("time") String time,
                        @Field("city_code") String cityCode,
                        @Field("street") String street,
                        @Field("house") String house,
                        @Field("floor") String floor,
                        @Field("apartment") String room,
                        @Field("floor_down") String liftFromStage,
                        @Field("weight") String weight,
                        @Field("comment") String comment,
                        @Field("cargo_type") String cargoType,
                        @Field("diameter") String diameter,
                        @Field("width") String width,
                        @Field("height") String height,
                        @Field("depth") String depth, // length
                        @Field("insurance_cost") String cost,
                        @Field("qty") String count,
                        @Field("pod_amount") String posSum,
                        @Field("type_pallets") String paleteType
                        );

  @POST("calculate")
  @FormUrlEncoded
  Call<Calculator> calculate(@Field("lang") String lang,
                             @Field("type_from") String typeFrom,
                             @Field("type_to") String typeTo,
                             @Field("from_code") String senderCity,
                             @Field("to_code") String receiverCity,
                             @Field("cargo_type") String cargoType,
                             @Field("weight") String weight,
                             @Field("width") String width,
                             @Field("height") String height,
                             @Field("depth") String length,
                             @Field("radius") String radius,
                             @Field("qty") String count,
                             @Field("type_pallets") String typePallets,
                             @Field("insurance_cost") String cost,
                             @Field("pod_amount") String podSum,
                             @Field("package") String packageCode,
                             @Field("date") String date,
                             @Field("return_documents") String returnDocuments);

  @POST("BillingWebService/")
  //@Headers("Content-Type: application/json")
  @Headers("Accept: application/json")
  Call<RowDataWrapper> calcBilling(@Header("Content-Type") String df, @Body DeclarationWrapper declaration);

  @POST("get_ttn_by_api_key_rest")
  @Headers({"Content-Type: application/json",
    "Accept: application/json"})
  Call<TTNList> ttnHistory(@Header("Content-Type") String df, @Body GetTTnList ttnListRequest);

  @POST("get_people_by_phone_rest")
  @Headers({"Content-Type: application/json",
    "Accept: application/json"})
  Call<ResponseBody> checkPeopleExists(@Header("Content-Type") String df, @Body RequestBody phone);

  @POST("get_people_by_phone_rest")
  @Headers({"Content-Type: application/json",
    "Accept: application/json"})
  Call<UserInfo> getUserInfo(@Header("Content-Type") String df, @Body RequestBody phone);

  @POST("auth_rest")
  @Headers({"Content-Type: application/json",
    "Accept: application/json"})
  Call<Auth> authUser(@Header("Content-Type") String df, @Body RequestBody auth);

  @POST("change_password_rest")
  @Headers({"Content-Type: application/json",
    "Accept: application/json"})
  Call<Recovery> changePassword(@Header("Content-Type") String df, @Body RequestBody recoveryPassword);

  @POST("add_user_rest")
  @Headers({"Content-Type: application/json",
    "Accept: application/json"})
  Call<NewUser> registerUser(@Header("Content-Type") String df, @Body RequestBody recoveryPassword);

  @GET("http://technomag-programms.blogspot.com/p/blog-page_9.html")
  Call<ResponseBody> getAppStatus();

  @POST("translate")
  @FormUrlEncoded
  Call<YaResult> translate(@Field("key") String key,
                           @Field("text") String text,
                           @Field("lang") String lang,
                           @Field("format") String format);

//  @POST("track-history")
//  @FormUrlEncoded
//  Call<TtnHistory> ttnhistory(@Field("ttn") String ttn,
//                              @Field("lang") String lang,
//                              @Field("date_format") String date_format);

  @POST("track-history")
  @FormUrlEncoded
  Call<TtnHistory> ttnhistory(@Field("ttn") String ttn,
                              @Field("lang") String lang);

  @POST("send-sms")
  @FormUrlEncoded
  Call<ResponseBody> sendSMS(@Field("phone") String phone, @Field("viber_text") String vider, @Field("sms_text") String sms);

  @POST("test")
  //@Multipart
  @FormUrlEncoded
  Call<ResponseBody> sendLog(@Field("key") String log);

}
