package com.ua.inr0001.intime;

import android.app.IntentService;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.text.TextUtils;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by technomag on 27.09.17.
 */

public class FetchAddressIntentService extends IntentService {
  private static final String TAG = "SelectOnMap";
  protected ResultReceiver mReceiver;

  public FetchAddressIntentService(String name) {
    super(name);
  }

  public FetchAddressIntentService() {
    super("SelectOnMapService");
  }

  @Override
  protected void onHandleIntent(Intent intent) {
    String errorMessage = "";
    mReceiver = intent.getParcelableExtra(Constants.RECEIVER);
    Geocoder geocoder = new Geocoder(this, Locale.getDefault()/*new Locale("uk")*/);

    // Get the location passed to this service through an extra.
    Location location = intent.getParcelableExtra(
      Constants.LOCATION_DATA_EXTRA);

    List<Address> addresses = null;

    try {
      addresses = geocoder.getFromLocation(
        location.getLatitude(),
        location.getLongitude(),
        // In this sample, get just a single address.
        1);
    } catch (IOException ioException) {
      // Catch network or other I/O problems.
      errorMessage = "service_not_available";
      Log.e(TAG, errorMessage, ioException);
    } catch (IllegalArgumentException illegalArgumentException) {
      // Catch invalid latitude or longitude values.
      errorMessage = "invalid_lat_long_used";
      Log.e(TAG, errorMessage + ". " +
        "Latitude = " + location.getLatitude() +
        ", Longitude = " +
        location.getLongitude(), illegalArgumentException);
    }

    // Handle case where no address was found.
    if (addresses == null || addresses.size()  == 0) {
      if (errorMessage.isEmpty()) {
        errorMessage = "no_address_found";
        Log.e(TAG, errorMessage);
      }
    } else {
      Address address = addresses.get(0);
      ArrayList<String> addressFragments = new ArrayList<String>();
      deliverResultToReceiver(Constants.SUCCESS_RESULT,
        address.getThoroughfare(),
        address.getLocality(),
        address.getSubThoroughfare()
        );
    }
  }

  private void deliverResultToReceiver(int resultCode, String street, String city, String streetNumber) {
    Bundle bundle = new Bundle();
    bundle.putString("Street", street);
    bundle.putString("City", city);
    bundle.putString("StreetNumber", streetNumber);
    mReceiver.send(resultCode, bundle);
  }
}
