package com.ua.inr0001.intime;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chauthai.swipereveallayout.SwipeRevealLayout;
import com.chauthai.swipereveallayout.ViewBinderHelper;

import intime.llc.ua.R;
import intime.ttnlistcommand.ArchiveCommand;
import intime.ttnlistcommand.CommandHistory;
import intime.ttnlistcommand.DeleteCommand;
import intime.ttnlistcommand.TtnListCommand;
import intime.utils.MyDateUtils;
import intime.utils.MyStringUtils;

import static com.ua.inr0001.intime.DictionariesDB.getColumnValue;

/**
 * Created by ILYA on 25.10.2016.
 */

public class TtnHistoryRvCursorAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

  public static final int PAGE_SIZE = 20;
  private static final int LOGIN_ITEM = 1067;
  private static final int TTN_ITEM = 1100;
  public static final int DELETE = 70;
  public static final int ARCHIVE = 71;

  private ViewBinderHelper binderHelper = new ViewBinderHelper();

  public interface OnItemClickListener {
    void onItemClick(View view, String id);
  }

  private Context context;

  public void dataUpdated() {
    cursor.requery();
    notifyDataSetChanged();
  }

  private Cursor cursor;
  private String locale = "";
  private OnItemClickListener mOnItemClickListener;
  private String prevStatusId = "-1";
  private ITTNManage ttnManage;
  //private RecyclerView mRecyclerView;
  private CommandHistory history;

  public int getStartPosition() {
    return startPosition;
  }

  private int startPosition = 0;

  public void setOnItemClickListener(OnItemClickListener onItemClickListener)
  {
    mOnItemClickListener = onItemClickListener;
  }

  public TtnHistoryRvCursorAdapter(Context context, Cursor cursor, ITTNManage deleteHint, CommandHistory history)
  {
    this.context = context;
    this.cursor = cursor;
    this.cursor.moveToFirst();
    locale = GlobalApplicationData.getCurrentLanguage();
    this.ttnManage = deleteHint;
    this.history = history;
    binderHelper.setOpenOnlyOne(true);
  }

  @Override
  public void onAttachedToRecyclerView(RecyclerView recyclerView) {
    super.onAttachedToRecyclerView(recyclerView);
    //mRecyclerView = recyclerView;
  }

  @Override
  public int getItemViewType(int position) {
    if (position==0 && GlobalApplicationData.getInstance().getUserInfo().getName().length()==0)
      return LOGIN_ITEM;
    if (position!=0 && cursor.getCount()>0)
      return TTN_ITEM;
    else
      return TTN_ITEM;
  }

  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    if (viewType==TTN_ITEM) {
      View view = LayoutInflater.from(context).inflate(R.layout.ttn_history_list_item_delivery, null, false);
      view.setLayoutParams(new RecyclerView.LayoutParams(
        RecyclerView.LayoutParams.MATCH_PARENT,
        RecyclerView.LayoutParams.WRAP_CONTENT
      ));
      return new TtnHolder(view);
    }
    else
    {
      View view = LayoutInflater.from(context).inflate(R.layout.ttn_list_login, null, false);
      view.setLayoutParams(new RecyclerView.LayoutParams(
        RecyclerView.LayoutParams.MATCH_PARENT,
        RecyclerView.LayoutParams.WRAP_CONTENT
      ));
      return new LoginHolder(view, context);
    }
  }

  public void updatePage(int count)
  {
    cursor.requery();
    notifyItemRangeChanged(startPosition, count);
  }

  private void ttnBind(final TtnHolder holder, final int position)
  {
    if (GlobalApplicationData.getInstance().getUserInfo().getName().length()==0)
      cursor.moveToPosition(position-1);
    else
      cursor.moveToPosition(position);
    final String data = getColumnValue(cursor, HistoryTtnTable.Column.ID);
    binderHelper.bind(holder.swipeLayout, data);
//    if (prevPosition<position) {
//      holder.prevHolder = prevTtnHolder;
//    }
    //Log.d("TrackTTN", "Bind Position: " + Integer.toString(position));
    String beginDate = getColumnValue(cursor, HistoryTtnTable.Column.DATE_CREATION);
    String endDate = getColumnValue(cursor, HistoryTtnTable.Column.DATE_DELIVERY);
    if (beginDate!=null && endDate!=null)
      holder.tvDate.setText(MyDateUtils.reformatDate(beginDate) + " - " + MyDateUtils.reformatDate(endDate));
    if (locale.compareTo("ru")==0)
      holder.tvStatus.setText(getColumnValue(cursor, HistoryTtnTable.Column.STATUS_NAME_RU));
    else
      holder.tvStatus.setText(getColumnValue(cursor, HistoryTtnTable.Column.STATUS_NAME_UA));
    String textTTN = getColumnValue(cursor, HistoryTtnTable.Column.TTN);
    holder.tvTTN.setText(MyStringUtils.getFormattedTTN(textTTN));
    if (locale.compareTo("ru")==0)
      holder.tvTtnFromCity.setText(getColumnValue(cursor, HistoryTtnTable.Column.LOCALITY_SENDER));
    else
      holder.tvTtnFromCity.setText(getColumnValue(cursor, HistoryTtnTable.Column.LOCALITY_SENDER_UA));
    if (locale.compareTo("ru")==0)
      holder.tvTtnToCity.setText(getColumnValue(cursor, HistoryTtnTable.Column.LOCALITY_RECEIVER));
    else
      holder.tvTtnToCity.setText(getColumnValue(cursor, HistoryTtnTable.Column.LOCALITY_RECEIVER_UA));
    try {
      Double toPay = Double.parseDouble(getColumnValue(cursor, HistoryTtnTable.Column.TO_PAY));
      Double cacheOnDelivery = Double.parseDouble(getColumnValue(cursor, HistoryTtnTable.Column.COD));
      Double totalSum = toPay + cacheOnDelivery;
      if (totalSum < 0d)
        totalSum = 0d;
      holder.tvTtnCost.setText(MyStringUtils.roundTotalSum(totalSum));
    }
    catch (Exception e)
    {
      holder.tvTtnCost.setText(getColumnValue(cursor, HistoryTtnTable.Column.TO_PAY));
    }
    if (getColumnValue(cursor, HistoryTtnTable.Column.VIEW_STATUS).compareTo("3")==0)
      holder.imgTtnDirection.setImageResource(R.drawable.ic_barcode);
    String statusId = getColumnValue(cursor, HistoryTtnTable.Column.STATUS_ID);
//    if (statusId.compareTo("0")==0)
//      holder.frDeleteBackground.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.edeclaration_gradient));
//    else
//      holder.frDeleteBackground.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.declaration_gradient));
//    if (position==0) {
//      holder.llBackground.setBackgroundColor(context.getResources().getColor(android.R.color.transparent));
//      //holder.llDeleteBackground.setBackgroundColor(context.getResources().getColor(android.R.color.transparent));
//    }
    if (prevStatusId.compareTo("0")==0 && position!=0)
    {
      holder.llBackground.setBackgroundColor(context.getResources().getColor(R.color.ttn_details_edecl_end_background));
      holder.frTopBackground.setBackgroundColor(context.getResources().getColor(R.color.ttn_details_edecl_end_background));
      //holder.llDeleteBackground.setBackgroundColor(context.getResources().getColor(R.color.ttn_details_edecl_end_background));
    }
    else if (position!=0) {
      holder.llBackground.setBackgroundColor(context.getResources().getColor(R.color.ttn_details_end_background));
      holder.frTopBackground.setBackgroundColor(context.getResources().getColor(R.color.ttn_details_end_background));
      //holder.llDeleteBackground.setBackgroundColor(context.getResources().getColor(R.color.ttn_details_end_background));
    }

    if (statusId.compareTo("0")==0)
      holder.llBackgroundDrawable.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.edeclaration_gradient));

    if (statusId.compareTo("3")==0) // "Выдан"
      SetTTNStatusView.setCargoGivenStatus(holder.imgStage1, holder.imgStage2, holder.imgStage3, holder.imgLine1, holder.imgLine2, holder.tvCargoGiven);
    else
    if (statusId.compareTo("2")==0) // "В пути"
      SetTTNStatusView.setOnWayStatus(holder.imgStage1, holder.imgStage2, holder.imgStage3, holder.imgLine1, holder.imgLine2);
    else
    if (statusId.compareTo("16")==0) // "На складе"
      SetTTNStatusView.setOnWarehouseStatus(holder.imgStage1, holder.imgStage2, holder.imgStage3, holder.imgLine1, holder.imgLine2);
    else
    if (statusId.compareTo("1")==0) // "Принят"
      SetTTNStatusView.setBeginStatus(holder.imgStage1, holder.imgStage2, holder.imgStage3, holder.imgLine1, holder.imgLine2);
    else
    if (statusId.compareTo("0")==0) // "Создан документ Веб-заявка"
      SetTTNStatusView.setWebDeclarationStatus(holder.flWebDecLine, holder.llWebDecLine);
    else
      SetTTNStatusView.setWebDeclarationStatus(holder.flWebDecLine, holder.llWebDecLine);

    prevStatusId = statusId;
    holder.viewForeground.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (mOnItemClickListener!=null) {
          if (GlobalApplicationData.getInstance().getUserInfo().getName().length()==0)
            cursor.moveToPosition(position-1);
          else
            cursor.moveToPosition(position);
          mOnItemClickListener.onItemClick(v, DictionariesDB.getColumnValue(cursor, HistoryTtnTable.Column.TTN));
        }
      }
    });
    holder.frDeleteBackground.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (ttnManage != null)
          ttnManage.showDeleteHint(DELETE);
        deleteItem(position);
        if (ttnManage != null)
          ttnManage.initTTNHistoryRecyclerView();
      }
    });
//    holder.frArchiveBackground.setOnClickListener(new View.OnClickListener() {
//      @Override
//      public void onClick(View v) {
//        if (ttnManage != null)
//          ttnManage.showDeleteHint(ARCHIVE);
//        archiveItem(position);
//        if (ttnManage != null)
//          ttnManage.initTTNHistoryRecyclerView();
//      }
//    });
//    prevPosition = position;
//    prevTtnHolder = holder;
  }

  private void deleteItem(int position) {
    String itemId = getActionItemId(position);
    DeleteCommand dc = new DeleteCommand(context);
    if (dc.execute(itemId))
      history.push(dc);
    cursor.requery();
    // notify the item removed by position
    // to perform recycler view delete animations
    // NOTE: don't call notifyDataSetChanged()
    //notifyItemRemoved(position);
    notifyDataSetChanged();
  }

  private void archiveItem(int position) {
    String itemId = getActionItemId(position);
    ArchiveCommand ac = new ArchiveCommand(context);
    if (ac.execute(itemId))
      history.push(ac);
    cursor.requery();
    // notify the item removed by position
    // to perform recycler view delete animations
    // NOTE: don't call notifyDataSetChanged()
    //notifyItemRemoved(position);
    notifyDataSetChanged();
  }

  private String getActionItemId(int position){
    if (GlobalApplicationData.getInstance().getUserInfo().getName().length()==0)
      cursor.moveToPosition(position-1);
    else
      cursor.moveToPosition(position);
    return getColumnValue(cursor, HistoryTtnTable.Column.ID);
  }

//  private void removeItem(int position, int deleteType) {
//    if (mRecyclerView!=null)
//    {
//      int prevPosition = position - 1;
//      int nextPosition = position + 1;
//      if (GlobalApplicationData.getInstance().getUserInfo().getName().length()==0 && prevPosition==0)
//      {
//        TtnHolder holder = (TtnHolder) mRecyclerView.getChildViewHolder(mRecyclerView.getChildAt(1));
//        holder.frBottomCard.setVisibility(View.VISIBLE);
//      }
//      else {
//        TtnHolder nextHolder;
//        if (prevPosition!=-1) {
//          TtnHolder holder = (TtnHolder) mRecyclerView.getChildViewHolder(mRecyclerView.getChildAt(prevPosition));
//
//          int countItems = 0;
//          if (GlobalApplicationData.getInstance().getUserInfo().getName().length() == 0 && cursor.getCount() > 0)
//            countItems = 1 + cursor.getCount();
//          else
//            countItems = cursor.getCount();
//          if (nextPosition < countItems) {
//            nextHolder = (TtnHolder) mRecyclerView.getChildViewHolder(mRecyclerView.getChildAt(nextPosition));
//            nextHolder.prevHolder = holder;
//            holder.frBottomCard.setTranslationX(0.0f);
//            ViewGroup.LayoutParams lp1 = holder.viewBackgroundBottom.getLayoutParams();
//            lp1.width = ViewGroup.LayoutParams.WRAP_CONTENT;
//            holder.viewBackgroundBottom.setLayoutParams(lp1);
//          } else {
//            holder.frBottomCard.setTranslationX(0.0f);
//            ViewGroup.LayoutParams lp1 = holder.viewBackgroundBottom.getLayoutParams();
//            lp1.width = ViewGroup.LayoutParams.WRAP_CONTENT;
//            holder.viewBackgroundBottom.setLayoutParams(lp1);
//            holder.frBottomLayout.setVisibility(View.GONE);
//          }
//        }
//      }
//    }
//    if (GlobalApplicationData.getInstance().getUserInfo().getName().length()==0)
//      cursor.moveToPosition(position-1);
//    else
//      cursor.moveToPosition(position);
//    idForDelete = getColumnValue(cursor, HistoryTtnTable.Column.ID);
//    if (deleteType==DELETE)
//      DictionariesDB.getInstance(context).tableTTNHistory.delete(idForDelete);
//    else
//      DictionariesDB.getInstance(context).tableTTNHistory.archive(idForDelete);
//    cursor.requery();
//    // notify the item removed by position
//    // to perform recycler view delete animations
//    // NOTE: don't call notifyDataSetChanged()
//    notifyItemRemoved(position);
//  }

//  public void fullDelete()
//  {
//    DictionariesDB.getInstance(context).tableTTNHistory.fullDelete(GlobalApplicationData.getInstance().idForDelete);
//  }

  public void undoremoveItem() {
    TtnListCommand tc = history.pop();
    tc.undo();
    cursor.requery();
    notifyDataSetChanged();
    if (ttnManage!=null)
      ttnManage.initTTNHistoryRecyclerView();
  }

  @Override
  public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
      if (GlobalApplicationData.getInstance().getUserInfo().getName().length()!=0)
        ttnBind((TtnHolder)holder, position);
      else
      {
        if (position!=0)
          ttnBind((TtnHolder)holder, position);
      }
  }

  @Override
  public int getItemCount() {
    if (GlobalApplicationData.getInstance().getUserInfo().getName().length()==0 && cursor.getCount()>0)
      return 1 + cursor.getCount();
    else if (GlobalApplicationData.getInstance().getUserInfo().getName().length()==0 && cursor.getCount()==0)
      return 1;
    else
      return cursor.getCount();
  }

  class LoginHolder extends RecyclerView.ViewHolder {
    Context mContext;
    View container;
    Button btnLogin;
    public LoginHolder(View view, Context context) {
      super(view);
      container = view;
      mContext = context;
      btnLogin = (Button) view.findViewById(R.id.btnLogin1);
      btnLogin.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          if (GlobalApplicationData.getInstance().confidentialityReaded==false)
            mContext.startActivity(new Intent(mContext, ConfidentialityActivity.class));
          else
            mContext.startActivity(new Intent(mContext, AutorizationActivity.class));
        }
      });
    }
  }

  public class TtnHolder extends RecyclerView.ViewHolder {

    SwipeRevealLayout swipeLayout;
    ImageView imgStage1;
    ImageView imgStage2;
    ImageView imgStage3;
    ImageView imgLine1;
    ImageView imgLine2;
    ImageView imgTtnDirection;
    FrameLayout flWebDecLine;
    LinearLayout llWebDecLine;
    LinearLayout llBackground;
    FrameLayout llBackgroundDrawable;
    FrameLayout frDeleteBackground;
    LinearLayout viewForeground;
    FrameLayout frTopBackground;
    //FrameLayout frArchiveBackground;
    TextView tvDate;
    TextView tvStatus;
    TextView tvTTN;
    TextView tvCargoGiven;
    TextView tvTtnFromCity;
    TextView tvTtnToCity;
    TextView tvTtnCost;

    View container;

    public TtnHolder(View view) {
      super(view);
      container = view;
      swipeLayout = (SwipeRevealLayout) view.findViewById(R.id.swipeLayout);
      //frArchiveBackground = (FrameLayout) view.findViewById(R.id.frArchiveBackground);
      frDeleteBackground = (FrameLayout) view.findViewById(R.id.frDeleteBackground);
      frTopBackground = (FrameLayout) view.findViewById(R.id.frTopBackground);
      imgStage1 = (ImageView) view.findViewById(R.id.imgStage1);
      imgStage2 = (ImageView) view.findViewById(R.id.imgStage2);
      imgStage3 = (ImageView) view.findViewById(R.id.imgStage3);
      imgLine1 = (ImageView) view.findViewById(R.id.imgLine1);
      imgLine2 = (ImageView) view.findViewById(R.id.imgLine2);
      tvDate = (TextView) view.findViewById(R.id.tvTtnHistoryDate);
      tvStatus = (TextView) view.findViewById(R.id.tvTtnHistoryStatus);
      tvTTN = (TextView) view.findViewById(R.id.tvTtnHistoryNumber);
      tvTtnFromCity = (TextView) view.findViewById(R.id.tvTtnFromCity);
      tvTtnToCity = (TextView) view.findViewById(R.id.tvTtnToCity);
      tvTtnCost = (TextView) view.findViewById(R.id.tvTtnCost);
      llBackground = (LinearLayout) view.findViewById(R.id.llTtnHistoryBackground);
      llBackgroundDrawable = (FrameLayout) view.findViewById(R.id.llTtnHistoryBackgroundDrawable);
      flWebDecLine = (FrameLayout) view.findViewById(R.id.flWebDecLine);
      llWebDecLine = (LinearLayout) view.findViewById(R.id.llWebDecLine);
      tvCargoGiven = (TextView) view.findViewById(R.id.tvCargoGiven);
      imgTtnDirection = (ImageView) view.findViewById(R.id.imgTtnDirection);
      viewForeground = (LinearLayout) view.findViewById(R.id.viewForeground);
    }
  }

}
