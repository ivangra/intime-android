package com.ua.inr0001.intime;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.FrameLayout;

import intime.utils.DisplayUnits;

/**
 * Created by ILYA on 12.09.2016.
 */
public class CameraFrameLayout extends FrameLayout {

  private Paint mPaint;
  private Context context;

  private void setPaint()
  {
    mPaint = new Paint();
    mPaint.setColor(Color.argb(80, 0, 0, 0));
    mPaint.setAlpha(100);
    mPaint.setARGB(100, 255, 255, 255);
    mPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
  }

  public CameraFrameLayout(Context context, AttributeSet attrs) {
    super(context, attrs);
    //this.setWillNotDraw(false);
    this.context = context;
    setPaint();
  }

  public CameraFrameLayout(Context context) {
    super(context);
    //this.setWillNotDraw(false);
    setPaint();
  }

  @Override
  protected void onDraw(Canvas canvas) {
    super.onDraw(canvas);
    int width = canvas.getWidth();
    int height = canvas.getHeight();
    canvas.drawRect(0,
      (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 100, getResources().getDisplayMetrics()),
      width,
      height - (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 120, getResources().getDisplayMetrics()),
      mPaint);
  }
}
