package com.ua.inr0001.intime;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.view.inputmethod.InputMethodSubtype;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.FilterQueryProvider;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import intime.llc.ua.R;

public class SelectCityActivity extends AppCompatActivity {

  private ListView cityList;
  private EditText editCity;
  private LinearLayout llSelectOnMap;
  private String cityRS;
  private String cityCode;
  private String cityName;
  private String street;
  private String streetNumber;
  private String warehouseId;
  private int hasStore = 0;
  private int hasPostomat = 0;
  private SimpleCursorAdapter scAdapter;
  private boolean currier = false;
  private Context mContext;
  private Cursor citiesCursor;

  private String[] from = new String[]{CityTable.Column.CITY, CityTable.Column.STATE, CityTable.Column.AREA};
  private int[] to = new int[]{R.id.tvCityName, R.id.tvState, R.id.tvArea};

  private Cursor getCityListLang(String city)
  {
    Cursor cur;
    if (city.length() == 0) {
      if (isUaString()) {
        cur = DictionariesDB.getInstance(mContext).tableCity.selectAllUA();
        if (cur.getCount() == 0)
        {
          cur.close();
          cur = DictionariesDB.getInstance(mContext).tableCity.selectAll();
        }
      }
      else {
        cur = DictionariesDB.getInstance(mContext).tableCity.selectAll();
        if (cur.getCount() == 0)
        {
          cur.close();
          cur = DictionariesDB.getInstance(mContext).tableCity.selectAllUA();
        }
      }
    }
    else {
      if (isUaString()) {
        cur = DictionariesDB.getInstance(mContext).tableCity.selectByNameUA(city);
        if (cur.getCount() == 0)
        {
          cur.close();
          cur = DictionariesDB.getInstance(mContext).tableCity.selectByName(city);
        }
      }
      else {
        cur = DictionariesDB.getInstance(mContext).tableCity.selectByName(city);
        if (cur.getCount() == 0)
        {
          cur.close();
          cur = DictionariesDB.getInstance(mContext).tableCity.selectByNameUA(city);
        }
      }
    }
    return cur;
  }

  private void setCitiesCursorAdapter(ListView listview)
  {

    String[] from =new String[]{CityTable.Column.CITY, CityTable.Column.STATE, CityTable.Column.AREA};
    int[] to = new int[]{R.id.tvCityName, R.id.tvState, R.id.tvArea};
    if (citiesCursor != null && citiesCursor.isClosed())
      citiesCursor = DictionariesDB.getInstance(mContext).tableCity.selectAll();
    scAdapter = new SimpleCursorAdapter(mContext, R.layout.select_city_item, citiesCursor, from , to);

    scAdapter.setFilterQueryProvider(new FilterQueryProvider() {
      @Override
      public Cursor runQuery(CharSequence charSequence) {
        return getCityListLang(charSequence.toString());
      }
    });

    listview.setAdapter(scAdapter);
    listview.setTextFilterEnabled(false);
    listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        cityName = DictionariesDB.getInstance(mContext).getColumnValue(scAdapter, position, CityTable.Column.CITY);
        cityCode = DictionariesDB.getInstance(mContext).getColumnValue(scAdapter, position, CityTable.Column.CODE);
        warehouseId = DictionariesDB.getInstance(mContext).getColumnValue(scAdapter, position, CityTable.Column.WAREHOUSE_ID);
        hasStore = Integer.parseInt(DictionariesDB.getInstance(mContext).getColumnValue(scAdapter, position, CityTable.Column.HAS_STORE));
        hasPostomat = Integer.parseInt(DictionariesDB.getInstance(mContext).getColumnValue(scAdapter, position, CityTable.Column.HAS_POSTOMAT));
        finishAct();
      }
    });
  }

  private void getCityData(final String searchString, final String lang)
  {
    Cursor cityDataCursor = null;
    try {

      cityDataCursor = DictionariesDB.getInstance(mContext).tableCity.selectByNameUA(searchString);
      cityDataCursor.moveToFirst();
      cityName = DictionariesDB.getColumnValue(cityDataCursor, CityTable.Column.CITY);
      cityCode = DictionariesDB.getColumnValue(cityDataCursor, CityTable.Column.CODE);
      warehouseId = DictionariesDB.getColumnValue(cityDataCursor, CityTable.Column.WAREHOUSE_ID);
      if (cityDataCursor!=null)
        cityDataCursor.close();
      finishAct();
    }
    catch(Exception e)
    {
      if (cityDataCursor != null)
        cityDataCursor.close();
//      Log.d("SelectCity", "Error1 : " + e.getMessage().toString());
      try {
        cityDataCursor = DictionariesDB.getInstance(mContext).tableCity.selectByName(searchString);
        cityDataCursor.moveToFirst();
        cityName = DictionariesDB.getColumnValue(cityDataCursor, CityTable.Column.CITY);
        cityCode = DictionariesDB.getColumnValue(cityDataCursor, CityTable.Column.CODE);
        warehouseId = DictionariesDB.getColumnValue(cityDataCursor, CityTable.Column.WAREHOUSE_ID);
        if (cityDataCursor != null)
          cityDataCursor.close();
        finishAct();
      }
      catch(Exception e1){
//        Log.d("SelectCity", "Error2: " + e1.getMessage().toString());
        if (cityDataCursor!=null)
          cityDataCursor.close();
      }
    }
    finally {
      if (cityDataCursor!=null)
        cityDataCursor.close();
    }
  }

  public void finishAct()
  {
    Intent intent = new Intent();
    intent.putExtra("City", cityRS);
    intent.putExtra("Code", cityCode);
    intent.putExtra("Name", cityName);
    intent.putExtra("HasStore", hasStore);
    intent.putExtra("HasPostomat", hasPostomat);
    intent.putExtra("Street", street);
    intent.putExtra("StreetNumber", streetNumber);
    intent.putExtra("WarehouseId", warehouseId);
    setResult(RESULT_OK, intent);
    finish();
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_select_city);
    mContext = this;
    Toolbar toolbar = (Toolbar) findViewById(R.id.selectCityToolbar);
    setSupportActionBar(toolbar);
    toolbar.setTitle("");
    toolbar.setNavigationOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        onBackPressed();
      }
    });
    cityRS = getIntent().getStringExtra("City");
    currier = getIntent().getBooleanExtra("Currier", false);
    cityList = (ListView) findViewById(R.id.lvCities);
    llSelectOnMap = (LinearLayout) findViewById(R.id.llSelectOnMap);
    if (currier)
      llSelectOnMap.setVisibility(View.VISIBLE);
    llSelectOnMap.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        startActivityForResult(new Intent(mContext, CurrierMapActivity.class), 2340);
      }
    });
    if (isInterfceUA())
      citiesCursor = DictionariesDB.getInstance(this).tableCity.selectAllUA();
    else
      citiesCursor = DictionariesDB.getInstance(this).tableCity.selectAll();
    startManagingCursor(citiesCursor);
    setCitiesCursorAdapter(cityList);
    editCity = (EditText)  findViewById(R.id.editCity);

    editCity.addTextChangedListener(new TextWatcher() {

      public void afterTextChanged(Editable s) {

      }

      public void beforeTextChanged(CharSequence s, int start,
                                    int count, int after) {
      }

      public void onTextChanged(CharSequence s, int start,
                                int before, int count)
      {
        scAdapter.getFilter().filter(s);
      }
    });

  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if ( resultCode==RESULT_OK) {
      if (data != null){
        cityName = data.getStringExtra("City");
        street = data.getStringExtra("Street");
        streetNumber = data.getStringExtra("StreetNumber");
        getCityData(cityName, GlobalApplicationData.getCurrentLanguage());
      }
    }
  }

  @Override
  protected void onPause() {
    super.onPause();
    hideSoftKeyboard();
  }

  @Override
  protected void onStop(){
    super.onStop();
    stopManagingCursor(citiesCursor);
    citiesCursor.close();
  }

  public void hideSoftKeyboard() {
    try {
      InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
      inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    } catch (Exception e) { }
  }

  private boolean isInterfceUA()
  {
    String curLang =GlobalApplicationData.getCurrentLanguage();
    if (curLang.compareTo("ua")==0)
      return true;
    else
      return false;
  }

  private boolean isUaString()
  {
    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
    InputMethodSubtype ims = imm.getCurrentInputMethodSubtype();
    if (ims==null) {
        return isInterfceUA();
    }
    else {
      String locale;
      String compared = "uk";
      try {
        if (Build.VERSION.SDK_INT <= 23) {
          locale = ims.getLocale();
          compared = "uk";
        } else {
          locale = ims.getExtraValueOf("InputBundleResource").toString();
          compared = "ime_uk";
        }
      }
      catch(Exception e)
      {
        locale = null;
      }
      if (locale == null || locale.length() == 0) {
        return isInterfceUA();
      } else {
        if (locale.compareTo(compared) == 0)
          return true;
        else
          return false;
      }
    }
  }

}
