package com.ua.inr0001.intime;

public class DistanceCalculator {

	private static final double equatorialEarthRadius = 6378.1370D;
	private static final double deg2rad = (Math.PI / 180);
	
	public static double distance(double lat1, double lon1, double lat2, double lon2) {
		double dlong = (lon2 - lon1) * deg2rad;
		double dlat = (lat2 - lat1) * deg2rad;
		double a = Math.pow(Math.sin(dlat / 2), 2) +
				Math.cos(lat1 * deg2rad) * Math.cos(lat2 * deg2rad) * Math.pow(Math.sin(dlong / 2), 2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		double d = equatorialEarthRadius * c;
		return d * 1000;
	}
	
}
