package com.ua.inr0001.intime;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import intime.llc.ua.R;

/**
 * Created by ILYA on 12.10.2016.
 */

public class TextChangeListener implements TextWatcher {

  protected EditText editText;
  private TextView textView;
  protected Context context;
  protected ImageView imgWarning;
  private String hint;

  public TextChangeListener(Context context, EditText editText, TextView textView, ImageView imgWarinig)
  {
    this.context = context;
    this.textView = textView;
    this.imgWarning = imgWarinig;
    this.editText = editText;
    if (editText!=null && editText.getHint()!=null)
      this.hint = editText.getHint().toString();
  }

  @Override
  public void beforeTextChanged(CharSequence s, int start, int count, int after) {
  }

  @Override
  public void onTextChanged(CharSequence s, int start, int before, int count) {
    if (textView!=null)
      textView.setTextColor(context.getResources().getColor(R.color.text_hint));
    if (imgWarning!=null)
      imgWarning.setVisibility(View.GONE);
    if (editText!=null)
      editText.setHintTextColor(context.getResources().getColor(R.color.paper_caption));
    if (editText!=null) {
      if (editText.getText().toString().length() > 0) {
        editText.setHint("");
        if (textView!=null)
          textView.setVisibility(View.VISIBLE);
      }
      else {
        editText.setHint(hint);
        if (textView!=null)
          textView.setVisibility(View.GONE);
      }
    }
  }

  @Override
  public void afterTextChanged(Editable s) {
  }
}
