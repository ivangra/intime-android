package com.ua.inr0001.intime;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.IOException;

import intime.api.model.auth.Auth;
import intime.api.model.userinfo.UserInfo;
import intime.llc.ua.R;
import intime.maskedtextedit.MaskedInputFilter;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AutorizationActivity extends AppCompatActivity {

  private static final String TAG = "Auth";

  //private TextView tvAuthName;
  private MaskedInputFilter maskedInputFilter;
  private Context context;
  private FrameLayout frWrongLogin;
  private EditText editLogin;
  private EditText editPassword;
  private CheckBox chLoginShowPassword;
  private TextView tvPassword;
  private TextView tvRegistration;
  private TextView tvRecoveryPassword;
  private LinearLayout layoutLogin;
  private Button btnAuth;
  //private Button btnCancel;
  private boolean exitFromLogin = true;
  private boolean activityPaused = false;
  private TextWatcher textWatcher;
  private Call<Auth> loginCall;
  private Call<UserInfo> userInfoCall;
  private Call<ResponseBody> userExistsCall;

  /*private Handler mMessageHandler = new Handler(){
    @Override
    public void handleMessage(Message m){
      Bundle b = m.getData();
        if (b.getString("result").equals(getResources().getString(android.R.string.ok)))
        {
          GlobalApplicationData.getInstance().setContragentID("");
          GlobalApplicationData.getInstance().setContragentKEY("");
          GlobalApplicationData.getInstance().setContragentNAME("");
          GlobalApplicationData.getInstance().saveAuthorizationData();
          finish();
        }
    }
  };*/

  private void hideSoftKeyboard() {
    try {
      InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
      inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    } catch (Exception e) { }
  }

  private void initShowPassword(final EditText edPassword, final CheckBox chPassword)
  {
    chPassword.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(isChecked) {
          edPassword.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
          //edPassword.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
          edPassword.setTextColor(getResources().getColor(R.color.base_text_color));
        } else {
          edPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
          //edPassword.setTextSize(TypedValue.COMPLEX_UNIT_SP, 32);
          edPassword.setTextColor(getResources().getColor(R.color.paper_caption));
        }
      }
    });
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_autorization);

    context = this;

    Toolbar mActionBarToolbar = (Toolbar) findViewById(R.id.authToolbar1);
    mActionBarToolbar.setTitle("");
    setSupportActionBar(mActionBarToolbar);

    overridePendingTransition(R.animator.slide_up, R.animator.fade_out);
    
    editLogin = (EditText) findViewById(R.id.editLogin);
    editPassword = (EditText) findViewById(R.id.editPasswd);
    chLoginShowPassword = (CheckBox) findViewById(R.id.chLoginShowPassword);
    initShowPassword(editPassword, chLoginShowPassword);
    frWrongLogin = (FrameLayout) findViewById(R.id.frameWrongLoginHint);
    textWatcher = new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {
        frWrongLogin.setVisibility(View.INVISIBLE);
      }

      @Override
      public void afterTextChanged(Editable s) {
        if (editLogin.getText().toString().trim().length()>=17)
          isUserExists(editLogin.getText().toString());
      }
    };

    editLogin.addTextChangedListener(textWatcher);
    editLogin.setText("+38 0");
    editLogin.setSelection(editLogin.length());
    maskedInputFilter = new MaskedInputFilter("+38 0## ### ## ##", editLogin, '#');
    editLogin.setFilters(new InputFilter[]{maskedInputFilter});
    //editPassword.addTextChangedListener(textWatcher);
    tvPassword = (TextView) findViewById(R.id.tvPassword);
    layoutLogin = (LinearLayout) findViewById(R.id.layoutLogin);
    //tvAuthName = (TextView) findViewById(R.id.tvAuthName);
    btnAuth = (Button) findViewById(R.id.btnLogin);
    btnAuth.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        btnLoginClick();
      }
    });
    tvRecoveryPassword = (TextView) findViewById(R.id.tvRecoveryPassword);
    tvRecoveryPassword.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent registerIntent = new Intent(context, RecoveryPasswordActivity.class);
        registerIntent.putExtra("phone", editLogin.getText().toString());
        startActivity(registerIntent);
      }
    });
    tvRegistration = (TextView) findViewById(R.id.tvRegistration);
    tvRegistration.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent registerIntent = new Intent(context, RegisterUserActivity.class);
        registerIntent.putExtra("phone", editLogin.getText().toString());
        startActivity(registerIntent);
      }
    });
    //btnCancel = (Button) findViewById(R.id.btnLoginCancel);
  }

  private void isUserExists(final String inPhone) {

    String phone = inPhone.replace(" ", "").replace("+", "");

    Retrofit retrofit = new Retrofit.Builder()
      .baseUrl("https://esb.intime.ua:4443/services/intime_auth_rest/")
      //.addConverterFactory(GsonConverterFactory.create())
      .client(GlobalApplicationData.getUnsafeOkHttpClient())
      .build();

    IRtfAPI service = retrofit.create(IRtfAPI.class);

    userExistsCall = service.checkPeopleExists("application/json",
      RequestBody.create(MediaType.parse("application/json"), "{\"_post_get_people_by_phone_rest\":{\"phone\":\"" + phone + "\"}}"));

    userExistsCall.enqueue(
      new Callback<ResponseBody>() {
        @Override
        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

          if (response.body()!=null) {
            String body = "";
            try {
              body = new String(response.body().bytes());
            } catch (IOException e) {

            }
            if (!body.contains("Name")) {
              frWrongLogin.setVisibility(View.VISIBLE);
            }
            else
            {
              frWrongLogin.setVisibility(View.GONE);
            }
          }
        }

        @Override
        public void onFailure(Call<ResponseBody> call, Throwable t) {
          //frPreCalcSum1.setVisibility(View.GONE);
          Log.d(TAG, "Check phone error: " + t.getMessage());
        }

      }
    );

  }


  private void getUserInfo(final String inPhone) {

    String phone = inPhone.replace(" ", "").replace("+", "");

    Retrofit retrofit = new Retrofit.Builder()
      .baseUrl("https://esb.intime.ua:4443/services/intime_auth_rest/")
      .addConverterFactory(GsonConverterFactory.create())
      .client(GlobalApplicationData.getUnsafeOkHttpClient())
      .build();

    IRtfAPI service = retrofit.create(IRtfAPI.class);

    userInfoCall = service.getUserInfo("application/json",
      RequestBody.create(MediaType.parse("application/json"), "{\"_post_get_people_by_phone_rest\":{\"phone\":\"" + phone + "\"}}"));

    userInfoCall.enqueue(
      new Callback<UserInfo>() {
        @Override
        public void onResponse(Call<UserInfo> call, Response<UserInfo> response) {
          UserInfo userInfo = response.body();
          if (userInfo!=null)
          {
            GlobalApplicationData.getInstance().saveUserInfo(userInfo.getEntriesGetPeopleByPhone().getEntryGetPeopleByPhone().get(0));
            finish();
          }
        }

        @Override
        public void onFailure(Call<UserInfo> call, Throwable t) {
          //frPreCalcSum1.setVisibility(View.GONE);
          Log.d(TAG, "Check phone error: " + t.getMessage());
        }

      }
    );

  }


  private void loginRequest(String inPhone, String password) {

    String phone = inPhone.replace(" ", "").replace("+", "");

    Retrofit retrofit = new Retrofit.Builder()
      .baseUrl("https://esb.intime.ua:4443/services/intime_auth_rest/")
      .addConverterFactory(GsonConverterFactory.create())
      .client(GlobalApplicationData.getUnsafeOkHttpClient())
      .build();

    IRtfAPI service = retrofit.create(IRtfAPI.class);
    loginCall = service.authUser("application/json",
      RequestBody.create(MediaType.parse("application/json"), "{\"_post_auth_rest\":{\"login\":\"" + phone + "\",\"password\":\"" + password + "\"}}"));
    loginCall.enqueue(
      new Callback<Auth>() {
        @Override
        public void onResponse(Call<Auth> call, Response<Auth> response) {
          //ResponseBody body = response.body();
          Auth auth = response.body();
          if (auth!=null)
          {
            String result = auth.getEntriesAuth().getEntryAuth().get(0).getResult();
            if (result.compareTo("0")!=0) {
              getUserInfo(editLogin.getText().toString());

            }
            else
            {
              showMessage(getResources().getString(R.string.dialog_title),
                getResources().getString(R.string.wrong_login_or_password));
            }
          }
        }

        @Override
        public void onFailure(Call<Auth> call, Throwable t) {
          //frPreCalcSum1.setVisibility(View.GONE);
          Log.d(TAG, "Auth Error: " + t.getMessage());
        }

      }
    );

  }


  @Override
  public void onBackPressed() {
    if (exitFromLogin)
      super.onBackPressed();
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();
    switch (id)
    {
      case R.id.action_close: onBackPressed(); return true;
      default: return super.onOptionsItemSelected(item);
    }
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.close_action_menu, menu);
    return true; //super.onCreateOptionsMenu(menu);
  }

  @Override
  protected void onStart() {
    super.onStart();
    /*if (GlobalApplicationData.getInstance().getContragentNAME().length()>0)
    {
      String str = String.format(getResources().getString(R.string.auth_message), GlobalApplicationData.getInstance().getContragentNAME());
      tvAuthName.setText(str);
      tvAuthName.setVisibility(View.VISIBLE);
      btnCancel.setVisibility(View.VISIBLE);
    }
    else
    {
      tvAuthName.setText(getResources().getString(R.string.text_welcome_login));
      btnCancel.setVisibility(View.GONE);
    }*/
  }

  @Override
  protected void onPause() {
    overridePendingTransition(R.animator.nothing, R.animator.slide_down);
    super.onPause();
    activityPaused = true;
    if (loginCall!=null)
      loginCall.cancel();
    if (userInfoCall!=null)
      userInfoCall.cancel();
    if (userExistsCall!=null)
      userExistsCall.cancel();
  }

  @Override
  protected void onResume() {
    super.onResume();
    activityPaused = false;
  }

  private void showMessage(String caption, String message)
  {
    if (activityPaused==false) {
      MessageDialog dlgWarning = new MessageDialog();
      dlgWarning.setParams(caption, message, false, null);
      dlgWarning.show(getSupportFragmentManager(), "Warning");
    }
  }
  
  private boolean validateForm()
  {
    if (editLogin.getText().toString().length() == 0 ||
        editPassword.getText().toString().length() == 0 )
    {
      showMessage(getResources().getString(R.string.dialog_title),
                  getResources().getString(R.string.dialog_message_fill_all_fields));
      return false;
    }
    
    if (editLogin.getText().toString().length()<17)
    {
      showMessage(getResources().getString(R.string.dialog_title),
          getResources().getString(R.string.dialog_message_wrong_phone));
      return false;
    }
    
    return true;
  }
  
  public void btnLoginClick()
  {
    if (!GlobalApplicationData.getInstance().IsOnline())
    {
      showMessage(getResources().getString(R.string.dialog_warning_caption),
                  getResources().getString(R.string.dialog_message_no_network_access));
    }
    else
      if (validateForm())
        loginRequest(editLogin.getText().toString(), editPassword.getText().toString());
  }
  
}
