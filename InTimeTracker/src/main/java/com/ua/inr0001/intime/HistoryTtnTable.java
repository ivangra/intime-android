package com.ua.inr0001.intime;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import intime.calculator.constants.CargoType;
import intime.llc.ua.R;

import static intime.utils.MyStringUtils.getLangStringArray;

public class HistoryTtnTable implements ITable {

  private SQLiteDatabase sqlDB;
  

  public HistoryTtnTable(SQLiteDatabase sqldb)
  {
    sqlDB = sqldb;
  }

  public static final class StatusView {
    public static final int DEFAULT = 0;
    public static final int DELETED = 1;
    public static final int ARCHIVE = 2;
    public static final int TRACKED = 3;
  }

  public static final class Column
  {
    public static final String ID = "_id";
    public static final String TTN = "Ttn";
    public static final String DECL_ID = "DelcId";
    public static final String DATE_CREATION = "DateCreation";
    public static final String DATE_DELIVERY = "DateDelivery";
    public static final String STATUS_NAME_RU = "Status";
    public static final String STATUS_NAME_UA = "StatusUa";
    public static final String LOCALITY_SENDER = "LocalitySender";
    public static final String LOCALITY_SENDER_UA = "LocalitySenderUa";
    public static final String LOCALITY_RECEIVER = "LocalityReceiver";
    public static final String LOCALITY_RECEIVER_UA = "LocalityReceiverUa";
    public static final String STATUS_ID = "StatusId";
    public static final String VIEW_STATUS = "StatusView"; // 1-deleted , 2 - archive, 3 = tarcked
    public static final String COST = "Cost";
    public static final String TO_PAY = "toPay";
    public static final String COD = "cod"; // cache_on_delivery
    public static final String COST_RETURN = "CostReturn";
    public static final String STORE_CODE = "StoreCode";
    public static final String TYPE = "Type";
    public static final String TYPE_UA = "TypeUA";
    public static final String DESCRIPTION = "Description";
    public static final String SEATS = "Seats";
    public static final String PAYER = "Payer";
    public static final String PAYER_UA = "PayerUA";
    public static final String ADDRESS = "Address";
    public static final String ADDRESS_UA = "AddressUA";
    public static final String STORE_TYPE = "StoreType";
    public static final String WEIGHT = "Weight";
    public static final String VOLUME = "Volume";
    public static final String CARGO_TYPE = "CargoType";
    public static final String CARGO_TYPE_UA = "CargoTypeUA";
  }
  
  public static final String NAME = "tblHistoryTtn";
  public static final String VIEW_NAME = "vwHistoryTtn";
  
  public static final String SQL_CREATE_TABLE =
      "create table if not exists " + NAME + " ( " +
              Column.ID + " integer primary key autoincrement, " +
              Column.DECL_ID + " text, " +
              Column.TTN + " text, " +
              Column.DATE_CREATION + " text, " +
              Column.DATE_DELIVERY + " text, " +
              Column.LOCALITY_SENDER + " text, " +
              Column.LOCALITY_SENDER_UA + " text, " +
              Column.LOCALITY_RECEIVER + " text, " +
              Column.LOCALITY_RECEIVER_UA + " text, " +
              Column.STATUS_NAME_RU + " text, " +
              Column.STATUS_NAME_UA + " text, " +
              Column.COST + " text, " +
              Column.TO_PAY + " text, " +
              Column.COD + " text, " +
              Column.COST_RETURN + " text, " +
        Column.STORE_TYPE + " text, " +
        Column.STORE_CODE + " text, " +
        Column.TYPE + " text, " +
        Column.TYPE_UA + " text, " +
        Column.DESCRIPTION + " text, " +
        Column.SEATS + " text, " +
        Column.PAYER + " text, " +
        Column.PAYER_UA + " text, " +
        Column.WEIGHT + " text, " +
        Column.VOLUME + " text, " +
        Column.ADDRESS + " text, " +
        Column.ADDRESS_UA + " text, " +
        Column.CARGO_TYPE + " text, " +
        Column.CARGO_TYPE_UA + " text, " +
              Column.VIEW_STATUS + " integer default 0, " +
              Column.STATUS_ID + " text);";

  public static final String SQL_CREATE_VIEW_6 =
    "CREATE VIEW IF NOT EXISTS " + VIEW_NAME + " AS" +
      " SELECT " + Column.ID + ", " +
      Column.DECL_ID + ", " +
      Column.TTN + ", " +
      Column.DATE_CREATION + ", " +
      Column.DATE_DELIVERY + ", " +
      Column.LOCALITY_SENDER + ", " +
      Column.LOCALITY_SENDER_UA + ", " +
      Column.LOCALITY_RECEIVER + ", " +
      Column.LOCALITY_RECEIVER_UA + ", " +
      Column.STATUS_NAME_RU + ", " +
      Column.STATUS_NAME_UA + ", " +
      Column.VIEW_STATUS + ", " +
      //Column.ARCHIVE + ", " +
      Column.COST + ", " +
      Column.TO_PAY + ", " +
      Column.COD + ", " +
      Column.COST_RETURN + ", " +
      //Column.DELETED + ", " +
      Column.STATUS_ID +
      " FROM " + NAME;

  public static final String SQL_CREATE_VIEW =
       "CREATE VIEW IF NOT EXISTS " + VIEW_NAME + " AS" +
           " SELECT " + Column.ID + ", " +
              Column.DECL_ID + ", " +
              Column.TTN + ", " +
              Column.DATE_CREATION + ", " +
              Column.DATE_DELIVERY + ", " +
              Column.LOCALITY_SENDER + ", " +
              Column.LOCALITY_SENDER_UA + ", " +
              Column.LOCALITY_RECEIVER + ", " +
              Column.LOCALITY_RECEIVER_UA + ", " +
              Column.STATUS_NAME_RU + ", " +
              Column.STATUS_NAME_UA + ", " +
              Column.VIEW_STATUS + ", " +
              Column.COST + ", " +
              Column.TO_PAY + ", " +
              Column.COD + ", " +
              Column.COST_RETURN + ", " +
         Column.STORE_TYPE + ", " +
         Column.STORE_CODE + ", " +
         Column.TYPE + ", " +
         Column.TYPE_UA + ", " +
         Column.DESCRIPTION + ", " +
         Column.SEATS + ", " +
         Column.PAYER + ", " +
         Column.PAYER_UA + ", " +
         Column.WEIGHT + ", " +
         Column.VOLUME + ", " +
         Column.ADDRESS + ", " +
         Column.ADDRESS_UA + ", " +
         Column.CARGO_TYPE + ", " +
         Column.CARGO_TYPE_UA + ", " +
              Column.STATUS_ID +
             " FROM " + NAME;

  public static final String SQL_ADD_TO_PAY = "ALTER TABLE " + NAME + " ADD COLUMN " + Column.TO_PAY + " TEXT;";
  public static final String SQL_ADD_COD = "ALTER TABLE " + NAME + " ADD COLUMN " + Column.COD + " TEXT;";
  public static final String SQL_ADD_COST_RETURN = "ALTER TABLE " + NAME + " ADD COLUMN " + Column.COST_RETURN + " TEXT;";
  public static final String SQL_ADD_STATUS_VIEW = "ALTER TABLE " + NAME + " ADD COLUMN " + Column.VIEW_STATUS + " INTEGER;";

  public static final String SQL_ADD_TYPE = "ALTER TABLE " + NAME + " ADD COLUMN " + Column.TYPE + " TEXT;";
  public static final String SQL_ADD_TYPE_UA = "ALTER TABLE " + NAME + " ADD COLUMN " + Column.TYPE_UA + " TEXT;";
  public static final String SQL_ADD_STORE_CODE = "ALTER TABLE " + NAME + " ADD COLUMN " + Column.STORE_CODE + " TEXT;";
  public static final String SQL_ADD_STORE_TYPE = "ALTER TABLE " + NAME + " ADD COLUMN " + Column.STORE_TYPE + " TEXT;";
  public static final String SQL_ADD_DESCRIPTION = "ALTER TABLE " + NAME + " ADD COLUMN " + Column.DESCRIPTION + " TEXT;";
  public static final String SQL_ADD_SEATS = "ALTER TABLE " + NAME + " ADD COLUMN " + Column.SEATS + " TEXT;";
  public static final String SQL_ADD_PAYER = "ALTER TABLE " + NAME + " ADD COLUMN " + Column.PAYER + " TEXT;";
  public static final String SQL_ADD_PAYER_UA = "ALTER TABLE " + NAME + " ADD COLUMN " + Column.PAYER_UA + " TEXT;";
  public static final String SQL_ADD_WEIGHT = "ALTER TABLE " + NAME + " ADD COLUMN " + Column.WEIGHT + " TEXT;";
  public static final String SQL_ADD_VOLUME = "ALTER TABLE " + NAME + " ADD COLUMN " + Column.VOLUME + " TEXT;";
  public static final String SQL_ADD_ADDRESS = "ALTER TABLE " + NAME + " ADD COLUMN " + Column.ADDRESS + " TEXT;";
  public static final String SQL_ADD_ADDRESS_UA = "ALTER TABLE " + NAME + " ADD COLUMN " + Column.ADDRESS_UA + " TEXT;";
  public static final String SQL_ADD_CARGO_TYPE = "ALTER TABLE " + NAME + " ADD COLUMN " + Column.CARGO_TYPE + " TEXT;";
  public static final String SQL_ADD_CARGO_TYPE_UA = "ALTER TABLE " + NAME + " ADD COLUMN " + Column.CARGO_TYPE_UA + " TEXT;";

  public static final String SQL_CREATE_INDEX_TTN =
      "CREATE INDEX if not exists idx_HistoryTtn ON " + NAME + " (" + Column.TTN +");";

  public static final String SQL_UPDATE_STATUS_VIEW = "UPDATE " + NAME + " SET " + Column.VIEW_STATUS + "=0";

  public static final String SQL_DROP_VIEW =
      "DROP VIEW IF EXISTS " + VIEW_NAME;

  public static final String SQL_DROP_TABLE =
      "DROP TABLE IF EXISTS " + NAME;


  @Override
  public Cursor selectByID(int Id) {
    return null;
  }

  public Cursor selectByID(String Id) {
    return sqlDB.query(NAME, null, Column.TTN + "=?", new String[]{Id}, null, null, Column.DATE_CREATION + " DESC");
  }

  @Override
  public Cursor selectByName(String Name) {
    return null;
  }

  @Override
  public Cursor selectAll() {
    return sqlDB.query(NAME, null, Column.VIEW_STATUS + " IN (0, 3)", null, null, null, Column.DATE_CREATION + " DESC");
  }

  public Cursor selectFiltered(){
    return sqlDB.rawQuery("SELECT * FROM " + NAME + " WHERE (" + GlobalApplicationData.getInstance().filterQuery +
      ") " + " ORDER BY " + Column.DATE_CREATION + " DESC", null);
  }

  @Override
  public boolean exists(String fieldName, String fieldValue) {
      return false;
  }

//  public void updateComment(String ttn, String comment)
//  {
//    ContentValues cv = new ContentValues();
//    cv.put(Column.COMMENT, comment);
//    sqlDB.update(NAME, cv,
//        Column.TTN + "=?",
//        new String[] { ttn });
//  }
  
  public void insert(String declId, String ttn, String dateDelivery, String dateCreation,
                     String localitySender,
                     String localitySenderUa,
                     String localityReceiver,
                     String localityReceiverUa,
                     String statusName,
                     String statusNameUa,
                     String cost,
                     String toPay,
                     String cod,
                     String cost_return,
                     String statusId,
                     String storeCode,
                     String storeType,
                     String type,
                     String type_ua,
                     String description,
                     String seats,
                     String weight,
                     String payer,
                     String payer_ua,
                     String address,
                     String address_ua,
                     String cargoType,
                     String cargoType_ua,
                     String volume,
                     int tracked
  )
  {
    ContentValues cv = new ContentValues();
    cv.put(Column.DECL_ID, declId);
    cv.put(Column.TTN, ttn);
    if (dateDelivery!=null && dateDelivery.length()>10)
      cv.put(Column.DATE_DELIVERY, dateDelivery.substring(0, 10));
    else
      cv.put(Column.DATE_DELIVERY, dateDelivery);
    if (dateCreation!=null && dateCreation.length()>10)
      cv.put(Column.DATE_CREATION, dateCreation.substring(0, 10));
    else
      cv.put(Column.DATE_CREATION, dateCreation);
    cv.put(Column.LOCALITY_SENDER, localitySender);
    cv.put(Column.LOCALITY_SENDER_UA, localitySenderUa);
    cv.put(Column.LOCALITY_RECEIVER, localityReceiver);
    cv.put(Column.LOCALITY_RECEIVER_UA, localityReceiverUa);
    cv.put(Column.STATUS_NAME_RU, statusName);
    cv.put(Column.STATUS_NAME_UA, statusNameUa);
    cv.put(Column.COST, cost);
    cv.put(Column.TO_PAY, toPay);
    cv.put(Column.COD, cod);
    cv.put(Column.COST_RETURN, cost_return);
    cv.put(Column.STATUS_ID, statusId);
    cv.put(Column.VIEW_STATUS, tracked);
    cv.put(Column.TYPE, type);
    cv.put(Column.TYPE_UA, type_ua);
    cv.put(Column.STORE_CODE, storeCode);
    storeType(cv, storeType);
    cv.put(Column.DESCRIPTION, description);
    cv.put(Column.SEATS, seats);
    cv.put(Column.WEIGHT, weight);
    cv.put(Column.VOLUME, volume);
    cv.put(Column.ADDRESS, address);
    cv.put(Column.ADDRESS_UA, address_ua);
    cv.put(Column.PAYER, payer);
    cv.put(Column.PAYER_UA, payer_ua);
    cargoTypeString(cv, cargoType, cargoType_ua);
    sqlDB.insert(NAME, null, cv);
  }

  private void storeType(ContentValues cv, String storeType)
  {
    try
    {
      int iStoreType = Integer.parseInt(storeType);
      if (iStoreType == 1)
        cv.put(Column.STORE_TYPE, "store");
      if (iStoreType == 3)
        cv.put(Column.STORE_TYPE, "postmat");
    }
    catch (Exception e) {
      cv.put(Column.STORE_TYPE, storeType);
    }
  }

  private void cargoTypeString(ContentValues cv, String cargoTypeRU, String cargoTypeUA)
  {
    try
    {
      int iCargoType = Integer.parseInt(cargoTypeRU);
      String[] cargoTextRU = getLangStringArray("ru", R.array.cargo_types);
      String[] cargoTextUA = getLangStringArray("uk", R.array.cargo_types);
      if (iCargoType == CargoType.CARGO) {
        cv.put(Column.CARGO_TYPE, cargoTextRU[0]);
        cv.put(Column.CARGO_TYPE_UA, cargoTextUA[0]);
      }
      if (iCargoType == CargoType.DOC) {
        cv.put(Column.CARGO_TYPE, cargoTextRU[1]);
        cv.put(Column.CARGO_TYPE_UA, cargoTextUA[1]);
      }
      if (iCargoType == CargoType.PALLET) {
        cv.put(Column.CARGO_TYPE, cargoTextRU[2]);
        cv.put(Column.CARGO_TYPE_UA, cargoTextUA[2]);
      }
      if (iCargoType == CargoType.WHEEL) {
        cv.put(Column.CARGO_TYPE, cargoTextRU[3]);
        cv.put(Column.CARGO_TYPE_UA, cargoTextUA[3]);
      }
    }
    catch (Exception e) {
      cv.put(Column.CARGO_TYPE, cargoTypeRU);
      cv.put(Column.CARGO_TYPE_UA, cargoTypeUA);
    }
  }

  public void update(String declId, String ttn, String dateDelivery, String dateCreation,
                     String localitySender,
                     String localitySenderUa,
                     String localityReceiver,
                     String localityReceiverUa,
                     String statusName,
                     String statusNameUa,
                     String cost,
                     String toPay,
                     String cod,
                     String cost_return,
                     String statusId,
                     String storeCode,
                     String storeType,
                     String type,
                     String type_ua,
                     String description,
                     String seats,
                     String weight,
                     String payer,
                     String payer_ua,
                     String address,
                     String address_ua,
                     String cargoType,
                     String cargoType_ua,
                     String volume,
                     int tracked)
  {
    ContentValues cv = new ContentValues();
    if (dateDelivery!=null && dateDelivery.length()>10)
      cv.put(Column.DATE_DELIVERY, dateDelivery.substring(0, 10));
    else
      cv.put(Column.DATE_DELIVERY, dateDelivery);
    if (dateCreation!=null && dateCreation.length()>10)
      cv.put(Column.DATE_CREATION, dateCreation.substring(0, 10));
    else
      cv.put(Column.DATE_CREATION, dateCreation);
    cv.put(Column.LOCALITY_SENDER, localitySender);
    cv.put(Column.LOCALITY_SENDER_UA, localitySenderUa);
    cv.put(Column.LOCALITY_RECEIVER, localityReceiver);
    cv.put(Column.LOCALITY_RECEIVER_UA, localityReceiverUa);
    cv.put(Column.STATUS_NAME_RU, statusName);
    cv.put(Column.STATUS_NAME_UA, statusNameUa);
    cv.put(Column.COST, cost);
    cv.put(Column.TO_PAY, toPay);
    cv.put(Column.COD, cod);
    cv.put(Column.COST_RETURN, cost_return);
    cv.put(Column.STATUS_ID, statusId);
    cv.put(Column.TYPE, type);
    cv.put(Column.TYPE_UA, type_ua);
    cv.put(Column.STORE_CODE, storeCode);
    storeType(cv, storeType);
    cv.put(Column.DESCRIPTION, description);
    cv.put(Column.SEATS, seats);
    cv.put(Column.WEIGHT, weight);
    cv.put(Column.VOLUME, volume);
    cv.put(Column.ADDRESS, address);
    cv.put(Column.ADDRESS_UA, address_ua);
    cv.put(Column.PAYER, payer);
    cv.put(Column.PAYER_UA, payer_ua);
    cargoTypeString(cv, cargoType, cargoType_ua);
    sqlDB.update(NAME, cv, Column.DECL_ID + "=?", new String[]{declId});
  }

  public boolean existDB(String id) {
    Cursor slCursor = sqlDB.query(VIEW_NAME, null, Column.DECL_ID + "=?", new String[]{id}, null, null, null);
    if (slCursor.getCount()>0)
    {
      slCursor.close();
      return true;
    }
    else
    {
      slCursor.close();
      return false;
    }
  }

  public Cursor getCount()
  {
    return sqlDB.rawQuery("SELECT count(1) FROM " + NAME + " WHERE " + Column.VIEW_STATUS + "!=" + StatusView.DELETED, null);
  }

//  public boolean existTracked(String id, String status) {
//    Cursor slCursor = sqlDB.query(NAME, null, Column.DECL_ID + "=? AND " + Column.STATUS_NAME_RU + "=?" , new String[]{id, status}, null, null, null);
//    if (slCursor.getCount()>0)
//    {
//      slCursor.close();
//      return true;
//    }
//    else
//    {
//      slCursor.close();
//      return false;
//    }
//  }

  public void clearTable()
  {
    sqlDB.delete(NAME, Column.VIEW_STATUS + "!='3'", null);
  }

  public void delete(String id)
  {
    ContentValues cv = new ContentValues();
    cv.put(Column.VIEW_STATUS, StatusView.DELETED);
    //sqlDB.delete(NAME, Column.ID + "=" + id , null);
    sqlDB.update(NAME, cv, Column.ID + "=" + id , null);
  }

  public void fullDelete(String id)
  {
    sqlDB.delete(NAME, Column.ID + "=" + id, null);
  }

  public void undoDelete(String id)
  {
    ContentValues cv = new ContentValues();
    cv.put(Column.VIEW_STATUS, StatusView.DEFAULT);
    sqlDB.update(NAME, cv, Column.ID + "=" + id , null);
  }

  public void archive(String id)
  {
    ContentValues cv = new ContentValues();
    cv.put(Column.VIEW_STATUS, StatusView.ARCHIVE);
    sqlDB.update(NAME, cv, Column.ID + "=" + id , null);
  }

  public void undoarchive(String id)
  {
    ContentValues cv = new ContentValues();
    cv.put(Column.VIEW_STATUS, StatusView.DEFAULT);
    sqlDB.update(NAME, cv, Column.ID + "=" + id , null);
  }

}
