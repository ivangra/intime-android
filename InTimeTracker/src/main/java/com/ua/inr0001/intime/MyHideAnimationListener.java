package com.ua.inr0001.intime;

import android.os.Handler;
import android.os.Looper;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;

/**
 * Created by technomag on 24.05.17.
 */

public class MyHideAnimationListener implements Animation.AnimationListener {

  private MenuItem mi;
  private boolean show;
  private IEndShoHideAnimation endAnimation;

  public MyHideAnimationListener(MenuItem mi, boolean show, IEndShoHideAnimation endAnimation)
  {
    this.mi = mi;
    this.show = show;
    this.endAnimation = endAnimation;
  }

  @Override
  public void onAnimationStart(Animation animation) {
  }

  @Override
  public void onAnimationEnd(Animation animation) {
    new Handler(Looper.getMainLooper()).post(new Runnable() {
      @Override
      public void run() {
        if (mi != null) {
          mi.getActionView().clearAnimation();
          if (!show)
            mi.getActionView().setVisibility(View.GONE);
          else
            mi.getActionView().setVisibility(View.VISIBLE);
          mi.setActionView(null);
          mi.setVisible(show);
          if (endAnimation != null)
            endAnimation.animatioEnd();
        }
      }
    });
  }

  @Override
  public void onAnimationRepeat(Animation animation) {

  }
}
