package com.ua.inr0001.intime;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by ILYA on 12.10.2016.
 */

public class AddressesTable implements ITable {

  private SQLiteDatabase sqlDB;

  public static final class Column{
    public static final String ID = "_id";
    public static final String NAME = "Name";
    public static final String PHONE = "Phone";
    public static final String MAIL = "Mail";
    public static final String CITY = "City";
    public static final String STREET = "Street";
    public static final String HOUSE = "House";
    public static final String ROOM = "Room";
    public static final String STAGE = "Stage";
    public static final String ACTIVE = "Active";
  }

  public static final String NAME = "tblAddresses";
  public static final String VIEW_NAME = "vwAddresses";

  public static final String SQL_CREATE_TABLE =
    "create table if not exists " + NAME + " ( " +
      AddressesTable.Column.ID + " integer primary key autoincrement, " +
      AddressesTable.Column.NAME + " text, " +
      AddressesTable.Column.PHONE + " text, " +
      AddressesTable.Column.MAIL + " text, " +
      AddressesTable.Column.CITY + " text, " +
      AddressesTable.Column.STREET + " text, " +
      AddressesTable.Column.HOUSE + " text, " +
      AddressesTable.Column.ROOM + " text, " +
      AddressesTable.Column.STAGE + " text, " +
      AddressesTable.Column.ACTIVE + " integer default 0 );";

  public static final String SQL_CREATE_VIEW =
    "CREATE VIEW if not exists " + VIEW_NAME + " AS" +
      " SELECT " + AddressesTable.Column.ID + ", " +
      AddressesTable.Column.NAME + ", " +
      AddressesTable.Column.PHONE + ", " +
      AddressesTable.Column.MAIL + ", " +
      AddressesTable.Column.CITY + ", " +
      AddressesTable.Column.STREET + ", " +
      AddressesTable.Column.HOUSE + ", " +
      AddressesTable.Column.ROOM + ", " +
      AddressesTable.Column.STAGE + ", " +
      AddressesTable.Column.ACTIVE +
      " FROM " + NAME;

  public AddressesTable(SQLiteDatabase sqlDB)
  {
    this.sqlDB = sqlDB;
  }

  @Override
  public Cursor selectByID(int Id) {
    return sqlDB.query(VIEW_NAME, null, Column.ID + "=?", new String[]{Integer.toString(Id)}, null, null, null);
  }

  @Override
  public Cursor selectByName(String Name) {
    return null;
  }

  @Override
  public Cursor selectAll() {
    return sqlDB.query(VIEW_NAME, null, null, null, null, null, null);
  }

  @Override
  public boolean exists(String fieldName, String fieldValue) {
    return false;
  }

  public void insert(String name, String phone, String mail, String city,
                     String street, String house, String room, String stage)
  {
    ContentValues cv = new ContentValues();
    cv.put(Column.NAME, name);
    cv.put(Column.PHONE, phone);
    cv.put(Column.MAIL, mail);

    cv.put(Column.CITY, city);
    cv.put(Column.STREET, street);
    cv.put(Column.HOUSE, house);
    cv.put(Column.ROOM, room);
    cv.put(Column.STAGE, stage);

    sqlDB.insert(NAME, null, cv);
  }

  public void update(String name, String phone, String mail, String city,
                     String street, String house, String room, String stage, String id)
  {
    ContentValues cv = new ContentValues();
    cv.put(Column.NAME, name);
    cv.put(Column.PHONE, phone);
    cv.put(Column.MAIL, mail);

    cv.put(Column.CITY, city);
    cv.put(Column.STREET, street);
    cv.put(Column.HOUSE, house);
    cv.put(Column.ROOM, room);
    cv.put(Column.STAGE, stage);

    sqlDB.update(NAME, cv, Column.ID + "=?", new String[]{id});
  }

}
