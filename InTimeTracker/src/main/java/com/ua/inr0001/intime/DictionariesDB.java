package com.ua.inr0001.intime;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.os.Build;
import android.util.Log;
import android.widget.SimpleCursorAdapter;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

public class DictionariesDB {

    private DBHelper dbHelper;
    private SQLiteDatabase sqlDB;
    private Context mContext;
    private final String dbName = "Dictionaries.db";
    private boolean createDb = false, upgradeDb = false;
    private final int DB_VERSION = 9; //7; // last new version 4

    /** Города */
    public CityTable tableCity;
    /** Детальное инфо о подразделениях (складах)  */
    public WarehouseDetailsTable tableWarehouseDetails;
    /** Вид упаковок */
    public PackageTable tablePackages;
//    /** История PUSH-уведомлений*/
//    public PushHistoryTable tablePushHistory;
//    /** история ТТН */
//    public HistoryTtnTable tableTTNHistory;

    private static DictionariesDB instance;

    private DictionariesDB(Context context)
    {
        mContext = context;
            OpenDB();
            dbHelper.close();
            dbHelper = null;
            OpenDB();

        String path = sqlDB.getPath();
        tableCity = new CityTable(sqlDB);
        tableWarehouseDetails = new WarehouseDetailsTable(sqlDB);
        tablePackages = new PackageTable(sqlDB);
//        tablePushHistory = new PushHistoryTable(sqlDB);
//        tableTTNHistory = new HistoryTtnTable(sqlDB);
    }

    public static DictionariesDB getInstance(Context context)
    {
      if (instance == null) {
        synchronized (DictionariesDB.class) {
          if (instance == null) {
            instance = new DictionariesDB(InTimeTrackerApp.getAppContext());
          }
        }
      }
      return instance;
    }

    private void OpenDB()
    {
        dbHelper = new DBHelper(mContext, dbName, null, DB_VERSION);
        sqlDB = dbHelper.getWritableDatabase();
    }

    public void closeDB()
    {
        if (dbHelper!=null)
            dbHelper.close();
    }

    public void beginTransaction()
    {
      sqlDB.beginTransaction();
    }

    public void endTransaction()
    {
      sqlDB.endTransaction();
    }

    public void commitTransaction()
    {
      sqlDB.setTransactionSuccessful();
    }

    public boolean isEmptyDB() {
      boolean result = true;
      Cursor cr = tableWarehouseDetails.getCount();
      if (cr != null && cr.moveToFirst()) {
        if (cr.getInt(0)==0)
          result = true;
        else
          result = false;
      }
      cr.close();
      return result;
    }

    public String getColumnValue(SimpleCursorAdapter scAdapter, int position, String columnName)
	  {
      Cursor mCursor = (Cursor) scAdapter.getItem(position);
      return mCursor.getString(mCursor.getColumnIndex(columnName));
	  }

    public static String getColumnValue(Cursor cursor, String columnName)
    {
      return cursor.getString(cursor.getColumnIndex(columnName));
    }

    public String getColumnValue(SimpleCursorAdapter scAdapter, int position, int columnId)
    {
			Cursor mCursor = (Cursor) scAdapter.getItem(position);
			return mCursor.getString(columnId);
    }

    public static void bindString(SQLiteStatement insertStatement, int index, String value)
    {
      try
      {
        insertStatement.bindString(index, value);
      }
      catch(Exception e)
      {
        insertStatement.bindNull(index);
      }
    }

    public static void bindLong(SQLiteStatement insertStatement, int index, String value)
    {
      try
      {
        insertStatement.bindLong(index, Integer.valueOf(value));
      }
      catch (Exception e)
      {
        insertStatement.bindNull(index);
      }
    }

    public static void bindLong(SQLiteStatement insertStatement, int index, int value)
    {
      try
      {
        insertStatement.bindLong(index, value);
      }
      catch (Exception e)
      {
        insertStatement.bindNull(index);
      }
    }

    public static void bindDouble(SQLiteStatement insertStatement, int index, String value)
    {
      try
      {
        insertStatement.bindDouble(index, Double.valueOf(value));
      }
      catch (Exception e)
      {
        insertStatement.bindNull(index);
      }
    }

    class DBHelper extends SQLiteOpenHelper
    {
        public DBHelper(Context context, String name, CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        @Override
        public void onOpen(SQLiteDatabase db) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                db.disableWriteAheadLogging();
            }
          Log.d("DBUpdate", "OnOpen");
          //super.onOpen(db);
          if (createDb) {// The db in the application package
            // context is being created.
            // So copy the contents from the db
            // file packaged in the assets
            // folder:
            Log.d("DBUpdate", "Copy DB by create");
            createDb = false;
            db.execSQL("VACUUM;");
            copyDatabaseFromAssets(db);
//              db= InTimeTrackerApp.getAppContext().openOrCreateDatabase(
//                      dbName, 0, null);
            GlobalApplicationData.getInstance().writeDateUpdateDB();
          }
          if (upgradeDb) {// The db in the application package
            // context is being upgraded from a lower to a higher version.
            Log.d("DBUpdate", "Copy DB by upgrade");
            upgradeDb = false;
            // Your db upgrade logic here:
            db.execSQL("VACUUM;");
            copyDatabaseFromAssets(db);
//              db= InTimeTrackerApp.getAppContext().openOrCreateDatabase(
//                      dbName, 0, null);
            GlobalApplicationData.getInstance().writeDateUpdateDB();
          }
          super.onOpen(db);
        }

      private void copyDatabaseFromAssets(SQLiteDatabase db) {
//    Log.i(TAG, "copyDatabase");
        InputStream myInput = null;
        OutputStream myOutput = null;
        try {
          // Open db packaged as asset as the input stream
          myInput = InTimeTrackerApp.getAppContext().getAssets().open("databases/Dictionaries.db");


          // Open the db in the application package context:
            myOutput = new FileOutputStream(db.getPath());
          db.close();

          // Transfer db file contents:
          byte[] buffer = new byte[1024];
          int length;
          while ((length = myInput.read(buffer)) > 0) {
            myOutput.write(buffer, 0, length);
          }
          myOutput.flush();

          // Set the version of the copied database to the current
          // version:
          SQLiteDatabase copiedDb = InTimeTrackerApp.getAppContext().openOrCreateDatabase(
            dbName, 0, null);
          copiedDb.execSQL("PRAGMA user_version = " + DB_VERSION);
          copiedDb.close();

        } catch (Exception e) {
          Log.d("UpdateDB", e.getMessage());
//      e.printStackTrace();
//      throw new Error(TAG + " Error copying database");
        } finally {
          // Close the streams
          try {
            if (myOutput != null) {
              myOutput.close();
            }
            if (myInput != null) {
              myInput.close();
            }
          } catch (Exception e) {
            Log.d("UpdateDB", e.getMessage());
//        e.printStackTrace();
//        throw new Error(TAG + " Error closing streams");
          }
        }
      }

      private void createDB(SQLiteDatabase db)
        {
          db.execSQL(CityTable.SQL_CREATE_TABLE);
          db.execSQL(CityTable.SQL_CREATE_VIEW);
          db.execSQL(CityTable.SQL_CREATE_INDEX_NAME);
          db.execSQL(CityTable.SQL_CREATE_INDEX_CODE);

          db.execSQL(CityTable.SQL_CREATE_TABLE_UA);
          db.execSQL(CityTable.SQL_CREATE_VIEW_UA);
          db.execSQL(CityTable.SQL_CREATE_INDEX_NAME_UA);
          db.execSQL(CityTable.SQL_CREATE_INDEX_CODE_UA);

          db.execSQL(WarehouseDetailsTable.SQL_CREATE_TABLE);
          db.execSQL(WarehouseDetailsTable.SQL_CREATE_VIEW);
          db.execSQL(WarehouseDetailsTable.SQL_CREATE_TABLE_UA);
          db.execSQL(WarehouseDetailsTable.SQL_CREATE_VIEW_UA);
          db.execSQL(WarehouseDetailsTable.SQL_CREATE_INDEX_ADDRESS_UA);
          db.execSQL(WarehouseDetailsTable.SQL_CREATE_INDEX_ADDRESS);
          db.execSQL(WarehouseDetailsTable.SQL_CREATE_INDEX_CODE);
          db.execSQL(WarehouseDetailsTable.SQL_CREATE_INDEX_CODE_UA);
          db.execSQL(WarehouseDetailsTable.SQL_CREATE_INDEX_LAT);
          db.execSQL(WarehouseDetailsTable.SQL_CREATE_INDEX_LNG);

          db.execSQL(PackageTable.SQL_CREATE_TABLE);
          db.execSQL(PackageTable.SQL_CREATE_VIEW);
          db.execSQL(PackageTable.SQL_CREATE_INDEX);

//          db.execSQL(PushHistoryTable.SQL_CREATE_TABLE);
//          db.execSQL(PushHistoryTable.SQL_CREATE_VIEW);
//          db.execSQL(PushHistoryTable.SQL_CREATE_INDEX);
//
//          db.execSQL(HistoryTtnTable.SQL_CREATE_TABLE);
//          db.execSQL(HistoryTtnTable.SQL_CREATE_VIEW);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
          Log.d("UpdateDB", "OnCreate");
          createDb = true;
//          createDB(db);
        }

        private void myExecSQL(SQLiteDatabase db, String query)
        {
          try{
            db.execSQL(query);
          } catch (Exception e){
            Log.d("UpdateDB","UpdateDB version: " + Integer.toString(DB_VERSION) + " " + e.getMessage());
          }
        }

//        private void updateHistoryTtn_6(SQLiteDatabase db)
//        {
//          myExecSQL(db, HistoryTtnTable.SQL_ADD_TO_PAY);
//          myExecSQL(db, HistoryTtnTable.SQL_ADD_COD);
//          myExecSQL(db, HistoryTtnTable.SQL_ADD_COST_RETURN);
//          myExecSQL(db, HistoryTtnTable.SQL_ADD_STATUS_VIEW);
//          myExecSQL(db, HistoryTtnTable.SQL_UPDATE_STATUS_VIEW);
//          myExecSQL(db, HistoryTtnTable.SQL_CREATE_VIEW_6);
//        }
//
//        private void updateHistoryTtn_7(SQLiteDatabase db)
//        {
//          myExecSQL(db, HistoryTtnTable.SQL_ADD_TO_PAY);
//          myExecSQL(db, HistoryTtnTable.SQL_ADD_COD);
//          myExecSQL(db, HistoryTtnTable.SQL_ADD_COST_RETURN);
//          myExecSQL(db, HistoryTtnTable.SQL_ADD_STATUS_VIEW);
//          myExecSQL(db, HistoryTtnTable.SQL_ADD_STORE_CODE);
//          myExecSQL(db, HistoryTtnTable.SQL_ADD_STORE_TYPE);
//          myExecSQL(db, HistoryTtnTable.SQL_ADD_DESCRIPTION);
//          myExecSQL(db, HistoryTtnTable.SQL_ADD_TYPE);
//          myExecSQL(db, HistoryTtnTable.SQL_ADD_SEATS);
//          myExecSQL(db, HistoryTtnTable.SQL_ADD_PAYER);
//          myExecSQL(db, HistoryTtnTable.SQL_ADD_WEIGHT);
//          myExecSQL(db, HistoryTtnTable.SQL_ADD_VOLUME);
//          myExecSQL(db, HistoryTtnTable.SQL_ADD_ADDRESS);
//          myExecSQL(db, HistoryTtnTable.SQL_ADD_CARGO_TYPE);
//          myExecSQL(db, HistoryTtnTable.SQL_UPDATE_STATUS_VIEW);
//          myExecSQL(db, HistoryTtnTable.SQL_CREATE_VIEW);
//        }

        private void cleanDBStructure(SQLiteDatabase db)
        {
          db.execSQL(CityTable.SQL_DROP_VIEW);
          db.execSQL(CityTable.SQL_DROP_TABLE);

          db.execSQL(CityTable.SQL_DROP_VIEW_UA);
          db.execSQL(CityTable.SQL_DROP_TABLE_UA);

          db.execSQL(CityTable.SQL_CREATE_TABLE);
          db.execSQL(CityTable.SQL_CREATE_VIEW);
          db.execSQL(CityTable.SQL_CREATE_INDEX_NAME);
          db.execSQL(CityTable.SQL_CREATE_INDEX_CODE);

          db.execSQL(CityTable.SQL_CREATE_TABLE_UA);
          db.execSQL(CityTable.SQL_CREATE_VIEW_UA);
          db.execSQL(CityTable.SQL_CREATE_INDEX_NAME_UA);
          db.execSQL(CityTable.SQL_CREATE_INDEX_CODE_UA);

          db.execSQL(WarehouseDetailsTable.SQL_DROP_VIEW);
          db.execSQL(WarehouseDetailsTable.SQL_DROP_TABLE);

          db.execSQL(WarehouseDetailsTable.SQL_DROP_VIEW_UA);
          db.execSQL(WarehouseDetailsTable.SQL_DROP_TABLE_UA);

          db.execSQL(WarehouseDetailsTable.SQL_CREATE_TABLE);
          db.execSQL(WarehouseDetailsTable.SQL_CREATE_VIEW);

          db.execSQL(WarehouseDetailsTable.SQL_CREATE_TABLE_UA);
          db.execSQL(WarehouseDetailsTable.SQL_CREATE_VIEW_UA);
          db.execSQL(WarehouseDetailsTable.SQL_CREATE_INDEX_ADDRESS_UA);
          db.execSQL(WarehouseDetailsTable.SQL_CREATE_INDEX_ADDRESS);
          db.execSQL(WarehouseDetailsTable.SQL_CREATE_INDEX_CODE);
          db.execSQL(WarehouseDetailsTable.SQL_CREATE_INDEX_CODE_UA);
          db.execSQL(WarehouseDetailsTable.SQL_CREATE_INDEX_LAT);
          db.execSQL(WarehouseDetailsTable.SQL_CREATE_INDEX_LNG);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
          Log.d("UpdateDB", "OnUpgrade");
          upgradeDb = true;
          if (newVersion == 2) {

            db.execSQL(CityTable.SQL_DROP_VIEW);
            db.execSQL(CityTable.SQL_DROP_TABLE);

            db.execSQL(CityTable.SQL_CREATE_TABLE);
            db.execSQL(CityTable.SQL_CREATE_VIEW);
            db.execSQL(CityTable.SQL_CREATE_INDEX_NAME);
            db.execSQL(CityTable.SQL_CREATE_INDEX_CODE);

            db.execSQL(CityTable.SQL_CREATE_TABLE_UA);
            db.execSQL(CityTable.SQL_CREATE_VIEW_UA);
            db.execSQL(CityTable.SQL_CREATE_INDEX_NAME_UA);
            db.execSQL(CityTable.SQL_CREATE_INDEX_CODE_UA);
          }

          if (newVersion == 4) {

            db.execSQL(CityTable.SQL_DROP_VIEW);
            db.execSQL(CityTable.SQL_DROP_TABLE);

            db.execSQL(CityTable.SQL_DROP_VIEW_UA);
            db.execSQL(CityTable.SQL_DROP_TABLE_UA);

            db.execSQL(CityTable.SQL_CREATE_TABLE);
            db.execSQL(CityTable.SQL_CREATE_VIEW);
            db.execSQL(CityTable.SQL_CREATE_INDEX_NAME);
            db.execSQL(CityTable.SQL_CREATE_INDEX_CODE);

            db.execSQL(CityTable.SQL_CREATE_TABLE_UA);
            db.execSQL(CityTable.SQL_CREATE_VIEW_UA);
            db.execSQL(CityTable.SQL_CREATE_INDEX_NAME_UA);
            db.execSQL(CityTable.SQL_CREATE_INDEX_CODE_UA);

//            db.execSQL(HistoryTtnTable.SQL_DROP_VIEW);
//            db.execSQL(HistoryTtnTable.SQL_DROP_TABLE);
//
//            db.execSQL(HistoryTtnTable.SQL_CREATE_TABLE);
//            db.execSQL(HistoryTtnTable.SQL_CREATE_VIEW);

            db.execSQL(WarehouseDetailsTable.SQL_DROP_VIEW);
            db.execSQL(WarehouseDetailsTable.SQL_DROP_TABLE);

            db.execSQL(WarehouseDetailsTable.SQL_DROP_VIEW_UA);
            db.execSQL(WarehouseDetailsTable.SQL_DROP_TABLE_UA);

            db.execSQL(WarehouseDetailsTable.SQL_CREATE_TABLE);
            db.execSQL(WarehouseDetailsTable.SQL_CREATE_VIEW);

            db.execSQL(WarehouseDetailsTable.SQL_CREATE_TABLE_UA);
            db.execSQL(WarehouseDetailsTable.SQL_CREATE_VIEW_UA);
            db.execSQL(WarehouseDetailsTable.SQL_CREATE_INDEX_ADDRESS_UA);
            db.execSQL(WarehouseDetailsTable.SQL_CREATE_INDEX_ADDRESS);
            db.execSQL(WarehouseDetailsTable.SQL_CREATE_INDEX_CODE);
            db.execSQL(WarehouseDetailsTable.SQL_CREATE_INDEX_CODE_UA);
            db.execSQL(WarehouseDetailsTable.SQL_CREATE_INDEX_LAT);
            db.execSQL(WarehouseDetailsTable.SQL_CREATE_INDEX_LNG);
          }

//          if (newVersion == 5) {
//            cleanDBStructure(db);
//            db.execSQL(HistoryTtnTable.SQL_DROP_VIEW);
//            updateHistoryTtn_6(db);
//          }

//          if (newVersion == 6) {
//            cleanDBStructure(db);
//            db.execSQL(HistoryTtnTable.SQL_DROP_VIEW);
//            updateHistoryTtn_6(db);
//          }
//
//          if (newVersion == 7) {
//            cleanDBStructure(db);
//            db.execSQL(HistoryTtnTable.SQL_DROP_VIEW);
//            updateHistoryTtn_7(db);
//          }

        }

    }

}
