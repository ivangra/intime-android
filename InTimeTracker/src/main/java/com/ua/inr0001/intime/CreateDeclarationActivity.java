package com.ua.inr0001.intime;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import intime.intime.mylistview.MyListView;
import intime.llc.ua.R;
import intime.newdeclaration.DeclarationModel;


/**
 * A simple {@link Fragment} subclass.
 */
public class CreateDeclarationActivity extends AppCompatActivity implements ICalculate {

  private Context mContext;
  private DeclarationModel calculatorModel;
  // hint Frame
  private TextView tvWarningDesc;
  private TextView btnOnHintOk;
  private FrameLayout frLayoutHint;

  private Button btnAddPlace;

  private TextView tvAddressCity1;
  private TextView editCity1;
  private TextView editStreet1;
  private TextView tvStreet;
  private EditText editHouse1;
  private EditText editBlock1;
  private EditText editRoom1;

  private EditText editPhone;
  private EditText editFullName;
  private TextView tvClientPhoneHint;
  private TextView tvClientFullNameHint;

  private EditText editPhone1;
  private EditText editFullName1;
  private TextView tvClientPhoneHint1;
  private TextView tvClientFullNameHint1;

  private EditText edCosts;

  private ImageView btnWarningCost;

  private LinearLayout layoutCost;

  private Button btnCreateDeclaration;

  private MyListView lvSendTo;
  private MyListView lvPaymentType;
  private MyListView lvPayer;

  // additiona services
  private SwitchCompat swDeclarationExchange;
  private SwitchCompat swExcahangeByCargo;
  private SwitchCompat swCargoExchange;
  private SwitchCompat swPackageRevert;
  private SwitchCompat swDeliveryToMarket;
  private SwitchCompat swLiftToStage;
  private SwitchCompat swLiftFromStage;
  private SwitchCompat swWaitCar;
  private SwitchCompat swSingleDelivery;
  private SwitchCompat swDontVerify;
  private SwitchCompat swSaveAsTemplate;

  private SimpleAdapter sendToAdapter;
  private SimpleAdapter paymentTypeAdapter;
  private SimpleAdapter payerAdapter;

  private int sendToSelected = 0;
  private int paymentTypeSelected = 0;
  private int payerSelected = 0;
  private int placesCoumt = 1;
  private int samplePackages = 1;

  public CreateDeclarationActivity() {
    // Required empty public constructor
  }

  private boolean validateForm() {

    boolean result = true;

    // стоимость
    try
    {
      int cost = Integer.parseInt(edCosts.getText().toString());
      if (cost<200) {
        btnWarningCost.setVisibility(View.VISIBLE);
        result = false;
      }
    }
    catch(Exception e)
    {
      btnWarningCost.setVisibility(View.VISIBLE);
      result = false;
    }

    // Объем
    /*if (edVolumeLength.getText().toString().length()==0)
    {
      edVolumeLength.setHintTextColor(getResources().getColor(R.color.error_massage));
      result = false;
    }

    if (edVolumeWidth.getText().toString().length()==0)
    {
      edVolumeWidth.setHintTextColor(getResources().getColor(R.color.error_massage));
      result = false;
    }

    if (edVolumeHeight.getText().toString().length()==0)
    {
      edVolumeHeight.setHintTextColor(getResources().getColor(R.color.error_massage));
      result = false;
    }*/

    if (editPhone.getText().toString().length()==0)
    {
      tvClientPhoneHint.setTextColor(getResources().getColor(R.color.error_massage));
      result = false;
    }

    if (editFullName.getText().toString().length()==0)
    {
      tvClientFullNameHint.setTextColor(getResources().getColor(R.color.error_massage));
      result = false;
    }

    if (editPhone1.getText().toString().length()==0)
    {
      tvClientPhoneHint1.setTextColor(getResources().getColor(R.color.error_massage));
      result = false;
    }

    if (editFullName1.getText().toString().length()==0)
    {
      tvClientFullNameHint1.setTextColor(getResources().getColor(R.color.error_massage));
      result = false;
    }

    if (editCity1.getText().toString().length()==0)
    {
      editCity1.setHintTextColor(getResources().getColor(R.color.error_massage));
      result = false;
    }

    if (editHouse1.getText().toString().length()==0)
    {
      editHouse1.setHintTextColor(getResources().getColor(R.color.error_massage));
      result = false;
    }

    if (editStreet1.getText().toString().length()==0)
    {
      editStreet1.setHintTextColor(getResources().getColor(R.color.error_massage));
      result = false;
    }

    if (editRoom1.getText().toString().length()==0)
    {
      editRoom1.setHintTextColor(getResources().getColor(R.color.error_massage));
      result = false;
    }

    /*if (edVolumeHeight.getText().toString().length()>0 &&
      edVolumeLength.getText().toString().length()>0 &&
      edVolumeLength.getText().toString().length()>0)
    {
      String volume = CalcVolume.CalcVolume(edVolumeWidth.getText().toString(),
        edVolumeHeight.getText().toString(),
        edVolumeLength.getText().toString());
      if (Double.parseDouble(volume) < 0.001d) {
        btnVolumeWarning.setVisibility(View.VISIBLE);
        result = false;
      }
    }*/

    return result;
  }

  private void initPhoneAndName()
  {
    editPhone = (EditText) findViewById(R.id.editClientPhone1);
    editFullName = (EditText) findViewById(R.id.editClientFullName1);
    tvClientFullNameHint = (TextView) findViewById(R.id.tvClientFullNameHint1);
    tvClientPhoneHint = (TextView) findViewById(R.id.tvClientPhoneHint1);

    editPhone.addTextChangedListener(new TextChangeListener(mContext, null, tvClientPhoneHint, null));
    editFullName.addTextChangedListener(new TextChangeListener(mContext, null, tvClientFullNameHint, null));
  }

  private void initPhoneAndName1()
  {
    editPhone1 = (EditText) findViewById(R.id.editClientPhone1);
    editFullName1 = (EditText) findViewById(R.id.editClientFullName1);
    tvClientFullNameHint1 = (TextView) findViewById(R.id.tvClientFullNameHint1);
    tvClientPhoneHint1 = (TextView) findViewById(R.id.tvClientPhoneHint1);

    editPhone1.addTextChangedListener(new TextChangeListener(mContext, null, tvClientPhoneHint1, null));
    editFullName1.addTextChangedListener(new TextChangeListener(mContext, null, tvClientFullNameHint1, null));
  }

  private void setSendToAdapter()
  {

    ArrayList<Map<String, Object>> sendToList = new ArrayList<Map<String, Object>>();
    String[] sendToText = getResources().getStringArray(R.array.send_to);

    Map<String, Object> m;
    for (int i = 0; i < sendToText.length; i++) {
      m = new HashMap<String, Object>();
      m.put("Text", sendToText[i]);
      sendToList.add(m);
    }

    String[] from = { "Text" };
    int[] to = { R.id.tvcCargoType};

    sendToAdapter = new SimpleAdapter(mContext, sendToList,
      R.layout.single_choise_item_def, from, to)
    {
      @NonNull
      @Override
      public View getView(int position, View convertView, ViewGroup parent) {
        final View view = super.getView(position, convertView, parent);
        ImageView cargoIcon = (ImageView) view.findViewById(R.id.imageCargoIcon);
        LinearLayout llSelectedItem = (LinearLayout) view.findViewById(R.id.llSingleChoiseItem);
        if (position == sendToSelected) {
          llSelectedItem.setBackgroundColor(getResources().getColor(R.color.background_white));
          cargoIcon.setImageResource(R.drawable.ic_list_check);
        }
        else {
          llSelectedItem.setBackgroundColor(getResources().getColor(R.color.app_background));
          cargoIcon.setImageResource(0);
        }
        return view;
      }
    };

    lvSendTo.setAdapter(sendToAdapter);
  }

  private void setPaymentTypeAdapter()
  {
    ArrayList<Map<String, Object>> sendToList = new ArrayList<Map<String, Object>>();
    String[] sendToText = getResources().getStringArray(R.array.payment_type);

    Map<String, Object> m;
    for (int i = 0; i < sendToText.length; i++) {
      m = new HashMap<String, Object>();
      m.put("Text", sendToText[i]);
      sendToList.add(m);
    }

    String[] from = { "Text" };
    int[] to = { R.id.tvcCargoType};

    paymentTypeAdapter = new SimpleAdapter(mContext, sendToList,
      R.layout.single_choise_item_def, from, to)
    {
      @NonNull
      @Override
      public View getView(int position, View convertView, ViewGroup parent) {
        final View view = super.getView(position, convertView, parent);
        ImageView cargoIcon = (ImageView) view.findViewById(R.id.imageCargoIcon);
        LinearLayout llSelectedItem = (LinearLayout) view.findViewById(R.id.llSingleChoiseItem);
        if (position == paymentTypeSelected) {
          llSelectedItem.setBackgroundColor(getResources().getColor(R.color.background_white));
          cargoIcon.setImageResource(R.drawable.ic_list_check);
        }
        else {
          llSelectedItem.setBackgroundColor(getResources().getColor(R.color.app_background));
          cargoIcon.setImageResource(0);
        }
        return view;
      }
    };

    lvPaymentType.setAdapter(paymentTypeAdapter);
  }

  private void setPayerAdapter()
  {
    ArrayList<Map<String, Object>> sendToList = new ArrayList<Map<String, Object>>();
    String[] sendToText = getResources().getStringArray(R.array.payer);

    Map<String, Object> m;
    for (int i = 0; i < 2; i++) {
      m = new HashMap<String, Object>();
      m.put("Text", sendToText[i]);
      sendToList.add(m);
    }

    String[] from = { "Text" };
    int[] to = { R.id.tvcCargoType};

    payerAdapter = new SimpleAdapter(mContext, sendToList,
      R.layout.single_choise_item_def, from, to)
    {
      @NonNull
      @Override
      public View getView(int position, View convertView, ViewGroup parent) {
        final View view = super.getView(position, convertView, parent);
        ImageView cargoIcon = (ImageView) view.findViewById(R.id.imageCargoIcon);
        LinearLayout llSelectedItem = (LinearLayout) view.findViewById(R.id.llSingleChoiseItem);
        if (position == payerSelected) {
          llSelectedItem.setBackgroundColor(getResources().getColor(R.color.background_white));
          cargoIcon.setImageResource(R.drawable.ic_list_check);
        }
        else {
          llSelectedItem.setBackgroundColor(getResources().getColor(R.color.app_background));
          cargoIcon.setImageResource(0);
        }
        return view;
      }
    };

    lvPayer.setAdapter(payerAdapter);
  }

  private void initListSendTo()
  {
    lvSendTo = (MyListView) findViewById(R.id.lvSendTo);
    lvSendTo.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
    lvSendTo.setItemsCanFocus(true);
    setSendToAdapter();
    lvSendTo.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        sendToSelected = position;
        sendToAdapter.notifyDataSetChanged();
      }
    });
  }

  private void initPaymentType()
  {
    lvPaymentType = (MyListView) findViewById(R.id.lvcPaymentType);
    lvPaymentType.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
    lvPaymentType.setItemsCanFocus(true);
    setPaymentTypeAdapter();
    lvPaymentType.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        paymentTypeSelected = position;
        paymentTypeAdapter.notifyDataSetChanged();
      }
    });
  }

  private void initCost()
  {
    layoutCost = (LinearLayout) findViewById(R.id.llCost);
    btnWarningCost = (ImageView) findViewById(R.id.imgWarningCost);
    btnWarningCost.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        showHint(getResources().getString(R.string.dialog_message_min_costs), layoutCost.getBottom());
      }
    });

    edCosts = (EditText) findViewById(R.id.editCargoCost);
    edCosts.addTextChangedListener(new TextChangeListener(mContext, null, null, btnWarningCost));
  }

  private void initPayer()
  {
    lvPayer = (MyListView) findViewById(R.id.lvcPayer);
    lvPayer.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
    lvPayer.setItemsCanFocus(true);
    setPayerAdapter();
    lvPayer.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        payerSelected = position;
        payerAdapter.notifyDataSetChanged();
      }
    });
  }

  private void showHint(String message, float buttom)
  {
    tvWarningDesc.setText(message);
    frLayoutHint.setTranslationY(buttom);
    frLayoutHint.setVisibility(View.VISIBLE);
  }

  private void initHintFrame()
  {
    frLayoutHint = (FrameLayout) findViewById(R.id.frWarningHint);
    frLayoutHint.setVisibility(View.GONE);
    tvWarningDesc = (TextView) findViewById(R.id.tvWarningDesc);

    btnOnHintOk = (TextView) findViewById(R.id.btnOnHint);
    btnOnHintOk.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        frLayoutHint.setVisibility(View.GONE);
      }
    });
  }

  private void initClientAddress1()
  {
    tvAddressCity1 = (TextView) findViewById(R.id.tvAddressCity1);
    tvStreet = (TextView) findViewById(R.id.tvAddressStreet1);
    editCity1 = (TextView) findViewById(R.id.editClientAddressCity1);
    editStreet1 = (TextView) findViewById(R.id.editClientAddressStreet1);
    editHouse1 = (EditText) findViewById(R.id.editClientAddressHouse1);
    editBlock1 = (EditText) findViewById(R.id.editClientAddressBlock1);
    editRoom1 = (EditText) findViewById(R.id.editClientAddressRoom1);

    editCity1.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent intent = new Intent(mContext, SelectCityActivity.class);
        intent.putExtra("City", "Receiver");
        startActivityForResult(intent, 2000);
      }
    });
    editStreet1.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (editCity1.getText().toString().length()>0) {
          Intent intent = new Intent(mContext, SelectStreetActivity.class);
          intent.putExtra("CityName", editCity1.getText().toString());
          startActivityForResult(intent, 2001);
        }
        else
          editCity1.setHintTextColor(getResources().getColor(R.color.error_massage));
      }
    });
    //editCity1.addTextChangedListener(new TextChangeListener(mContext, editCity1, tvAddressCity1, null));
    //editStreet1.addTextChangedListener(new TextChangeListener(mContext, editStreet1, null, null));
    editHouse1.addTextChangedListener(new TextChangeListener(mContext, editHouse1, null, null));
    editBlock1.addTextChangedListener(new TextChangeListener(mContext, editBlock1, null, null));
    editRoom1.addTextChangedListener(new TextChangeListener(mContext, editRoom1, null, null));
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);

    if (data == null) return;

    if (resultCode == RESULT_OK) {

      try {
        if (requestCode==2000) {
          String city = data.getStringExtra("City");
          if (city.compareTo("Sender") == 0) {
            calculatorModel.setSenderCityName(data.getStringExtra("Name"));
            calculatorModel.setSenderCityCode(data.getStringExtra("Code"));

//          sendFromSelected = 0;
//          setSendFromAdapter(data.getIntExtra("HasStore", 0)==1);
//          setTypeFrom(sendFromSelected);
//          lvSendFrom.setSelection(0);
//          sendFromAdapter.notifyDataSetChanged();
//          editCity1.setText(calculatorModel.getSenderCityName()); // ----
          } else {
            calculatorModel.setReceiverCityName(data.getStringExtra("Name"));
            calculatorModel.setReceiverCityCode(data.getStringExtra("Code"));
//          sendToSelected = 0;
//          setSendToAdapter(data.getIntExtra("HasStore", 0)==1, data.getIntExtra("HasPostomat", 0)==1);
//          setTypeTo(sendToSelected);
//          lvSendTo.setSelection(0);
//          sendToAdapter.notifyDataSetChanged();
            editCity1.setText(calculatorModel.getReceiverCityName()); // ---
            if (editCity1.getText().toString().length() > 0)
              tvAddressCity1.setVisibility(View.VISIBLE);
            else
              tvAddressCity1.setVisibility(View.GONE);
          }
        }

        if (requestCode==2001) {
          String street = data.getStringExtra("StreetName");
          editStreet1.setText(street);
          editStreet1.setHintTextColor(getResources().getColor(R.color.paper_caption));
          if (editStreet1.getText().toString().length()>0)
            tvStreet.setVisibility(View.VISIBLE);
          else
            tvStreet.setVisibility(View.GONE);
        }

      }
      catch(Exception e)
      {
        //Log.d(TAG, "Error select city: " + e.getMessage());
      }

    }
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    mContext = this;
    calculatorModel = DeclarationModel.getInstance(this);
    setContentView(R.layout.fragment_create_declaration);
    Toolbar toolbar = (Toolbar) findViewById(R.id.createDeclarationToolbar);
    setSupportActionBar(toolbar);
    toolbar.setNavigationOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        onBackPressed();
      }
    });

    btnAddPlace = (Button) findViewById(R.id.btnAddPlace);
    btnAddPlace.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        PlaceDeclarationFragment fragment1 = new PlaceDeclarationFragment();
        fragment1.setFragmentNumber(Integer.toString(++placesCoumt));
        ft.add(R.id.llPlaces, fragment1, "fragment_" + Integer.toString(placesCoumt));
        ft.commit();
      }
    });

    //initHintFrame();

    btnCreateDeclaration = (Button) findViewById(R.id.btnCreateDeclaration);
    btnCreateDeclaration.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (validateForm())
        {
          Log.d("Test", "Create declatation validated");
        }
      }
    });

    initCost();
    initListSendTo();
    initPaymentType();
    initPayer();
    initPhoneAndName();
    initPhoneAndName1();
    initClientAddress1();
    initAdditionaServices();

    android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
    PlaceDeclarationFragment fragment1 = new PlaceDeclarationFragment();
    fragment1.setFragmentNumber(Integer.toString(placesCoumt));
    ft.add(R.id.llPlaces, fragment1, "fragment_1");
    ft.commit();

  }

  private void initAdditionaServices() {
    swDeclarationExchange = (SwitchCompat) findViewById(R.id.swDeclarationExchange);
    swExcahangeByCargo = (SwitchCompat) findViewById(R.id.swExcahangeByCargo);
    swCargoExchange = (SwitchCompat) findViewById(R.id.swCargoExchange);
    swPackageRevert = (SwitchCompat) findViewById(R.id.swPackageRevert);
    swDeliveryToMarket = (SwitchCompat) findViewById(R.id.swDeliveryToMarket);
    swLiftToStage = (SwitchCompat) findViewById(R.id.swLiftToStage);
    swLiftFromStage = (SwitchCompat) findViewById(R.id.swLiftFromStage);
    swWaitCar = (SwitchCompat) findViewById(R.id.swWaitCar);
    swSingleDelivery = (SwitchCompat) findViewById(R.id.swSingleDelivery);
    swDontVerify = (SwitchCompat) findViewById(R.id.swDontVerify);
    swSaveAsTemplate = (SwitchCompat) findViewById(R.id.swSaveAsTemplate);

    swDeclarationExchange.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
          if (isChecked)
          {

          }
      }
    });

    swExcahangeByCargo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked)
        {

        }
      }
    });

    swCargoExchange.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked)
        {

        }
      }
    });

    swPackageRevert.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked)
        {

        }
      }
    });

    swDeliveryToMarket.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked)
        {

        }
      }
    });

    swLiftToStage.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked)
        {

        }
      }
    });

    swLiftFromStage.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked)
        {

        }
      }
    });

    swWaitCar.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked)
        {

        }
      }
    });

    swSingleDelivery.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked)
        {

        }
      }
    });

    swDontVerify.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked)
        {

        }
      }
    });

    swSaveAsTemplate.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked)
        {

        }
      }
    });

  }

  @Override
  public void onWindowFocusChanged (boolean hasFocus) {
    // the height will be set at this point
    //setWeightFramePos(firstWeightItem);
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
//    MenuInflater inflater = getMenuInflater();
//    inflater.inflate(R.menu.create_declaration_menu, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    //int id = item.getItemId();
    return super.onOptionsItemSelected(item);
  }

  @Override
  public void doCalcRequest() {

  }
}
