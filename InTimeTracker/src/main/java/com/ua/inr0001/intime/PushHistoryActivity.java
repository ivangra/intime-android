package com.ua.inr0001.intime;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import intime.llc.ua.R;

public class PushHistoryActivity extends AppCompatActivity {
  
  /*private Button btnDelete;
  private Button btnSelectAll;*/
  private Context mContext;
  private ListView listviewPush;
  private GlobalApplicationData appData;
  private InTimeDB dictionariesDB;
  private Cursor selectAllCursor;
  private String action = "";
  private String[] from;
  private int[] to;
  private PushHistoryCursorAdapter scAdapter;
  
  private String TTN = "";
  private String links = "";
  
  private void browseLink(String strExtras)
  {
    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(strExtras));
    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    intent.addFlags(Intent.FLAG_ACTIVITY_PREVIOUS_IS_TOP);
    startActivity(intent);
    //finish();
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_push_history_list);
    mContext = this;

    Toolbar toolbar = (Toolbar) findViewById(R.id.pushToolbar);
    setSupportActionBar(toolbar);

    listviewPush = (ListView) findViewById(R.id.lvPushHistory);
    listviewPush.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
    listviewPush.setItemsCanFocus(true);
    listviewPush.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        scAdapter.setSelectedPosition(position);
        scAdapter.notifyDataSetChanged();
      }
    });
    dictionariesDB = InTimeDB.getInstance(mContext);
    selectAllCursor = dictionariesDB.tablePushHistory.selectAll();

    startManagingCursor(selectAllCursor);

    from = new String[]{PushHistoryTable.Column.NAME, PushHistoryTable.Column.DESCRIPTION, PushHistoryTable.Column.DATE};
    to = new int[]{R.id.tvPushTitle, R.id.tvPushDetails, R.id.tvDateUpdate};

    scAdapter = new PushHistoryCursorAdapter(mContext, R.layout.push_history_item,
      selectAllCursor, from , to);
    //scAdapter.setButtons(btnSelectAll, btnDelete);
    listviewPush.setAdapter(scAdapter);
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.accept_action_menu, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();
    switch (id)
    {
      case R.id.menu_action_accept: onBackPressed(); return true;
      default: return super.onOptionsItemSelected(item);
    }
  }

}
