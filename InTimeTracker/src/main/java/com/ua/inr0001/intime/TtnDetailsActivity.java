package com.ua.inr0001.intime;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import intime.llc.ua.databinding.ActivityHistoryTtnDetailsBinding;

import intime.api.model.trackttn.Data;
import intime.api.model.trackttn.DateCreation;
import intime.api.model.trackttn.DateDelivery;
import intime.api.model.trackttn.From;
import intime.api.model.trackttn.Store;
import intime.api.model.trackttn.To;
import intime.api.model.trackttn.TrackTtn;
import intime.llc.ua.R;
import intime.utils.MyDateUtils;
import intime.utils.MyStringUtils;

public class TtnDetailsActivity extends AppCompatActivity {

  private Context context;

  private TrackTtn trackTtn;

  private ActivityHistoryTtnDetailsBinding binding;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_history_ttn_details);

    context = this;

    Intent intent = getIntent();
    binding = DataBindingUtil.setContentView(this, R.layout.activity_history_ttn_details);
    binding.ttnDetailsToolbar.setTitle("");
    binding.tvTtnDetailsTitle.setText(MyStringUtils.getFormattedTTN(intent.getStringExtra("ttn")));
    binding.ttnDetailsToolbar.setNavigationOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        onBackPressed();
      }
    });
    trackTtn = new TrackTtn();
    Data data = new Data();
    data.setNumber(MyStringUtils.getFormattedTTN(intent.getStringExtra("ttn")));
    From from = new From();
    from.setCity(intent.getStringExtra("fromCity"));
    data.setFrom(from);
    To to = new To();
    to.setCity(intent.getStringExtra("toCity"));
    to.setStoreCode(intent.getStringExtra("toStoreCode"));
    data.setTo(to);
    data.setPayer(intent.getStringExtra("payer"));
    try {
      Double toPay = Double.parseDouble(intent.getStringExtra("toPay"));
      Double cacheOnDelivery = Double.parseDouble(intent.getStringExtra("cache_on_delivery"));
      Double totalSum = toPay + cacheOnDelivery;
      if (totalSum < 0d)
        totalSum = 0d;
      data.setToPay(MyStringUtils.roundTotalSum(totalSum));
    }
    catch(Exception e)
    {
      data.setToPay(intent.getStringExtra("toPay"));
    }
    data.setWeight(intent.getStringExtra("weight"));
    data.setVolume(intent.getStringExtra("volume"));
    data.setCost(intent.getStringExtra("cost"));
    data.setStatus(intent.getStringExtra("status"));
    data.setStatusId(intent.getStringExtra("statusId"));
    data.setCargoType(intent.getStringExtra("cargo_type"));
    data.setSeats(intent.getStringExtra("seats"));
    DateCreation dateCreation = new DateCreation();
    dateCreation.setDate(intent.getStringExtra("dateCreation"));
    data.setDateCreation(dateCreation);
    DateDelivery dateDelivery = new DateDelivery();
    dateDelivery.setDate(intent.getStringExtra("dateDelivery"));
    data.setDateDelivery(dateDelivery);
    trackTtn.setData(data);
    Store store = new Store();
    store.setAddress(intent.getStringExtra("address"));
    store.setType(intent.getStringExtra("store_type"));
    data.getTo().setStore(store);
    binding.setTrackTtn(trackTtn);
    if (trackTtn.getData().getTo().getStore().getType() != null &&
      trackTtn.getData().getTo().getStore().getType().length() != 0) {
      try{
        Cursor cursor;
        if (GlobalApplicationData.getCurrentLanguage().compareTo("ru") == 0)
          cursor = DictionariesDB.getInstance(context).tableWarehouseDetails.selectWarehouseByCode(trackTtn.getData().getTo().getStoreCode());
        else
          cursor = DictionariesDB.getInstance(context).tableWarehouseDetails.selectWarehouseByCodeUA(trackTtn.getData().getTo().getStoreCode());
        cursor.moveToFirst();
        DepartmentsRvAdapter.setworkStatusText(this, binding.tvhdDepWork, MyDateUtils.getWorhHoursStart(cursor),
          MyDateUtils.getWorhHoursEnd(cursor),
          MyDateUtils.getNextWorhHours(cursor));
        trackTtn.getData().getTo().getStore().setType(DictionariesDB.getColumnValue(cursor, WarehouseDetailsTable.Column.TYPE));
        if (DictionariesDB.getColumnValue(cursor, WarehouseDetailsTable.Column.TYPE).compareTo("store")==0)
          binding.tvhDepNumber.setText(getResources().getString(R.string.text_department) + " №" + DictionariesDB.getColumnValue(cursor, WarehouseDetailsTable.Column.NUMBER));
        else
          binding.tvhDepNumber.setText(getResources().getString(R.string.text_postomat) + " №" + DictionariesDB.getColumnValue(cursor, WarehouseDetailsTable.Column.NUMBER));
        binding.tvhdAddressValue.setText(DictionariesDB.getColumnValue(cursor, WarehouseDetailsTable.Column.CITY) + " - " + DictionariesDB.getColumnValue(cursor, WarehouseDetailsTable.Column.ADDRESS));
        if (cursor != null)
          cursor.close();
      }
      catch (Exception e)
      {
        Log.d("TrackTTN", "Error bind " + e.getMessage());
      }
    }

    if (trackTtn.getData().getStatusId().compareTo("3")==0) // "Выдан"
      SetTTNStatusView.setCargoGivenStatus(binding.imgStage1, binding.imgStage2, binding.imgStage3, binding.imgLine1, binding.imgLine2, binding.tvCargoGiven);
    else
    if (trackTtn.getData().getStatusId().compareTo("2")==0) // "В пути"
      SetTTNStatusView.setOnWayStatus(binding.imgStage1, binding.imgStage2, binding.imgStage3, binding.imgLine1, binding.imgLine2);
    else
    if (trackTtn.getData().getStatusId().compareTo("16")==0) // "На складе"
      SetTTNStatusView.setOnWarehouseStatus(binding.imgStage1, binding.imgStage2, binding.imgStage3, binding.imgLine1, binding.imgLine2);
    else
    if (trackTtn.getData().getStatusId().compareTo("1")==0) // "Принят"
      SetTTNStatusView.setBeginStatus(binding.imgStage1, binding.imgStage2, binding.imgStage3, binding.imgLine1, binding.imgLine2);
    else
    if (trackTtn.getData().getStatusId().compareTo("0")==0) // "Создан документ Веб-заявка"
      SetTTNStatusView.setWebDeclarationStatus(binding.flWebDecLine, binding.llWebDecLine);
    else
      SetTTNStatusView.setWebDeclarationStatus(binding.flWebDecLine, binding.llWebDecLine);

    binding.btnShowOnMap.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (trackTtn.getData().getTo().getStore().getType().compareTo("store")==0)
          showDetails(trackTtn.getData().getTo().getStoreCode(), trackTtn.getData().getTo().getStore().getType());
      }
    });

    if (!GlobalApplicationData.getInstance().IsOnline())
      binding.btnFullStatusHistory.setVisibility(View.GONE);

    binding.btnFullStatusHistory.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        showFullTtnStatusList(trackTtn.getData().getNumber());
      }
    });

    binding.tvhdTTN.setOnLongClickListener(new View.OnLongClickListener() {
      @Override
      public boolean onLongClick(View view) {
        ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        String ttn = binding.tvhdTTN.getText().toString().replaceAll(" ", "");
        ClipData clip = ClipData.newPlainText("TTN", ttn);
        clipboard.setPrimaryClip(clip);
        Toast.makeText(context,
          String.format(context.getResources().getString(R.string.number_copied_to_clipboard), ttn),
          Toast.LENGTH_LONG).show();
        return false;
      }
    });
  }

  private void showFullTtnStatusList(final String ttn)
  {
    Intent intent = new Intent(this, TtnStatusHistoryAcitvity.class);
    intent.putExtra("ttn", ttn);
    startActivity(intent);
  }

  private void showDetails(final String code, final String depType) {
    Intent intent = new Intent(this, DepartmentsActivity.class);
    intent.putExtra("code", code);
    intent.putExtra("depType", depType);
    startActivity(intent);
  }

//  @Override
//  public boolean onCreateOptionsMenu(Menu menu) {
//    MenuInflater inflater = getMenuInflater();
//    inflater.inflate(R.menu.delivery_details_menu, menu);
//    return true;
//  }
//
//  private void shareDelivery()
//  {
//    Intent sendIntent = new Intent();
//    sendIntent.setAction(Intent.ACTION_SEND);
//    sendIntent.putExtra(Intent.EXTRA_TEXT, "This is my text to send.");
//    sendIntent.setType("text/plain");
//    startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.action_share)));
//  }
//
//  @Override
//  public boolean onOptionsItemSelected(MenuItem item) {
//    int id = item.getItemId();
//    switch (id)
//    {
//      case R.id.menu_share: shareDelivery(); return true;
//      default: return super.onOptionsItemSelected(item);
//    }
//  }

  @Override
  public void onBackPressed(){
    setResult(RESULT_OK);
    super.onBackPressed();
  }

}
