package com.ua.inr0001.intime;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import intime.llc.ua.R;

public class DepartmentsActivity extends AppCompatActivity {
  @Override
  public void onBackPressed() {
    super.onBackPressed();
    finish();
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_show_on_map);
    Intent depIntent = getIntent();
    String depCode = depIntent.getStringExtra("code");
    String depType = depIntent.getStringExtra("depType");
    setTitle("");
    if (GlobalApplicationData.getInstance().IsOnline()) {
      DepartmentsFragment frDepartments = new DepartmentsFragment();
      frDepartments.setDepCodeDepType(depCode, depType, true);
      getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
      FragmentTransaction fTrans = getSupportFragmentManager().beginTransaction();
      fTrans.replace(R.id.frShowOnMap, frDepartments);
      fTrans.commit();
    } else{
      DepartmentDetailsFragment frDepDetails = new DepartmentDetailsFragment();
      frDepDetails.setCode(depCode);
      FragmentTransaction fTrans = getSupportFragmentManager().beginTransaction();
      fTrans.replace(R.id.frShowOnMap, frDepDetails);
      fTrans.commit();
    }
    //MainFragmentManager.pushFragment(getActivity(), frDepDetails, "DepDetails");
  }

}
