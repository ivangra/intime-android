package com.ua.inr0001.intime;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import intime.llc.ua.R;

public class CallCurrierConfirmActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_call_currier_confirm);

    TextView tvCallCurrier = (TextView) findViewById(R.id.tvCurrierTextConfirm);
    TextView tvNumberOrder = (TextView) findViewById(R.id.tvNumberOrder);

    tvNumberOrder.setText("Заявка №" + MailCallCurrier.getInstance().getNumberOrder());

      /*String.format(getResources().getString(R.string.call_currier_confirm),
        MailCallCurrier.getInstance().humanDate,
        MailCallCurrier.getInstance().currierFromCity + " "
          + MailCallCurrier.getInstance().currierFromStreet + " " +
            MailCallCurrier.getInstance().currierFromHouse
      );*/

    tvCallCurrier.setText(getResources().getString(R.string.call_currier_confirm1));

    Button btnOk = (Button) findViewById(R.id.btnCallCurrierOk);
    btnOk.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        finish();
      }
    });

  }
}
