package com.ua.inr0001.intime;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.TextView;

import intime.llc.ua.R;

public class DepartmentsFilterActivity extends AppCompatActivity {

  private SwitchCompat swTo30kg;
  private SwitchCompat swTo70kg;
  private SwitchCompat swTo1000kg;
  private SwitchCompat swPostomats;

  private TextView tvTo30kg;
  private TextView tvTo70kg;
  private TextView tvTo1000kg;
  private TextView tvPostomats;

  private DepFilterSettings depFilterSettings;

  @Override
  protected void onPause() {
    super.onPause();
    GlobalApplicationData.getInstance().saveDepartmentFilterSettings(depFilterSettings);
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_departments_filter);
    Toolbar toolbar = (Toolbar) findViewById(R.id.filterToolbar);
    setSupportActionBar(toolbar);
    toolbar.setTitle("");
    toolbar.setNavigationOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        onBackPressed();
      }
    });
    depFilterSettings = GlobalApplicationData.getInstance().loadDepartmentFilterSettings();
    swTo30kg = (SwitchCompat) findViewById(R.id.swDepFilterTo30);
    swTo70kg = (SwitchCompat) findViewById(R.id.swDepFilterTo70);
    swTo1000kg = (SwitchCompat) findViewById(R.id.swDepFilterOver30);
    swPostomats = (SwitchCompat) findViewById(R.id.swDepFilterPostomats);

    tvTo30kg = (TextView) findViewById(R.id.tvDepFilterTo30);
    tvTo70kg = (TextView) findViewById(R.id.tvDepFilterTo70);
    tvTo1000kg = (TextView) findViewById(R.id.tvDepFilterOver30);
    tvPostomats = (TextView) findViewById(R.id.tvDepFilterPostomats);

    swTo30kg.setChecked(depFilterSettings.to30kg);
    swTo70kg.setChecked(depFilterSettings.to70kg);
    swTo1000kg.setChecked(depFilterSettings.to1000kg);
    swPostomats.setChecked(depFilterSettings.postomats);
    swTo30kg.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (!isChecked)
          tvTo30kg.setTextColor(getResources().getColor(R.color.text_hint));
        else
          tvTo30kg.setTextColor(getResources().getColor(R.color.base_text_color));
        depFilterSettings.to30kg = isChecked;
        isAllUnchecked();
      }
    });
    swTo1000kg.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (!isChecked)
          tvTo1000kg.setTextColor(getResources().getColor(R.color.text_hint));
        else
          tvTo1000kg.setTextColor(getResources().getColor(R.color.base_text_color));
        depFilterSettings.to1000kg = isChecked;
        isAllUnchecked();
      }
    });
    swTo70kg.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (!isChecked)
          tvTo70kg.setTextColor(getResources().getColor(R.color.text_hint));
        else
          tvTo70kg.setTextColor(getResources().getColor(R.color.base_text_color));
        depFilterSettings.to70kg = isChecked;
        isAllUnchecked();
      }
    });
    swPostomats.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (!isChecked)
          tvPostomats.setTextColor(getResources().getColor(R.color.text_hint));
        else
          tvPostomats.setTextColor(getResources().getColor(R.color.base_text_color));
        depFilterSettings.postomats = isChecked;
        isAllUnchecked();
      }
    });
  }

  private void isAllUnchecked()
  {
    if (!swTo30kg.isChecked() &&
      !swTo70kg.isChecked() &&
      !swTo1000kg.isChecked() &&
      !swPostomats.isChecked()
      )
    {
      tvTo30kg.setTextColor(getResources().getColor(R.color.base_text_color));
      swTo30kg.setChecked(true);
      depFilterSettings.to30kg = true;
    }
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.accept_action_menu, menu);
    return true;
  }

  private void finishActivity()
  {
    Intent intent = new Intent();
    setResult(RESULT_OK, intent);
    finish();
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();
    switch (id)
    {
      case R.id.menu_action_accept: finishActivity(); return true;
      default: return super.onOptionsItemSelected(item);
    }
  }

}
