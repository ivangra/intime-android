package com.ua.inr0001.intime;

import android.database.sqlite.SQLiteDatabase;

public final class WarehouseTable /*implements ITable*/ {

	private SQLiteDatabase sqlDB;
	//private SQLiteStatement insertStatement;
	
	public static final String VIEW_NAME = "vwWarehouse";
	public static final String NAME = "tblWarehouse";
	/*public static final String INSERT_WAREHOUSE =
	    "insert into " + NAME + "(" + 
	        WarehouseTable.Column.CITY_ID + ", " +
	        WarehouseTable.Column.NAME + ", " +
	        WarehouseTable.Column.CODE + ", " +
	        WarehouseTable.Column.PHONE + ", " +
	        WarehouseTable.Column.ADDRESS + ", " +
	        WarehouseTable.Column.NUMBER + ", " +
	        WarehouseTable.Column.LOGISTIC_ZONE + ", " +
	        WarehouseTable.Column.NUMERICAL_CODE + ", " +
	        WarehouseTable.Column.REGION_CODE + ", " +
	        WarehouseTable.Column.WEIGHT_LIMIT + ", " +
	        WarehouseTable.Column.LENGTH_LIMIT +
	      ") VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )";
	
	public WarehouseTable(SQLiteDatabase sqldb)
	{
		sqlDB = sqldb;
		insertStatement = sqlDB.compileStatement(INSERT_WAREHOUSE);
	}
	
	public static final class Column
	{
		public static final String ID = "_id";
		public static final String CITY_ID = "CityId";
	    public static final String NAME = "Name";
	    public static final String CODE = "Code";
	    public static final String PHONE = "Phone";
	    public static final String ADDRESS = "Address";
	    public static final String NUMBER = "Number";
	    public static final String LOGISTIC_ZONE = "LogisticZone";
	    public static final String NUMERICAL_CODE = "NumericalCode";
	    public static final String REGION_CODE = "RegionCode";
	    public static final String WEIGHT_LIMIT = "WeightLimit";
	    public static final String LENGTH_LIMIT = "LengthLimit";
	}
	
	public static final String SQL_CREATE_TABLE = 
    		"create table if not exists " + NAME + " ( " +
    				Column.ID + " integer primary key autoincrement, " +
    				Column.CITY_ID + " integer, " +
    				Column.NUMBER + " integer, " +
    				Column.NAME + " text, " +
    				Column.CODE + " text, " +
    				Column.PHONE + " text, " +
    				Column.ADDRESS + " text, " +
    				Column.LOGISTIC_ZONE + " text, " +
    				Column.NUMERICAL_CODE + " text, " +
    				Column.REGION_CODE + " text, " +
    				Column.WEIGHT_LIMIT + " text, " +
    				Column.LENGTH_LIMIT + " text );";

    public static final String SQL_CREATE_VIEW = 
    		"CREATE VIEW if not exists " + VIEW_NAME + " AS " +
    		"SELECT " +
    				Column.ID + ", " +
    				Column.CITY_ID + ", " +
    				Column.NUMBER + ", " +
    				Column.NAME + ", " +
    				Column.CODE + ", " +
    				Column.PHONE + ", " +
    				Column.ADDRESS + ", " +
    				Column.LOGISTIC_ZONE + ", " +
    				Column.NUMERICAL_CODE + ", " +
    				Column.REGION_CODE + ", " +
    				Column.WEIGHT_LIMIT + ", " +
    				Column.LENGTH_LIMIT + 
    	    		" FROM " + NAME;
    	    
    public static final String SQL_CREATE_INDEX_NAME =
    		"CREATE INDEX if not exists idx_WarehouseName ON " + NAME +
    		" (" + Column.NAME +");";
    
    public static final String SQL_CREATE_INDEX_CITYID =
    		"CREATE INDEX if not exists idx_WarehouseCityID ON " + NAME +
    		" (" + Column.CITY_ID +");";*/
    
    public static final String SQL_DROP_TABLE = "DROP TABLE IF EXISTS " + NAME;
    
    public static final String SQL_DROP_VIEW = "DROP VIEW IF EXISTS " + VIEW_NAME;
	
	/*@Override
	public Cursor selectByID(int Id) {
	    return sqlDB.query(VIEW_NAME, null,
    			Column.CITY_ID + "=" + Integer.toString(Id) 
    			,null ,null, null, Column.NAME);
	}

	@Override
	public Cursor selectByName(String Name) {
		String searchString = Name.replaceFirst(String.valueOf(Name.charAt(0)), String.valueOf(Name.charAt(0)).toUpperCase());
    	return sqlDB.query(VIEW_NAME,
    			new String[]{Column.ID, Column.CODE, Column.NAME},
    			Column.NAME + " like '" + searchString + "%'",
				null ,null, null, Column.NAME);
	}

	@Override
	public Cursor selectAll() {
		 return sqlDB.query(VIEW_NAME,
    			new String[]{Column.ID, Column.CODE, Column.NAME},
				null, null ,null, null, Column.NAME);
	}

	public void clearTable()
  {
    sqlDB.delete(NAME, null, null);
  }
	
	public void insert(int cityId, String Number, String Name, String Code, String Phone, String Address,
			String LogisticZone, String NumericalCode, String RegionCode, String WeightLimit,
			String LengthLimit)
  {
		if (!exists(Column.NAME, Name))
   	{

		  insertStatement.clearBindings();
		  insertStatement.bindLong(1, cityId);
		  insertStatement.bindString(2, Number);
		  insertStatement.bindString(3, Name);
		  insertStatement.bindString(4, Code);
		  insertStatement.bindString(5, Phone);
		  insertStatement.bindString(6, Address);
		  insertStatement.bindString(7, LogisticZone);
		  insertStatement.bindString(8, NumericalCode);
		  insertStatement.bindString(9, RegionCode);
		  insertStatement.bindString(10, WeightLimit);
		  insertStatement.bindString(11, LengthLimit);
			insertStatement.executeInsert();
   	}
  }

	@Override
	public boolean exists(String fieldName, String fieldValue) {
		Cursor slCursor = sqlDB.query(VIEW_NAME, null, fieldName + "=?", new String[]{fieldValue}, null, null, null);
    	if (slCursor.getCount()>0)
    	{
    		slCursor.close();
    		return true;
    	}
    	else
    	{
    		slCursor.close();
    		return false;
    	}
	}
	
	public int getLastID()
    {
    	int mID;
    	Cursor qCursor;
    	qCursor = sqlDB.rawQuery("select seq from sqlite_sequence where name='tblWarehouse'", null);
    	qCursor.moveToFirst();
    	mID = qCursor.getInt(0);
    	qCursor.close();
    	return mID;
    }*/
	
}
