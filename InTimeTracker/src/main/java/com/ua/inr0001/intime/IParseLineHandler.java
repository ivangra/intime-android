package com.ua.inr0001.intime;

public interface IParseLineHandler {

  public void parseLine(String line);
  
}
