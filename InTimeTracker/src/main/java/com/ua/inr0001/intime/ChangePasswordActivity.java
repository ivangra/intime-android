package com.ua.inr0001.intime;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;

import intime.llc.ua.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class ChangePasswordActivity extends AppCompatActivity {

  private EditText editPassword;
  private CheckBox chShowPassword;

  private void initShowPassword(final EditText edPassword, final CheckBox chPassword)
  {
    chPassword.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(isChecked) {
          edPassword.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
          edPassword.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
          edPassword.setTextColor(getResources().getColor(R.color.base_text_color));
        } else {
          edPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
          edPassword.setTextSize(TypedValue.COMPLEX_UNIT_SP, 32);
          edPassword.setTextColor(getResources().getColor(R.color.paper_caption));
        }
      }
    });
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.fragment_change_password);

    Toolbar toolbar = (Toolbar) findViewById(R.id.changePasswordToolbar);
    setSupportActionBar(toolbar);
    toolbar.setNavigationOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        onBackPressed();
      }
    });

    editPassword = (EditText) findViewById(R.id.editPasswd);
    chShowPassword = (CheckBox) findViewById(R.id.chChangeShowPassword);
    initShowPassword(editPassword, chShowPassword);
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.accept_action_menu, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();
    switch (id)
    {
      case R.id.menu_action_accept: onBackPressed(); return true;
      default: return super.onOptionsItemSelected(item);
    }
  }

}
