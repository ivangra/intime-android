package com.ua.inr0001.intime;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.Random;

import intime.api.model.createuser.NewUser;
import intime.llc.ua.R;
import intime.maskedtextedit.MaskedInputFilter;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RegisterUserActivity extends AppCompatActivity {

    public final static String GET_SMS = "intime_get_sms";
    private final static String TAG = "Register";
    private final int SMS_GRANT_REQUEST = 22;
    private BroadcastReceiver brRegisterUserSMS;
    private MaskedInputFilter maskedInputFilter;
    private Context context;
    private EditText editPhone;
    private EditText editFirstName;
    private EditText editPatronomic;
    private EditText editLastName;
    private EditText editMail;
    private EditText editSMS;
    private EditText editPassword;

    private FrameLayout frUserExistHint;
    private TextView tvPhone;
    private TextView tvName;
    private TextView tvSurName;
    private TextView tvLastName;
    private TextView tvMail;
    private TextView tvSMS;
    private TextView tvPassword;

    private Button btnRegister;

    private int code = -1;
    private TextWatcher textWatcher;

    private CheckBox chShowPassword;
    private boolean stopped = false;
    private Call<ResponseBody> sendSMSCall;
    private Call<NewUser> regusterUserCall;
    private Call<ResponseBody> userExistsCall;

    private void initShowPassword(final EditText edPassword, final CheckBox chPassword) {
        chPassword.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    edPassword.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    //edPassword.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                    edPassword.setTextColor(getResources().getColor(R.color.base_text_color));
                } else {
                    edPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    //edPassword.setTextSize(TypedValue.COMPLEX_UNIT_SP, 32);
                    edPassword.setTextColor(getResources().getColor(R.color.paper_caption));
                }
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            if (brRegisterUserSMS != null)
                context.unregisterReceiver(brRegisterUserSMS);
        } catch (Exception e) {
            Log.d(TAG, "onStop: cannot unregister.", e);

        }
        if (sendSMSCall != null)
            sendSMSCall.cancel();
        if (regusterUserCall != null)
            regusterUserCall.cancel();
        if (userExistsCall != null)
            userExistsCall.cancel();
        stopped = true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_close:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.close_action_menu, menu);
        return true; //super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_user);

        context = this;

        Toolbar mActionBarToolbar = findViewById(R.id.registerUserToolbar);
        mActionBarToolbar.setTitle("");
        setSupportActionBar(mActionBarToolbar);

        tvPhone = findViewById(R.id.tvRegPhone);
        tvName = findViewById(R.id.tvRegName);
        tvLastName = findViewById(R.id.tvRegLastName);
        tvSurName = findViewById(R.id.tvRegSurName);
        tvMail = findViewById(R.id.tvRegMail);
        tvSMS = findViewById(R.id.tvRegSMS);
        tvPassword = findViewById(R.id.tvRegPassword);

        editPhone = findViewById(R.id.editRegPhone);
        editPhone.addTextChangedListener(new TextChangeListener(context, editPhone, tvPhone, null));
        editFirstName = findViewById(R.id.editRegFirstName);
        editFirstName.addTextChangedListener(new TextChangeListener(context, editFirstName, tvName, null));
        editLastName = findViewById(R.id.editRegLastName);
        editLastName.addTextChangedListener(new TextChangeListener(context, editLastName, tvLastName, null));
        editPatronomic = findViewById(R.id.editRegPatronomic);
        editPatronomic.addTextChangedListener(new TextChangeListener(context, editPatronomic, tvSurName, null));
        editMail = findViewById(R.id.editRegMail);
        editMail.addTextChangedListener(new TextChangeListener(context, editMail, tvMail, null));
        editSMS = findViewById(R.id.editRegSMS);
        editSMS.addTextChangedListener(new TextChangeListener(context, editSMS, tvSMS, null));
        editPassword = findViewById(R.id.editRegPasswd);
        //editPassword.addTextChangedListener(new TextChangeListener(context, editPassword, tvPassword, null));
        chShowPassword = findViewById(R.id.chRegisterShowPassword);
        initShowPassword(editPassword, chShowPassword);

        frUserExistHint = findViewById(R.id.frUserExistHint);
        textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                frUserExistHint.setVisibility(View.INVISIBLE);
            }

            @Override
            public void afterTextChanged(Editable s) {
                String phone = editPhone.getText().toString().trim();
                if (phone.length() >= 17) {
                    if (code == -1) {
                        isUserExists(phone);
                    }
                } else
                    code = -1;
            }
        };
        editPhone.addTextChangedListener(textWatcher);
        btnRegister = findViewById(R.id.btnRegister);
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validateForm())
                    registerUser(
                            editPhone.getText().toString(),
                            editPassword.getText().toString(),
                            editLastName.getText().toString(),
                            editFirstName.getText().toString(),
                            editPatronomic.getText().toString(),
                            editMail.getText().toString()
                    );
            }
        });
        Intent intent = getIntent();
        editPhone.setText(intent.getStringExtra("phone"));

        brRegisterUserSMS = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String strCode = intent.getStringExtra("sms_code");
                editSMS.setText(strCode);
            }
        };

        if (editPhone.getText().length() == 0)
            editPhone.setText("+38 0");
        editPhone.setSelection(editPhone.length());
        maskedInputFilter = new MaskedInputFilter("+38 0## ### ## ##", editPhone, '#');
        editPhone.setFilters(new InputFilter[]{maskedInputFilter});

        if (code == -1 && editPhone.getText().toString().length() >= 17) {
            String phone = editPhone.getText().toString().trim();
            isUserExists(phone);
        }
        isSMSReceivePermissionGranted();
    }

    @Override
    public void onResume() {
        super.onResume();
        stopped = false;
    }

    private void registerUser(String inPhone, String password, String last_name,
                              String first_name, String patronomic, String email) {
        String phone = inPhone.replace(" ", "").replace("+", "");
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://esb.intime.ua:4443/services/intime_auth_rest/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(GlobalApplicationData.getUnsafeOkHttpClient())
                .build();

        IRtfAPI service = retrofit.create(IRtfAPI.class);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"),
                "{\"_post_add_user_rest\":{" +
                        "\"login\":\"" + phone + "\"," +
                        "\"password\":\"" + password + "\"," +
                        "\"last_name\":\"" + last_name + "\"," +
                        "\"first_name\":\"" + first_name + "\"," +
                        "\"patronymic\":\"" + patronomic + "\"," +
                        "\"phones\":\"" + phone + "\"," +
                        "\"emails\":\"" + email + "\"}}");

        regusterUserCall = service.registerUser("application/json", requestBody);

        regusterUserCall.enqueue(
                new Callback<NewUser>() {
                    @Override
                    public void onResponse(Call<NewUser> call, Response<NewUser> response) {
                        NewUser user = response.body();
                        if (user != null) {
                            if (user.getEntriesAddUser().getEntryAddUser().get(0).getResult().compareTo("OK") == 0) {
                                Toast.makeText(context, getResources().getString(R.string.user_created), Toast.LENGTH_LONG).show();
                                finish();
                            } else {
                                showMessage(getResources().getString(R.string.dialog_title),
                                        getResources().getString(R.string.create_user_error) + user.getEntriesAddUser().getEntryAddUser().get(0).getResult());
                            }
                        } else
                            showMessage(getResources().getString(R.string.dialog_title),
                                    getResources().getString(R.string.create_user_error));
                    }

                    @Override
                    public void onFailure(Call<NewUser> call, Throwable t) {
                        //frPreCalcSum1.setVisibility(View.GONE);
                        Log.d(TAG, "Auth Error: " + t.getMessage());
                        showMessage(getResources().getString(R.string.dialog_title),
                                getResources().getString(R.string.dialog_message_wrong_phone));
                    }

                }
        );

    }

    private boolean isValidMail(final CharSequence mail) {
        if (mail != null)
            return android.util.Patterns.EMAIL_ADDRESS.matcher(mail).matches();
        else
            return false;
    }

    private boolean isValidPhone(final CharSequence phone) {
        if (phone != null)
            return android.util.Patterns.PHONE.matcher(phone).matches();
        else
            return false;
    }

    private boolean validateForm() {
        boolean result = true;
        if (editPhone.getText().length() < 17 || !isValidPhone(editPhone.getText())) {
            tvPhone.setVisibility(View.VISIBLE);
            tvPhone.setTextColor(getResources().getColor(R.color.error_massage));
            result = false;
        }
        if (editFirstName.getText().length() == 0) {
            tvName.setVisibility(View.VISIBLE);
            tvName.setTextColor(getResources().getColor(R.color.error_massage));
            result = false;
        }

        if (editLastName.getText().length() == 0) {
            tvLastName.setVisibility(View.VISIBLE);
            tvLastName.setTextColor(getResources().getColor(R.color.error_massage));
            result = false;
        }
        if (editPatronomic.getText().length() == 0) {
            tvSurName.setVisibility(View.VISIBLE);
            tvSurName.setTextColor(getResources().getColor(R.color.error_massage));
            result = false;
        }

        if (editPassword.getText().length() == 0) {
            tvPassword.setTextColor(getResources().getColor(R.color.error_massage));
            result = false;
        }
        if (editSMS.getText().length() == 0 || editSMS.getText().toString().compareTo(Integer.toString(code)) != 0) {
            tvSMS.setVisibility(View.VISIBLE);
            tvSMS.setTextColor(getResources().getColor(R.color.error_massage));
            result = false;
        }
        if (editMail.getText().length() == 0 || !isValidMail(editMail.getText())) {
            tvMail.setVisibility(View.VISIBLE);
            tvMail.setTextColor(getResources().getColor(R.color.error_massage));
            result = false;
        }
        return result;
    }

    private void sendSMS(final String inPhone) {
        String phone = inPhone.replace(" ", "").replace("+", "");
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://newapi.intime.ua/")
                .addConverterFactory(GsonConverterFactory.create())
                //.client(GlobalApplicationData.getUnsafeOkHttpClient())
                .build();

        IRtfAPI service = retrofit.create(IRtfAPI.class);
        Random rand = new Random();
        code = rand.nextInt(9999);
        if (code < 1000)
            code = code + 1000;
        sendSMSCall = service.sendSMS(phone, Integer.toString(code) + " - код підтвердження реєстрації",
                Integer.toString(code) + " - kod pidtverdzhennya reyestratsiyi");
        sendSMSCall.enqueue(
                new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        Log.d(TAG, "onResponse.SendSMS.Body: " + call.request().body().toString()
                                + ", Response: " + response.toString());
                        //ResponseBody body = response.body();
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        //frPreCalcSum1.setVisibility(View.GONE);
                        Log.d(TAG, "Auth Error: " + t.getMessage());
                    }

                }
        );
    }

    private void showMessage(String caption, String message) {
        if (!stopped) {
            MessageDialog dlgWarning = new MessageDialog();
            dlgWarning.setParams(caption, message, false, null);
            dlgWarning.show(getSupportFragmentManager(), "Warning");
        }
    }

    private void smsPremissionRequest() {
        ActivityCompat.requestPermissions(this,
                new String[]{
                        Manifest.permission.RECEIVE_SMS
                },
                SMS_GRANT_REQUEST);
    }

    private boolean isSMSReceivePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.RECEIVE_SMS)
                    == PackageManager.PERMISSION_GRANTED) {
                // Log.v("TAG","Permission is granted");
                IntentFilter intFilt = new IntentFilter(RegisterUserActivity.GET_SMS);
                context.registerReceiver(brRegisterUserSMS, intFilt);
                return true;
            } else {

                // Log.v("TAG","Permission is revoked");
                smsPremissionRequest();
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            // Log.v("TAG", "Permission is granted");
            IntentFilter intFilt = new IntentFilter(RegisterUserActivity.GET_SMS);
            context.registerReceiver(brRegisterUserSMS, intFilt);
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == SMS_GRANT_REQUEST) {
            IntentFilter intFilt = new IntentFilter(RegisterUserActivity.GET_SMS);
            context.registerReceiver(brRegisterUserSMS, intFilt);
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void isUserExists(final String inPhone) {

        String phone = inPhone.replace(" ", "").replace("+", "");

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://esb.intime.ua:4443/services/intime_auth_rest/")
                //.addConverterFactory(GsonConverterFactory.create())
                .client(GlobalApplicationData.getUnsafeOkHttpClient())
                .build();

        IRtfAPI service = retrofit.create(IRtfAPI.class);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"),
                "{\"_post_get_people_by_phone_rest\":{\"phone\":\"" + phone + "\"}}");
        userExistsCall = service.checkPeopleExists("application/json", requestBody);

        userExistsCall.enqueue(
                new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                        if (response.body() != null) {
                            String body = "";
                            try {
                                body = new String(response.body().bytes());
                            } catch (IOException e) {
                                Log.d(TAG, "error parsing response body for phone check", e);
                            }
                            if (body.contains("Name")) {
                                frUserExistHint.setVisibility(View.VISIBLE);
                            } else {
                                frUserExistHint.setVisibility(View.GONE);
                                sendSMS(inPhone);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        //frPreCalcSum1.setVisibility(View.GONE);
                        Log.d(TAG, "Check phone error: " + t.getMessage());
                    }

                }
        );

    }

}
