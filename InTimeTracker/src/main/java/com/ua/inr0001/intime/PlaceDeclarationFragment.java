package com.ua.inr0001.intime;


import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import intime.intime.mylistview.MyListView;
import intime.intime.weightseekbar.WeightSeekBar;
import intime.llc.ua.R;
import intime.newdeclaration.DeclarationModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class PlaceDeclarationFragment extends Fragment implements WeightSeekBar.IWeightSeekBar {

  private Context mContext;
  private String fragmentNumber = "";
  private DeclarationModel calculatorModel;

  private LinearLayout llVolumePack;
  private TextView tvSizeCaption;
  private TextView tvWeightCaption;
  private EditText edVolumeWidth;
  private EditText edVolumeLength;
  private EditText edVolumeHeight;
  private TextView tvVolumeWidth;
  private TextView tvVolumeLength;
  private TextView tvVolumeHeight;
  private TextView tvPlaceNumber;

  private Button btnRemovePlace;

  private MyListView lvCargoType;
  private MyListView lvPackages;
  private ArrayList<Map<String, Object>> packageList = new ArrayList<Map<String, Object>>();
  private List<PackageSpinnerItem> packages;

  private FrameLayout weightFrame;
  private LinearLayout prevWeightLayout = null;

  private LinearLayout llWheels;
  private SeekBar sbRadius;
  private TextView tvSelectedRadius;
  private RadioButton rbWheelsLight;
  private TextView tvWheelBeginArray;
  private TextView tvWheelEndArray;
  private String wheelRadius[];

  // pallete types controls
  private RadioGroup rgPaleteType;
  private RadioButton rbPalete1;
  private RadioButton rbPalete2;
  private RadioButton rbPalete3;
  private CompoundButton.OnCheckedChangeListener listener;

  // weight controls
  private LinearLayout llWeights;
  private LinearLayout llSeekBarWeights;
  private ImageView imgHumanWeight;
  private TextView tvWeightDescritption;
  private WeightSeekBar sbWeight;
  private EditText tvWeight;
  private FrameLayout frBoxWeight;
  private LinearLayout firstWeightItem;
  private String[] weights;

  private int[] cargoIcons = new int[]
    { R.drawable.ic_cargo_normal,
      R.drawable.ic_paleta_normal,
      R.drawable.ic_wheel_normal,
      R.drawable.ic_doc_normal};
  private int[] cargoIconsSelected = new int[]
    { R.drawable.ic_cargo_active,
      R.drawable.ic_paleta_active,
      R.drawable.ic_wheel_active,
      R.drawable.ic_doc_active};

  private SimpleAdapter cargoTypeAdapter;

  private SimpleAdapter packageAdapter;

  private int cargoTypeSelected = 0;
  private int selectedWeight = 0;
  private int packageSelected = 0;

  public PlaceDeclarationFragment() {
    // Required empty public constructor
  }

  public void setFragmentNumber(String number)
  {
    fragmentNumber = number;
  }

  @Override
  public void weightChanged(Double weight) {
    calculatorModel.setWeight(String.valueOf(weight));
    initPackageList();
  }

  private void initVolume(final View view)
  {
    edVolumeWidth = (EditText) view.findViewById(R.id.editVolumeWidth);
    edVolumeHeight = (EditText) view.findViewById(R.id.editVolumeHeight);
    edVolumeLength = (EditText) view.findViewById(R.id.editVolumeLength);
    tvVolumeWidth = (TextView) view.findViewById(R.id.tvVolumeWidth);
    tvVolumeLength = (TextView) view.findViewById(R.id.tvVolumeLength);
    tvVolumeHeight = (TextView) view.findViewById(R.id.tvVolumeHeight);
    TextWatcher tw = new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {

      }

      @Override
      public void afterTextChanged(Editable s) {
        if (edVolumeWidth.getText().toString().length()==0)
          calculatorModel.setWidth("1");
        if (edVolumeLength.getText().toString().length()==0)
          calculatorModel.setLength("1");
        if (edVolumeHeight.getText().toString().length()==0)
          calculatorModel.setHeight("1");
        initPackageList();
      }
    };
    edVolumeLength.addTextChangedListener(new VolumeTextChangeListener(getActivity(), edVolumeLength, tvVolumeLength));
    edVolumeLength.addTextChangedListener(new CalcTextChangeListener("length"));
    edVolumeLength.addTextChangedListener(tw);
    edVolumeHeight.addTextChangedListener(new VolumeTextChangeListener(getActivity(), edVolumeHeight, tvVolumeHeight));
    edVolumeHeight.addTextChangedListener(new CalcTextChangeListener("height"));
    edVolumeHeight.addTextChangedListener(tw);
    edVolumeWidth.addTextChangedListener(new VolumeTextChangeListener(getActivity(), edVolumeWidth, tvVolumeWidth));
    edVolumeWidth.addTextChangedListener(new CalcTextChangeListener("width"));
    edVolumeWidth.addTextChangedListener(tw);
  }

  private void resetPrevLayout(LinearLayout v)
  {
    if (v!=null) {
      TextView tvWeightValue = (TextView) v.findViewById(R.id.tvWeightValue);
      tvWeightValue.setTypeface(null, Typeface.NORMAL);
      tvWeightValue.setTextColor(GlobalApplicationData.getInstance().getContext().getResources().getColor(R.color.text_hint));
      tvWeightValue = (TextView) v.findViewById(R.id.tvTopWeight);
      tvWeightValue.setVisibility(View.INVISIBLE);
      tvWeightValue = (TextView) v.findViewById(R.id.tvWeightUnit);
      tvWeightValue.setVisibility(View.INVISIBLE);
    }
  }

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    mContext = context;
  }

  private LinearLayout createWightsList(Context context, LinearLayout parent, int cargo)
  {
    parent.removeAllViewsInLayout();
    LinearLayout firstItem = null;
    LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    switch (cargo) {
      case 0: weights = GlobalApplicationData.getInstance().getContext().getResources().getStringArray(R.array.wight_values); break;
      case 1: weights = GlobalApplicationData.getInstance().getContext().getResources().getStringArray(R.array.doc_weights); break;
      case 2: weights = GlobalApplicationData.getInstance().getContext().getResources().getStringArray(R.array.palete_weights); break;
    }


    for (int i = 0; i<weights.length; i++) {
      LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.list_weight_item, null);
      if (i==0) {
        prevWeightLayout = layout;
        firstItem = layout;
        selectedWeight = i;

        TextView tvWeightValue = (TextView) layout.findViewById(R.id.tvWeightValue);
        tvWeightValue.setTypeface(null, Typeface.BOLD);
        tvWeightValue.setTextColor(GlobalApplicationData.getInstance().getContext().getResources().getColor(R.color.base_text_color));
        tvWeightValue = (TextView) layout.findViewById(R.id.tvTopWeight);
        tvWeightValue.setVisibility(View.VISIBLE);
        tvWeightValue = (TextView) layout.findViewById(R.id.tvWeightUnit);
        tvWeightValue.setVisibility(View.VISIBLE);

      }
      layout.setTag(i);
      layout.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          resetPrevLayout(prevWeightLayout);
          prevWeightLayout = (LinearLayout)v;
          ViewGroup.LayoutParams params1 = weightFrame.getLayoutParams();

          params1.width = v.getWidth();
          weightFrame.setLayoutParams(params1);
          weightFrame.animate().x(v.getX()).setDuration(500).start();

          selectedWeight = (int) v.getTag();
          calculatorModel.setWeight(weights[selectedWeight].replaceAll(",", "."));
          TextView tvWeightValue = (TextView) v.findViewById(R.id.tvWeightValue);
          tvWeightValue.setTypeface(null, Typeface.BOLD);
          tvWeightValue.setTextColor(GlobalApplicationData.getInstance().getContext().getResources().getColor(R.color.base_text_color));
          tvWeightValue = (TextView) v.findViewById(R.id.tvTopWeight);
          tvWeightValue.setVisibility(View.VISIBLE);
          tvWeightValue = (TextView) v.findViewById(R.id.tvWeightUnit);
          tvWeightValue.setVisibility(View.VISIBLE);
        }
      });
      TextView tvValue = (TextView) layout.findViewById(R.id.tvWeightValue);
      tvValue.setText(weights[i]);
      LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
        LinearLayout.LayoutParams.WRAP_CONTENT,
        LinearLayout.LayoutParams.MATCH_PARENT
      );
      params.weight = 1;

      parent.addView(layout, params);

    }
    return firstItem;
  }

  private void setSubtypeViews(int cargoType)
  {
    switch (cargoType)
    {
      case 0: // Cargo
        rgPaleteType.setVisibility(View.GONE);
        //rlPackageCount.setVisibility(View.GONE);
        llSeekBarWeights.setVisibility(View.VISIBLE);
        llWeights.setVisibility(View.GONE);
        llVolumePack.setVisibility(View.VISIBLE);
        tvSizeCaption.setVisibility(View.VISIBLE);
        tvWeightCaption.setVisibility(View.VISIBLE);
        llWheels.setVisibility(View.GONE);
        calculatorModel.setWeight("0.1");
        break;
      case 1:
        rgPaleteType.setVisibility(View.GONE);
        llSeekBarWeights.setVisibility(View.GONE);
        llWeights.setVisibility(View.VISIBLE);
        //rlPackageCount.setVisibility(View.GONE);
        llVolumePack.setVisibility(View.GONE);
        tvSizeCaption.setVisibility(View.GONE);
        tvWeightCaption.setVisibility(View.VISIBLE);
        llWheels.setVisibility(View.GONE);
        firstWeightItem = createWightsList(getContext(), llWeights, 1);
        calculatorModel.setWeight("0.1");
        break;
      case 2:
        rgPaleteType.setVisibility(View.VISIBLE);
        llWeights.setVisibility(View.GONE);
        llSeekBarWeights.setVisibility(View.GONE);
        //rlPackageCount.setVisibility(View.VISIBLE);
        llVolumePack.setVisibility(View.GONE);
        tvSizeCaption.setVisibility(View.GONE);
        tvWeightCaption.setVisibility(View.GONE);
        llWheels.setVisibility(View.GONE);
        //firstWeightItem = createWightsList(getContext(), llWeights, 2);
        rbPalete2.setChecked(true);
        calculatorModel.setTypePallets(GlobalApplicationData.getInstance().getContext().getResources().getString(R.string.pallete2));
        calculatorModel.setWeight("500");
        break;
      case 3:
        rgPaleteType.setVisibility(View.GONE);
        //rlPackageCount.setVisibility(View.VISIBLE);
        llVolumePack.setVisibility(View.GONE);
        llWeights.setVisibility(View.GONE);
        llSeekBarWeights.setVisibility(View.GONE);
        tvSizeCaption.setVisibility(View.GONE);
        tvWeightCaption.setVisibility(View.GONE);
        llWheels.setVisibility(View.VISIBLE);
        rbWheelsLight.setChecked(true);
        calculatorModel.setWeight("0.1");
        initWheelsRadius(true);
        break;
      default:
        rgPaleteType.setVisibility(View.GONE);
        llWheels.setVisibility(View.GONE);
        break;
    }
  }

  private void initPackageList() {
    packageSelected = 0;
    packageList.clear();
    packages = DictionariesDB.getInstance(mContext).tablePackages.getPackages(calculatorModel.getCargoType(),
      Double.valueOf(calculatorModel.getWidth()),
      Double.valueOf(calculatorModel.getHeight()),
      Double.valueOf(calculatorModel.getLength()),
      Double.valueOf(calculatorModel.getWeight())
    );

    Map<String, Object> m;
    for (int i = 0; i < packages.size(); i++) {
      m = new HashMap<String, Object>();
      m.put("Text", packages.get(i).toString());
      m.put("Price", packages.get(i).getPrice());
      packageList.add(m);
    }

    String[] from = { "Text" , "Price"};
    int[] to = { R.id.tvcCargoType, R.id.tvcPrice};

    packageAdapter = new SimpleAdapter(getContext(), packageList,
      R.layout.package_single_choise_item_def, from, to)
    {
      @NonNull
      @Override
      public View getView(int position, View convertView, ViewGroup parent) {
        final View view = super.getView(position, convertView, parent);
        ImageView cargoIcon = (ImageView) view.findViewById(R.id.imageCargoIcon);
        TextView tvDescription = (TextView) view.findViewById(R.id.tvcCargoType);
        TextView tvPrice = (TextView) view.findViewById(R.id.tvcPrice);
        LinearLayout llSelectedItem = (LinearLayout) view.findViewById(R.id.llSingleChoiseItem);
        if (position == packageSelected) {
          llSelectedItem.setBackgroundColor(GlobalApplicationData.getInstance().getContext().getResources().getColor(R.color.background_white));
          cargoIcon.setImageResource(R.drawable.ic_list_check);
          tvDescription.setTextColor(GlobalApplicationData.getInstance().getContext().getResources().getColor(R.color.base_text_color));
          tvPrice.setTextColor(GlobalApplicationData.getInstance().getContext().getResources().getColor(R.color.base_text_color));
        }
        else {
          llSelectedItem.setBackgroundColor(GlobalApplicationData.getInstance().getContext().getResources().getColor(R.color.app_background));
          cargoIcon.setImageResource(0);
          tvDescription.setTextColor(GlobalApplicationData.getInstance().getContext().getResources().getColor(R.color.paper_caption));
          tvPrice.setTextColor(GlobalApplicationData.getInstance().getContext().getResources().getColor(R.color.paper_caption));
        }
        return view;
      }
    };

    lvPackages.setAdapter(packageAdapter);
    calculatorModel.setPackage("0");
  }

  private void initWheelsRadius(boolean checked)
  {
    if (checked) {
      calculatorModel.setCargoType("wheels_car");
      wheelRadius = GlobalApplicationData.getInstance().getContext().getResources().getStringArray(R.array.wheels_light);
    }
    else {
      calculatorModel.setCargoType("wheels_truck");
      wheelRadius = GlobalApplicationData.getInstance().getContext().getResources().getStringArray(R.array.wheels_cargo);
    }
    tvWheelBeginArray.setText(wheelRadius[0]);
    tvWheelEndArray.setText(wheelRadius[wheelRadius.length-1]);
    sbRadius.setMax(wheelRadius.length-1);
    sbRadius.setProgress(0);
    calculatorModel.setRadius(wheelRadius[0]);
  }

  private void initWheels(View view)
  {
    llWheels = (LinearLayout) view.findViewById(R.id.llWheels);
    sbRadius = (SeekBar) view.findViewById(R.id.sbRadius);
    sbRadius.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
      @Override
      public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        tvSelectedRadius.setText(wheelRadius[progress]);
        calculatorModel.setRadius(wheelRadius[progress]);
      }

      @Override
      public void onStartTrackingTouch(SeekBar seekBar) {

      }

      @Override
      public void onStopTrackingTouch(SeekBar seekBar) {
      }
    });
    rbWheelsLight = (RadioButton) view.findViewById(R.id.rbWheelsLight);
    rbWheelsLight.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        initWheelsRadius(isChecked);
      }
    });
    tvSelectedRadius = (TextView) view.findViewById(R.id.tvSelectedRadius);
    tvWheelBeginArray = (TextView) view.findViewById(R.id.tvWheelsBeginArray);
    tvWheelEndArray = (TextView) view.findViewById(R.id.tvWheelsEndArray);
  }

  private void initPalleteTypes(View view)
  {
    rgPaleteType = (RadioGroup) view.findViewById(R.id.rgPaleteType);
    rbPalete1 = (RadioButton) view.findViewById(R.id.rbPalete1);
    rbPalete2 = (RadioButton) view.findViewById(R.id.rbPalete2);
    rbPalete3 = (RadioButton) view.findViewById(R.id.rbPalete3);
    listener = new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
          calculatorModel.setTypePallets(buttonView.getText().toString());
//          if (buttonView.getText().toString().compareTo("1200x1000")==0 ||
//            buttonView.getText().toString().compareTo("1200x1200")==0)
//            llWeights.setVisibility(View.GONE);
//          else {
          firstWeightItem = createWightsList(getContext(), llWeights, 2);
          llWeights.setVisibility(View.VISIBLE);
          calculatorModel.setWeight("500");
//          }
        }
      }
    };
    rbPalete1.setOnCheckedChangeListener(listener);
    rbPalete2.setOnCheckedChangeListener(listener);
    rbPalete3.setOnCheckedChangeListener(listener);
  }

  private void setWeightFramePos(final LinearLayout firstItem)
  {
    if (firstItem!=null) {
      ViewGroup.LayoutParams params1 = weightFrame.getLayoutParams();
      params1.width = firstItem.getWidth();
      weightFrame.setLayoutParams(params1);
      weightFrame.setVisibility(View.VISIBLE);
      weightFrame.animate().x(firstItem.getX()).setDuration(500).start();
    }
  }

  private void setCargoTypeAdapter()
  {

    ArrayList<Map<String, Object>> cargoTypeList = new ArrayList<Map<String, Object>>();
    String[] cargoText = GlobalApplicationData.getInstance().getContext().getResources().getStringArray(R.array.cargo_types);

    Map<String, Object> m;
    for (int i = 0; i < cargoText.length; i++) {
      m = new HashMap<String, Object>();
      m.put("Image", Integer.toString(cargoIcons[i]));
      m.put("Text", cargoText[i]);
      cargoTypeList.add(m);
    }

    String[] from = { "Image", "Text" };
    int[] to = { R.id.imageCargoIcon, R.id.tvcCargoType};

    cargoTypeAdapter = new SimpleAdapter(getContext(), cargoTypeList,
      R.layout.single_choise_item_def, from, to)
    {
      @NonNull
      @Override
      public View getView(int position, View convertView, ViewGroup parent) {
        final View view = super.getView(position, convertView, parent);
        ImageView cargoIcon = (ImageView) view.findViewById(R.id.imageCargoIcon);
        LinearLayout llSelectedItem = (LinearLayout) view.findViewById(R.id.llSingleChoiseItem);
        TextView tvDescription = (TextView) view.findViewById(R.id.tvcCargoType);
        if (position == cargoTypeSelected) {
          llSelectedItem.setBackgroundColor(GlobalApplicationData.getInstance().getContext().getResources().getColor(R.color.background_white));
          cargoIcon.setImageResource(cargoIconsSelected[position]);
          tvDescription.setTextColor(GlobalApplicationData.getInstance().getContext().getResources().getColor(R.color.base_text_color));
        }
        else {
          llSelectedItem.setBackgroundColor(GlobalApplicationData.getInstance().getContext().getResources().getColor(R.color.app_background));
          cargoIcon.setImageResource(cargoIcons[position]);
          tvDescription.setTextColor(GlobalApplicationData.getInstance().getContext().getResources().getColor(R.color.paper_caption));
        }
        return view;
      }
    };

    lvCargoType.setAdapter(cargoTypeAdapter);
  }

  private void initCargoType(final View view)
  {
    lvCargoType = (MyListView) view.findViewById(R.id.lvCargoType);
    lvCargoType.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
    lvCargoType.setItemsCanFocus(true);
    calculatorModel.setCargoType("cargo");
    setCargoTypeAdapter();
    lvCargoType.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        cargoTypeSelected = position;
        switch (cargoTypeSelected)
        {
          case 0: calculatorModel.setCargoType("cargo"); break;
          case 1: calculatorModel.setCargoType("doc"); break;
          case 2: calculatorModel.setCargoType("pallet"); break;
          default: calculatorModel.setCargoType("cargo");
        }
        cargoTypeAdapter.notifyDataSetChanged();
        //initPackageList(calculatorModel.getCargoType());
        setSubtypeViews(cargoTypeSelected);
        initPackageList();
      }
    });
    initPackageList();
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_place_declaration, container, false);
    mContext = getContext();
    calculatorModel = DeclarationModel.getInstance(null);
    weightFrame = (FrameLayout) view.findViewById(R.id.frameRectangleWieght);
    btnRemovePlace = (Button) view.findViewById(R.id.btnRemovePlace);
    llWeights = (LinearLayout) view.findViewById(R.id.llWeights);
    sbWeight = (WeightSeekBar) view.findViewById(R.id.sbWeight);
    tvWeight = (EditText) view.findViewById(R.id.tvWeight);
    tvWeightDescritption = (TextView) view.findViewById(R.id.tvWeightDescription);
    frBoxWeight = (FrameLayout) view.findViewById(R.id.frBoxWeight);
    imgHumanWeight = (ImageView) view.findViewById(R.id.imgHumanWeight);
    sbWeight.setParams(tvWeight, imgHumanWeight, tvWeightDescritption, frBoxWeight, this);
    sbWeight.setMax(1000);
    llSeekBarWeights = (LinearLayout) view.findViewById(R.id.llSeekBarWeights);
    llVolumePack = (LinearLayout) view.findViewById(R.id.llVolumePack);
    tvSizeCaption = (TextView) view.findViewById(R.id.tvSizeCaption);
    tvWeightCaption = (TextView) view.findViewById(R.id.tvWeightCaption);
    tvPlaceNumber = (TextView) view.findViewById(R.id.tvPlaceNumber);

    lvPackages = (MyListView) view.findViewById(R.id.lvPackages);
    lvPackages.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
    lvPackages.setItemsCanFocus(true);
    lvPackages.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        packageSelected = position;
        calculatorModel.setPackage(packages.get(position).getCode());
        packageAdapter.notifyDataSetChanged();
      }
    });


    btnRemovePlace.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (fragmentNumber.compareTo("1")!=0) {
          android.support.v4.app.FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
          Fragment fragment1 = new PlaceDeclarationFragment();
          ft.remove(getActivity().getSupportFragmentManager().findFragmentByTag("fragment_" + fragmentNumber));
          ft.commit();
        }
      }
    });

    initCargoType(view);
    initPalleteTypes(view);
    initWheels(view);

    initVolume(view);

    view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
      @Override
      public void onGlobalLayout() {
        setWeightFramePos(firstWeightItem);
        firstWeightItem = null;
      }
    });

    setSubtypeViews(cargoTypeSelected);
    tvPlaceNumber.setText(getResources().getString(R.string.place_number) + " " + fragmentNumber);
    return view;
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);

  }



}
