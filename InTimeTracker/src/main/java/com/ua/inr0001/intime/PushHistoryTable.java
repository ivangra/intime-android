package com.ua.inr0001.intime;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class PushHistoryTable implements ITable {

  private SQLiteDatabase sqlDB;

  public PushHistoryTable(SQLiteDatabase sqldb)
  {
    sqlDB = sqldb;
  }
  
  public static final class Column
  {
    public static final String ID = "_id";
    public static final String NAME = "Name";
    public static final String DATE = "Date";
    public static final String DESCRIPTION = "Description";
    public static final String TTN = "ttn";
    public static final String LINKS = "Links";
  }
  
  public static final String NAME = "tblPushHistory";
  public static final String VIEW_NAME = "vwPushHistory";
  
  public static final String SQL_CREATE_TABLE =
      "create table if not exists " + NAME + " ( " +
              Column.ID + " integer primary key autoincrement, " +
              Column.NAME + " text, " +
              Column.DATE + " text, " +
              Column.TTN + " text, " +
              Column.LINKS + " text, " +
              Column.DESCRIPTION + " text); ";
  
  public static final String SQL_CREATE_VIEW =
      "CREATE VIEW if not exists " + VIEW_NAME + " AS" + 
        " SELECT " + Column.ID + ", " +
             Column.NAME + ", " +
             Column.DATE + ", " +
             Column.TTN + ", " +
             Column.LINKS + ", " +
             Column.DESCRIPTION +
        " FROM " + NAME;

  public static final String SQL_CREATE_INDEX =
      "CREATE INDEX if not exists idx_PushName ON " + NAME + " (" + Column.NAME + ");\n" +
      "CREATE INDEX if not exists idx_PushDate ON " + NAME + " (" + Column.DATE + ");";
  
  public static final String SQL_DROP_TABLE =
      "DROP TABLE IF EXISTS " + NAME;
  
  public static final String SQL_DROP_VIEW =
      "DROP VIEW IF EXISTS " + VIEW_NAME;
  
  public void insert(String title, String ttn, String links, String description)
  {
    //if (!exists(Column.NAME, title))
    //{
      SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());
      String strDate = df.format(Calendar.getInstance().getTime());
      ContentValues cv = new ContentValues();
      cv.put(Column.NAME, title);
      cv.put(Column.DATE, strDate);
      cv.put(Column.TTN, ttn);
      cv.put(Column.LINKS, links);
      cv.put(Column.DESCRIPTION, description);
      sqlDB.insert(NAME, null, cv);
    //}
  }
  
  @Override
  public Cursor selectByID(int Id) {
    return null;
  }

  @Override
  public Cursor selectByName(String Name) {
    return null;
  }

  @Override
  public Cursor selectAll() {
    return sqlDB.query(VIEW_NAME, null, null, null, null, null, Column.NAME);
  }

  @Override
  public boolean exists(String fieldName, String fieldValue) {
    Cursor slCursor = sqlDB.query(VIEW_NAME, null, fieldName + "=?", new String[]{fieldValue}, null, null, null);
    if (slCursor.getCount()>0)
    {
      slCursor.close();
      return true;
    }
    else
    {
      slCursor.close();
      return false;
    }
  }

  public int getCountPush()
  {
    Cursor cursor = selectAll();
    int count = cursor.getCount();
    cursor.close();
    return count;
  }
  
  public void clearTable()
  {
    sqlDB.delete(NAME, null, null);
  }
  
  public void delete(String id)
  {
    sqlDB.delete(NAME, Column.ID + "=" + id , null);
  }

}
