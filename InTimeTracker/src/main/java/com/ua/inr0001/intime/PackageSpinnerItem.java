package com.ua.inr0001.intime;

import java.text.DecimalFormat;

/**
 * Created by ILYA on 16.02.2017.
 */

public class PackageSpinnerItem {

  private String simplePackageName;
  private String packageName;
  private String code;
  private String price;
  private Double width;
  private Double height;
  private Double length;
  private Double weight_max;
  private Double weight_min;

  public PackageSpinnerItem(String packageName, String code, String price,
                Double width, Double height, Double leght, Double weight_max, Double weight_min)
  {
    this.code = code;
    if (price.compareTo("0")==0)
      this.price = "";
    else
      this.price = price;
    this.width = width;
    this.height = height;
    this.length = leght;
    this.weight_max = weight_max;
    this.weight_min = weight_min;
    this.simplePackageName = packageName;

    if (this.width<=0 && this.height<=0 && this.length<=0)
    {
      this.packageName = packageName;
    }
    else {
      StringBuilder sb = new StringBuilder();
      DecimalFormat format = new DecimalFormat("#.#");
      Double mWidth = Math.abs(width);
      Double mHeight = Math.abs(height);
      Double mLength = Math.abs(leght);
      sb.append(packageName);
      sb.append("\n");
      sb.append(format.format(mWidth).replaceAll(",", "."));
      sb.append("x");
      sb.append(format.format(mHeight).replaceAll(",", "."));
      sb.append("x");
      sb.append(format.format(mLength).replaceAll(",", "."));
      sb.append(" см");
      this.packageName = sb.toString();
    }
  }

  @Override
  public String toString() {
    return packageName;
  }

  public String getCode()
  {
    return code;
  }

  public String getPrice()
  {
    if (price.length()==0)
      return "";
    else
      return "+" + price + " грн";
  }

  public Double getWidth() {
    return width;
  }

  public Double getHeight() {
    return height;
  }

  public Double getLength() {
    return length;
  }

  public Double getWeightMax() {
    return weight_max;
  }

  public Double getWeightMin() {
    return weight_min;
  }

  public String getSimplePackageName() {
    return simplePackageName;
  }

}
