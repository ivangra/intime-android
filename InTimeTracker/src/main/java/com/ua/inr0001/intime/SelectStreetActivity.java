package com.ua.inr0001.intime;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import java.util.Locale;

import intime.llc.ua.R;

public class SelectStreetActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

  private GoogleApiClient mGoogleApiClient;
  private PlaceListViewAdapter mAdapter;
  private Context context;
  private ListView cityList;
  private EditText editCity;
//  private Call<YaResult> yaCall;

  private void finishActivity(final String selectedStreet)
  {
    Intent intent = new Intent();
    intent.putExtra("StreetName", selectedStreet);
    setResult(RESULT_OK, intent);
    finish();
  }

//  private void getTranslation(final String textToTranslate)
//  {
//
//    Retrofit retrofit = new Retrofit.Builder()
//      .baseUrl("https://translate.yandex.net/api/v1.5/tr.json/")
//      .addConverterFactory(GsonConverterFactory.create())
//      //.addConverterFactory(createGsonConverter())
//      .client(GlobalApplicationData.getUnsafeOkHttpClient())
//      .build();
//
//    IRtfAPI service = retrofit.create(IRtfAPI.class);
//    yaCall = service.translate("trnsl.1.1.20170421T062340Z.f4850849e54089b6.3ff815daa296ef93f84694bb9ca1333b33c230a0",
//      textToTranslate, "ru-uk", "plain");
//    //Log.d("TrackTTN", call.request().url().encodedQuery());
//    yaCall.enqueue(
//      new Callback<YaResult>() {
//        @Override
//        public void onResponse(Call<YaResult> call, Response<YaResult> response) {
//
//          YaResult transaltedResult = response.body();
//          if (transaltedResult!=null) {
//            String translatedText = transaltedResult.getText().get(0);
//            if (translatedText != null) {
//              //doRequest(translatedText, "ua");
//              finishActivity(translatedText);
//            } else
//              finishActivity(textToTranslate);
//            //doRequest(textToTranslate, GlobalApplicationData.getCurrentLanguage());
//          }
//        }
//
//        @Override
//        public void onFailure(Call<YaResult> call, Throwable t) {
//          Log.d("Currier", "CallCurrier error: " + t.getMessage());
//          //doRequest(textToTranslate, GlobalApplicationData.getCurrentLanguage());
//          finishActivity(textToTranslate);
//        }
//
//      }
//    );
//  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    Locale locale = new Locale("uk_UK");
    Locale.setDefault(locale);
    setContentView(R.layout.activity_select_street);
    Toolbar toolbar = (Toolbar) findViewById(R.id.selectCityToolbar);
    setSupportActionBar(toolbar);
    toolbar.setTitle("");
    toolbar.setNavigationOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        onBackPressed();
      }
    });
    Log.d("SelectStreet", "CreateView");
    context = this;
    cityList = (ListView) findViewById(R.id.lvCities);
    editCity = (EditText)  findViewById(R.id.editCity);
    initGooglePlaces();
    Intent intent = getIntent();
    mAdapter = new PlaceListViewAdapter(context, mGoogleApiClient, intent.getStringExtra("CityName"));
    cityList.setAdapter(mAdapter);
    cityList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        final AutocompletePrediction item = mAdapter.getItem(position);
        final CharSequence primaryText = item.getPrimaryText(null);
        //if (isUaString(primaryText.toString()))
          finishActivity(primaryText.toString());
        //else
        //  getTranslation(primaryText.toString());
      }
    });
    editCity.addTextChangedListener(new TextWatcher() {

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (s.toString().length() >= 3)
          mAdapter.getFilter().filter(s);
      }

      @Override
      public void beforeTextChanged(CharSequence s, int start, int count,
                                    int after) {

      }

      @Override
      public void afterTextChanged(Editable s) {

      }
    });

  }

//  private boolean isUaString(final String testString)
//  {
//    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
//    InputMethodSubtype ims = imm.getCurrentInputMethodSubtype();
//    String locale = ims.getLocale();
//    if (locale.compareTo("uk")==0)
//      return true;
//    else
//      return false;
//  }

  private void initGooglePlaces()
  {
    mGoogleApiClient = new GoogleApiClient
      .Builder(context)
      .addApi(Places.GEO_DATA_API)
      .addConnectionCallbacks(this)
      .addOnConnectionFailedListener(this)
      .build();
  }

  @Override
  public void onStart() {
    super.onStart();
    mGoogleApiClient.connect();
  }

  @Override
  public void onStop() {
    super.onStop();
    mGoogleApiClient.disconnect();
  }

  @Override
  protected void onPause() {
    super.onPause();
    hideSoftKeyboard();
  }

  public void hideSoftKeyboard() {
    try {
      InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
      inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    } catch (Exception e) { }
  }

  @Override
  public void onConnected(@Nullable Bundle bundle) {

  }

  @Override
  public void onConnectionSuspended(int i) {

  }

  @Override
  public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

  }

}
