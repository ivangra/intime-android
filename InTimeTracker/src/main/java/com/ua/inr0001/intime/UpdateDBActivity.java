package com.ua.inr0001.intime;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import intime.llc.ua.R;

public class UpdateDBActivity extends AppCompatActivity implements DoneTaskCallBack, IProgressUpdate {

  private Button btnSkipUpdate;
  private TextView tvStatusUpdate;
  private ProgressBar progresUpdate;
  private UpdateDbTask dbTask;

  public void done()
  {
    finish();
  }

  @Override
  public void onBackPressed() {
    //super.onBackPressed();
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_update_db);

    btnSkipUpdate = (Button) findViewById(R.id.btnSkipUpdate);
    if (DictionariesDB.getInstance(this).isEmptyDB())
      btnSkipUpdate.setVisibility(View.INVISIBLE);
    btnSkipUpdate.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        dbTask.cancel(true);
      }
    });

    progresUpdate = (ProgressBar) findViewById(R.id.progressUpdate);
    tvStatusUpdate = (TextView) findViewById(R.id.tvStatusUpdate);
  }

  @Override
  protected void onResume() {
    super.onResume();
    dbTask = UpdateDbTask.getInstance(this, this, this, false);
    if (!dbTask.getStatus().equals(AsyncTask.Status.RUNNING))
      dbTask.execute();
  }

  @Override
  public void update(final String... values) {
    tvStatusUpdate.setText(values[0]);
    progresUpdate.setProgress(Integer.parseInt(values[1]));
  }

  @Override
  public void setMaxProgress(int max) {
    progresUpdate.setMax(max);
  }
}
