package com.ua.inr0001.intime;

public interface IInsertCity {

  void insert(String City, String Code, String WarehouseId, String State, String Area,
              int hasStore, int hasPostomat, int cityType, int hits);
  
}
