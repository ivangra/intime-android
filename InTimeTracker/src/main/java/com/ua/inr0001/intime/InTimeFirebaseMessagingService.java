/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ua.inr0001.intime;

import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class InTimeFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "FCM";

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
            sendNotification(remoteMessage.getData().get("head"),
              remoteMessage.getData().get("message"),
              remoteMessage.getData().get("ttn"),
              remoteMessage.getData().get("links"),
              GlobalApplicationData.getInstance().getNotificationId());
            /*DictionariesDB.getInstance(GlobalApplicationData.getInstance().getContext())
              .tablePushHistory.insert(remoteMessage.getData().get("head"), remoteMessage.getData().get("ttn"),
              remoteMessage.getData().get("links"), remoteMessage.getData().get("message"));*/
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            //Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

    }
    // [END receive_message]

    private void sendNotification(String head, String msg, String ttn, String links, int notifyId)
    {
        /*NotificationManager mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

        Intent notifyIntent = new Intent(this, PushHistoryActivity.class);
        notifyIntent.putExtra("TTN", ttn);
        notifyIntent.putExtra("links", links);
        notifyIntent.putExtra("head", head);
        notifyIntent.putExtra("message", msg);

        int requestID = (int) System.currentTimeMillis(); // нужен для привязки интента к нотифаю
        PendingIntent contentIntent = PendingIntent.getActivity(this, requestID, notifyIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        Uri sound = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.door);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
          .setSmallIcon(R.drawable.ic_launcher)
          .setContentTitle(head)
          .setStyle(new NotificationCompat.BigTextStyle()
          .bigText(msg))
          .setSound(sound)
          .setContentText(msg);

        mBuilder.setAutoCancel(true);
        mBuilder.setContentIntent(contentIntent);
        mNotificationManager.notify(notifyId, mBuilder.build()); // отправляем сообщение*/
    }


}
